package com.taskos;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.taskos.data.DataManager;
import com.taskos.ui.auth.LoginViewModel;
import com.taskos.ui.auth.moreInfo.RegisterViewModel;
import com.taskos.ui.company.AddCompanyViewModel;
import com.taskos.ui.employee.EmployeeDetailsViewModel;
import com.taskos.ui.main.MainViewModel;
import com.taskos.ui.main.client.ClientViewModel;
import com.taskos.ui.main.client.addclient.AddClientViewModel;
import com.taskos.ui.main.client.customers.SelectCustomerViewModel;
import com.taskos.ui.main.client.detail.ClientDetailViewModel;
import com.taskos.ui.main.client.detail.addevent.AddEventViewModel;
import com.taskos.ui.main.client.detail.comment.CommentViewModel;
import com.taskos.ui.main.client.detail.eventrepeat.EventCommonViewModel;
import com.taskos.ui.main.client.detail.status_sold.SoldViewModel;
import com.taskos.ui.main.employee.EmployeesViewModel;
import com.taskos.ui.main.more.MoreViewModel;
import com.taskos.ui.main.network.MyNetworkViewModel;
import com.taskos.ui.main.network.add.AddToNetworkViewModel;
import com.taskos.ui.main.network.search.SearchToNetworkViewModel;
import com.taskos.ui.main.openjobs.OpenJobsViewModel;
import com.taskos.ui.main.openjobs.addfolder.AddFolderViewModel;
import com.taskos.ui.main.openjobs.addjob.AddJobViewModel;
import com.taskos.ui.main.openjobs.detail.JobDetailViewModel;
import com.taskos.ui.main.openjobs.documents.DocumentViewModel;
import com.taskos.ui.main.openjobs.financial.FinancialViewModel;
import com.taskos.ui.main.openjobs.images.ImagesViewModel;
import com.taskos.ui.main.openjobs.imageselect.ImagePickModel;
import com.taskos.ui.main.openjobs.imageselect.ImageSelectViewModel;
import com.taskos.ui.main.openjobs.schedule.JobScheduleViewModel;
import com.taskos.ui.main.openjobs.searchuser.SearchSharedUserViewModel;
import com.taskos.ui.main.openjobs.share.JobShareViewModel;
import com.taskos.ui.main.tasks.TasksViewModel;
import com.taskos.ui.main.tasks.addtask.AddTaskViewModel;
import com.taskos.ui.main.tasks.detail.TasksDetailViewModel;
import com.taskos.ui.main.tasks.detail.edit.EditTaskDetailsViewModel;
import com.taskos.ui.main.tasks.jobs.SelectJobsViewModel;
import com.taskos.ui.splash.SplashViewModel;

/**
 * Created by Hemant Sharma on 23-02-20.
 */
public class ViewModelProviderFactory extends ViewModelProvider.NewInstanceFactory {

    private static ViewModelProviderFactory instance;
    private final DataManager dataManager;

    public ViewModelProviderFactory(DataManager dataManager) {
        this.dataManager = dataManager;
    }

    public static ViewModelProviderFactory getInstance() {
        if (instance == null) {
            instance = new ViewModelProviderFactory(TaskOS.getInstance().getDataManager());
        }
        return instance;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        if (modelClass.isAssignableFrom(SplashViewModel.class)) {
            //noinspection unchecked
            return (T) new SplashViewModel(dataManager);
        } else if (modelClass.isAssignableFrom(MainViewModel.class)) {
            //noinspection unchecked
            return (T) new MainViewModel(dataManager);
        } else if (modelClass.isAssignableFrom(ClientViewModel.class)) {
            //noinspection unchecked
            return (T) new ClientViewModel(dataManager);
        } else if (modelClass.isAssignableFrom(OpenJobsViewModel.class)) {
            //noinspection unchecked
            return (T) new OpenJobsViewModel(dataManager);
        } else if (modelClass.isAssignableFrom(TasksViewModel.class)) {
            //noinspection unchecked
            return (T) new TasksViewModel(dataManager);
        } else if (modelClass.isAssignableFrom(MyNetworkViewModel.class)) {
            //noinspection unchecked
            return (T) new MyNetworkViewModel(dataManager);
        } else if (modelClass.isAssignableFrom(MoreViewModel.class)) {
            //noinspection unchecked
            return (T) new MoreViewModel(dataManager);
        } else if (modelClass.isAssignableFrom(AddClientViewModel.class)) {
            //noinspection unchecked
            return (T) new AddClientViewModel(dataManager);
        } else if (modelClass.isAssignableFrom(AddJobViewModel.class)) {
            //noinspection unchecked
            return (T) new AddJobViewModel(dataManager);
        } else if (modelClass.isAssignableFrom(AddTaskViewModel.class)) {
            //noinspection unchecked
            return (T) new AddTaskViewModel(dataManager);
        } else if (modelClass.isAssignableFrom(ClientDetailViewModel.class)) {
            //noinspection unchecked
            return (T) new ClientDetailViewModel(dataManager);
        } else if (modelClass.isAssignableFrom(CommentViewModel.class)) {
            //noinspection unchecked
            return (T) new CommentViewModel(dataManager);
        } else if (modelClass.isAssignableFrom(SoldViewModel.class)) {
            //noinspection unchecked
            return (T) new SoldViewModel(dataManager);
        } else if (modelClass.isAssignableFrom(AddEventViewModel.class)) {
            //noinspection unchecked
            return (T) new AddEventViewModel(dataManager);
        } else if (modelClass.isAssignableFrom(EventCommonViewModel.class)) {
            //noinspection unchecked
            return (T) new EventCommonViewModel(dataManager);
        } else if (modelClass.isAssignableFrom(JobDetailViewModel.class)) {
            //noinspection unchecked
            return (T) new JobDetailViewModel(dataManager);
        } else if (modelClass.isAssignableFrom(JobScheduleViewModel.class)) {
            //noinspection unchecked
            return (T) new JobScheduleViewModel(dataManager);
        } else if (modelClass.isAssignableFrom(EmployeesViewModel.class)) {
            //noinspection unchecked
            return (T) new EmployeesViewModel(dataManager);
        }else if (modelClass.isAssignableFrom(TasksDetailViewModel.class)){
            //noinspection unchecked
            return (T) new TasksDetailViewModel(dataManager);
        } else if (modelClass.isAssignableFrom(EditTaskDetailsViewModel.class)) {
            //noinspection unchecked
            return (T) new EditTaskDetailsViewModel(dataManager);
        } else if (modelClass.isAssignableFrom(SelectCustomerViewModel.class)) {
            //noinspection unchecked
            return (T) new SelectCustomerViewModel(dataManager);
        } else if (modelClass.isAssignableFrom(JobShareViewModel.class)) {
            //noinspection unchecked
            return (T) new JobShareViewModel(dataManager);
        }else if (modelClass.isAssignableFrom(LoginViewModel.class)){
            //noinspection unchecked
            return (T) new LoginViewModel(dataManager);
        } else if (modelClass.isAssignableFrom(ImagesViewModel.class)) {
            //noinspection unchecked
            return (T) new ImagesViewModel(dataManager);
        } else if (modelClass.isAssignableFrom(AddFolderViewModel.class)) {
            //noinspection unchecked
            return (T) new AddFolderViewModel(dataManager);
        } else if (modelClass.isAssignableFrom(SelectJobsViewModel.class)) {
            //noinspection unchecked
            return (T) new SelectJobsViewModel(dataManager);
        }else if (modelClass.isAssignableFrom(AddCompanyViewModel.class)) {
            //noinspection unchecked
            return (T) new AddCompanyViewModel(dataManager);
        }else if (modelClass.isAssignableFrom(RegisterViewModel.class)) {
            //noinspection unchecked
            return (T) new RegisterViewModel(dataManager);
        } else if (modelClass.isAssignableFrom(AddToNetworkViewModel.class)) {
            //noinspection unchecked
            return (T) new AddToNetworkViewModel(dataManager);
        } else if (modelClass.isAssignableFrom(SearchToNetworkViewModel.class)) {
            //noinspection unchecked
            return (T) new SearchToNetworkViewModel(dataManager);
        } else if (modelClass.isAssignableFrom(SearchSharedUserViewModel.class)) {
            //noinspection unchecked
            return (T) new SearchSharedUserViewModel(dataManager);
        } else if (modelClass.isAssignableFrom(ImageSelectViewModel.class)) {
            //noinspection unchecked
            return (T) new ImageSelectViewModel(dataManager);
        } else if (modelClass.isAssignableFrom(ImagePickModel.class)) {
            //noinspection unchecked
            return (T) new ImagePickModel(dataManager);
        } else if (modelClass.isAssignableFrom(DocumentViewModel.class)) {
            //noinspection unchecked
            return (T) new DocumentViewModel(dataManager);
        } else if (modelClass.isAssignableFrom(FinancialViewModel.class)) {
            //noinspection unchecked
            return (T) new FinancialViewModel(dataManager);
        }else if (modelClass.isAssignableFrom(EmployeeDetailsViewModel.class)) {
                //noinspection unchecked
                return (T) new EmployeeDetailsViewModel(dataManager);
        }else if (modelClass.isAssignableFrom(com.taskos.ui.main.openjobs.detail.comment.CommentViewModel.class)) {
                //noinspection unchecked
                return (T) new com.taskos.ui.main.openjobs.detail.comment.CommentViewModel(dataManager);
        }

        throw new IllegalArgumentException("Unknown ViewModel class: " + modelClass.getName());
    }
}