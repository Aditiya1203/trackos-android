package com.taskos.data.model.other;

import com.taskos.data.model.api.Job;

/**
 * Created by Hemant Sharma on 15-03-20.
 * Divergent software labs pvt. ltd
 */
public class JobSelect {
    public boolean isSelected = false;
    private Job job;

    public JobSelect(Job job) {
        this.job = job;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public Job getJob() {
        return job;
    }

    public void setJob(Job job) {
        this.job = job;
    }
}
