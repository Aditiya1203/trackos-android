package com.taskos.data.model.api;

import java.util.ArrayList;
import java.util.Date;

public class EstimateInvoiceNewItem {
    private String uuid;
    private String owner;
    private String name;
    private String description;
    private Boolean docIssued;
    private Double rate;
    private Double quantity;
    private String taxId;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getDocIssued() {
        return docIssued;
    }

    public void setDocIssued(Boolean docIssued) {
        this.docIssued = docIssued;
    }

    public Double getRate() {
        return rate;
    }

    public void setRate(Double rate) {
        this.rate = rate;
    }

    public Double getQuantity() {
        return quantity;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }

    public String getTaxId() {
        return taxId;
    }

    public void setTaxId(String taxId) {
        this.taxId = taxId;
    }
}
