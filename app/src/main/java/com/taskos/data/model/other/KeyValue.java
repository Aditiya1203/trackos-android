package com.taskos.data.model.other;

/**
 * Created by Hemant Sharma on 01-03-20.
 * Divergent software labs pvt. ltd
 */
public class KeyValue {
    public boolean isSelected = false;
    private String key;
    private String value;

    public KeyValue(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public KeyValue(String key, String value, boolean isSelected) {
        this.isSelected = isSelected;
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
