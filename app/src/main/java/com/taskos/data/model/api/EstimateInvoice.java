package com.taskos.data.model.api;

import java.util.ArrayList;
import java.util.Date;

public class EstimateInvoice {
    private String uuid;
    private String owner;
    private Boolean documentSeen;
    private Boolean docIssued;
    private String jobName;
    private Boolean invoice;
    private String customerId;
    private int estimateNumber;
    private Date estimateDate;
    private int invoiceNumber;
    private int paymentTerms;
    private int poNumber;
    private Date invoiceDate;
    private String companyLogo;
    private String docNotes;
    private String contract;
    private String clientNames;
    private String clientSignatures;
    private String salesNames;
    private String salesSignatures;
    private ArrayList<String> lineItemIds;
    private ArrayList<String> lineItemNames;
    private ArrayList<String> lineItemDescriptions;
    private ArrayList<String> paymentSchedUuid;
    private ArrayList<String> paymentSchedNames;
    private ArrayList<String> paymentUuid;
    private ArrayList<String> paymentNames;
    private ArrayList<Double> lineItemRates;
    private ArrayList<Double> payments;
    private ArrayList<Double> lineItemQuantities;
    private ArrayList<Double> lineItemTaxes;
    private ArrayList<Double> paymentSchedAmounts;
    private ArrayList<Date> paymentDates;
    private Double subtotal;
    private Double salesTax;
    private Double paymentTotal;
    private Double remainingBalance;
    private Double markup;
    private Double discount;
    private Boolean markupInDollar;
    private Boolean discountInDollar;
    private Boolean schedInDollar;
    private Boolean clientSignatureRequired;
    private Boolean salesSignatureRequired;
    private Boolean completed;
    private Boolean canceled;
    private Boolean onlinePayment;
    private Date dateEdited;
    private Date dateViewed;


    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public Boolean getDocumentSeen() {
        return documentSeen;
    }

    public void setDocumentSeen(Boolean documentSeen) {
        this.documentSeen = documentSeen;
    }

    public Boolean getDocIssued() {
        return docIssued;
    }

    public void setDocIssued(Boolean docIssued) {
        this.docIssued = docIssued;
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public Boolean getInvoice() {
        return invoice;
    }

    public void setInvoice(Boolean invoice) {
        this.invoice = invoice;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public int getEstimateNumber() {
        return estimateNumber;
    }

    public void setEstimateNumber(int estimateNumber) {
        this.estimateNumber = estimateNumber;
    }

    public Date getEstimateDate() {
        return estimateDate;
    }

    public void setEstimateDate(Date estimateDate) {
        this.estimateDate = estimateDate;
    }

    public int getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(int invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public int getPaymentTerms() {
        return paymentTerms;
    }

    public void setPaymentTerms(int paymentTerms) {
        this.paymentTerms = paymentTerms;
    }

    public int getPoNumber() {
        return poNumber;
    }

    public void setPoNumber(int poNumber) {
        this.poNumber = poNumber;
    }

    public Date getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(Date invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public String getCompanyLogo() {
        return companyLogo;
    }

    public void setCompanyLogo(String companyLogo) {
        this.companyLogo = companyLogo;
    }

    public String getDocNotes() {
        return docNotes;
    }

    public void setDocNotes(String docNotes) {
        this.docNotes = docNotes;
    }

    public String getContract() {
        return contract;
    }

    public void setContract(String contract) {
        this.contract = contract;
    }

    public String getClientNames() {
        return clientNames;
    }

    public void setClientNames(String clientNames) {
        this.clientNames = clientNames;
    }

    public String getClientSignatures() {
        return clientSignatures;
    }

    public void setClientSignatures(String clientSignatures) {
        this.clientSignatures = clientSignatures;
    }

    public String getSalesNames() {
        return salesNames;
    }

    public void setSalesNames(String salesNames) {
        this.salesNames = salesNames;
    }

    public String getSalesSignatures() {
        return salesSignatures;
    }

    public void setSalesSignatures(String salesSignatures) {
        this.salesSignatures = salesSignatures;
    }

    public ArrayList<String> getLineItemIds() {
        return lineItemIds;
    }

    public void setLineItemIds(ArrayList<String> lineItemIds) {
        this.lineItemIds = lineItemIds;
    }

    public ArrayList<String> getLineItemNames() {
        return lineItemNames;
    }

    public void setLineItemNames(ArrayList<String> lineItemNames) {
        this.lineItemNames = lineItemNames;
    }

    public ArrayList<String> getLineItemDescriptions() {
        return lineItemDescriptions;
    }

    public void setLineItemDescriptions(ArrayList<String> lineItemDescriptions) {
        this.lineItemDescriptions = lineItemDescriptions;
    }

    public ArrayList<String> getPaymentSchedUuid() {
        return paymentSchedUuid;
    }

    public void setPaymentSchedUuid(ArrayList<String> paymentSchedUuid) {
        this.paymentSchedUuid = paymentSchedUuid;
    }

    public ArrayList<String> getPaymentSchedNames() {
        return paymentSchedNames;
    }

    public void setPaymentSchedNames(ArrayList<String> paymentSchedNames) {
        this.paymentSchedNames = paymentSchedNames;
    }

    public ArrayList<String> getPaymentUuid() {
        return paymentUuid;
    }

    public void setPaymentUuid(ArrayList<String> paymentUuid) {
        this.paymentUuid = paymentUuid;
    }

    public ArrayList<String> getPaymentNames() {
        return paymentNames;
    }

    public void setPaymentNames(ArrayList<String> paymentNames) {
        this.paymentNames = paymentNames;
    }

    public ArrayList<Double> getLineItemRates() {
        return lineItemRates;
    }

    public void setLineItemRates(ArrayList<Double> lineItemRates) {
        this.lineItemRates = lineItemRates;
    }

    public ArrayList<Double> getPayments() {
        return payments;
    }

    public void setPayments(ArrayList<Double> payments) {
        this.payments = payments;
    }

    public ArrayList<Double> getLineItemQuantities() {
        return lineItemQuantities;
    }

    public void setLineItemQuantities(ArrayList<Double> lineItemQuantities) {
        this.lineItemQuantities = lineItemQuantities;
    }

    public ArrayList<Double> getLineItemTaxes() {
        return lineItemTaxes;
    }

    public void setLineItemTaxes(ArrayList<Double> lineItemTaxes) {
        this.lineItemTaxes = lineItemTaxes;
    }

    public ArrayList<Double> getPaymentSchedAmounts() {
        return paymentSchedAmounts;
    }

    public void setPaymentSchedAmounts(ArrayList<Double> paymentSchedAmounts) {
        this.paymentSchedAmounts = paymentSchedAmounts;
    }

    public ArrayList<Date> getPaymentDates() {
        return paymentDates;
    }

    public void setPaymentDates(ArrayList<Date> paymentDates) {
        this.paymentDates = paymentDates;
    }

    public Double getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(Double subtotal) {
        this.subtotal = subtotal;
    }

    public Double getSalesTax() {
        return salesTax;
    }

    public void setSalesTax(Double salesTax) {
        this.salesTax = salesTax;
    }

    public Double getPaymentTotal() {
        return paymentTotal;
    }

    public void setPaymentTotal(Double paymentTotal) {
        this.paymentTotal = paymentTotal;
    }

    public Double getRemainingBalance() {
        return remainingBalance;
    }

    public void setRemainingBalance(Double remainingBalance) {
        this.remainingBalance = remainingBalance;
    }

    public Double getMarkup() {
        return markup;
    }

    public void setMarkup(Double markup) {
        this.markup = markup;
    }

    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }

    public Boolean getMarkupInDollar() {
        return markupInDollar;
    }

    public void setMarkupInDollar(Boolean markupInDollar) {
        this.markupInDollar = markupInDollar;
    }

    public Boolean getDiscountInDollar() {
        return discountInDollar;
    }

    public void setDiscountInDollar(Boolean discountInDollar) {
        this.discountInDollar = discountInDollar;
    }

    public Boolean getSchedInDollar() {
        return schedInDollar;
    }

    public void setSchedInDollar(Boolean schedInDollar) {
        this.schedInDollar = schedInDollar;
    }

    public Boolean getClientSignatureRequired() {
        return clientSignatureRequired;
    }

    public void setClientSignatureRequired(Boolean clientSignatureRequired) {
        this.clientSignatureRequired = clientSignatureRequired;
    }

    public Boolean getSalesSignatureRequired() {
        return salesSignatureRequired;
    }

    public void setSalesSignatureRequired(Boolean salesSignatureRequired) {
        this.salesSignatureRequired = salesSignatureRequired;
    }

    public Boolean getCompleted() {
        return completed;
    }

    public void setCompleted(Boolean completed) {
        this.completed = completed;
    }

    public Boolean getCanceled() {
        return canceled;
    }

    public void setCanceled(Boolean canceled) {
        this.canceled = canceled;
    }

    public Boolean getOnlinePayment() {
        return onlinePayment;
    }

    public void setOnlinePayment(Boolean onlinePayment) {
        this.onlinePayment = onlinePayment;
    }

    public Date getDateEdited() {
        return dateEdited;
    }

    public void setDateEdited(Date dateEdited) {
        this.dateEdited = dateEdited;
    }

    public Date getDateViewed() {
        return dateViewed;
    }

    public void setDateViewed(Date dateViewed) {
        this.dateViewed = dateViewed;
    }
}
