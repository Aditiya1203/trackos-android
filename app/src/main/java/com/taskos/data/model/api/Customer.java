package com.taskos.data.model.api;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Hemant Sharma on 28-02-20.
 * Divergent software labs pvt. ltd
 */
public class Customer {

    private String uuid;
    private String name;
    private String phone;
    private String email;
    private String streetAddress;
    private String city;
    private String state;
    private String zipcode;
    private Date appointmentTime;
    private Date appointmentEntered;
    private String salesperson;
    private String source;
    private String productOfInterest;
    private String customerStatus;
    private String listOrganizer; // (options: Open, Not Sold, Customer)
    private String owner;
    private String salesUuid;
    private ArrayList<String> subscribers;
    private boolean appointmentSet;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getStreetAddress() {
        return streetAddress;
    }

    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public Date getAppointmentTime() {
        return appointmentTime;
    }

    public void setAppointmentTime(Date appointmentTime) {
        this.appointmentTime = appointmentTime;
    }

    public Date getAppointmentEntered() {
        return appointmentEntered;
    }

    public void setAppointmentEntered(Date appointmentEntered) {
        this.appointmentEntered = appointmentEntered;
    }

    public String getSalesperson() {
        return salesperson;
    }

    public void setSalesperson(String salesperson) {
        this.salesperson = salesperson;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getProductOfInterest() {
        return productOfInterest;
    }

    public void setProductOfInterest(String productOfInterest) {
        this.productOfInterest = productOfInterest;
    }

    public String getCustomerStatus() {
        return customerStatus;
    }

    public void setCustomerStatus(String customerStatus) {
        this.customerStatus = customerStatus;
    }

    public String getListOrganizer() {
        return listOrganizer;
    }

    public void setListOrganizer(String listOrganizer) {
        this.listOrganizer = listOrganizer;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getSalesUuid() {
        return salesUuid;
    }

    public void setSalesUuid(String salesUuid) {
        this.salesUuid = salesUuid;
    }

    public ArrayList<String> getSubscribers() {
        return subscribers;
    }

    public void setSubscribers(ArrayList<String> subscribers) {
        this.subscribers = subscribers;
    }

    public boolean isAppointmentSet() {
        return appointmentSet;
    }

    public void setAppointmentSet(boolean appointmentSet) {
        this.appointmentSet = appointmentSet;
    }
}
