package com.taskos.data.model.other;

import com.taskos.data.model.api.Customer;

/**
 * Created by Hemant Sharma on 15-03-20.
 * Divergent software labs pvt. ltd
 */
public class CustomerSelect {
    public boolean isSelected = false;
    private Customer customer;

    public CustomerSelect(Customer customer) {
        this.customer = customer;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }
}
