package com.taskos.data.model.api;

import java.util.Date;
import java.util.List;

/**
 * Created by Hemant Sharma on 28-02-20.
 * Divergent software labs pvt. ltd
 */
public class JobFinancial {

    private String uuid;
    private String customerUuid;
    private String jobUuid;
    private Date soldDate;
    private double subtotal;
    private double salesTax;
    private double salesTotal;
    private double paymentAmountReceived;
    private String invoiceLinks;   //(pass empty array)
    private String owner;
    private List<Double> paymentAmounts;
    private List<String> paymentUuid;
    private List<String> expenseItem;
    private List<Double> expenseAmount;
    private List<String> expenseUuid;
    private List<String> paymentType;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getCustomerUuid() {
        return customerUuid;
    }

    public void setCustomerUuid(String customerUuid) {
        this.customerUuid = customerUuid;
    }

    public String getJobUuid() {
        return jobUuid;
    }

    public void setJobUuid(String jobUuid) {
        this.jobUuid = jobUuid;
    }

    public Date getSoldDate() {
        return soldDate;
    }

    public void setSoldDate(Date soldDate) {
        this.soldDate = soldDate;
    }

    public double getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(double subtotal) {
        this.subtotal = subtotal;
    }

    public double getSalesTax() {
        return salesTax;
    }

    public void setSalesTax(double salesTax) {
        this.salesTax = salesTax;
    }

    public double getSalesTotal() {
        return salesTotal;
    }

    public void setSalesTotal(double salesTotal) {
        this.salesTotal = salesTotal;
    }

    public double getPaymentAmountReceived() {
        return paymentAmountReceived;
    }

    public void setPaymentAmountReceived(double paymentAmountReceived) {
        this.paymentAmountReceived = paymentAmountReceived;
    }

    public String getInvoiceLinks() {
        return invoiceLinks;
    }

    public void setInvoiceLinks(String invoiceLinks) {
        this.invoiceLinks = invoiceLinks;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public List<Double> getPaymentAmounts() {
        return paymentAmounts;
    }

    public void setPaymentAmounts(List<Double> paymentAmounts) {
        this.paymentAmounts = paymentAmounts;
    }

    public List<String> getPaymentUuid() {
        return paymentUuid;
    }

    public void setPaymentUuid(List<String> paymentUuid) {
        this.paymentUuid = paymentUuid;
    }

    public List<String> getExpenseItem() {
        return expenseItem;
    }

    public void setExpenseItem(List<String> expenseItem) {
        this.expenseItem = expenseItem;
    }

    public List<Double> getExpenseAmount() {
        return expenseAmount;
    }

    public void setExpenseAmount(List<Double> expenseAmount) {
        this.expenseAmount = expenseAmount;
    }

    public List<String> getExpenseUuid() {
        return expenseUuid;
    }

    public void setExpenseUuid(List<String> expenseUuid) {
        this.expenseUuid = expenseUuid;
    }

    public List<String> getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(List<String> paymentType) {
        this.paymentType = paymentType;
    }
}
