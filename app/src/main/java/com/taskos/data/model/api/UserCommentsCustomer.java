package com.taskos.data.model.api;

import java.util.Date;

/**
 * Created by Hemant Sharma on 28-02-20.
 * Divergent software labs pvt. ltd
 */
public class UserCommentsCustomer {

    private String uuid;
    private String userFirstName;
    private String userLastName;
    private String userUUID;
    private String username;
    private String customerUuid;
    private String comment;
    private Date commentPostedTime;
    private String customerStatus;
    private String imageLink; //(pass empty string)
    private String owner;
    private String jobUui;

    public String getJobUui() {
        return jobUui;
    }

    public void setJobUui(String jobUui) {
        this.jobUui = jobUui;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getUserFirstName() {
        return userFirstName;
    }

    public void setUserFirstName(String userFirstName) {
        this.userFirstName = userFirstName;
    }

    public String getUserLastName() {
        return userLastName;
    }

    public void setUserLastName(String userLastName) {
        this.userLastName = userLastName;
    }

    public String getUserUUID() {
        return userUUID;
    }

    public void setUserUUID(String userUUID) {
        this.userUUID = userUUID;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getCustomerUuid() {
        return customerUuid;
    }

    public void setCustomerUuid(String customerUuid) {
        this.customerUuid = customerUuid;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Date getCommentPostedTime() {
        return commentPostedTime;
    }

    public void setCommentPostedTime(Date commentPostedTime) {
        this.commentPostedTime = commentPostedTime;
    }

    public String getCustomerStatus() {
        return customerStatus;
    }

    public void setCustomerStatus(String customerStatus) {
        this.customerStatus = customerStatus;
    }

    public String getImageLink() {
        return imageLink == null ? "" : imageLink;
    }

    public void setImageLink(String imageLink) {
        this.imageLink = imageLink;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }
}
