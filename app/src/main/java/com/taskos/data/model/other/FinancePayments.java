package com.taskos.data.model.other;

public class FinancePayments {
    private double paymentAmounts;
    private String paymentUuid;
    private String paymentType;

    public double getPaymentAmounts() {
        return paymentAmounts;
    }

    public void setPaymentAmounts(double paymentAmounts) {
        this.paymentAmounts = paymentAmounts;
    }

    public String getPaymentUuid() {
        return paymentUuid;
    }

    public void setPaymentUuid(String paymentUuid) {
        this.paymentUuid = paymentUuid;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }
}
