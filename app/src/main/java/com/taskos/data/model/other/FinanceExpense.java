package com.taskos.data.model.other;

public class FinanceExpense {
    private String expenseItem = "";
    private double expenseAmount;
    private String expenseUuid = "";

    public String getExpenseItem() {
        return expenseItem;
    }

    public void setExpenseItem(String expenseItem) {
        this.expenseItem = expenseItem;
    }

    public double getExpenseAmount() {
        return expenseAmount;
    }

    public void setExpenseAmount(double expenseAmount) {
        this.expenseAmount = expenseAmount;
    }

    public String getExpenseUuid() {
        return expenseUuid;
    }

    public void setExpenseUuid(String expenseUuid) {
        this.expenseUuid = expenseUuid;
    }

}
