package com.taskos.data.model.api;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Hemant Sharma on 28-02-20.
 * Divergent software labs pvt. ltd
 */
public class Job {

    private String uuid;
    private String name;
    private String streetAddress;
    private String city;
    private String state;
    private String zipcode;
    private String soldItem;
    private Date installStart;
    private Date installEnd;
    private List<String> imageLinks;  //(empty array if needed)
    private String salesperson;
    private String customerId;
    private ArrayList<String> assignedEmployees;
    private String scheduledStatus; //(“Not Ready”, “Ready to Schedule”, etc.)
    private String owner;
    private Date soldDate;
    private ArrayList<String> subscribers;
    private boolean shared;
    private String salesUuid;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStreetAddress() {
        return streetAddress;
    }

    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getSoldItem() {
        return soldItem;
    }

    public void setSoldItem(String soldItem) {
        this.soldItem = soldItem;
    }

    public Date getInstallStart() {
        return installStart;
    }

    public void setInstallStart(Date installStart) {
        this.installStart = installStart;
    }

    public Date getInstallEnd() {
        return installEnd;
    }

    public void setInstallEnd(Date installEnd) {
        this.installEnd = installEnd;
    }

    public List<String> getImageLinks() {
        return imageLinks;
    }

    public void setImageLinks(List<String> imageLinks) {
        this.imageLinks = imageLinks;
    }

    public String getSalesperson() {
        return salesperson;
    }

    public void setSalesperson(String salesperson) {
        this.salesperson = salesperson;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public ArrayList<String> getAssignedEmployees() {
        return assignedEmployees;
    }

    public void setAssignedEmployees(ArrayList<String> assignedEmployees) {
        this.assignedEmployees = assignedEmployees;
    }

    public String getScheduledStatus() {
        return scheduledStatus;
    }

    public void setScheduledStatus(String scheduledStatus) {
        this.scheduledStatus = scheduledStatus;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public Date getSoldDate() {
        return soldDate;
    }

    public void setSoldDate(Date soldDate) {
        this.soldDate = soldDate;
    }

    public ArrayList<String> getSubscribers() {
        return subscribers;
    }

    public void setSubscribers(ArrayList<String> subscribers) {
        this.subscribers = subscribers;
    }

    public boolean isShared() {
        return shared;
    }

    public void setShared(boolean shared) {
        this.shared = shared;
    }

    public String getSalesUuid() {
        return salesUuid;
    }

    public void setSalesUuid(String salesUuid) {
        this.salesUuid = salesUuid;
    }
}
