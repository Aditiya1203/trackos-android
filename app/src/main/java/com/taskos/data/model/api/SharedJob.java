package com.taskos.data.model.api;

import java.util.Date;
import java.util.List;

/**
 * Created by Hemant Sharma on 28-02-20.
 * Divergent software labs pvt. ltd
 */
public class SharedJob {

    private String uuid;
    private String jobUuid;
    private String name;
    private String streetAddress;
    private String city;
    private String state;
    private String zipcode;
    private String soldItem;
    private Date installStart;
    private Date installEnd;
    private List<String> assignedEmployees;
    private String scheduledStatus;
    private String owner;
    private List<String> subscribers;
    private String sharedUser;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getJobUuid() {
        return jobUuid;
    }

    public void setJobUuid(String jobUuid) {
        this.jobUuid = jobUuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStreetAddress() {
        return streetAddress;
    }

    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getSoldItem() {
        return soldItem;
    }

    public void setSoldItem(String soldItem) {
        this.soldItem = soldItem;
    }

    public Date getInstallStart() {
        return installStart;
    }

    public void setInstallStart(Date installStart) {
        this.installStart = installStart;
    }

    public Date getInstallEnd() {
        return installEnd;
    }

    public void setInstallEnd(Date installEnd) {
        this.installEnd = installEnd;
    }


    public String getScheduledStatus() {
        return scheduledStatus;
    }

    public void setScheduledStatus(String scheduledStatus) {
        this.scheduledStatus = scheduledStatus;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public List<String> getAssignedEmployees() {
        return assignedEmployees;
    }

    public void setAssignedEmployees(List<String> assignedEmployees) {
        this.assignedEmployees = assignedEmployees;
    }

    public List<String> getSubscribers() {
        return subscribers;
    }

    public void setSubscribers(List<String> subscribers) {
        this.subscribers = subscribers;
    }

    public String getSharedUser() {
        return sharedUser;
    }

    public void setSharedUser(String sharedUser) {
        this.sharedUser = sharedUser;
    }
}
