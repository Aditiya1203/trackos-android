package com.taskos.data;

import com.google.firebase.firestore.FirebaseFirestore;
import com.taskos.data.local.prefs.PreferencesHelper;
import com.taskos.data.remote.ApiHelper;


/**
 * Created by Hemant Sharma on 23-02-20.
 */

public interface DataManager extends PreferencesHelper, ApiHelper {

    FirebaseFirestore getFirestoreInstance();

    String getDeviceToken();

    void setDeviceToken(String fcmToken);
}
