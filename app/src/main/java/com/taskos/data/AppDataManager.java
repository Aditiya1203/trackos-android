package com.taskos.data;

import android.app.Activity;
import android.content.Context;

import com.google.firebase.firestore.FirebaseFirestore;
import com.taskos.data.local.prefs.AppPreferencesHelper;
import com.taskos.data.local.prefs.PreferencesHelper;
import com.taskos.data.remote.ApiHelper;
import com.taskos.data.remote.AppApiHelper;


/**
 * Created by Hemant Sharma on 23-02-20.
 */


public final class AppDataManager implements DataManager {

    private static AppDataManager instance;
    private final ApiHelper mApiHelper;
    private final PreferencesHelper mPreferencesHelper;
    private FirebaseFirestore mFirestore;
    private String deviceToken = "";


    private AppDataManager(Context context) {
        mPreferencesHelper = new AppPreferencesHelper(context);
        mApiHelper = AppApiHelper.getAppApiInstance();
        mFirestore = FirebaseFirestore.getInstance();
    }

    public synchronized static AppDataManager getInstance(Context context) {
        if (instance == null) {
            instance = new AppDataManager(context);
        }
        return instance;
    }

    @Override
    public boolean isLoggedIn() {
        return mPreferencesHelper.isLoggedIn();
    }

    @Override
    public String getUUID() {
        return mPreferencesHelper.getUUID();
    }

    @Override
    public void setUUID(String uuid) {
        mPreferencesHelper.setUUID(uuid);
    }

    @Override
    public String getCompanyID() {
        return mPreferencesHelper.getCompanyID();
    }

    @Override
    public void setCompanyID(String uuid) {
        mPreferencesHelper.setCompanyID(uuid);
    }

    @Override
    public String getCompanyName() {
        return mPreferencesHelper.getCompanyName();
    }

    @Override
    public void setCompanyName(String uuid) {
        mPreferencesHelper.setCompanyName(uuid);
    }

    @Override
    public String getUserDisplayName() {
        return mPreferencesHelper.getUserDisplayName();
    }

    @Override
    public void setUserDisplayName(String name) {
        mPreferencesHelper.setUserDisplayName(name);
    }

    @Override
    public String getUserDisplayPicture() {
        return mPreferencesHelper.getUserDisplayPicture();
    }

    @Override
    public void setUserDisplayPicture(String name) {
        mPreferencesHelper.setUserDisplayPicture(name);
    }

    @Override
    public boolean isRegisterAdded() {
        return mPreferencesHelper.isRegisterAdded();
    }

    @Override
    public void setRegisterAdded(boolean isRegisterAdded) {
        mPreferencesHelper.setRegisterAdded(isRegisterAdded);
    }

    @Override
    public boolean isCompanyAdded() {
        return mPreferencesHelper.isCompanyAdded();
    }

    @Override
    public void setCompanyAdded(boolean isCompanyAdded) {
        mPreferencesHelper.setCompanyAdded(isCompanyAdded);
    }

    @Override
    public void setLoggedIn(boolean isLoggedIn) {
        mPreferencesHelper.setLoggedIn(isLoggedIn);
    }

    @Override
    public boolean isUserCompanyAddRequested() {
        return mPreferencesHelper.isUserCompanyAddRequested();
    }

    @Override
    public void setUserCompanyAddRequested(boolean userCompanyAddRequested) {
        mPreferencesHelper.setUserCompanyAddRequested(userCompanyAddRequested);
    }

    @Override
    public boolean isUserApproveCompanyAdd() {
        return mPreferencesHelper.isUserApproveCompanyAdd();
    }

    @Override
    public void setUserApproveCompanyAdd(boolean userApproveCompanyAdd) {
        mPreferencesHelper.setUserApproveCompanyAdd(userApproveCompanyAdd);
    }

    @Override
    public void logout(Activity activity) {
        mPreferencesHelper.logout(activity);
    }

    @Override
    public FirebaseFirestore getFirestoreInstance() {
        return mFirestore;
    }

    @Override
    public String getDeviceToken() {
        return deviceToken;
    }

    @Override
    public void setDeviceToken(String fcmToken) {
        deviceToken = fcmToken;
    }
}
