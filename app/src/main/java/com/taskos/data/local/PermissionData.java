package com.taskos.data.local;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PermissionData {

    private static String[] kCustomerPermissionsArray_1 = {"Access Customers", "See All Leads", "Edit Customer", "Edit Salesperson", "Delete Customer", "Change Customer Status", "Delete Customer Comment"};
    private static  String[] kJobPermissionsArray_1 = {"Access Jobs","Add Job", "Edit Job", "Change Job Status", "Assign Employees to Job", "See Financials", "Delete Job Comment", "Share Job"};
    private static  String[] kTaskPermissionArray_1 = {"See All Tasks", "Assign Task to Employee", "Delete Task"};
    private static  String[] kCompanyPermissionsArray_1 = {"Add Employees", "Remove Employees", "Grant Permissions", "Edit Company"};

    public static List<String> kCustomerPermissionsArray = Arrays.asList(kCustomerPermissionsArray_1);
    public static List<String> kJobPermissionsArray = Arrays.asList(kJobPermissionsArray_1);
    public static List<String> kTaskPermissionArray = Arrays.asList(kTaskPermissionArray_1);
    public static List<String> kCompanyPermissionsArray = Arrays.asList(kCompanyPermissionsArray_1);

    public static List<String> getFullPermission(){
        List<String> allPermission = new ArrayList<>();
        allPermission.addAll(kCompanyPermissionsArray);
        allPermission.addAll(kTaskPermissionArray);
        allPermission.addAll(kJobPermissionsArray);
        allPermission.addAll(kCustomerPermissionsArray);
        return allPermission;
    }

    public static List<PermissionModel> getCustomerPermission(List<String> addedPermission){
        List<PermissionModel> allPermission = new ArrayList<>();

        for(int i = 0; i < kCustomerPermissionsArray_1.length; i++){
            allPermission.add(new PermissionModel(kCustomerPermissionsArray_1[i], addedPermission.contains(kCustomerPermissionsArray_1[i])));
        }

        return allPermission;
    }
    public static List<PermissionModel> getJobPermission(List<String> addedPermission){
        List<PermissionModel> allPermission = new ArrayList<>();

        for(int i = 0; i < kJobPermissionsArray_1.length; i++){
            allPermission.add(new PermissionModel(kJobPermissionsArray_1[i],addedPermission.contains(kJobPermissionsArray_1[i])));
        }

        return allPermission;
    }
    public static List<PermissionModel> getTaskPermission(List<String> addedPermission){
        List<PermissionModel> allPermission = new ArrayList<>();

        for(int i = 0; i < kTaskPermissionArray_1.length; i++){
            allPermission.add(new PermissionModel(kTaskPermissionArray_1[i],addedPermission.contains(kTaskPermissionArray_1[i])));
        }

        return allPermission;
    }
    public static List<PermissionModel> getCompanyPermission(List<String> addedPermission){
        List<PermissionModel> allPermission = new ArrayList<>();

        for(int i = 0; i < kCompanyPermissionsArray_1.length; i++){
            allPermission.add(new PermissionModel(kCompanyPermissionsArray_1[i],addedPermission.contains(kCompanyPermissionsArray_1[i])));
        }

        return allPermission;
    }
}