package com.taskos.data.local.prefs;


import android.app.Activity;

/**
 * Created by Hemant Sharma on 23-02-20.
 */

public interface PreferencesHelper {

    boolean isLoggedIn();

    String getUUID();

    void setUUID(String uuid);

    String getCompanyID();

    void setCompanyID(String uuid);

    String getCompanyName();

    void setCompanyName(String uuid);

    String getUserDisplayName();

    void setUserDisplayName(String name);

    String getUserDisplayPicture();

    void setUserDisplayPicture(String name);

    boolean isRegisterAdded();

    void setRegisterAdded(boolean isRegisterAdded);

    boolean isCompanyAdded();

    void setCompanyAdded(boolean isCompanyAdded);

    void setLoggedIn(boolean isLoggedIn);

    boolean isUserCompanyAddRequested();

    void setUserCompanyAddRequested(boolean userCompanyAddRequested);

    boolean isUserApproveCompanyAdd();

    void setUserApproveCompanyAdd(boolean userApproveCompanyAdd);

    void logout(Activity activity);
}
