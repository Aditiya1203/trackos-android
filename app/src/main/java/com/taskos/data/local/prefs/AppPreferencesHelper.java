package com.taskos.data.local.prefs;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.taskos.ui.splash.SplashActivity;

/**
 * Created by Hemant Sharma on 23-02-20.
 */

public final class AppPreferencesHelper implements PreferencesHelper {

    private static final String APP_PREFERENCE = "AppPreference";

    private static final String PREF_KEY_USER_LOGGED_IN_MODE = "PREF_KEY_USER_LOGGED_IN_MODE";

    private static final String UUID = "UUID";

    private static final String companyID = "companyID";

    private static final String companyName = "companyName";

    private static final String COMPANY_ADDED = "COMPANY_ADDED";

    private static final String REGISTRATION_ADDED = "REGISTRATION_ADDED";

    private static final String USER_DISPLAY_NAME = "USER_DISPLAY_NAME";
    private static final String USER_DISPLAY_PICTURE = "USER_DISPLAY_PICTURE";

    // prefs object
    private final SharedPreferences mPrefs;

    public AppPreferencesHelper(Context context) {
        mPrefs = context.getSharedPreferences(APP_PREFERENCE, Context.MODE_PRIVATE);
    }

    @Override
    public boolean isLoggedIn() {
        return mPrefs.getBoolean(PREF_KEY_USER_LOGGED_IN_MODE, false);
    }

    @Override
    public void setLoggedIn(boolean isLoggedIn) {
        mPrefs.edit().putBoolean(PREF_KEY_USER_LOGGED_IN_MODE, isLoggedIn).apply();
    }

    @Override
    public boolean isUserCompanyAddRequested() {
        //Todo set
        return true;
    }

    @Override
    public void setUserCompanyAddRequested(boolean userCompanyAddRequested) {
//Todo set
    }

    @Override
    public boolean isUserApproveCompanyAdd() {
        //Todo set
        return true;
    }

    @Override
    public void setUserApproveCompanyAdd(boolean userApproveCompanyAdd) {
//Todo set
    }

    @Override
    public boolean isCompanyAdded() {
        return mPrefs.getBoolean(COMPANY_ADDED, false);
    }

    @Override
    public void setCompanyAdded(boolean isCompanyAdded) {
        mPrefs.edit().putBoolean(COMPANY_ADDED, isCompanyAdded).apply();
    }

    @Override
    public boolean isRegisterAdded() {
        return mPrefs.getBoolean(REGISTRATION_ADDED, false);
    }

    @Override
    public void setRegisterAdded(boolean isRegisterAdded) {
        mPrefs.edit().putBoolean(REGISTRATION_ADDED, isRegisterAdded).apply();
    }

    @Override
    public String getUUID() {
        return mPrefs.getString(UUID, "");
    }

    @Override
    public void setUUID(String uuid) {
        mPrefs.edit().putString(UUID, uuid).apply();
    }

    @Override
    public String getUserDisplayName() {
        return mPrefs.getString(USER_DISPLAY_NAME, "");
    }

    @Override
    public void setUserDisplayName(String name) {
        mPrefs.edit().putString(USER_DISPLAY_NAME, name).apply();
    }

    @Override
    public String getUserDisplayPicture() {
        return mPrefs.getString(USER_DISPLAY_PICTURE, "");
    }

    @Override
    public void setUserDisplayPicture(String name) {
        mPrefs.edit().putString(USER_DISPLAY_PICTURE, name).apply();
    }

    @Override
    public String getCompanyID() {
        return mPrefs.getString(companyID, "");
    }

    @Override
    public void setCompanyID(String uuid) {
        mPrefs.edit().putString(companyID, uuid).apply();
    }

    @Override
    public String getCompanyName() {
        return mPrefs.getString(companyName, "");
    }

    @Override
    public void setCompanyName(String uuid) {
        mPrefs.edit().putString(companyName, uuid).apply();
    }

    @Override
    public void logout(Activity activity) {
        setLoggedIn(false);
        setCompanyID("");
        setCompanyAdded(false);
        setRegisterAdded(false);
        Intent intent =new Intent(activity, SplashActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        activity.startActivity(intent);
        activity.finish();
    }

}
