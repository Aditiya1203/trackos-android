package com.taskos.data.local;

public class PermissionModel {
    String name;
    boolean selected;

    PermissionModel(String name,Boolean selected){
        this.name = name;
        this.selected = selected;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
