

package com.taskos.ui.auth.moreInfo;

import android.app.Activity;
import android.net.Uri;

import com.taskos.ui.auth.MUser;

public interface RegisterNavigator {

    void goToMainScreen();
    void successful(String message);
    void error(String message);
    Activity getActivity();
    String getFirstName();
    String getLastName();
    String getCity();
    String getState();
    String getPhone();
    String getJob();
    String getStreetAddress();
    String getZipCode();
    Uri getURI();
    void setUserData(MUser userData);

}
