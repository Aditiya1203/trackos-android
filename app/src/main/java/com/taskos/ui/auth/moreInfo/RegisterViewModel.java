package com.taskos.ui.auth.moreInfo;

import android.annotation.SuppressLint;
import android.net.Uri;
import android.util.Log;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.taskos.data.DataManager;
import com.taskos.data.model.api.Job;
import com.taskos.ui.auth.MUser;
import com.taskos.ui.base.BaseViewModel;
import com.taskos.ui.fcm.FcmNotificationBuilder;
import com.taskos.util.AppLogger;
import com.taskos.util.AppUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

public class RegisterViewModel extends BaseViewModel<RegisterNavigator> {


    private FirebaseAuth mAuth;

    public RegisterViewModel(DataManager dataManager) {
        super(dataManager);
        mAuth = FirebaseAuth.getInstance();
    }


    public String getUID(){
        return Objects.requireNonNull(mAuth.getCurrentUser()).getUid();
    }

    public void completeProfile(){
        if(getNavigator().getFirstName().isEmpty()||
            getNavigator().getLastName().isEmpty()||
            getNavigator().getCity().isEmpty()||
            getNavigator().getJob().isEmpty()||
            getNavigator().getPhone().isEmpty()||
            getNavigator().getState().isEmpty()){
            getNavigator().error("Please enter all field");
        }else{
            setDataToFirebase();
        }
    }
    public void updateProfile(){
        if(getNavigator().getFirstName().isEmpty()||
            getNavigator().getLastName().isEmpty()||
            getNavigator().getCity().isEmpty()||
            getNavigator().getJob().isEmpty()||
            getNavigator().getPhone().isEmpty()||
            getNavigator().getStreetAddress().isEmpty()||
            getNavigator().getZipCode().isEmpty()||
            getNavigator().getState().isEmpty()){
            getNavigator().error("Please enter all field");
        }else{
            updateDataToFirebase();
        }
    }

    public void getUserData(){
        getDataManager().getFirestoreInstance().collection("MUser").document(getDataManager().getUUID())
                .addSnapshotListener((snapshot, e) -> {

                    if (snapshot != null && snapshot.exists()) {
                        Log.e("User Data already ", "Current data: all" + snapshot.getData());
                        Log.e("User Data already ", "Current data: all" + snapshot.getData().get("companyUuid"));

                        MUser mUser = snapshot.toObject(MUser.class);

                        getNavigator().setUserData(mUser);
                    }

                });
    }

    private void updateDataToFirebase(){

            @SuppressLint("SimpleDateFormat")
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
            String name = timeStamp + ".jpg";

        if(getNavigator().getURI() != null){
            StorageReference storageRef = FirebaseStorage.getInstance().getReference();
            final StorageReference ref = storageRef.child("images/".concat(name));
            UploadTask uploadTask = ref.putFile(getNavigator().getURI());
            uploadTask.continueWithTask(task -> {
                if (!task.isSuccessful()) {
                    getNavigator().error(task.getException().getMessage());
                }
                // Continue with the task to get the download URL
                return ref.getDownloadUrl();
            }).addOnCompleteListener(task -> {
                if (task.isSuccessful()) {
                    Uri downloadUri = task.getResult();
                    getDataManager().getFirestoreInstance().collection("MUser").document(getUID())
                            .update(
                                    "userFirstName", getNavigator().getFirstName(),
                                    "userLastName", getNavigator().getLastName(),
                                    "city", getNavigator().getCity(),
                                    "jobRole", getNavigator().getJob(),
                                    "phone", getNavigator().getPhone(),
                                    "state", getNavigator().getState(),
                                    "zipcode", getNavigator().getZipCode(),
                                    "streetAddress", getNavigator().getStreetAddress(),
                                    "userProfileImageLink", downloadUri.toString(),
                                    "onBoarded", true,
                                    "username",getNavigator().getFirstName()+" "+getNavigator().getLastName()
                            )
                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    getNavigator().successful("Updated Successfully");
                                }
                            })
                            .addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    getNavigator().error(e.getMessage());
                                }
                            });
                }
            });
        }else{
            getDataManager().getFirestoreInstance().collection("MUser").document(getUID())
                    .update(
                            "userFirstName", getNavigator().getFirstName(),
                            "userLastName", getNavigator().getLastName(),
                            "city", getNavigator().getCity(),
                            "jobRole", getNavigator().getJob(),
                            "phone", getNavigator().getPhone(),
                            "state", getNavigator().getState(),
                            "zipcode", getNavigator().getZipCode(),
                            "streetAddress", getNavigator().getStreetAddress(),
                            "onBoarded", true,
                            "username",getNavigator().getFirstName()+" "+getNavigator().getLastName()
                    )
                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            getNavigator().successful("Updated Successfully");
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            getNavigator().error(e.getMessage());
                        }
                    });
        }


    }

    private void setDataToFirebase(){



        @SuppressLint("SimpleDateFormat")
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String name = timeStamp + ".jpg";

        if(getNavigator().getURI() != null){
            StorageReference storageRef = FirebaseStorage.getInstance().getReference();
            final StorageReference ref = storageRef.child("images/".concat(name));
            UploadTask uploadTask = ref.putFile(getNavigator().getURI());
            uploadTask.continueWithTask(task -> {
                if (!task.isSuccessful()) {
                    getNavigator().error(task.getException().getMessage());
                }
                // Continue with the task to get the download URL
                return ref.getDownloadUrl();
            }).addOnCompleteListener(task -> {
                if (task.isSuccessful()) {
                    Uri downloadUri = task.getResult();

                    getDataManager().getFirestoreInstance().collection("MUser").document(getUID())
                            .update(
                                    "userFirstName", getNavigator().getFirstName(),
                                    "userFirstName", getNavigator().getLastName(),
                                    "city", getNavigator().getCity(),
                                    "jobRole", getNavigator().getJob(),
                                    "phone", getNavigator().getPhone(),
                                    "state", getNavigator().getState(),
                                    "userProfileImageLink", downloadUri.toString(),
                                    "onBoarded", true,
                                    "username",getNavigator().getFirstName()+" "+getNavigator().getLastName()
                            )
                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    getDataManager().setRegisterAdded(true);
                                    getNavigator().goToMainScreen();
                                }
                            })
                            .addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    getNavigator().error(e.getMessage());
                                }
                            });

                }
            });
        }else{
            getDataManager().getFirestoreInstance().collection("MUser").document(getUID())
                    .update(
                            "userFirstName", getNavigator().getFirstName(),
                            "userFirstName", getNavigator().getLastName(),
                            "city", getNavigator().getCity(),
                            "jobRole", getNavigator().getJob(),
                            "phone", getNavigator().getPhone(),
                            "state", getNavigator().getState(),
                            "onBoarded", true,
                            "username",getNavigator().getFirstName()+" "+getNavigator().getLastName()
                    )
                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            getDataManager().setRegisterAdded(true);
                            getNavigator().goToMainScreen();
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            getNavigator().error(e.getMessage());
                        }
                    });
        }


    }
}
