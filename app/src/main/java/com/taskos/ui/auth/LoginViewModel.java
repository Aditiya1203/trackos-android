package com.taskos.ui.auth;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.taskos.data.DataManager;
import com.taskos.ui.base.BaseViewModel;

public class LoginViewModel extends BaseViewModel<LoginNavigator> {


    private FirebaseAuth mAuth;

    public LoginViewModel(DataManager dataManager) {
        super(dataManager);
        mAuth = FirebaseAuth.getInstance();
    }

    private void resetPassword(){
        mAuth.sendPasswordResetEmail(getNavigator().getEmail())
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            getNavigator().successful("Reset Password Email Sent!");
                        } else {
                            // If sign in fails, display a message to the user.
                            getNavigator().error("Error Registering: "+ task.getException().getMessage());
                        }
                    }
                });
    }

    public void checkSignUp(){
        if (getNavigator().getEmail().isEmpty() || getNavigator().getPassword().isEmpty()) {
            getNavigator().error("Please Enter an Email and Password, then click Sign Up for Account. This Email and password will be used for login in after verification");
        } else {
            registerUserWith();
        }
    }

    public void checkSignIn(){
        if (getNavigator().getEmail().isEmpty() || getNavigator().getPassword().isEmpty()) {
            getNavigator().error("Please Enter an Email and Password, then click Sign In for Account.");
        } else {
            loginUserWith();
        }
    }

    public void checkForgotPassword(){
        if (getNavigator().getEmail().isEmpty()) {
            getNavigator().error("Please Enter an Email.");
        } else {
            resetPassword();
        }
    }

    private void registerUserWith() {
        mAuth.createUserWithEmailAndPassword(getNavigator().getEmail(), getNavigator().getPassword())
            .addOnCompleteListener(getNavigator().getActivity(), new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if (task.isSuccessful()) {
                        // Sign in success, update UI with the signed-in user's information
                       resendVerificationEmail();
                       getNavigator().successful("Verification Email Sent");
                       task.getResult().getUser().sendEmailVerification();
                    } else {
                        // If sign in fails, display a message to the user.
                        getNavigator().error("Email verification error: "+ task.getException().getMessage());
                    }
                }
            });
    }

    private void loginUserWith() {
        mAuth.signInWithEmailAndPassword(getNavigator().getEmail(), getNavigator().getPassword())
                .addOnCompleteListener(getNavigator().getActivity(), new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            if(mAuth.getCurrentUser().isEmailVerified()) {
                                getDataManager().setLoggedIn(true);
                                getDataManager().setUUID(mAuth.getCurrentUser().getUid());
                                setDataToFirebase();
                            }else{
                                getNavigator().error("Please verify your email!");
                            }
                        } else {
                            // If sign in fails, display a message to the user.
                            getNavigator().error(task.getException().getMessage());
                        }
                    }
                });
    }

    public void resendVerificationEmail(){

        mAuth.getCurrentUser().sendEmailVerification()
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            getNavigator().successful("");
                        }
                    }
                });
    }

    public String getUID(){
     return    mAuth.getCurrentUser().getUid();
    }

    private void setDataToFirebase(){
        getDataManager().getFirestoreInstance().collection("MUser").document(getUID())
                .addSnapshotListener(new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot snapshot,
                                @Nullable FirebaseFirestoreException e) {
                if (snapshot != null && snapshot.exists()) {
                    Log.e("User Data already ", "Current data: all" + snapshot.getData());
                    Log.e("User Data already ", "Current data: all" + snapshot.getData().get("companyUuid"));

                    MUser mUser = snapshot.toObject(MUser.class);

                    boolean isFoundToken = false;
                    String token = getDataManager().getDeviceToken();

                    for(int i = 0; i < mUser.getToken().size(); i++){
                        if(token.equalsIgnoreCase(mUser.getToken().get(i))){
                            isFoundToken = true;
                            break;
                        }
                    }

                    if(!isFoundToken) {
                        mUser.getToken().add(token);
                        getDataManager().getFirestoreInstance().collection("MUser").document(getUID())
                                .set(mUser);
                    }

                    if(mUser.getUserFirstName() == null || mUser.getUserFirstName().isEmpty()){
                        if(mUser.getCompanyUuid() == null || mUser.getCompanyUuid().isEmpty()) {
                            getNavigator().goToWelcome();
                        }else{
                            getDataManager().setRegisterAdded(true);
                            getNavigator().goToRegistration();
                        }
                    }else{
                        getDataManager().setRegisterAdded(true);
                        if(mUser.getCompanyUuid() != null && !mUser.getCompanyUuid().isEmpty()) {
                            getDataManager().setCompanyAdded(true);
                            getDataManager().setCompanyID(mUser.getCompanyUuid());
                            getDataManager().setCompanyName(mUser.getCompany());
                        }

                        getDataManager().setUserDisplayName(mUser.getUserFirstName() + " " + mUser.getUserLastName());

                        getNavigator().goToMainScreen();
                    }

                } else {
                    getDataManager().getFirestoreInstance().collection("MUser").document(getUID())
                            .set(new MUser(getUID(),
                                getNavigator().getEmail(),
                                    getDataManager().getDeviceToken()))
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                getNavigator().goToWelcome();
                                Log.d("MUser", "DocumentSnapshot successfully written!");
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                getNavigator().error(e.getMessage());
                                Log.w("MUser", "Error writing document", e);
                            }
                        });
                }

            }
        });

    }
}
