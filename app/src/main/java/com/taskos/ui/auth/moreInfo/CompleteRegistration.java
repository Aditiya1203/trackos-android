package com.taskos.ui.auth.moreInfo;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.taskos.R;
import com.taskos.ViewModelProviderFactory;
import com.taskos.databinding.ActivityRegistrationBinding;
import com.taskos.ui.auth.MUser;
import com.taskos.ui.base.BaseActivity;
import com.taskos.ui.main.MainActivity;
import com.taskos.ui.main.openjobs.imageselect.ImagePickBottomSheetDialog;
import com.taskos.util.PermissionUtils;
import com.taskos.util.UsPhoneNumberFormatter;

import java.lang.ref.WeakReference;

public class CompleteRegistration extends BaseActivity<ActivityRegistrationBinding, RegisterViewModel> implements RegisterNavigator {
    private RegisterViewModel mRegisterViewModel;

    private Uri uri;

    @Override
    public int getBindingVariable() {
        return com.taskos.BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_registration;
    }

    @Override
    public RegisterViewModel getViewModel() {
        mRegisterViewModel = new ViewModelProvider(this, ViewModelProviderFactory.getInstance()).get(RegisterViewModel.class);
        return mRegisterViewModel;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mRegisterViewModel.setNavigator(this);
        init();
    }

    private void init() {
        findViewById(R.id.register).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showLoading();
                mRegisterViewModel.completeProfile();
            }
        });
        findViewById(R.id.add_profile).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (PermissionUtils.RequestMultiplePermissionCamera(getActivity())) {
                    ImagePickBottomSheetDialog.newInstance((uri, bitmap) -> {
                        CompleteRegistration.this.uri = uri;
                        Glide.with(getActivity())
                                .asBitmap()
                                .load(bitmap)
                                .centerCrop()
                                .placeholder(R.drawable.ic_gallery)
                                .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                                .error(R.drawable.ic_gallery)
                                .into(getViewDataBinding().userProfile);
                    }).show(getSupportFragmentManager(),"Select Image");
                }
            }
        });

        try {
            UsPhoneNumberFormatter addLineNumberFormatter = new UsPhoneNumberFormatter(
                    new WeakReference<EditText>(getViewDataBinding().edtPhone));
            getViewDataBinding().edtPhone.addTextChangedListener(addLineNumberFormatter);
        }catch (Exception e){}
    }

    @Override
    public void goToMainScreen() {
        hideLoading();
        startActivity(new Intent(CompleteRegistration.this, MainActivity.class));
        finish();
    }

    @Override
    public void successful(String message) {

    }

    @Override
    public void error(String message) {
        hideLoading();
        showOkAlert(message);
    }

    @Override
    public Activity getActivity() {
        return this;
    }

    @Override
    public String getFirstName() {
        return getViewDataBinding().edtFirstName.getText().toString().trim();
    }

    @Override
    public String getLastName() {
        return getViewDataBinding().edtLastName.getText().toString().trim();
    }

    @Override
    public String getCity() {
        return getViewDataBinding().edtCity.getText().toString().trim();
    }

    @Override
    public String getState() {
        return getViewDataBinding().edtState.getText().toString().trim();
    }

    @Override
    public String getPhone() {
        return getViewDataBinding().edtPhone.getText().toString().trim();
    }

    @Override
    public String getJob() {
        return getViewDataBinding().edtJob.getText().toString().trim();
    }

    @Override
    public String getStreetAddress() {
        return null;
    }

    @Override
    public String getZipCode() {
        return null;
    }

    @Override
    public Uri getURI() {
        return uri;
    }

    @Override
    public void setUserData(MUser userData) {

    }
}
