package com.taskos.ui.auth.moreInfo;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.taskos.R;
import com.taskos.ViewModelProviderFactory;
import com.taskos.databinding.ActivityEditProfileBinding;
import com.taskos.databinding.ActivityRegistrationBinding;
import com.taskos.ui.auth.MUser;
import com.taskos.ui.base.BaseActivity;
import com.taskos.ui.main.MainActivity;
import com.taskos.ui.main.openjobs.imageselect.ImagePickBottomSheetDialog;
import com.taskos.util.PermissionUtils;
import com.taskos.util.UsPhoneNumberFormatter;

import java.lang.ref.WeakReference;
import java.net.URI;

public class EditProfile extends BaseActivity<ActivityEditProfileBinding, RegisterViewModel> implements RegisterNavigator {
    private RegisterViewModel mRegisterViewModel;

    private Uri uri;

    @Override
    public int getBindingVariable() {
        return com.taskos.BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_edit_profile;
    }

    @Override
    public RegisterViewModel getViewModel() {
        mRegisterViewModel = new ViewModelProvider(this, ViewModelProviderFactory.getInstance()).get(RegisterViewModel.class);
        return mRegisterViewModel;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mRegisterViewModel.setNavigator(this);
        init();
    }

    private void init() {
        findViewById(R.id.tv_save).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showLoading();
                mRegisterViewModel.updateProfile();
            }
        });
        findViewById(R.id.tv_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        findViewById(R.id.change_profile).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (PermissionUtils.RequestMultiplePermissionCamera(getActivity())) {
                    ImagePickBottomSheetDialog.newInstance((uri, bitmap) -> {
                        EditProfile.this.uri = uri;
                        Glide.with(getActivity())
                                .asBitmap()
                                .load(bitmap)
                                .centerCrop()
                                .placeholder(R.drawable.ic_gallery)
                                .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                                .error(R.drawable.ic_gallery)
                                .into(getViewDataBinding().userProfile);
                    }).show(getSupportFragmentManager(),"Select Image");
                }
            }
        });
        mRegisterViewModel.getUserData();

        try {
            UsPhoneNumberFormatter addLineNumberFormatter = new UsPhoneNumberFormatter(
                    new WeakReference<EditText>(getViewDataBinding().edtCompanyPhone));
            getViewDataBinding().edtCompanyPhone.addTextChangedListener(addLineNumberFormatter);
        }catch (Exception e){}
    }

    @Override
    public void goToMainScreen() {
        hideLoading();
        startActivity(new Intent(EditProfile.this, MainActivity.class));
        finish();
    }

    @Override
    public void error(String message) {
        hideLoading();
        showOkAlert(message);
    }

    @Override
    public void successful(String message) {
        hideLoading();
        showOkAlert(message);
    }

    @Override
    public Activity getActivity() {
        return this;
    }

    @Override
    public String getFirstName() {
        return getViewDataBinding().edtFirstName.getText().toString().trim();
    }

    @Override
    public String getLastName() {
        return getViewDataBinding().edtLastName.getText().toString().trim();
    }

    @Override
    public String getCity() {
        return getViewDataBinding().edtCity.getText().toString().trim();
    }

    @Override
    public String getState() {
        return getViewDataBinding().edtState.getText().toString().trim();
    }

    @Override
    public String getPhone() {
        return getViewDataBinding().edtCompanyPhone.getText().toString().trim();
    }

    @Override
    public String getJob() {
        return getViewDataBinding().skills.getText().toString().trim();
    }

    @Override
    public String getStreetAddress() {
        return getViewDataBinding().edtStreetAddress.getText().toString().trim();
    }

    @Override
    public String getZipCode() {
        return getViewDataBinding().edtZipcode.getText().toString().trim();
    }

    @Override
    public Uri getURI() {
        return this.uri;
    }

    @Override
    public void setUserData(MUser userData) {
        try {
            getViewDataBinding().edtFirstName.setText(userData.getUserFirstName());
            getViewDataBinding().edtLastName.setText(userData.getUserLastName());
            getViewDataBinding().edtCity.setText(userData.getCity());
            getViewDataBinding().edtCompanyPhone.setText(userData.getPhone());
            getViewDataBinding().edtState.setText(userData.getState());
            getViewDataBinding().skills.setText(userData.getJobRole());
            getViewDataBinding().edtStreetAddress.setText(userData.getStreetAddress());
            getViewDataBinding().edtZipcode.setText(userData.getZipcode());


            if (userData.getUserProfileImageLink() != null && !userData.getUserProfileImageLink().equalsIgnoreCase("")) {
                Glide.with(getActivity())
                        .load(userData.getUserProfileImageLink())
                        .centerCrop()
                        .placeholder(R.drawable.ic_def_user)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .error(R.drawable.ic_def_user)
                        .into(getViewDataBinding().userProfile);
            }
        }catch (Exception e){e.printStackTrace();}
    }
}
