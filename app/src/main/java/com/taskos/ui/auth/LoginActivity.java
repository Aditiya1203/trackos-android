package com.taskos.ui.auth;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;

import com.taskos.BR;
import com.taskos.R;
import com.taskos.ViewModelProviderFactory;
import com.taskos.data.local.prefs.AppPreferencesHelper;
import com.taskos.ui.auth.moreInfo.CompleteRegistration;
import com.taskos.ui.base.BaseActivity;
import com.taskos.ui.main.MainActivity;
import com.taskos.databinding.ActivityLoginBinding;
import com.taskos.ui.onboard.WelcomeActivity;
import com.taskos.ui.tearm.TermsOfServices;


public class LoginActivity extends BaseActivity<ActivityLoginBinding, LoginViewModel> implements LoginNavigator, View.OnClickListener {

    private LoginViewModel mLoginViewModel;

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_login;
    }

    @Override
    public LoginViewModel getViewModel() {
        mLoginViewModel = new ViewModelProvider(this, ViewModelProviderFactory.getInstance()).get(LoginViewModel.class);
        return mLoginViewModel;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mLoginViewModel.setNavigator(this);
        init();
    }

    private void init(){
        findViewById(R.id.login).setOnClickListener(this);
        findViewById(R.id.forget_password).setOnClickListener(this);
        findViewById(R.id.signup).setOnClickListener(this);
        findViewById(R.id.term_policy).setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {
        showLoading();
        switch (view.getId()){
            case R.id.login:
                mLoginViewModel.checkSignIn();
                break;
            case R.id.forget_password:
                mLoginViewModel.checkForgotPassword();
                break;
            case R.id.signup:
                mLoginViewModel.checkSignUp();
                break;
            case R.id.term_policy:
                startActivity(new Intent(getActivity(), TermsOfServices.class));
                break;
        }
    }

    @Override
    public void successful(String message) {
        hideLoading();
        if(message!=null && !message.isEmpty() && !message.equalsIgnoreCase(""))
        showOkAlert(message);
    }

    @Override
    public void goToMainScreen() {
        hideLoading();
        Intent intent1 = new Intent(this, MainActivity.class);
        startActivity(intent1);
        finish();
    }

    @Override
    public void goToWelcome() {
        hideLoading();
        Intent intent1 = new Intent(this, WelcomeActivity.class);
        startActivity(intent1);
        finish();
    }

    @Override
    public void goToRegistration() {
        hideLoading();
        Intent intent1 = new Intent(this, CompleteRegistration.class);
        startActivity(intent1);
        finish();
    }

    @Override
    public void error(String message) {
        hideLoading();
        showOkAlert(message);
    }

    @Override
    public String getEmail() {
        return getViewDataBinding().etEmail.getText().toString().trim();
    }

    @Override
    public String getPassword() {
        return getViewDataBinding().etPassword.getText().toString().trim();
    }

    @Override
    public Activity getActivity() {
        return this;
    }
}
