

package com.taskos.ui.auth;

import android.app.Activity;

public interface LoginNavigator {

    void successful(String message);
    void goToMainScreen();
    void goToWelcome();
    void goToRegistration();
    void error(String message);
    String getEmail();
    String getPassword();
    Activity getActivity();

}
