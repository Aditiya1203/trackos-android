package com.taskos.ui.auth;

import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class MUser {

    String uuid = "";
    String userFirstName = "";
    String userLastName = "";
    String email = "";
    String phone = "";
    String streetAddress = "";
    String city  = "";
    String state  = "";
    String zipcode  = "";
    String company = "";
    String companyUuid = "";
    String jobRole = "";
    String username = "";
    String userProfileImageLink = "";
    String userSubscriptionStart = "";
    String userSubscriptionEnd = "";
    boolean userPaid = false;
    boolean userApproveCompanyAdd = false;
    boolean userCompanyAddRequested = false;
    boolean onBoarded = false;
    List<String> token = new ArrayList<String>();
    List<String> userNetwork = new ArrayList<String>();
    List<String> permissionLevel = new ArrayList<String>();

    public MUser(){
    }

    public MUser(String uuid, String email,String token){
        this.uuid = uuid;
        this.email = email;
        this.token.add(token);
    }



    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getUserFirstName() {
        return userFirstName == null ? "" : userFirstName;
    }

    public void setUserFirstName(String userFirstName) {
        this.userFirstName = userFirstName;
    }

    public String getUserLastName() {
        return userLastName == null ? "" : userLastName;
    }

    public void setUserLastName(String userLastName) {
        this.userLastName = userLastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getStreetAddress() {
        return streetAddress;
    }

    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getCompanyUuid() {
        return companyUuid;
    }

    public void setCompanyUuid(String companyUuid) {
        this.companyUuid = companyUuid;
    }

    public List<String> getPermissionLevel() {
        return permissionLevel;
    }

    public void setPermissionLevel(List<String> permissionLevel) {
        this.permissionLevel = permissionLevel;
    }

    public String getJobRole() {
        return jobRole;
    }

    public void setJobRole(String jobRole) {
        this.jobRole = jobRole;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUserProfileImageLink() {
        return userProfileImageLink;
    }

    public void setUserProfileImageLink(String userProfileImageLink) {
        this.userProfileImageLink = userProfileImageLink;
    }

    public String getUserSubscriptionStart() {
        return userSubscriptionStart;
    }

    public void setUserSubscriptionStart(String userSubscriptionStart) {
        this.userSubscriptionStart = userSubscriptionStart;
    }

    public String getUserSubscriptionEnd() {
        return userSubscriptionEnd;
    }

    public void setUserSubscriptionEnd(String userSubscriptionEnd) {
        this.userSubscriptionEnd = userSubscriptionEnd;
    }

    public boolean isUserPaid() {
        return userPaid;
    }

    public void setUserPaid(boolean userPaid) {
        this.userPaid = userPaid;
    }

    public boolean isUserApproveCompanyAdd() {
        return userApproveCompanyAdd;
    }

    public void setUserApproveCompanyAdd(boolean userApproveCompanyAdd) {
        this.userApproveCompanyAdd = userApproveCompanyAdd;
    }

    public boolean isUserCompanyAddRequested() {
        return userCompanyAddRequested;
    }

    public void setUserCompanyAddRequested(boolean userCompanyAddRequested) {
        this.userCompanyAddRequested = userCompanyAddRequested;
    }

    public boolean isOnBoarded() {
        return onBoarded;
    }

    public void setOnBoarded(boolean onBoarded) {
        this.onBoarded = onBoarded;
    }

    public List<String> getToken() {
        return token;
    }

    public void setToken(List<String> token) {
        this.token = token;
    }

    public List<String> getUserNetwork() {
        return userNetwork;
    }

    public void setUserNetwork(ArrayList<String> userNetwork) {
        this.userNetwork = userNetwork;
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        MUser mUser = (MUser) obj;
        return this.uuid.equals((Objects.requireNonNull(mUser).uuid));
    }
}

