

package com.taskos.ui.splash;

import android.app.Activity;

/**
 * Created by Hemant Sharma on 23-02-20.
 */

public interface SplashNavigator {

    void openLoginActivity();

    void openMainActivity();

    void openRegisterActivity();

    void openWelcomeActivity();

    Activity getActivity();
}
