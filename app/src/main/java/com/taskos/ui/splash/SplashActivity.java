package com.taskos.ui.splash;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;

import com.taskos.BR;
import com.taskos.R;
import com.taskos.ViewModelProviderFactory;
import com.taskos.data.local.PermissionData;
import com.taskos.databinding.ActivitySplashBinding;
import com.taskos.ui.auth.moreInfo.CompleteRegistration;
import com.taskos.ui.auth.LoginActivity;
import com.taskos.ui.base.BaseActivity;
import com.taskos.ui.main.MainActivity;
import com.taskos.ui.onboard.WelcomeActivity;


public class SplashActivity extends BaseActivity<ActivitySplashBinding, SplashViewModel> implements SplashNavigator {

    private SplashViewModel mSplashViewModel;

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_splash;
    }

    @Override
    public SplashViewModel getViewModel() {
        mSplashViewModel = new ViewModelProvider(this, ViewModelProviderFactory.getInstance()).get(SplashViewModel.class);
        return mSplashViewModel;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSplashViewModel.setNavigator(this);
        mSplashViewModel.decideNextActivity();


       for(int i = 0; i < PermissionData.kCustomerPermissionsArray.size(); i++){
           Log.e("kCustomerPermissionsArray",PermissionData.kCustomerPermissionsArray.get(i));
       }
    }

    @Override
    public void openLoginActivity() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void openMainActivity() {
        Intent intent = MainActivity.newIntent(SplashActivity.this);
        startActivity(intent);
        finish();
    }

    @Override
    public void openRegisterActivity() {
        Intent intent = new Intent(this, CompleteRegistration.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void openWelcomeActivity() {
        Intent intent = new Intent(this, WelcomeActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public Activity getActivity() {
        return this;
    }
}
