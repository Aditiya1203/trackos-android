package com.taskos.ui.splash;

import com.google.firebase.auth.FirebaseAuth;
import com.taskos.data.DataManager;
import com.taskos.ui.base.BaseViewModel;

/**
 * Created by Hemant Sharma on 23-02-20.
 */
public class SplashViewModel extends BaseViewModel<SplashNavigator> {

    private FirebaseAuth mAuth;
    public SplashViewModel(DataManager dataManager) {
        super(dataManager);
        mAuth = FirebaseAuth.getInstance();
    }

    public void decideNextActivity() {
        if (getDataManager().isLoggedIn()) {
            if (getDataManager().isRegisterAdded()) {
                getNavigator().openMainActivity();
            }else{
                getNavigator().openRegisterActivity();
          //getNavigator().openWelcomeActivity();
            }
        }else{
            getNavigator().openLoginActivity();
        }
    }
}
