package com.taskos.ui.main.network;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.taskos.R;
import com.taskos.data.model.api.SharedJob;
import com.taskos.ui.base.BaseViewHolder;
import com.taskos.ui.base.ItemClickCallback;

import java.util.List;

/**
 * Created by Hemant Sharma on 23-02-20.
 */
public class SharedJobAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    private List<SharedJob> sharedJobList;
    private ItemClickCallback itemClickCallback;

    SharedJobAdapter(List<SharedJob> sharedJobList, ItemClickCallback itemClickCallback) {
        this.sharedJobList = sharedJobList;
        this.itemClickCallback = itemClickCallback;
    }


    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(
                LayoutInflater.from(parent.getContext()).inflate(R.layout.item_common_network, parent, false));
    }


    @Override
    public int getItemCount() {
        if (sharedJobList != null && sharedJobList.size() > 0) {
            return sharedJobList.size();
        } else {
            return 0;
        }
    }

    public class ViewHolder extends BaseViewHolder {
        private TextView tv_name, tv_job;

        public ViewHolder(View itemView) {
            super(itemView);
            tv_name = itemView.findViewById(R.id.tv_name);
            tv_job = itemView.findViewById(R.id.tv_job);

            itemView.setOnClickListener(v -> {
                if (itemClickCallback != null && getAdapterPosition() != -1) {
                    itemClickCallback.onItemClick(getAdapterPosition());
                }
            });
        }

        public void onBind(int position) {
            SharedJob tmpBean = sharedJobList.get(position);
            tv_name.setText(tmpBean.getName());

            //name is different for using same layouts
            tv_job.setText(tmpBean.getCity());
        }
    }
}
