package com.taskos.ui.main.openjobs.share;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.ViewModelProvider;

import com.taskos.BR;
import com.taskos.R;
import com.taskos.ViewModelProviderFactory;
import com.taskos.data.model.api.Job;
import com.taskos.data.model.api.SharedJob;
import com.taskos.databinding.FragmentJobShareBinding;
import com.taskos.ui.auth.MUser;
import com.taskos.ui.base.BaseFragment;
import com.taskos.ui.main.network.MyNetworkAdapter;
import com.taskos.ui.main.openjobs.searchuser.SearchSharedUserBottomSheetDialog;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class JobShareFragment extends BaseFragment<FragmentJobShareBinding, JobShareViewModel> implements View.OnClickListener, JobShareNavigator {

    private JobShareViewModel jobShareViewModel;
    private String jobUUID = "";
    private Job jobBean;
    private List<MUser> userList = new ArrayList<>();

    public static JobShareFragment newInstance(String jobUUID) {

        Bundle args = new Bundle();

        JobShareFragment fragment = new JobShareFragment();
        args.putString("JobKey", jobUUID);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_job_share;
    }

    @Override
    public JobShareViewModel getViewModel() {
        jobShareViewModel = new ViewModelProvider(this, ViewModelProviderFactory.getInstance()).get(JobShareViewModel.class);
        return jobShareViewModel;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            jobUUID = getArguments().getString("JobKey");
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        jobShareViewModel.setNavigator(this);
        TextView tvTitle = view.findViewById(R.id.tvTitle);
        setOnClickListener(tvTitle
                , view.findViewById(R.id.img_back)
                , getViewDataBinding().rlShare, getViewDataBinding().llMain, getViewDataBinding().tvAddUser);

        jobShareViewModel.getJobDetail(jobUUID).observe(getViewLifecycleOwner(), jobDetail -> {
            jobBean = jobDetail;
            tvTitle.setText(jobDetail.getName());

            if (jobDetail.isShared()) {
                onShowShareUsers();
            }
        });

        jobShareViewModel.getShareJobUsers(jobUUID).observe(getViewLifecycleOwner(), mUserList -> {
            userList.clear();
            userList.addAll(mUserList);

            MyNetworkAdapter networkAdapter = new MyNetworkAdapter(mUserList, MyNetworkAdapter.VIEW_TYPE_NETWORK);
            getViewDataBinding().recyclerView.setAdapter(networkAdapter);
        });
    }

    private void setOnClickListener(View... views) {
        for (View view : views) {
            view.setOnClickListener(this);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tvTitle:
            case R.id.img_back:
                getBaseActivity().onBackPressed();
                break;

            case R.id.rl_share:
                String value = getViewDataBinding().tvMsg.getText().toString();
                if (value.equals(getString(R.string.share_job))) {
                    getViewDataBinding().imgVerify.setColorFilter(ContextCompat.getColor(getBaseActivity(),
                            R.color.green), android.graphics.PorterDuff.Mode.SRC_IN);
                    getViewDataBinding().tvMsg.setText(R.string.job_is_share);
                    getViewDataBinding().tvAddUser.setText(R.string.add_shared_user);
                    getViewDataBinding().tvShareWith.setVisibility(View.VISIBLE);
                } else {
                    if (isNetworkConnected()) {
                        AlertDialog alertDialog = alertDialogInstance(getString(R.string.unshare_title), getString(R.string.unshare_body));
                        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, getString(R.string.cancel), (dialog, which) -> dialog.dismiss());
                        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.unshare), (dialog, which) -> {
                            dialog.dismiss();
                            jobShareViewModel.doUnshareJob(jobUUID);
                            getViewDataBinding().imgVerify.setColorFilter(ContextCompat.getColor(getBaseActivity(),
                                    R.color.gray), android.graphics.PorterDuff.Mode.SRC_IN);
                            getViewDataBinding().tvMsg.setText(R.string.share_job);
                            getViewDataBinding().tvAddUser.setText("");
                            getViewDataBinding().tvShareWith.setVisibility(View.GONE);
                        });
                        alertDialog.show();
                        alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.light_blue));
                        alertDialog.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorPrimary));
                    } else {
                        showToast(getString(R.string.noInternet));
                    }
                }
                break;

            case R.id.tv_add_user:
                if (getViewDataBinding().tvAddUser.getText().toString().equals(getString(R.string.add_shared_user))) {
                    getViewDataBinding().tvAddUser.setClickable(false);
                    SearchSharedUserBottomSheetDialog.newInstance(mUser -> {
                        if (isNetworkConnected()) {
                            if (jobBean != null) {
                                if (!userList.contains(mUser)) {
                                    addSharedUser(mUser);
                                } else {
                                    getViewDataBinding().tvAddUser.setClickable(true);
                                }
                            } else showToast(getString(R.string.something_went_wrong));
                        } else {
                            showToast(getString(R.string.noInternet));
                        }
                    }).show(getChildFragmentManager());
                }
                break;
        }
    }

    private void addSharedUser(MUser mUser) {
        SharedJob sharedJob = new SharedJob();
        sharedJob.setUuid(UUID.randomUUID().toString());
        sharedJob.setJobUuid(jobBean.getUuid());
        sharedJob.setName(jobBean.getName());
        sharedJob.setStreetAddress(jobBean.getStreetAddress());
        sharedJob.setCity(jobBean.getCity());
        sharedJob.setState(jobBean.getState());
        sharedJob.setZipcode(jobBean.getZipcode());
        sharedJob.setSoldItem(jobBean.getSoldItem());
        sharedJob.setInstallStart(jobBean.getInstallStart());
        sharedJob.setInstallEnd(jobBean.getInstallEnd());
        sharedJob.setAssignedEmployees(jobBean.getAssignedEmployees());
        sharedJob.setScheduledStatus(jobBean.getScheduledStatus());
        sharedJob.setOwner(jobBean.getOwner());
        sharedJob.setSubscribers(jobBean.getSubscribers());
        sharedJob.setSharedUser(mUser.getUuid());

        jobBean.setShared(true);
        jobShareViewModel.doShareJob(sharedJob, jobBean, userList);
    }

    @Override
    public void onShareSuccess() {
        getViewDataBinding().tvAddUser.setClickable(true);
    }

    @Override
    public void onSaveError() {
        getViewDataBinding().tvAddUser.setClickable(true);
    }

    @Override
    public void onShowShareUsers() {
        getViewDataBinding().imgVerify.setColorFilter(ContextCompat.getColor(getBaseActivity(),
                R.color.green), android.graphics.PorterDuff.Mode.SRC_IN);
        getViewDataBinding().tvMsg.setText(R.string.job_is_share);
        getViewDataBinding().tvAddUser.setText(R.string.add_shared_user);
        getViewDataBinding().tvShareWith.setVisibility(View.VISIBLE);
    }

    @Override
    public void onUnShareUsers() {
        userList.clear();
        getViewDataBinding().tvAddUser.setClickable(true);
    }
}

