package com.taskos.ui.main.employee;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.taskos.R;
import com.taskos.ui.auth.MUser;
import com.taskos.ui.base.BaseViewHolder;
import com.taskos.ui.base.ItemClickCallback;

import java.util.List;

/**
 * Created by Hemant Sharma on 23-02-20.
 */
public class EmployeeAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    private ItemClickCallback callback;
    private List<MUser> mUserList;
    private int lastPos = 0;

    EmployeeAdapter(List<MUser> keyValueList, ItemClickCallback callback) {
        this.mUserList = keyValueList;
        this.callback = callback;
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(
                LayoutInflater.from(parent.getContext()).inflate(R.layout.item_employee, parent, false));
    }

    @Override
    public int getItemCount() {
        if (mUserList != null && mUserList.size() > 0) {
            return mUserList.size();
        } else {
            return 0;
        }
    }


    public class ViewHolder extends BaseViewHolder implements View.OnClickListener {
        private TextView tv_name;
        private RadioButton radio_button;

        public ViewHolder(View itemView) {
            super(itemView);
            tv_name = itemView.findViewById(R.id.tv_name);
            radio_button = itemView.findViewById(R.id.radio_button);

            radio_button.setOnClickListener(this);
            itemView.setOnClickListener(this);
        }

        public void onBind(int position) {
            MUser tmpBean = mUserList.get(position);
            tv_name.setText(tmpBean.getUserFirstName().concat(" ").concat(tmpBean.getUserLastName()));
            radio_button.setVisibility(View.GONE);
            //radio_button.setChecked(tmpBean.isSelected);
        }

        @Override
        public void onClick(View view) {
            int tempPos = getAdapterPosition();
            if (tempPos != -1 && callback != null) {
                /*mUserList.get(lastPos).isSelected = false;
                notifyItemChanged(lastPos);*/

                callback.onItemClick(getAdapterPosition());
                /*mUserList.get(tempPos).isSelected = true;
                notifyItemChanged(tempPos);

                lastPos = tempPos;*/
            }
        }
    }

}
