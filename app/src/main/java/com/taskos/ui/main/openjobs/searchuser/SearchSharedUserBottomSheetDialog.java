package com.taskos.ui.main.openjobs.searchuser;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProvider;

import com.taskos.R;
import com.taskos.ViewModelProviderFactory;
import com.taskos.databinding.DialogSearchSharedUserBinding;
import com.taskos.ui.auth.MUser;
import com.taskos.ui.base.BaseBottomSheetDialog;
import com.taskos.ui.base.BaseNavigator;
import com.taskos.ui.employee.EmployeeDetailsActivity;
import com.taskos.ui.main.network.MyNetworkAdapter;

import java.util.ArrayList;
import java.util.List;


public class SearchSharedUserBottomSheetDialog extends BaseBottomSheetDialog<DialogSearchSharedUserBinding, SearchSharedUserViewModel> implements View.OnClickListener, BaseNavigator {

    private static final String TAG = "SearchSharedUserBottomS";
    private SearchSharedUserViewModel searchSharedUserViewModel;
    private List<MUser> mUserList;
    private MyNetworkAdapter networkAdapter;
    private SearchNetworkCallback searchNetworkCallback;

    public static SearchSharedUserBottomSheetDialog newInstance(SearchNetworkCallback searchNetworkCallback) {

        Bundle args = new Bundle();

        SearchSharedUserBottomSheetDialog fragment = new SearchSharedUserBottomSheetDialog();
        fragment.setCallback(searchNetworkCallback);
        fragment.setArguments(args);
        return fragment;
    }

    private void setCallback(SearchNetworkCallback searchNetworkCallback) {
        this.searchNetworkCallback = searchNetworkCallback;
    }

    @Override
    public int getBindingVariable() {
        return com.taskos.BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.dialog_search_shared_user;
    }

    @Override
    public SearchSharedUserViewModel getViewModel() {
        searchSharedUserViewModel = new ViewModelProvider(this, ViewModelProviderFactory.getInstance()).get(SearchSharedUserViewModel.class);
        return searchSharedUserViewModel;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        searchSharedUserViewModel.setNavigator(this);
        getViewDataBinding().tvCancel.setOnClickListener(this);
        getViewDataBinding().tvSearch.setOnClickListener(this);
        addTextWatcher();

        mUserList = new ArrayList<>();
        initAdapter();
    }

    private void initAdapter() {
        networkAdapter = new MyNetworkAdapter(mUserList, MyNetworkAdapter.VIEW_TYPE_NETWORK, pos -> {
            if (searchNetworkCallback != null) {
                dismissDialog(TAG);
                searchNetworkCallback.onUserClick(mUserList.get(pos));
            }
        });
        getViewDataBinding().recyclerView.setAdapter(networkAdapter);
    }

    private void addTextWatcher() {
        getViewDataBinding().etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().isEmpty()) {
                    getViewDataBinding().tvSearch.setAlpha(0.5f);
                    getViewDataBinding().tvSearch.setClickable(false);
                } else {
                    getViewDataBinding().tvSearch.setAlpha(1);
                    getViewDataBinding().tvSearch.setClickable(true);
                }
            }
        });
    }

    public void show(FragmentManager fragmentManager) {
        super.show(fragmentManager, TAG);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_cancel:
                dismissDialog(TAG);
                break;

            case R.id.tv_search:
                if (!getViewDataBinding().etSearch.getText().toString().isEmpty()) {
                    searchSharedUserViewModel.getUserList(getViewDataBinding().etSearch.getText().toString()).observe(getViewLifecycleOwner(), myUserList -> {
                                mUserList.clear();
                                mUserList.addAll(myUserList);
                                if (networkAdapter == null) initAdapter();
                                else networkAdapter.notifyDataSetChanged();
                            }
                    );
                }
                break;
        }
    }

    public interface SearchNetworkCallback {
        void onUserClick(MUser mUser);
    }
}
