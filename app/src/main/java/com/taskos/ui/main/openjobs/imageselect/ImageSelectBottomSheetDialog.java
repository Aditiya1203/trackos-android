package com.taskos.ui.main.openjobs.imageselect;

import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProvider;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.taskos.R;
import com.taskos.ViewModelProviderFactory;
import com.taskos.data.model.api.Job;
import com.taskos.databinding.DialogImageSelectBinding;
import com.taskos.ui.auth.MUser;
import com.taskos.ui.base.BaseBottomSheetDialog;
import com.taskos.util.PermissionUtils;


public class ImageSelectBottomSheetDialog extends BaseBottomSheetDialog<DialogImageSelectBinding, ImageSelectViewModel> implements View.OnClickListener, ImageSelectNavigator {

    private static final String TAG = "ImageSelectBottomSheetD";
    private ImageSelectViewModel imageSelectViewModel;
    private Uri uri;
    private String jobUUID = "";
    private Job jobDetail;
    private MUser mUser;

    public static ImageSelectBottomSheetDialog newInstance(String jobUUID) {

        Bundle args = new Bundle();

        ImageSelectBottomSheetDialog fragment = new ImageSelectBottomSheetDialog();
        args.putString("JobKey", jobUUID);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            jobUUID = getArguments().getString("JobKey");
        }
    }

    @Override
    public int getBindingVariable() {
        return com.taskos.BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.dialog_image_select;
    }

    @Override
    public ImageSelectViewModel getViewModel() {
        imageSelectViewModel = new ViewModelProvider(this, ViewModelProviderFactory.getInstance()).get(ImageSelectViewModel.class);
        return imageSelectViewModel;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        imageSelectViewModel.setNavigator(this);
        getViewDataBinding().tvCancel.setOnClickListener(this);
        getViewDataBinding().tvSave.setOnClickListener(this);
        getViewDataBinding().tvSelect.setOnClickListener(this);

        imageSelectViewModel.getJobDetail(jobUUID).observe(getViewLifecycleOwner(), job -> {
            jobDetail = job;

            imageSelectViewModel.getUserInfo(jobDetail.getSalesUuid()).observe(getViewLifecycleOwner(), mUser -> {
                this.mUser = mUser;
            });
        });
    }


    public void show(FragmentManager fragmentManager) {
        super.show(fragmentManager, TAG);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_cancel:
                dismissDialog(TAG);
                break;

            case R.id.tv_select:
                if (PermissionUtils.RequestMultiplePermissionCamera(getBaseActivity())) {
                    ImagePickBottomSheetDialog.newInstance((uri, bitmap) -> {
                        this.uri = uri;
                        getViewDataBinding().imgShow.setVisibility(View.VISIBLE);
                        Glide.with(getBaseActivity())
                                .asBitmap()
                                .load(bitmap)
                                .centerCrop()
                                .placeholder(R.drawable.ic_gallery)
                                .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                                .error(R.drawable.ic_gallery)
                                .into(getViewDataBinding().imgShow);
                    }).show(getChildFragmentManager(), TAG);
                }
                break;

            case R.id.tv_save:
                if (isNetworkConnected()) {
                    if (verifyInputs()) {
                        hideKeyboard();
                        getViewDataBinding().tvSave.setClickable(false);
                        showLoading();
                        String name = getViewDataBinding().etName.getText().toString().concat(".jpg");
                        imageSelectViewModel.doUploadImage(jobDetail, name, uri, mUser.getToken());
                    }
                } else {
                    showToast(getString(R.string.noInternet));
                }
                break;
        }
    }

    private boolean verifyInputs() {
        if (getViewDataBinding().etName.getText().toString().isEmpty()) {
            showToast(getString(R.string.alert_name_empty));
            return false;
        } else if (uri == null) {
            showToast(getString(R.string.alert_image_select));
            return false;
        } else return true;
    }

    @Override
    public void onUploadSuccess() {
        hideLoading();
        dismissDialog(TAG);
        getViewDataBinding().tvSave.setClickable(true);
        showToast(getString(R.string.image_upload_success));
    }

    @Override
    public void onUploadFail() {
        hideLoading();
        getViewDataBinding().tvSave.setClickable(true);
        showToast(getString(R.string.something_went_wrong_image));
    }
}
