package com.taskos.ui.main.network;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;

import com.taskos.BR;
import com.taskos.R;
import com.taskos.ViewModelProviderFactory;
import com.taskos.data.model.api.SharedJob;
import com.taskos.databinding.FragmentMyNetworkBinding;
import com.taskos.ui.auth.MUser;
import com.taskos.ui.base.BaseFragment;
import com.taskos.ui.base.ItemClickCallback;
import com.taskos.ui.employee.EmployeeDetailsActivity;
import com.taskos.ui.employee.UserDetailsActivity;
import com.taskos.ui.fcm.FcmNotificationBuilder;
import com.taskos.ui.main.network.search.SearchToNetworkFragment;
import com.taskos.ui.main.openjobs.detail.JobDetailFragment;

import java.util.ArrayList;
import java.util.List;


public class MyNetworkFragment extends BaseFragment<FragmentMyNetworkBinding, MyNetworkViewModel> implements View.OnClickListener, MyNetworkNavigator {

    private MyNetworkViewModel myNetworkViewModel;
    private int currentPos = 0;
    private List<MUser> mUserList;
    private List<SharedJob> mSharedJobList;
    private boolean isNotification = false;

    public static MyNetworkFragment newInstance(Bundle bundle) {

        if (bundle == null) {
            bundle = new Bundle();
        }

        MyNetworkFragment fragment = new MyNetworkFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_my_network;
    }

    @Override
    public MyNetworkViewModel getViewModel() {
        myNetworkViewModel = new ViewModelProvider(this, ViewModelProviderFactory.getInstance()).get(MyNetworkViewModel.class);
        return myNetworkViewModel;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            String view = getArguments().getString("tabView");
            switch (view == null ? "" : view) {
                case FcmNotificationBuilder.TAB_COWORKERS:
                    currentPos = 1;
                    break;

                case FcmNotificationBuilder.TAB_SHARED_JOB:
                    currentPos = 2;
                    break;

                case FcmNotificationBuilder.TAB_MY_NETWORK:
                default:
                    currentPos = 0;
                    break;

            }
            String uuid = getArguments().getString("selectedUuid");
            if (uuid != null) {
                myNetworkViewModel.getSharedJob(uuid).observe(this, sharedJob -> navigateToDetail(sharedJob.getJobUuid()));
            }
        }
    }

    private void navigateToDetail(String uuid) {
        getBaseActivity().addFragment(JobDetailFragment.newInstance(0, uuid), R.id.nav_host_fragment, true);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        myNetworkViewModel.setNavigator(this);
        getViewDataBinding().imgSearch.setOnClickListener(this);
        getViewDataBinding().tvSearch.setOnClickListener(this);
        getViewDataBinding().tvJoin.setOnClickListener(this);
        getViewDataBinding().tvAddNetwork.setOnClickListener(this);

        getViewDataBinding().segmentedButtonGroup.setPosition(currentPos);
        filterData("");
        getViewDataBinding().segmentedButtonGroup.setOnClickedButtonListener(position -> {
            currentPos = position;
            filterData("");
        });

        addTextWatcher();
    }

    private void filterData(String search) {
        if (currentPos == 0) {
            doGetNetworkData(0, search);
        } else if (currentPos == 1) {
            doGetNetworkData(1, search);
        } else {
            doGetNetworkData(2, search);
        }
    }

    private void addTextWatcher() {
        getViewDataBinding().etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().isEmpty()) {
                    getViewDataBinding().tvSearch.setAlpha(0.5f);
                    getViewDataBinding().tvSearch.setClickable(false);
                    getViewDataBinding().tvJoin.setVisibility(View.GONE);
                    filterData("");
                } else {
                    getViewDataBinding().tvSearch.setAlpha(1);
                    getViewDataBinding().tvSearch.setClickable(true);
                    getViewDataBinding().tvJoin.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_search:
                if (getViewDataBinding().rlSearch.getVisibility() == View.GONE) {
                    getViewDataBinding().rlSearch.setVisibility(View.VISIBLE);
                } else {
                    getViewDataBinding().tvJoin.setVisibility(View.GONE);
                    getViewDataBinding().rlSearch.setVisibility(View.GONE);
                }
                break;

            case R.id.tv_search:
                if (!getViewDataBinding().etSearch.getText().toString().isEmpty())
                    filterData(getViewDataBinding().etSearch.getText().toString());
                break;

            case R.id.tv_join:
                break;

            case R.id.tv_add_network:
                getBaseActivity().addFragment(SearchToNetworkFragment.newInstance(), R.id.nav_host_fragment, true);
                break;
        }
    }


    private void doGetNetworkData(int currentPos, String search) {
        if (search.isEmpty()) {
            if (currentPos == 0) {
                myNetworkViewModel.getMyNetworkList().observe(getViewLifecycleOwner(), myNetworkList -> {
                            mUserList = new ArrayList<>(myNetworkList);
                            MyNetworkAdapter networkAdapter = new MyNetworkAdapter(mUserList, MyNetworkAdapter.VIEW_TYPE_NETWORK,itemClickUser);
                            getViewDataBinding().recyclerViewNetwork.setAdapter(networkAdapter);
                        }
                );
            } else if (currentPos == 1) {
                myNetworkViewModel.getMyCoworkerList().observe(getViewLifecycleOwner(), myNetworkList -> {
                            mUserList = new ArrayList<>(myNetworkList);
                            MyNetworkAdapter networkAdapter = new MyNetworkAdapter(mUserList, MyNetworkAdapter.VIEW_TYPE_COMMON,itemClick);
                            getViewDataBinding().recyclerViewNetwork.setAdapter(networkAdapter);
                        }
                );
            } else {
                myNetworkViewModel.getSharedJobList().observe(getViewLifecycleOwner(), shareList -> {
                            mSharedJobList = new ArrayList<>(shareList);
                    SharedJobAdapter sharedJobAdapter = new SharedJobAdapter(mSharedJobList, pos ->
                            navigateToDetail(mSharedJobList.get(pos).getJobUuid()));
                            getViewDataBinding().recyclerViewNetwork.setAdapter(sharedJobAdapter);
                        }
                );
            }
        } else {
            List<MUser> mSearchUserList;
            if (currentPos == 0) {
                mSearchUserList = new ArrayList<>();
                for (MUser tmpBean : mUserList) {
                    String name = tmpBean.getUserFirstName().concat(" ").concat(tmpBean.getUserLastName()).toLowerCase();
                    if (name.contains(search.toLowerCase())) {
                        mSearchUserList.add(tmpBean);
                    }
                }
                MyNetworkAdapter networkAdapter = new MyNetworkAdapter(mSearchUserList, MyNetworkAdapter.VIEW_TYPE_NETWORK,itemClickUser );
                getViewDataBinding().recyclerViewNetwork.setAdapter(networkAdapter);
            } else if (currentPos == 1) {
                mSearchUserList = new ArrayList<>();
                for (MUser tmpBean : mUserList) {
                    String name = tmpBean.getUserFirstName().concat(" ").concat(tmpBean.getUserLastName()).toLowerCase();
                    if (name.contains(search.toLowerCase())) {
                        mSearchUserList.add(tmpBean);
                    }
                }
                MyNetworkAdapter networkAdapter = new MyNetworkAdapter(mSearchUserList, MyNetworkAdapter.VIEW_TYPE_COMMON, itemClick);
                getViewDataBinding().recyclerViewNetwork.setAdapter(networkAdapter);
            } else {
                List<SharedJob> mSearchSharedJobList = new ArrayList<>();
                for (SharedJob tmpBean : mSharedJobList) {
                    String name = tmpBean.getName().toLowerCase();
                    if (name.contains(search.toLowerCase())) {
                        mSearchSharedJobList.add(tmpBean);
                    }
                }
                SharedJobAdapter sharedJobAdapter = new SharedJobAdapter(mSearchSharedJobList, pos -> navigateToDetail(mSharedJobList.get(pos).getJobUuid()));
                getViewDataBinding().recyclerViewNetwork.setAdapter(sharedJobAdapter);
            }
        }
    }

    ItemClickCallback itemClick = new ItemClickCallback() {
        @Override
        public void onItemClick(int pos) {
            Intent intent = new Intent(getContext(), EmployeeDetailsActivity.class);
            intent.putExtra("UUID", mUserList.get(pos).getUuid());
            startActivity(intent);
        }
    };
    ItemClickCallback itemClickUser = new ItemClickCallback() {
        @Override
        public void onItemClick(int pos) {
            Intent intent = new Intent(getContext(), UserDetailsActivity.class);
            intent.putExtra("UUID", mUserList.get(pos).getUuid());
            startActivity(intent);
        }
    };
}