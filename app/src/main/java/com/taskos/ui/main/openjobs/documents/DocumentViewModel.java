package com.taskos.ui.main.openjobs.documents;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.taskos.data.DataManager;
import com.taskos.data.model.api.DocumentFolder;
import com.taskos.data.model.api.Job;
import com.taskos.ui.base.BaseNavigator;
import com.taskos.ui.base.BaseViewModel;

import java.util.ArrayList;
import java.util.List;

public class DocumentViewModel extends BaseViewModel<BaseNavigator> {
    private static final String TAG = "DocumentViewModel";
    private MutableLiveData<Job> jobMutableLiveData = new MutableLiveData<>();
    private List<DocumentFolder> folderList = new ArrayList<>();
    private MutableLiveData<List<DocumentFolder>> docFolderMutableLiveData = new MutableLiveData<>();

    public DocumentViewModel(DataManager dataManager) {
        super(dataManager);
    }

    MutableLiveData<Job> getJobDetail(String uuid) {
        DocumentReference query = getDataManager().getFirestoreInstance().collection("Job").document(uuid);

        query.addSnapshotListener((documentSnapshot, e) -> {
            if (e != null) {
                Log.e(TAG, "onEvent: Listen failed.", e);
                return;
            }

            if (documentSnapshot != null) {
                Job job = documentSnapshot.toObject(Job.class);
                if (job != null) {
                    jobMutableLiveData.setValue(job);
                }
            }
        });

        return jobMutableLiveData;
    }

    MutableLiveData<List<DocumentFolder>> getFolders(String jobUUID) {
        folderList.clear();
        Query query = getDataManager().getFirestoreInstance().collection("DocumentFolder")
                .whereEqualTo("jobUuid", jobUUID);

        query.addSnapshotListener((queryDocumentSnapshots, e) -> {
            if (e != null) {
                Log.e(TAG, "onEvent: Listen failed.", e);
                return;
            }

            if (queryDocumentSnapshots != null) {
                folderList.clear();
                for (QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                    DocumentFolder documentFolder = documentSnapshot.toObject(DocumentFolder.class);
                    folderList.add(documentFolder);
                }
                docFolderMutableLiveData.setValue(folderList);
            }
        });

        return docFolderMutableLiveData;
    }
}