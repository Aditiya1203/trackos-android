package com.taskos.ui.main.openjobs.financial;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.taskos.R;
import com.taskos.data.model.other.FinanceExpense;
import com.taskos.data.model.other.FinancePayments;
import com.taskos.ui.base.BaseViewHolder;
import com.taskos.ui.base.ItemClickCallback;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Hemant Sharma on 23-02-20.
 */
public class FinancialAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    public static int VIEW_TYPE_EXPENSE = 1;
    public static int VIEW_TYPE_PAYMENT = 2;
    private ItemClickCallback callback;
    private List<FinancePayments> paymentList;
    private List<FinanceExpense> expenseList;
    private int viewType;
    private boolean isRemove = false;

    public FinancialAdapter(List<FinancePayments> paymentList, ItemClickCallback callback) {
        this.viewType = VIEW_TYPE_PAYMENT;
        this.paymentList = paymentList;
        this.callback = callback;
    }

    public FinancialAdapter(ArrayList<FinanceExpense> expenseList, ItemClickCallback callback) {
        this.expenseList = expenseList;
        this.callback = callback;
    }

    public void setRemove(boolean remove) {
        isRemove = remove;
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_PAYMENT) {
            return new ViewHolder(
                    LayoutInflater.from(parent.getContext()).inflate(R.layout.item_payment, parent, false));
        } else {
            return new ViewHolder(
                    LayoutInflater.from(parent.getContext()).inflate(R.layout.item_expense, parent, false));
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (viewType == VIEW_TYPE_PAYMENT) {
            return VIEW_TYPE_PAYMENT;
        } else {
            return VIEW_TYPE_EXPENSE;
        }
    }

    @Override
    public int getItemCount() {
        if (paymentList != null && paymentList.size() > 0) {
            return paymentList.size();
        } else if (expenseList != null && expenseList.size() > 0) {
            return expenseList.size();
        } else {
            return 0;
        }
    }

    public class ViewHolder extends BaseViewHolder {
        private TextView tv_name, tv_amount;
        private FrameLayout frame_remove;

        public ViewHolder(View itemView) {
            super(itemView);
            tv_name = itemView.findViewById(R.id.tv_name);
            tv_amount = itemView.findViewById(R.id.tv_amount);
            frame_remove = itemView.findViewById(R.id.frame_remove);

            frame_remove.setOnClickListener(v -> {
                if (callback != null && getAdapterPosition() != -1) {
                    callback.onItemClick(getAdapterPosition());
                }
            });
        }

        public void onBind(int position) {

            frame_remove.setVisibility(isRemove ? View.VISIBLE : View.GONE);
            if (viewType == VIEW_TYPE_PAYMENT) {
                FinancePayments financePayments = paymentList.get(position);
                tv_amount.setText(String.valueOf(financePayments.getPaymentAmounts()));
            } else {
                FinanceExpense financeExpense = expenseList.get(position);
                tv_name.setText(financeExpense.getExpenseItem());
                tv_amount.setText(String.valueOf(financeExpense.getExpenseAmount()));
            }
        }
    }
}
