package com.taskos.ui.main.myleads;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.taskos.R;
import com.taskos.data.model.api.Customer;
import com.taskos.data.model.api.ToDo;
import com.taskos.ui.main.client.addclient.AddClientBottomSheetDialog;
import com.taskos.ui.main.estimate.EstimateBottomSheetDialog;
import com.taskos.ui.main.invoice.InvoiceBottomSheetDialog;
import com.taskos.ui.main.openjobs.addjob.AddJobBottomSheetDialog;
import com.taskos.ui.main.tasks.addtask.AddTaskBottomSheetDialog;

import java.util.List;


public class MyLeadsfragment extends Fragment {

    TextView tv_no_openLeads;
    TextView tv_open_leads;
    TextView tv_unscheduled;
    TextView tv_scheduled;
    TextView tv_percentdue;
    TextView tv_toal_task;
    TextView tv_duetask;
    TextView tv_open_task;
    TextView tv_complete_task;
    ProgressBar progressBar;
    int openleads = 0;
    int scheduled = 0;
    int unscheduled = 0;

    int totaltask = 0;
    int duetask = 0;
    int opentask = 0;
    int completetask = 0;

    FloatingActionMenu menu_float;
    FloatingActionButton fab_add_client;
    FloatingActionButton fab_add_estimate;
    FloatingActionButton fab_add_invoice;
    FloatingActionButton fab_add_job;
    FloatingActionButton fab_add_service;
    FloatingActionButton fab_add_task;

    public static MyLeadsfragment newInstance(Bundle bundle) {

        if (bundle == null) {
            bundle = new Bundle();
        }
        MyLeadsfragment fragment = new MyLeadsfragment();
        fragment.setArguments(bundle);
        return fragment;

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.myleads_fragment, container, false);
        tv_no_openLeads = v.findViewById(R.id.tv_no_openLeads);
        tv_open_leads = v.findViewById(R.id.tv_open_leads);
        tv_unscheduled = v.findViewById(R.id.tv_unscheduled);
        tv_scheduled = v.findViewById(R.id.tv_scheduled);

        menu_float = v.findViewById(R.id.menu_float);
        fab_add_client = v.findViewById(R.id.fab_add_client);
        fab_add_estimate = v.findViewById(R.id.fab_add_estimate);
        fab_add_invoice = v.findViewById(R.id.fab_add_invoice);
        fab_add_job = v.findViewById(R.id.fab_add_job);
        fab_add_service = v.findViewById(R.id.fab_add_service);
        fab_add_task = v.findViewById(R.id.fab_add_task);

        progressBar = v.findViewById(R.id.progressBar);
        tv_percentdue = v.findViewById(R.id.tv_percentdue);
        tv_toal_task = v.findViewById(R.id.tv_toal_task);
        tv_duetask = v.findViewById(R.id.tv_duetask);
        tv_open_task = v.findViewById(R.id.tv_open_task);
        tv_complete_task = v.findViewById(R.id.tv_complete_task);

        FirebaseFirestore db = FirebaseFirestore.getInstance();

        db.collection("Customers")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            if (task.getResult() != null) {
                                List<Customer> leedsList = task.getResult().toObjects(Customer.class);
                                for (Customer leeds : leedsList) {
                                    Log.e("", leeds.getCustomerStatus() + "");

                                    if (leeds.getCustomerStatus().equals("Open Lead")) {
                                        openleads = openleads + 1;
                                        Log.e("Openleads",openleads+"");
                                        tv_open_leads.setText("All Open Leads: " + openleads + "");
                                        tv_no_openLeads.setText(openleads + " Open Leads");
                                    } else if (leeds.getCustomerStatus().equals("Appointment Set")) {
                                        scheduled = scheduled + 1;
                                        tv_scheduled.setText("Scheduled: " + scheduled + "");
                                    } else {
                                        unscheduled = unscheduled + 1;
                                        tv_unscheduled.setText("UnScheduled: " + unscheduled + "");
                                    }

                                    if(openleads==0){
                                        tv_no_openLeads.setText(openleads + "No Open Leads");
                                    }
                                }
                            }
                        } else {
                            Log.w("TAG", "Error getting documents.", task.getException());
                        }
                    }
                });

    FirebaseFirestore dbtask = FirebaseFirestore.getInstance();

        dbtask.collection("ToDo")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            if (task.getResult() != null) {
                                List<ToDo> tasklist = task.getResult().toObjects(ToDo.class);
                                for (ToDo tasks : tasklist) {
                                    totaltask=totaltask+1;
                                    Log.e("", tasks.getCompletionStatus() + "");
                                    if(tasks.getCompletionStatus().equals("Past Due")){
                                        duetask=duetask+1;
                                    } else if(tasks.getCompletionStatus().equals("Open")){
                                        opentask=opentask+1;
                                    }else if(tasks.getCompletionStatus().equals("Complete")){
                                        completetask=completetask+1;
                                    }else if(tasks.getCompletionStatus().equals("Completed")){
                                        completetask=completetask+1;
                                    }
                                    double precent=(duetask*100)/totaltask;
                                    int percent=(duetask*100)/totaltask;
                                    progressBar.setProgress(percent);
                                    tv_percentdue.setText(precent+"%");
                                    tv_toal_task.setText("Tasks: "+totaltask);
                                    tv_duetask.setText("Overdue: "+duetask);
                                    tv_complete_task.setText("Complete Task: "+completetask);
                                    tv_open_task.setText("Open Task: "+opentask);
                                }
                            }
                        } else {
                            Log.w("TAG", "Error getting documents.", task.getException());
                        }
                    }
                });

        fab_add_client.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddClientBottomSheetDialog.newInstance().show(getChildFragmentManager());
                menu_float.close(true);
            }
        });

        fab_add_estimate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EstimateBottomSheetDialog.newInstance().show(getChildFragmentManager());
                menu_float.close(true);
            }
        });

        fab_add_invoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InvoiceBottomSheetDialog.newInstance().show(getChildFragmentManager());
                menu_float.close(true);
            }
        });

        fab_add_job.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddJobBottomSheetDialog.newInstance().show(getChildFragmentManager());
                menu_float.close(true);
            }
        });

        fab_add_service.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                menu_float.close(true);
            }
        });

        fab_add_task.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddTaskBottomSheetDialog.newInstance().show(getChildFragmentManager());
                menu_float.close(true);
            }
        });

        return v;
    }


}
