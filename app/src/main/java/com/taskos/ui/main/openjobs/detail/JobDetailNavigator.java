package com.taskos.ui.main.openjobs.detail;


/**
 * Created by Hemant Sharma on 23-02-20.
 */
public interface JobDetailNavigator {
    void onJobScheduleSuccess();
}
