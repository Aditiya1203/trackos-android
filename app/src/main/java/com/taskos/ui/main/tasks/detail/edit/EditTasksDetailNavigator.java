package com.taskos.ui.main.tasks.detail.edit;


public interface EditTasksDetailNavigator {
    void openDashboard();

    void onSaveError();
}
