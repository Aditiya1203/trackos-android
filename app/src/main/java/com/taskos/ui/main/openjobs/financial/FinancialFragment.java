package com.taskos.ui.main.openjobs.financial;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;

import com.taskos.BR;
import com.taskos.R;
import com.taskos.ViewModelProviderFactory;
import com.taskos.data.model.api.Job;
import com.taskos.data.model.api.JobFinancial;
import com.taskos.data.model.other.FinanceExpense;
import com.taskos.data.model.other.FinancePayments;
import com.taskos.databinding.FragmentFinancialBinding;
import com.taskos.ui.base.BaseFragment;
import com.taskos.ui.main.openjobs.addjob.AddJobNavigator;
import com.taskos.ui.main.openjobs.documents.DocumentFragment;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import antonkozyriatskyi.circularprogressindicator.CircularProgressIndicator;

public class FinancialFragment extends BaseFragment<FragmentFinancialBinding, FinancialViewModel> implements View.OnClickListener, AddJobNavigator {

    private FinancialViewModel financialViewModel;
    private String jobUUID = "";
    private Job job;
    private JobFinancial jobFinancial;
    private static final CircularProgressIndicator.ProgressTextAdapter TEXT_ADAPTER = value -> {
        String data = String.format(Locale.US, "%.1f", value);
        return data.concat("%");
    };

    private FinancialAdapter financialPayAdapter, financialExpenseAdapter;
    private List<FinancePayments> financePaymentsList;
    private ArrayList<FinanceExpense> financeExpensesList;

    public static FinancialFragment newInstance(String jobUUID) {

        Bundle args = new Bundle();

        FinancialFragment fragment = new FinancialFragment();
        args.putString("JobKey", jobUUID);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_financial;
    }

    @Override
    public FinancialViewModel getViewModel() {
        financialViewModel = new ViewModelProvider(this, ViewModelProviderFactory.getInstance()).get(FinancialViewModel.class);
        return financialViewModel;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            jobUUID = getArguments().getString("JobKey");
        }
    }

    private double soldPrice, total, salesTax;

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        financialViewModel.setNavigator(this);
        TextView tvTitle = view.findViewById(R.id.tvTitle);
        setOnClickListener(tvTitle, view.findViewById(R.id.img_back), getViewDataBinding().tvEdit, getViewDataBinding().llMain,
                getViewDataBinding().framePayment, getViewDataBinding().frameExpense,
                getViewDataBinding().llAddPaymentView, getViewDataBinding().llAddExpenseView,
                getViewDataBinding().tvCancelPay, getViewDataBinding().tvCancelExpense,
                getViewDataBinding().tvRemovePay, getViewDataBinding().tvRemoveExpense,
                getViewDataBinding().tvSavePayment, getViewDataBinding().tvSaveExpense,
                getViewDataBinding().tvSave, getViewDataBinding().rlFinanceDoc);

        financePaymentsList = new ArrayList<>();
        financeExpensesList = new ArrayList<>();

        financialViewModel.getJobDetail(jobUUID).observe(getViewLifecycleOwner(), job -> {
            this.job = job;
            tvTitle.setText(job.getName());
            setJobValue(job);
        });

        financialViewModel.getJobFinancialDetail(jobUUID).observe(getViewLifecycleOwner(), jobFinancial -> {
            this.jobFinancial = jobFinancial;

            setJobFinanceValue(jobFinancial);
            financePaymentsList.clear();
            financeExpensesList.clear();

            double ttlPayAmt = 0, ttlExpenseAmt = 0;
            for (int i = 0; i < jobFinancial.getPaymentUuid().size(); i++) {
                FinancePayments tmpBean = new FinancePayments();
                tmpBean.setPaymentUuid(jobFinancial.getPaymentUuid().get(i));
                ttlPayAmt += jobFinancial.getPaymentAmounts().get(i);
                tmpBean.setPaymentAmounts(jobFinancial.getPaymentAmounts().get(i));
                tmpBean.setPaymentType(jobFinancial.getPaymentType().get(i));
                financePaymentsList.add(tmpBean);
            }
            updatePayAdapter();

            for (int i = 0; i < jobFinancial.getExpenseUuid().size(); i++) {
                FinanceExpense tmpBean = new FinanceExpense();
                tmpBean.setExpenseUuid(jobFinancial.getExpenseUuid().get(i));
                ttlExpenseAmt += jobFinancial.getExpenseAmount().get(i);
                tmpBean.setExpenseAmount(jobFinancial.getExpenseAmount().get(i));
                tmpBean.setExpenseItem(jobFinancial.getExpenseItem().get(i));
                financeExpensesList.add(tmpBean);
            }
            updateExpenseAdapter();

            updateCircleView(ttlPayAmt, ttlExpenseAmt);
        });
        addTextWatcher();
    }

    private void setJobFinanceValue(JobFinancial jobFinancial) {
        getViewDataBinding().etSalesTax.setText(String.valueOf(jobFinancial.getSalesTax()));
        getViewDataBinding().etSubTtl.setText(String.valueOf(jobFinancial.getSubtotal()));
        getViewDataBinding().tvTotal.setText("$".concat(String.valueOf(jobFinancial.getSalesTotal())));
    }

    private void setJobValue(Job job) {
        getViewDataBinding().etSold.setText(job.getSoldItem());
    }

    private void updateCircleView(double ttlPayAmt, double ttlExpenseAmt) {
        double finalAmt = ttlPayAmt - ttlExpenseAmt;
        if (finalAmt < 0) {
            finalAmt = 0;
        }
        getViewDataBinding().tvCurProfit.setText("$".concat(String.valueOf(finalAmt)));
        getViewDataBinding().circleReceived.setProgressStrokeWidthDp(19);
        getViewDataBinding().circleReceived.setProgressBackgroundStrokeWidthDp(0);

        int receiveValue = (int) (ttlPayAmt / financePaymentsList.size());
        getViewDataBinding().circleReceived.setProgress(Math.abs(receiveValue), 100);

        getViewDataBinding().circleMargin.setProgressStrokeWidthDp(19);
        getViewDataBinding().circleMargin.setProgressBackgroundStrokeWidthDp(0);

        int expenseValue = (int) (ttlExpenseAmt / financeExpensesList.size());
        getViewDataBinding().circleMargin.setProgress(Math.abs(expenseValue), 100);


        getViewDataBinding().circleMargin.setProgressTextAdapter(TEXT_ADAPTER);
        getViewDataBinding().circleReceived.setProgressTextAdapter(TEXT_ADAPTER);
    }

    private void updateExpenseAdapter() {
        if (financialExpenseAdapter == null) {
            financialExpenseAdapter = new FinancialAdapter(financeExpensesList, pos -> {
                if (isNetworkConnected()) {
                    if (job != null && jobFinancial != null) {
                        financeExpensesList.remove(pos);
                        financialExpenseAdapter.notifyItemRemoved(pos);

                        List<String> expenseUUIDList = jobFinancial.getExpenseUuid();
                        if (expenseUUIDList.size() - 1 >= pos) {
                            expenseUUIDList.remove(pos);
                        }
                        jobFinancial.setExpenseUuid(expenseUUIDList);

                        List<String> expenseItemList = jobFinancial.getExpenseItem();
                        if (expenseItemList.size() - 1 >= pos) {
                            expenseItemList.remove(pos);
                        }
                        jobFinancial.setExpenseItem(expenseItemList);

                        List<Double> expenseAmtList = jobFinancial.getExpenseAmount();
                        if (expenseAmtList.size() - 1 >= pos) {
                            expenseAmtList.remove(pos);
                        }
                        jobFinancial.setExpenseAmount(expenseAmtList);
                        financialViewModel.doSaveJobInformation(job, jobFinancial, "");
                    } else {
                        showToast(getString(R.string.something_went_wrong));
                    }
                } else showToast(getString(R.string.noInternet));
            });
            getViewDataBinding().recyclerViewExpense.setAdapter(financialExpenseAdapter);
        } else {
            financialExpenseAdapter.notifyDataSetChanged();
            getViewDataBinding().scrollView.scrollTo(0, getViewDataBinding().scrollView.getBottom());
            if (!financeExpensesList.isEmpty())
                getViewDataBinding().recyclerViewExpense.smoothScrollToPosition(financeExpensesList.size() - 1);
        }
    }

    private void setOnClickListener(View... views) {
        for (View view : views) {
            view.setOnClickListener(this);
        }
    }

    private void updatePayAdapter() {
        if (financialPayAdapter == null) {
            financialPayAdapter = new FinancialAdapter(financePaymentsList, pos -> {
                if (isNetworkConnected()) {
                    if (job != null && jobFinancial != null) {
                        financePaymentsList.remove(pos);
                        financialPayAdapter.notifyItemRemoved(pos);

                        List<String> payUUIDList = jobFinancial.getPaymentUuid();
                        if (payUUIDList.size() - 1 >= pos) {
                            payUUIDList.remove(pos);
                        }
                        jobFinancial.setPaymentUuid(payUUIDList);

                        List<Double> payAmtList = jobFinancial.getPaymentAmounts();
                        if (payAmtList.size() - 1 >= pos) {
                            payAmtList.remove(pos);
                        }
                        jobFinancial.setPaymentAmounts(payAmtList);

                        List<String> patTypeList = jobFinancial.getPaymentType();
                        if (patTypeList.size() - 1 >= pos) {
                            patTypeList.remove(pos);
                        }
                        jobFinancial.setPaymentType(patTypeList);

                        financialViewModel.doSaveJobInformation(job, jobFinancial, "");
                    } else {
                        showToast(getString(R.string.something_went_wrong));
                    }
                } else showToast(getString(R.string.noInternet));
            });
            getViewDataBinding().recyclerViewPay.setAdapter(financialPayAdapter);
        } else {
            financialPayAdapter.notifyDataSetChanged();
            getViewDataBinding().scrollView.scrollTo(0, getViewDataBinding().scrollView.getBottom());
            if (!financePaymentsList.isEmpty())
                getViewDataBinding().recyclerViewPay.smoothScrollToPosition(financePaymentsList.size() - 1);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tvTitle:
            case R.id.img_back:
                getBaseActivity().onBackPressed();
                break;

            case R.id.rl_finance_doc:
                getBaseActivity().addFragment(DocumentFragment.newInstance(jobUUID), R.id.nav_host_fragment, true);
                break;

            case R.id.tv_edit:
                if (getViewDataBinding().tvEdit.getText().toString().equals(getString(R.string.edit_financial))) {
                    enableDisableFinance(true);
                    getViewDataBinding().tvEdit.setText(R.string.cancel);
                } else {
                    setJobValue(job);
                    setJobFinanceValue(jobFinancial);
                    enableDisableFinance(false);
                    getViewDataBinding().tvEdit.setText(R.string.edit_financial);
                }
                hideKeyboard();
                break;

            case R.id.tv_save:
                if (isNetworkConnected()) {
                    hideKeyboard();
                    if (verifyInput()) {
                        if (job != null && jobFinancial != null) {
                            getViewDataBinding().tvSave.setClickable(false);
                            job.setSoldItem(getViewDataBinding().etSold.getText().toString());
                            jobFinancial.setSubtotal(soldPrice);
                            jobFinancial.setSalesTax(salesTax);
                            jobFinancial.setSalesTotal(total);
                            financialViewModel.doSaveJobInformation(job, jobFinancial, "JOB_EDIT");
                            enableDisableFinance(false);
                            getViewDataBinding().tvEdit.setText(R.string.edit_financial);
                        } else {
                            showToast(getString(R.string.something_went_wrong));
                        }
                    }
                } else showToast(getString(R.string.noInternet));
                break;

            case R.id.frame_payment:
                setFrameBackgroundView(true);
                break;

            case R.id.frame_expense:
                setFrameBackgroundView(false);
                break;

            case R.id.ll_add_payment_view:
                setAddBackgroundView(true, false);
                break;

            case R.id.ll_add_expense_view:
                setAddBackgroundView(false, false);
                break;

            case R.id.tv_cancel_pay:
                setAddBackgroundView(true, true);
                clearInput();
                break;

            case R.id.tv_cancel_expense:
                setAddBackgroundView(false, true);
                clearInput();
                break;

            case R.id.tv_remove_pay:
                hideKeyboard();
                if (getViewDataBinding().tvRemovePay.getText().toString().equals(getString(R.string.remove_payment))) {
                    financialPayAdapter.setRemove(true);
                    financialPayAdapter.notifyDataSetChanged();
                    getViewDataBinding().tvRemovePay.setText(R.string.close);
                } else {
                    financialPayAdapter.setRemove(false);
                    financialPayAdapter.notifyDataSetChanged();
                    getViewDataBinding().tvRemovePay.setText(R.string.remove_payment);
                }
                break;

            case R.id.tv_remove_expense:
                hideKeyboard();
                if (getViewDataBinding().tvRemoveExpense.getText().toString().equals(getString(R.string.remove_expenses))) {
                    financialExpenseAdapter.setRemove(true);
                    financialExpenseAdapter.notifyDataSetChanged();
                    getViewDataBinding().tvRemoveExpense.setText(R.string.close);
                } else {
                    financialExpenseAdapter.setRemove(false);
                    financialExpenseAdapter.notifyDataSetChanged();
                    getViewDataBinding().tvRemoveExpense.setText(R.string.remove_expenses);
                }
                break;

            case R.id.tv_save_payment:
                if (isNetworkConnected()) {
                    hideKeyboard();
                    if (verifyPayInput()) {
                        if (job != null && jobFinancial != null) {
                            getViewDataBinding().tvSavePayment.setClickable(false);
                            List<String> payUUIDList = jobFinancial.getPaymentUuid();
                            payUUIDList.add(UUID.randomUUID().toString());
                            jobFinancial.setPaymentUuid(payUUIDList);

                            List<Double> payAmtList = jobFinancial.getPaymentAmounts();
                            payAmtList.add(Double.parseDouble(getViewDataBinding().etPayAmount.getText().toString()));
                            jobFinancial.setPaymentAmounts(payAmtList);

                            List<String> patTypeList = jobFinancial.getPaymentType();
                            patTypeList.add("");
                            jobFinancial.setPaymentType(patTypeList);

                            financialViewModel.doSaveJobInformation(job, jobFinancial, "");
                        } else {
                            showToast(getString(R.string.something_went_wrong));
                        }
                    }
                } else showToast(getString(R.string.noInternet));
                break;

            case R.id.tv_save_expense:
                if (isNetworkConnected()) {
                    hideKeyboard();
                    if (verifyExpenseInput()) {
                        if (job != null && jobFinancial != null) {
                            getViewDataBinding().tvSaveExpense.setClickable(false);
                            List<String> expenseUUIDList = jobFinancial.getExpenseUuid();
                            expenseUUIDList.add(UUID.randomUUID().toString());
                            jobFinancial.setExpenseUuid(expenseUUIDList);

                            List<String> expenseItemList = jobFinancial.getExpenseItem();
                            expenseItemList.add(getViewDataBinding().etExpenseName.getText().toString());
                            jobFinancial.setExpenseItem(expenseItemList);

                            List<Double> expenseAmtList = jobFinancial.getExpenseAmount();
                            expenseAmtList.add(Double.parseDouble(getViewDataBinding().etExpenseAmt.getText().toString()));
                            jobFinancial.setExpenseAmount(expenseAmtList);
                            financialViewModel.doSaveJobInformation(job, jobFinancial, "");
                        } else {
                            showToast(getString(R.string.something_went_wrong));
                        }
                    }
                } else showToast(getString(R.string.noInternet));
                break;
        }
    }

    private void addTextWatcher() {
        getViewDataBinding().etSubTtl.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!editable.toString().isEmpty()) {
                    String soldTxt = getViewDataBinding().etSalesTax.getText().toString();
                    salesTax = soldTxt.isEmpty() ? 0 : Double.parseDouble(soldTxt);
                    soldPrice = editable.toString().equals(".") ? 0 : Double.parseDouble(editable.toString());
                    //old logic
                    //total = soldPrice + (soldPrice * (salesTax / 100));
                    total = soldPrice + salesTax;
                    getViewDataBinding().tvTotal.setText(total + "");
                }
            }
        });

        getViewDataBinding().etSalesTax.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!editable.toString().isEmpty()) {
                    String soldTxt = getViewDataBinding().etSubTtl.getText().toString();
                    soldPrice = soldTxt.isEmpty() ? 0 : Double.parseDouble(soldTxt);
                    salesTax = editable.toString().equals(".") ? 0 : Double.parseDouble(editable.toString());
                    //old logic
                    //total = soldPrice + (soldPrice * (salesTax / 100));
                    total = soldPrice + salesTax;
                    getViewDataBinding().tvTotal.setText(String.valueOf(total));
                }
            }
        });
    }

    private boolean verifyPayInput() {
        if (getViewDataBinding().etPayAmount.getText().toString().isEmpty()) {
            showToast(getString(R.string.alert_empty_amount));
            return false;
        } else {
            return true;
        }
    }

    private boolean verifyExpenseInput() {
        if (getViewDataBinding().etExpenseName.getText().toString().isEmpty()) {
            showToast(getString(R.string.alert_name_empty));
            return false;
        } else if (getViewDataBinding().etExpenseAmt.getText().toString().isEmpty()) {
            showToast(getString(R.string.alert_empty_amount));
            return false;
        } else {
            return true;
        }
    }

    private void enableDisableFinance(boolean isEnable) {
        getViewDataBinding().etSold.setEnabled(isEnable);
        getViewDataBinding().etSalesTax.setEnabled(isEnable);
        getViewDataBinding().etSubTtl.setEnabled(isEnable);
        getViewDataBinding().tvSave.setVisibility(isEnable ? View.VISIBLE : View.GONE);
        getViewDataBinding().rlFinanceDoc.setVisibility(isEnable ? View.GONE : View.VISIBLE);
    }

    private void setAddBackgroundView(boolean isPayment, boolean isGone) {
        if (isGone) {
            getViewDataBinding().llAddPayment.setVisibility(View.GONE);
            getViewDataBinding().llAddExpense.setVisibility(View.GONE);
        } else {
            if (isPayment) {
                getViewDataBinding().llAddPayment.setVisibility(View.VISIBLE);
                getViewDataBinding().llAddExpense.setVisibility(View.GONE);
            } else {
                getViewDataBinding().llAddPayment.setVisibility(View.GONE);
                getViewDataBinding().llAddExpense.setVisibility(View.VISIBLE);
            }
        }
    }

    private boolean verifyInput() {
        if (getViewDataBinding().etSold.getText().toString().isEmpty()) {
            showToast(getString(R.string.alert_item_sold_empty));
            return false;
        } else if (getViewDataBinding().etSubTtl.getText().toString().isEmpty()) {
            showToast(getString(R.string.alert_item_sub_total));
            return false;
        } else {
            return true;
        }
    }

    private void setFrameBackgroundView(boolean isPayment) {
        if (isPayment) {
            getViewDataBinding().framePayment.setBackgroundResource(R.drawable.bg_green_drawable2);
            getViewDataBinding().frameExpense.setBackgroundResource(R.color.transparent);

            getViewDataBinding().tvPaymentLabel.setTextColor(getResources().getColor(R.color.white));
            getViewDataBinding().tvExpenseLabel.setTextColor(getResources().getColor(R.color.dark_gray));

            getViewDataBinding().llPayment.setVisibility(View.VISIBLE);
            getViewDataBinding().llExpense.setVisibility(View.GONE);
        } else {
            getViewDataBinding().frameExpense.setBackgroundResource(R.drawable.bg_red_drawable);
            getViewDataBinding().framePayment.setBackgroundResource(R.color.transparent);

            getViewDataBinding().tvExpenseLabel.setTextColor(getResources().getColor(R.color.white));
            getViewDataBinding().tvPaymentLabel.setTextColor(getResources().getColor(R.color.dark_gray));

            getViewDataBinding().llExpense.setVisibility(View.VISIBLE);
            getViewDataBinding().llPayment.setVisibility(View.GONE);
        }
    }

    @Override
    public void dismiss(String TAG) {

        if (TAG.equalsIgnoreCase("JOB_EDIT")) {
            getViewDataBinding().tvSave.setClickable(true);
        } else {
            getViewDataBinding().tvSaveExpense.setClickable(true);
            getViewDataBinding().tvSavePayment.setClickable(true);
            clearInput();
        }
    }

    private void clearInput() {
        getViewDataBinding().etPayAmount.setText("");
        getViewDataBinding().etExpenseName.setText("");
        getViewDataBinding().etExpenseAmt.setText("");
    }

    @Override
    public void onSaveError() {
        getViewDataBinding().tvSaveExpense.setClickable(true);
        getViewDataBinding().tvSavePayment.setClickable(true);
        getViewDataBinding().tvSave.setClickable(true);
        showToast(getString(R.string.something_went_wrong_save));
    }
}