package com.taskos.ui.main.openjobs.images;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.taskos.R;
import com.taskos.ui.base.BaseViewHolder;

import java.util.List;

/**
 * Created by Hemant Sharma on 23-02-20.
 */
public class ImageAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    private List<String> imageList;

    ImageAdapter(List<String> keyValueList) {
        this.imageList = keyValueList;
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(
                LayoutInflater.from(parent.getContext()).inflate(R.layout.item_images, parent, false));
    }

    @Override
    public int getItemCount() {
        if (imageList != null && imageList.size() > 0) {
            return imageList.size();
        } else {
            return 0;
        }
    }


    public class ViewHolder extends BaseViewHolder {

        private ImageView img_gallery;

        public ViewHolder(View itemView) {
            super(itemView);
            img_gallery = itemView.findViewById(R.id.img_gallery);
        }

        public void onBind(int position) {
            String imageLink = imageList.get(position);

            if (!imageLink.isEmpty()) {
                Glide.with(img_gallery.getContext())
                        .load(imageLink)
                        .centerCrop()
                        .placeholder(R.drawable.ic_gallery)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .error(R.drawable.ic_gallery)
                        .into(img_gallery);
            } else {
                Glide.with(img_gallery.getContext())
                        .load(R.drawable.ic_gallery)
                        .placeholder(R.drawable.ic_gallery)
                        .centerCrop()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .error(R.drawable.ic_gallery)
                        .into(img_gallery);
            }
        }

    }

}
