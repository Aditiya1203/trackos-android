package com.taskos.ui.main.client.customers;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProvider;

import com.taskos.R;
import com.taskos.ViewModelProviderFactory;
import com.taskos.data.model.other.CustomerSelect;
import com.taskos.databinding.DialogEmployeeBinding;
import com.taskos.ui.base.BaseBottomSheetDialog;

import java.util.ArrayList;
import java.util.List;


public class SelectCustomerBottomSheetDialog extends BaseBottomSheetDialog<DialogEmployeeBinding, SelectCustomerViewModel> implements View.OnClickListener {

    private static final String TAG = "SelectCustomerBottomShe";
    private SelectCustomerViewModel selectCustomerViewModel;
    private List<CustomerSelect> customerSelectList;
    private CustomerSelectCallback callback;

    public static SelectCustomerBottomSheetDialog newInstance(CustomerSelectCallback callback) {

        Bundle args = new Bundle();

        SelectCustomerBottomSheetDialog fragment = new SelectCustomerBottomSheetDialog();
        fragment.setCallback(callback);
        fragment.setArguments(args);
        return fragment;
    }

    private void setCallback(CustomerSelectCallback callback) {
        this.callback = callback;
    }

    @Override
    public int getBindingVariable() {
        return com.taskos.BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.dialog_employee;
    }

    @Override
    public SelectCustomerViewModel getViewModel() {
        selectCustomerViewModel = new ViewModelProvider(this, ViewModelProviderFactory.getInstance()).get(SelectCustomerViewModel.class);
        return selectCustomerViewModel;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getViewDataBinding().imgBack.setOnClickListener(this);
        getViewDataBinding().tvTitle.setOnClickListener(this);

        customerSelectList = new ArrayList<>();
        SelectCustomerAdapter selectCustomerAdapter = new SelectCustomerAdapter(customerSelectList, pos -> {
            if (callback != null) {
                callback.onClick(customerSelectList.get(pos));
                dismissDialog(TAG);
            }
        });
        getViewDataBinding().recyclerView.setAdapter(selectCustomerAdapter);

        selectCustomerViewModel.getAllClients().observe(getViewLifecycleOwner(), list -> {
            customerSelectList.addAll(list);
            selectCustomerAdapter.notifyDataSetChanged();
        });
    }

    public void show(FragmentManager fragmentManager) {
        super.show(fragmentManager, TAG);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_title:
            case R.id.img_back:
                dismissDialog(TAG);
                break;
        }
    }

    public interface CustomerSelectCallback {
        void onClick(CustomerSelect customerSelect);
    }
}
