package com.taskos.ui.main.employee;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProvider;

import com.taskos.R;
import com.taskos.ViewModelProviderFactory;
import com.taskos.databinding.DialogEmployeeBinding;
import com.taskos.ui.auth.MUser;
import com.taskos.ui.base.BaseBottomSheetDialog;

import java.util.ArrayList;
import java.util.List;


public class EmployeesBottomSheetDialog extends BaseBottomSheetDialog<DialogEmployeeBinding, EmployeesViewModel> implements View.OnClickListener {

    private static final String TAG = "EmployeesBottomSheetDia";
    private EmployeesViewModel employeesViewModel;
    private List<MUser> employeeList;
    private EmployeeCallback callback;

    public static EmployeesBottomSheetDialog newInstance(EmployeeCallback callback) {

        Bundle args = new Bundle();

        EmployeesBottomSheetDialog fragment = new EmployeesBottomSheetDialog();
        fragment.setCallback(callback);
        fragment.setArguments(args);
        return fragment;
    }

    private void setCallback(EmployeeCallback callback) {
        this.callback = callback;
    }

    @Override
    public int getBindingVariable() {
        return com.taskos.BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.dialog_employee;
    }

    @Override
    public EmployeesViewModel getViewModel() {
        employeesViewModel = new ViewModelProvider(this, ViewModelProviderFactory.getInstance()).get(EmployeesViewModel.class);
        return employeesViewModel;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getViewDataBinding().imgBack.setOnClickListener(this);
        getViewDataBinding().tvTitle.setOnClickListener(this);

        employeeList = new ArrayList<>();
        EmployeeAdapter employeeAdapter = new EmployeeAdapter(employeeList, pos -> {
            if (callback != null) {
                callback.onClick(employeeList.get(pos));
                dismissDialog(TAG);
            }
        });
        getViewDataBinding().recyclerView.setAdapter(employeeAdapter);

        employeesViewModel.getEmployees().observe(getViewLifecycleOwner(), list -> {
            employeeList.clear();
            employeeList.addAll(list);
            employeeAdapter.notifyDataSetChanged();
        });
    }

    public void show(FragmentManager fragmentManager) {
        super.show(fragmentManager, TAG);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_title:
            case R.id.img_back:
                dismissDialog(TAG);
                break;
        }
    }

    public interface EmployeeCallback {
        void onClick(MUser mUser);
    }
}
