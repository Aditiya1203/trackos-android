package com.taskos.ui.main.client;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.taskos.data.DataManager;
import com.taskos.data.model.api.Customer;
import com.taskos.ui.base.BaseViewModel;
import com.taskos.util.AppConstants;
import com.taskos.util.AppLogger;

import java.util.ArrayList;

public class ClientViewModel extends BaseViewModel<ClientNavigator> {

    private static final String TAG = "ClientViewModel";
    private ArrayList<Customer> list = new ArrayList<>();
    private MutableLiveData<ArrayList<Customer>> mutableLiveData = new MutableLiveData<>();
    private String ownerID;

    public ClientViewModel(DataManager dataManager) {
        super(dataManager);
        ownerID = doDetermineOwnerID();
    }


    private String doDetermineOwnerID() {
        if (!getDataManager().getCompanyID().isEmpty() &&
                getDataManager().isUserCompanyAddRequested() &&
                getDataManager().isUserApproveCompanyAdd()) {
            return getDataManager().getCompanyID();
        } else {
            return getDataManager().getUUID();
        }
    }

    MutableLiveData<ArrayList<Customer>> getOpenClients(int currentPos, String search) {
        if (ownerID == null) {
            ownerID = getDataManager().getCompanyID();
            AppLogger.e(TAG, ownerID);
        }
        AppLogger.e(TAG, ownerID);

        list.clear();
        Query query;
        if (currentPos == 1) {
            if (search.isEmpty()) {
                query = getDataManager().getFirestoreInstance().collection("Customers")
                        .whereEqualTo("listOrganizer", AppConstants.kStatusListOrganiser[0])
                        .whereEqualTo("salesUuid", getDataManager().getUUID())
                        .whereEqualTo("owner", ownerID)
                        .orderBy("appointmentTime", Query.Direction.DESCENDING);
            } else {
                query = getDataManager().getFirestoreInstance().collection("Customers")
                        .whereEqualTo("name", search)
                        .whereEqualTo("salesUuid", getDataManager().getUUID())
                        .whereEqualTo("listOrganizer", AppConstants.kStatusListOrganiser[0])
                        .whereEqualTo("owner", ownerID)
                        .orderBy("appointmentTime", Query.Direction.DESCENDING);
            }
        } else if (currentPos == 0) {
            if (search.isEmpty()) {
                query = getDataManager().getFirestoreInstance().collection("Customers")
                        .whereEqualTo("owner", ownerID)
                        .whereEqualTo("salesUuid", getDataManager().getUUID())
                        .whereEqualTo("listOrganizer", AppConstants.kStatusListOrganiser[0])
                        .orderBy("appointmentTime", Query.Direction.DESCENDING);
            } else {
                query = getDataManager().getFirestoreInstance().collection("Customers")
                        .whereEqualTo("name", search)
                        .whereEqualTo("salesUuid", getDataManager().getUUID())
                        .whereEqualTo("owner", ownerID)
                        .whereEqualTo("listOrganizer", AppConstants.kStatusListOrganiser[0])
                        .orderBy("appointmentTime", Query.Direction.DESCENDING);
            }
        } else {
            if (search.isEmpty()) {
                query = getDataManager().getFirestoreInstance().collection("Customers")
                        .whereIn("listOrganizer", AppConstants.geListOrganiser())
                        .whereEqualTo("salesUuid", getDataManager().getUUID())
                        .whereEqualTo("owner", ownerID)
                        .orderBy("appointmentTime", Query.Direction.DESCENDING);
            } else {
                query = getDataManager().getFirestoreInstance().collection("Customers")
                        .whereEqualTo("name", search)
                        .whereEqualTo("salesUuid", getDataManager().getUUID())
                        .whereEqualTo("owner", ownerID)
                        .whereIn("listOrganizer", AppConstants.geListOrganiser())
                        .orderBy("appointmentTime", Query.Direction.DESCENDING);
            }
        }

        query.addSnapshotListener((queryDocumentSnapshots, e) -> {
            if (e != null) {
                Log.e(TAG, "onEvent: Listen failed.", e);
                return;
            }

            if (queryDocumentSnapshots != null) {
                list.clear();
                for (QueryDocumentSnapshot doc : queryDocumentSnapshots) {
                    Customer customer = doc.toObject(Customer.class);
                    list.add(customer);
                }
                mutableLiveData.setValue(list);
            }
        });

        return mutableLiveData;
    }
}