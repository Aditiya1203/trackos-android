package com.taskos.ui.main.client;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;

import com.taskos.BR;
import com.taskos.R;
import com.taskos.ViewModelProviderFactory;
import com.taskos.databinding.FragmentClientBinding;
import com.taskos.ui.base.BaseFragment;
import com.taskos.ui.fcm.FcmNotificationBuilder;
import com.taskos.ui.main.client.addclient.AddClientBottomSheetDialog;
import com.taskos.ui.main.client.detail.ClientDetailFragment;
import com.taskos.ui.main.openjobs.addjob.AddJobBottomSheetDialog;
import com.taskos.ui.main.tasks.addtask.AddTaskBottomSheetDialog;


public class ClientFragment extends BaseFragment<FragmentClientBinding, ClientViewModel> implements View.OnClickListener {

    private ClientViewModel clientViewModel;
    private int currentPos = 0;

    public static ClientFragment newInstance(Bundle bundle) {

        if (bundle == null) {
            bundle = new Bundle();
        }
        ClientFragment fragment = new ClientFragment();
        fragment.setArguments(bundle);
        return fragment;

    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_client;
    }

    @Override
    public ClientViewModel getViewModel() {
        clientViewModel = new ViewModelProvider(this, ViewModelProviderFactory.getInstance()).get(ClientViewModel.class);
        return clientViewModel;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            String view = getArguments().getString("tabView");
            if (FcmNotificationBuilder.TAB_ALL_CUSTOMER.equals((view == null ? "" : view))) {
                currentPos = 2;
            } else {
                currentPos = 0;
            }
            String uuid = getArguments().getString("selectedUuid");
            if (uuid != null)
                navigateToDetail(uuid);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getViewDataBinding().menuFloat.setOnMenuToggleListener(isVisible -> {
            if (isVisible) {
                getViewDataBinding().blurringView.setVisibility(View.VISIBLE);
                getViewDataBinding().blurringView.setBlurredView(getViewDataBinding().llMain);
            } else invalidateBlurView();
        });

        getViewDataBinding().fabAddClient.setOnClickListener(this);
        getViewDataBinding().fabAddJob.setOnClickListener(this);
        getViewDataBinding().fabAddTask.setOnClickListener(this);
        getViewDataBinding().tvSearch.setOnClickListener(this);

        getViewDataBinding().segmentedButtonGroup.setPosition(currentPos);
        filterData("");
        getViewDataBinding().segmentedButtonGroup.setOnClickedButtonListener(position -> {
            currentPos = position;
            filterData("");
        });

        doAddTextWatcher();
    }

    private void filterData(String search) {
        if (currentPos == 0) {
            getViewDataBinding().etSearch.setHint("Search My Leads");
            getViewDataBinding().cvAllClient.setVisibility(View.GONE);
            doGetClientData(0, search);
        } else if (currentPos == 1) {
            getViewDataBinding().etSearch.setHint("Search All Open Leads");
            getViewDataBinding().cvAllClient.setVisibility(View.GONE);
            doGetClientData(1, search);
        } else {
            getViewDataBinding().etSearch.setHint("Search All Clients");
            getViewDataBinding().cvAllClient.setVisibility(View.VISIBLE);
            doGetClientData(2, search);
        }
    }

    private void doAddTextWatcher() {
        getViewDataBinding().etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!editable.toString().isEmpty()) {
                    if (editable.toString().length() > 2)
                        filterData(editable.toString());
                } else {
                    filterData("");
                }
            }
        });
    }

    private void doGetClientData(int currentPos, String search) {
        clientViewModel.getOpenClients(currentPos, search).observe(getViewLifecycleOwner(), customerArrayList -> {
            ClientAdapter clientAdapter = new ClientAdapter(currentPos == 2 ? ClientAdapter.VIEW_TYPE_CLIENT : ClientAdapter.VIEW_TYPE_COMMON, customerArrayList, pos ->
                    navigateToDetail(customerArrayList.get(pos).getUuid()));

                    getViewDataBinding().rvOpenLeads.setAdapter(clientAdapter);
                }
        );
    }

    private void navigateToDetail(String uuid) {
        getBaseActivity().addFragment(ClientDetailFragment.newInstance(uuid), R.id.nav_host_fragment, true);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fab_add_client:
                getViewDataBinding().llSearch.setVisibility(View.GONE);
                AddClientBottomSheetDialog.newInstance().show(getChildFragmentManager());
                getViewDataBinding().menuFloat.close(true);
                break;

            case R.id.fab_add_job:
                getViewDataBinding().llSearch.setVisibility(View.GONE);
                AddJobBottomSheetDialog.newInstance().show(getChildFragmentManager());
                getViewDataBinding().menuFloat.close(true);
                break;

            case R.id.fab_add_task:
                getViewDataBinding().llSearch.setVisibility(View.GONE);
                AddTaskBottomSheetDialog.newInstance().show(getChildFragmentManager());
                getViewDataBinding().menuFloat.close(true);
                break;

            case R.id.tv_search:
                getViewDataBinding().llSearch.setVisibility(getViewDataBinding().llSearch.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
                break;
        }
    }

    private void invalidateBlurView() {
        getViewDataBinding().blurringView.setVisibility(View.GONE);
        getViewDataBinding().blurringView.invalidate();
    }
}