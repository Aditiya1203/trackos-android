package com.taskos.ui.main.network.add;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.taskos.data.DataManager;
import com.taskos.data.model.api.CompanyAddRequest;
import com.taskos.data.model.api.NetworkAddRequest;
import com.taskos.ui.auth.MUser;
import com.taskos.ui.base.BaseViewModel;

public class AddToNetworkViewModel extends BaseViewModel<AddToNetworkNavigator> {

    private static final String TAG = "AddToNetworkViewModel";
    private MutableLiveData<MUser> mutableLiveDataUsers = new MutableLiveData<>();
    private MutableLiveData<NetworkAddRequest> mutableLiveDataNetworkRequest = new MutableLiveData<>();
    private MutableLiveData<CompanyAddRequest> mutableLiveDataCompanyRequest = new MutableLiveData<>();

    public AddToNetworkViewModel(DataManager dataManager) {
        super(dataManager);
    }

    MutableLiveData<MUser> getMUser(String userUUID) {
        getDataManager().getFirestoreInstance().collection("MUser").document(userUUID).addSnapshotListener((documentSnapshot, e) -> {
            if (e != null) {
                Log.e(TAG, "onEvent: Listen failed.", e);
                return;
            }
            if (documentSnapshot != null) {
                MUser mUser = documentSnapshot.toObject(MUser.class);
                if (mUser != null) {
                    mutableLiveDataUsers.setValue(mUser);
                }
            }
        });
        return mutableLiveDataUsers;
    }

    MutableLiveData<NetworkAddRequest> getNetworkRequest(String requestedUserUUID) {
        getDataManager().getFirestoreInstance().collection("NetworkAddRequest")
                .whereEqualTo("requestedUser", requestedUserUUID)
                .whereEqualTo("sentRequestUser", getDataManager().getUUID())
                .addSnapshotListener((documentSnapshot, e) -> {
                    if (e != null) {
                        Log.e(TAG, "onEvent: Listen failed.", e);
                        return;
                    }
                    if (documentSnapshot != null) {
                        for (QueryDocumentSnapshot doc : documentSnapshot) {
                            NetworkAddRequest networkAddRequest = doc.toObject(NetworkAddRequest.class);
                            mutableLiveDataNetworkRequest.setValue(networkAddRequest);
                        }
                    }
                });
        return mutableLiveDataNetworkRequest;
    }

    MutableLiveData<CompanyAddRequest> getCompanyAddRequest(String requestedUserUUID) {
        getDataManager().getFirestoreInstance().collection("CompanyAddRequest")
                .whereEqualTo("requestedUser", requestedUserUUID)
                .whereEqualTo("companyRequester", getDataManager().getCompanyID())
                .addSnapshotListener((documentSnapshot, e) -> {
                    if (e != null) {
                        Log.e(TAG, "onEvent: Listen failed.", e);
                        return;
                    }
                    if (documentSnapshot != null) {
                        for (QueryDocumentSnapshot doc : documentSnapshot) {
                            CompanyAddRequest companyAddRequest = doc.toObject(CompanyAddRequest.class);
                            mutableLiveDataCompanyRequest.setValue(companyAddRequest);
                        }
                    }
                });
        return mutableLiveDataCompanyRequest;
    }

    void doAddNetwork(NetworkAddRequest networkAddRequest, MUser mUser) {
        getDataManager().getFirestoreInstance().collection("NetworkAddRequest").document(networkAddRequest.getUuid())
                .set(networkAddRequest).addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                /*ArrayList<String> tokenList = new ArrayList<>(mUser.getToken());
                FcmNotificationBuilder.newInstance().setTitle("New Network Invitation")
                        .setBody(networkAddRequest.getMessage().isEmpty() ? mUser.getUsername().concat(" send a Network Invitation ") : networkAddRequest.getMessage())
                        .setSelectedView(FcmNotificationBuilder.VIEW_NETWORK)
                        .setTabView(FcmNotificationBuilder.TAB_SEARCH_NETWORK)
                        .setSelectedUUID(networkAddRequest.getUuid())
                        .setReceiverFCMTokenList(AppUtils.removeDuplicates(tokenList))
                        .sendNotification();*/
                getNavigator().updateNetworkUI(false);
            } else getNavigator().onSaveError();
        });
    }

    void doRemoveRequestedNetwork(NetworkAddRequest networkAddRequest) {
        getDataManager().getFirestoreInstance().collection("NetworkAddRequest").document(networkAddRequest.getUuid())
                .delete().addOnSuccessListener(aVoid -> getNavigator().networkInvitationCanceled())
                .addOnFailureListener(e -> getNavigator().onSaveError());
    }

    void doAddEmployee(CompanyAddRequest companyAddRequest) {
        getDataManager().getFirestoreInstance().collection("CompanyAddRequest").document(companyAddRequest.getUuid())
                .set(companyAddRequest).addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                getNavigator().updateEmployeeUI(false);
            } else getNavigator().onSaveError();
        });
    }

    void doRemoveEmployee(CompanyAddRequest companyAddRequest) {
        getDataManager().getFirestoreInstance().collection("CompanyAddRequest").document(companyAddRequest.getUuid())
                .delete().addOnSuccessListener(aVoid -> getNavigator().employeeInvitationCanceled())
                .addOnFailureListener(e -> getNavigator().onSaveError());
    }
}