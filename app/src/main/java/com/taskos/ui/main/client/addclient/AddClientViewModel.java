package com.taskos.ui.main.client.addclient;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.taskos.data.DataManager;
import com.taskos.data.model.api.Customer;
import com.taskos.data.model.api.UserCommentsCustomer;
import com.taskos.ui.auth.MUser;
import com.taskos.ui.base.BaseViewModel;

public class AddClientViewModel extends BaseViewModel<AddClientNavigator> {

    private static final String TAG = "AddClientViewModel";
    private MutableLiveData<MUser> mutableLiveData = new MutableLiveData<>();

    public AddClientViewModel(DataManager dataManager) {
        super(dataManager);
    }


    MutableLiveData<MUser> getCurrentUser() {
        getDataManager().getFirestoreInstance().collection("MUser").document(getDataManager().getUUID()).addSnapshotListener((documentSnapshot, e) -> {
            if (e != null) {
                Log.e(TAG, "onEvent: Listen failed.", e);
                return;
            }
            if (documentSnapshot != null) {
                MUser mUser1 = documentSnapshot.toObject(MUser.class);
                if (mUser1 != null) {
                    mutableLiveData.setValue(mUser1);
                }
            }
        });
        return mutableLiveData;
    }

    void doAddClient(Customer customer, UserCommentsCustomer userCommentsCustomer) {
        // Get a reference to the Customers collection
        getDataManager().getFirestoreInstance().collection("Customers").document(customer.getUuid()).set(customer).addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                if (userCommentsCustomer != null) {
                    sendComment(userCommentsCustomer);
                }
                getNavigator().openDashboard();
            } else getNavigator().onSaveError();
        });
    }

    private void sendComment(UserCommentsCustomer userCommentsCustomer) {
        getDataManager().getFirestoreInstance().collection("Comments").document(userCommentsCustomer.getUuid())
                .set(userCommentsCustomer);
    }
}