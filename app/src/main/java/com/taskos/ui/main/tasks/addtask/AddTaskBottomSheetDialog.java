package com.taskos.ui.main.tasks.addtask;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProvider;

import com.taskos.R;
import com.taskos.ViewModelProviderFactory;
import com.taskos.data.model.api.Job;
import com.taskos.data.model.api.ToDo;
import com.taskos.databinding.DialogAddTaskBinding;
import com.taskos.ui.auth.MUser;
import com.taskos.ui.base.BaseBottomSheetDialog;
import com.taskos.ui.main.employee.EmployeesBottomSheetDialog;
import com.taskos.ui.main.tasks.jobs.SelectJobsBottomSheetDialog;
import com.taskos.util.AppConstants;
import com.taskos.util.CalenderUtils;

import java.util.Date;
import java.util.UUID;


public class AddTaskBottomSheetDialog extends BaseBottomSheetDialog<DialogAddTaskBinding, AddTaskViewModel> implements View.OnClickListener, AddTaskNavigator {

    private static final String TAG = "AddTaskBottomSheetDialo";
    private AddTaskViewModel addTaskViewModel;
    private Date targetTime = new Date();
    private Job job;
    private MUser mUser;

    public static AddTaskBottomSheetDialog newInstance() {

        Bundle args = new Bundle();

        AddTaskBottomSheetDialog fragment = new AddTaskBottomSheetDialog();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getBindingVariable() {
        return com.taskos.BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.dialog_add_task;
    }

    @Override
    public AddTaskViewModel getViewModel() {
        addTaskViewModel = new ViewModelProvider(this, ViewModelProviderFactory.getInstance()).get(AddTaskViewModel.class);
        return addTaskViewModel;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        addTaskViewModel.setNavigator(this);
        getViewDataBinding().tvCancel.setOnClickListener(this);
        getViewDataBinding().tvSaveTask.setOnClickListener(this);
        getViewDataBinding().rlTaskCompletion.setOnClickListener(this);
        getViewDataBinding().rlAssignUserName.setOnClickListener(this);
        getViewDataBinding().rlJob.setOnClickListener(this);
        getViewDataBinding().edtDateTargetCompletion.setText(CalenderUtils.formatDate(targetTime, CalenderUtils.TIMESTAMP_FORMAT));

        getViewDataBinding().switchCoworker.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                getViewDataBinding().rlAssignUserName.setVisibility(View.VISIBLE);
                getViewDataBinding().viewAssignName.setVisibility(View.VISIBLE);
            } else {
                getViewDataBinding().rlAssignUserName.setVisibility(View.GONE);
                getViewDataBinding().viewAssignName.setVisibility(View.GONE);
            }
        });
        getViewDataBinding().switch1.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                getViewDataBinding().rlJob.setVisibility(View.VISIBLE);
                getViewDataBinding().viewLineJob.setVisibility(View.VISIBLE);
            } else {
                getViewDataBinding().rlJob.setVisibility(View.GONE);
                getViewDataBinding().viewLineJob.setVisibility(View.GONE);
            }
        });

        //if user is not selected then current user set
        addTaskViewModel.getCurrentUser().observe(getViewLifecycleOwner(), mUser -> {
            this.mUser = mUser;
            getViewDataBinding().tvAssignUserName.setText(mUser.getUserFirstName().concat(" ").concat(mUser.getUserLastName()));
            getViewDataBinding().switchCoworker.setChecked(true);
        });
        getViewDataBinding().tvJob.setText("Personal");
        getViewDataBinding().switch1.setChecked(true);
    }

    public void show(FragmentManager fragmentManager) {
        super.show(fragmentManager, TAG);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_cancel:
                dismissDialog(TAG);
                break;

            case R.id.rlAssignUserName:
                EmployeesBottomSheetDialog.newInstance(employee -> {
                    mUser = employee;
                    getViewDataBinding().tvAssignUserName.setText(employee.getUserFirstName().concat(" ").concat(employee.getUserLastName()));
                }).show(getChildFragmentManager());
                break;

            case R.id.rlJob:
                SelectJobsBottomSheetDialog.newInstance(job -> {
                    this.job = job.getJob();
                    getViewDataBinding().tvJob.setText(job.getJob().getName());
                }).show(getChildFragmentManager());
                break;

            case R.id.tvSaveTask:
                if (isNetworkConnected()) {
                    if (verifyInputs()) {
                        showLoading();
                        getViewDataBinding().tvSaveTask.setClickable(false);
                        ToDo toDo = new ToDo();
                        toDo.setUuid(UUID.randomUUID().toString());
                        toDo.setTask(getViewDataBinding().edtTaskToComplete.getText().toString());
                        toDo.setCompletionStatus(AppConstants.kCompletionStatusArray[0]);
                        toDo.setDateEntered(targetTime);
                        toDo.setDateCompleted(targetTime);
                        toDo.setCompletedBy(mUser.getUserFirstName().concat(" ").concat(mUser.getUserLastName()));
                        toDo.setCompletedByUuid(mUser.getUuid());
                        toDo.setAssignedBy(getViewDataBinding().tvAssignUserName.getText().toString());
                        toDo.setOwner(mUser.getCompanyUuid());
                        toDo.setNotes(getViewDataBinding().edtTaskNote.getText().toString());

                        //if job is not selected then set job as personal
                        toDo.setCustomerUuid(job == null ? "" : job.getCustomerId());
                        toDo.setJobUuid(job == null ? "" : job.getUuid());
                        toDo.setJobName(job == null ? "Personal" : getViewDataBinding().tvJob.getText().toString());
                        addTaskViewModel.doAddTask(toDo, mUser.getToken());
                    }
                } else {
                    showToast(getString(R.string.noInternet));
                }
                break;

            case R.id.rlTaskCompletion:
                if (getViewDataBinding().dateTimePicker.getVisibility() == View.VISIBLE) {
                    getViewDataBinding().view.setVisibility(View.GONE);
                    getViewDataBinding().dateTimePicker.setVisibility(View.GONE);
                } else {
                    getViewDataBinding().view.setVisibility(View.VISIBLE);
                    getViewDataBinding().dateTimePicker.setVisibility(View.VISIBLE);
                }
                getViewDataBinding().dateTimePicker.addOnDateChangedListener((displayed, date) -> {
                    targetTime = date;
                    getViewDataBinding().edtDateTargetCompletion.setText(CalenderUtils.formatDate(date, CalenderUtils.TIMESTAMP_FORMAT));
                });
                break;
        }
    }

    private boolean verifyInputs() {
        if (getViewDataBinding().edtTaskToComplete.getText().toString().isEmpty()) {
            showToast(getString(R.string.alert_complete_task));
            return false;
        } else if (getViewDataBinding().tvJob.getText().toString().isEmpty()) {
            showToast(getString(R.string.alert_select_job));
            return false;
        } else if (getViewDataBinding().tvAssignUserName.getText().toString().isEmpty()) {
            showToast(getString(R.string.alert_select_employee));
            return false;
        } else return true;
    }


    @Override
    public void openDashboard() {
        hideLoading();
        getViewDataBinding().tvSaveTask.setClickable(true);
        dismissDialog(TAG);
    }

    @Override
    public void onSaveError() {
        hideLoading();
        getViewDataBinding().tvSaveTask.setClickable(true);
    }
}
