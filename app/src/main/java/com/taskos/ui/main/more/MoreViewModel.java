package com.taskos.ui.main.more;

import com.taskos.data.DataManager;
import com.taskos.ui.base.BaseViewModel;

public class MoreViewModel extends BaseViewModel<MoreNavigator> {

    public MoreViewModel(DataManager dataManager) {
        super(dataManager);
    }

}