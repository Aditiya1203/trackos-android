package com.taskos.ui.main.network.search;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.taskos.data.DataManager;
import com.taskos.ui.auth.MUser;
import com.taskos.ui.base.BaseNavigator;
import com.taskos.ui.base.BaseViewModel;

import java.util.ArrayList;
import java.util.List;

public class SearchToNetworkViewModel extends BaseViewModel<BaseNavigator> {

    private static final String TAG = "SearchToNetworkViewMode";
    private List<MUser> mUsers = new ArrayList<>();
    private MutableLiveData<List<MUser>> mutableLiveDataUsers = new MutableLiveData<>();

    public SearchToNetworkViewModel(DataManager dataManager) {
        super(dataManager);
    }

    MutableLiveData<List<MUser>> getUserList(String search) {
        mUsers.clear();
        getDataManager().getFirestoreInstance().collection("MUser").addSnapshotListener((queryDocumentSnapshots, e) -> {
            if (e != null) {
                Log.e(TAG, "onEvent: Listen failed.", e);
                return;
            }
            if (queryDocumentSnapshots != null) {
                mUsers.clear();
                for (QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                    MUser mUser = documentSnapshot.toObject(MUser.class);
                    String name = mUser.getUserFirstName().concat(" ").concat(mUser.getUserLastName()).toLowerCase();
                    if (name.contains(search.toLowerCase())) {
                        mUsers.add(mUser);
                    }
                }
                mutableLiveDataUsers.setValue(mUsers);
            }
        });
        return mutableLiveDataUsers;
    }
}