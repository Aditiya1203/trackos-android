package com.taskos.ui.main.network;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.taskos.data.DataManager;
import com.taskos.data.model.api.SharedJob;
import com.taskos.ui.auth.MUser;
import com.taskos.ui.base.BaseViewModel;
import com.taskos.ui.company.Company;
import com.taskos.util.AppLogger;

import java.util.ArrayList;
import java.util.List;

public class MyNetworkViewModel extends BaseViewModel<MyNetworkNavigator> {

    private static final String TAG = "MyNetworkViewModel";
    private List<MUser> mUsers = new ArrayList<>();
    private MutableLiveData<List<MUser>> mutableLiveDataUsers = new MutableLiveData<>();

    private List<SharedJob> sharedJobList = new ArrayList<>();
    private MutableLiveData<List<SharedJob>> mutableLiveDataSharedJob = new MutableLiveData<>();

    //notification data
    private MutableLiveData<SharedJob> sharedJobMutableLiveData = new MutableLiveData<>();

    public MyNetworkViewModel(DataManager dataManager) {
        super(dataManager);
        AppLogger.e("UUID", getDataManager().getUUID());
    }

    MutableLiveData<List<MUser>> getMyNetworkList() {
        mUsers.clear();
        getDataManager().getFirestoreInstance().collection("MUser").document(getDataManager().getUUID()).addSnapshotListener((documentSnapshot, e) -> {
            if (e != null) {
                Log.e(TAG, "onEvent: Listen failed.", e);
                return;
            }
            if (documentSnapshot != null) {
                mUsers.clear();
                MUser mUser = documentSnapshot.toObject(MUser.class);
                if (mUser != null) {
                    for (String mUserUUID : mUser.getUserNetwork()) {
                        getDataManager().getFirestoreInstance().collection("MUser").document(mUserUUID).addSnapshotListener((documentSnapshot1, e1) -> {
                            if (e1 != null) {
                                Log.e(TAG, "onEvent: Listen failed.", e1);
                                return;
                            }
                            if (documentSnapshot1 != null) {
                                MUser mUser1 = documentSnapshot1.toObject(MUser.class);
                                if (mUser1 != null) {
                                    mUsers.add(mUser1);
                                }
                                mutableLiveDataUsers.setValue(mUsers);
                            }
                        });
                    }
                }
            }
        });
        return mutableLiveDataUsers;
    }

    MutableLiveData<List<MUser>> getMyCoworkerList() {
        mUsers.clear();
        getDataManager().getFirestoreInstance().collection("Company").document(getDataManager().getCompanyID()).addSnapshotListener((documentSnapshot, e) -> {
            if (e != null) {
                Log.e(TAG, "onEvent: Listen failed.", e);
                return;
            }
            if (documentSnapshot != null) {
                mUsers.clear();
                Company company = documentSnapshot.toObject(Company.class);
                if (company != null) {
                    for (String mUserUUID : company.getEmployeeList()) {
                        getDataManager().getFirestoreInstance().collection("MUser").document(mUserUUID).addSnapshotListener((documentSnapshot1, e1) -> {
                            if (e1 != null) {
                                Log.e(TAG, "onEvent: Listen failed.", e1);
                                return;
                            }
                            if (documentSnapshot1 != null) {
                                MUser mUser1 = documentSnapshot1.toObject(MUser.class);
                                if (mUser1 != null) {
                                    mUsers.add(mUser1);
                                }
                                mutableLiveDataUsers.setValue(mUsers);
                            }
                        });
                    }
                }
            }
        });
        return mutableLiveDataUsers;
    }

    MutableLiveData<List<SharedJob>> getSharedJobList() {
        sharedJobList.clear();
        Query query = getDataManager().getFirestoreInstance().collection("SharedJob").whereEqualTo("sharedUser", getDataManager().getUUID());

        query.addSnapshotListener((documentSnapshot, e) -> {
            if (e != null) {
                Log.e(TAG, "onEvent: Listen failed.", e);
                return;
            }
            if (documentSnapshot != null) {
                sharedJobList.clear();
                for (QueryDocumentSnapshot doc : documentSnapshot) {
                    SharedJob sharedJob = doc.toObject(SharedJob.class);
                    sharedJobList.add(sharedJob);
                    mutableLiveDataSharedJob.setValue(sharedJobList);
                }

            }
        });
        return mutableLiveDataSharedJob;
    }


    MutableLiveData<SharedJob> getSharedJob(String uuid) {
        DocumentReference query = getDataManager().getFirestoreInstance().collection("SharedJob").document(uuid);

        query.addSnapshotListener((documentSnapshot, e) -> {
            if (e != null) {
                Log.e(TAG, "onEvent: Listen failed.", e);
                return;
            }
            if (documentSnapshot != null) {
                SharedJob sharedJob = documentSnapshot.toObject(SharedJob.class);
                if (sharedJob != null) {
                    sharedJobMutableLiveData.setValue(sharedJob);
                }
            }
        });
        return sharedJobMutableLiveData;
    }
}