package com.taskos.ui.main.myleads;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.taskos.R;
import com.taskos.data.model.api.Customer;
import com.taskos.ui.main.client.addclient.AddClientBottomSheetDialog;
import com.taskos.ui.main.estimate.EstimateBottomSheetDialog;
import com.taskos.ui.main.invoice.InvoiceBottomSheetDialog;
import com.taskos.ui.main.openjobs.addjob.AddJobBottomSheetDialog;
import com.taskos.ui.main.tasks.addtask.AddTaskBottomSheetDialog;

import java.util.ArrayList;
import java.util.List;
public class AllOpenfragment extends Fragment  implements TextWatcher {

    RecyclerView ll_openlist;
    EditText et_filteritems;
    TextView tv_cancel;
    OpenLeadsAdapter openLeadsAdapter;
    List<Customer> Open_list;
    int start_frage = 0;
    List<Customer> Open_listNofilter;

    FloatingActionMenu menu_float;
    FloatingActionButton fab_add_client;
    FloatingActionButton fab_add_estimate;
    FloatingActionButton fab_add_invoice;
    FloatingActionButton fab_add_job;
    FloatingActionButton fab_add_service;
    FloatingActionButton fab_add_task;

    public static AllOpenfragment newInstance(Bundle bundle) {

        if (bundle == null) {
            bundle = new Bundle();
        }
        AllOpenfragment fragment = new AllOpenfragment();
        fragment.setArguments(bundle);
        return fragment;

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.allopen_fragment, container, false);
        ll_openlist = v.findViewById(R.id.ll_openlist);
        et_filteritems = v.findViewById(R.id.et_filteritems);
        tv_cancel = v.findViewById(R.id.tv_cancel);

        menu_float = v.findViewById(R.id.menu_float);
        fab_add_client = v.findViewById(R.id.fab_add_client);
        fab_add_estimate = v.findViewById(R.id.fab_add_estimate);
        fab_add_invoice = v.findViewById(R.id.fab_add_invoice);
        fab_add_job = v.findViewById(R.id.fab_add_job);
        fab_add_service = v.findViewById(R.id.fab_add_service);
        fab_add_task = v.findViewById(R.id.fab_add_task);

        et_filteritems.addTextChangedListener(this);
        tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                et_filteritems.getText().clear();
                calldata();
            }
        });
        calldata();

        fab_add_client.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddClientBottomSheetDialog.newInstance().show(getChildFragmentManager());
                menu_float.close(true);
            }
        });

        fab_add_estimate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EstimateBottomSheetDialog.newInstance().show(getChildFragmentManager());
                menu_float.close(true);
            }
        });

        fab_add_invoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InvoiceBottomSheetDialog.newInstance().show(getChildFragmentManager());
                menu_float.close(true);
            }
        });

        fab_add_job.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddJobBottomSheetDialog.newInstance().show(getChildFragmentManager());
                menu_float.close(true);
            }
        });

        fab_add_service.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                menu_float.close(true);
            }
        });

        fab_add_task.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddTaskBottomSheetDialog.newInstance().show(getChildFragmentManager());
                menu_float.close(true);
            }
        });

        return v;
    }

    public void calldata(){
        FirebaseFirestore db = FirebaseFirestore.getInstance();

        db.collection("Customers")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            if (task.getResult() != null) {
                                Open_list = task.getResult().toObjects(Customer.class);
                                Open_listNofilter = task.getResult().toObjects(Customer.class);
                                openLeadsAdapter = new OpenLeadsAdapter(Open_list, getActivity());
                                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
                                ll_openlist.setLayoutManager(linearLayoutManager);
                                ll_openlist.setAdapter(openLeadsAdapter);
                                ll_openlist.setHasFixedSize(true);
                                for (Customer downloadInfo : Open_list) {
                                    Log.e("", downloadInfo.getCustomerStatus() + "");
                                }
                            }
                        } else {
                            Log.w("TAG", "Error getting documents.", task.getException());
                        }
                    }
                });
    }
    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
    }

    @Override
    public void afterTextChanged(Editable editable) {
        if (editable.length() > 0 && !editable.equals("") && !editable.equals(" ")) {
            tv_cancel.setVisibility(View.VISIBLE);
            start_frage = 1;
            if (Open_list.size() == 0) {
                tv_cancel.setVisibility(View.GONE);
                this.Open_list.addAll(Open_listNofilter);
                openLeadsAdapter.notifyDataSetChanged();
            }
            ArrayList<Customer> data_form = new ArrayList<>();
            data_form.addAll(this.Open_list);
            int x = 0;
            for (Customer form : data_form) {
                String ext_Val = editable.toString().toLowerCase();
                String database_val = form.getName().toLowerCase();
                if (!database_val.startsWith(ext_Val)) {
                    this.Open_list.remove(form);
                }
                x++;
            }
            openLeadsAdapter.notifyDataSetChanged();
        } else if (editable.length() == 0) {
            if (start_frage > 0) {
                if (this.Open_list != null) {
                    this.Open_list.clear();
                    start_frage = 0;

                    this.Open_list.addAll(Open_listNofilter);
                    openLeadsAdapter.notifyDataSetChanged();
                }
            }
        }
    }



}
