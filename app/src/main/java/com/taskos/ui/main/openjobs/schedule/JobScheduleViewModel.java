package com.taskos.ui.main.openjobs.schedule;

import android.app.Activity;
import android.content.Intent;
import android.provider.CalendarContract;

import com.taskos.R;
import com.taskos.data.DataManager;
import com.taskos.data.model.api.Job;
import com.taskos.ui.base.BaseViewModel;

public class JobScheduleViewModel extends BaseViewModel<JobScheduleNavigator> {

    public JobScheduleViewModel(DataManager dataManager) {
        super(dataManager);
    }

    void doScheduleJob(Job jobDetail) {
        getDataManager().getFirestoreInstance().collection("Job").document(jobDetail.getUuid()).
                set(jobDetail).addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                getNavigator().onJobScheduleSuccess();
            } else getNavigator().onSaveError();
        });
    }

    void addEventToCalendar(Activity activity, Job jobDetail) {
        Intent intent = new Intent(Intent.ACTION_INSERT);
        intent.setType("vnd.android.cursor.item/event");

        intent.putExtra(CalendarContract.Events.EVENT_COLOR, R.color.light_pink);
        intent.putExtra(CalendarContract.Events.AVAILABILITY, CalendarContract.Events.AVAILABILITY_BUSY);

        intent.putExtra(CalendarContract.Events.TITLE, "Job: ".concat(jobDetail.getName()));
        intent.putExtra(CalendarContract.Events.DESCRIPTION, "");


        String address = jobDetail.getStreetAddress().concat(" ").concat(jobDetail.getCity()).concat(", ").concat(jobDetail.getState()).concat(" ").concat(jobDetail.getZipcode());

        intent.putExtra(CalendarContract.Events.EVENT_LOCATION, address);

        activity.startActivity(intent);
    }
}