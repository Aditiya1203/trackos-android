package com.taskos.ui.main.tasks.detail;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.google.firebase.firestore.DocumentReference;
import com.taskos.data.DataManager;
import com.taskos.data.model.api.ToDo;
import com.taskos.ui.base.BaseViewModel;

public class TasksDetailViewModel extends BaseViewModel<TasksDetailNavigator> {
    private static final String TAG = "TasksDetailViewModel";
    private MutableLiveData<ToDo> mutableLiveData = new MutableLiveData<>();
    //private ListenerRegistration userListener;

    public TasksDetailViewModel(DataManager dataManager) {
        super(dataManager);
    }

    MutableLiveData<ToDo> getTaskDetail(String taskKey) {
        DocumentReference query = getDataManager().getFirestoreInstance().collection("ToDo").document(taskKey);

        query.addSnapshotListener((documentSnapshot, e) -> {
            if (e != null) {
                Log.e(TAG, "onEvent: Listen failed.", e);
                return;
            }

            if (documentSnapshot != null) {
                ToDo toDo = documentSnapshot.toObject(ToDo.class);
                if (toDo != null) {
                    mutableLiveData.setValue(toDo);
                }
            }
        });

        return mutableLiveData;
    }

    void changeTaskStatus(ToDo toDo) {
        getDataManager().getFirestoreInstance().collection("ToDo").document(toDo.getUuid()).set(toDo);

        /*.addOnSuccessListener(aVoid -> {
            userListener = getDataManager().getFirestoreInstance().collection("MUser").document(toDo.getCompletedByUuid()).addSnapshotListener((documentSnapshot, e) -> {
                userListener.remove();
                if (e != null) {
                    Log.e(TAG, "onEvent: Listen failed.", e);
                    return;
                }

                if (documentSnapshot != null) {
                    MUser mUser = documentSnapshot.toObject(MUser.class);
                    if (mUser!=null){
                        ArrayList<String> tokenList = new ArrayList<>();
                        tokenList.add(getDataManager().getDeviceToken());
                        tokenList.addAll(mUser.getToken());
                        FcmNotificationBuilder.newInstance().setTitle("You Have a new Customer Appointment")
                                .setBody("New Task Assigned by ".concat(toDo.getAssignedBy()))
                                .setSelectedView(FcmNotificationBuilder.VIEW_TASK)
                                .setTabView(FcmNotificationBuilder.TAB_MY_TASK)
                                .setSelectedUUID(toDo.getUuid())
                                .setReceiverFCMTokenList(AppUtils.removeDuplicates(tokenList))
                                .sendNotification();
                    }
                }
            });
        });*/
    }
}