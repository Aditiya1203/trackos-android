package com.taskos.ui.main.tasks.detail;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.ViewModelProvider;

import com.taskos.R;
import com.taskos.ViewModelProviderFactory;
import com.taskos.data.model.api.ToDo;
import com.taskos.databinding.FragmentTaskDetailsBinding;
import com.taskos.ui.base.BaseFragment;
import com.taskos.ui.main.tasks.detail.edit.EditTaskDetailsBottomSheetDialog;
import com.taskos.util.AppConstants;
import com.taskos.util.CalenderUtils;


public class TaskDetailsFragment extends BaseFragment<FragmentTaskDetailsBinding, TasksDetailViewModel>
        implements TasksDetailNavigator, View.OnClickListener {

    private TasksDetailViewModel tasksDetailViewModel;
    private String taskKey = "";
    private ToDo toDo;


    public static TaskDetailsFragment newInstance(String taskKey) {
        Bundle args = new Bundle();
        TaskDetailsFragment fragment = new TaskDetailsFragment();
        args.putString("TaskKey", taskKey);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getViewDataBinding().tvTitle.setText(R.string.title_tasks);
        getViewDataBinding().tvTitle.setOnClickListener(this);
        getViewDataBinding().tvEdit.setOnClickListener(this);
        getViewDataBinding().imgBack.setOnClickListener(this);
        getViewDataBinding().rlMarkComplete.setOnClickListener(this);

        tasksDetailViewModel.getTaskDetail(taskKey).observe(getViewLifecycleOwner(), toDo -> {
            this.toDo = toDo;
            getViewDataBinding().tvClientName.setText(toDo.getJobName());
            getViewDataBinding().tvTaskName.setText(toDo.getTask());
            getViewDataBinding().tvNote.setText(toDo.getNotes());
            getViewDataBinding().tvTaskName.setText(toDo.getTask());
            getViewDataBinding().etAssignedUser.setText(toDo.getAssignedBy());
            getViewDataBinding().etTargetCompletion.setText(CalenderUtils.formatDate(toDo.getDateCompleted(), CalenderUtils.CUSTOM_TIMESTAMP_FORMAT_AM));
            getViewDataBinding().etTaskentered.setText(CalenderUtils.formatDate(toDo.getDateEntered(), CalenderUtils.CUSTOM_TIMESTAMP_FORMAT_AM));

            if (toDo.getCompletionStatus().equals(AppConstants.kCompletionStatusArray[1])) {
                getViewDataBinding().tvMarkOpenComplete.setText(getResources().getString(R.string.markOpen));
                getViewDataBinding().imgVerify.setColorFilter(ContextCompat.getColor(getBaseActivity(),
                        R.color.green), android.graphics.PorterDuff.Mode.SRC_IN);
            } else {
                getViewDataBinding().tvMarkOpenComplete.setText(getResources().getString(R.string.mark_complete));
                getViewDataBinding().imgVerify.setColorFilter(ContextCompat.getColor(getBaseActivity(),
                        R.color.black), android.graphics.PorterDuff.Mode.SRC_IN);
            }
        });
    }

    @Override
    public int getBindingVariable() {
        return com.taskos.BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_task_details;
    }

    @Override
    public TasksDetailViewModel getViewModel() {
        tasksDetailViewModel = new ViewModelProvider(this, ViewModelProviderFactory.getInstance()).get(TasksDetailViewModel.class);
        return tasksDetailViewModel;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        tasksDetailViewModel.setNavigator(this);
        if (getArguments() != null) {
            taskKey = getArguments().getString("TaskKey");
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.tvTitle:
            case R.id.img_back:
                getBaseActivity().onBackPressed();
                break;

            case R.id.tv_edit:
                EditTaskDetailsBottomSheetDialog.newInstance(taskKey).show(getChildFragmentManager());
                break;

            case R.id.rl_mark_complete:
                if (isNetworkConnected()) {
                    if (toDo.getCompletionStatus().equals(AppConstants.kCompletionStatusArray[1])) {
                        toDo.setCompletionStatus(AppConstants.kCompletionStatusArray[0]);
                        tasksDetailViewModel.changeTaskStatus(toDo);
                        getViewDataBinding().tvMarkOpenComplete.setText(getResources().getString(R.string.markOpen));
                        getViewDataBinding().imgVerify.setColorFilter(ContextCompat.getColor(getBaseActivity(),
                                R.color.green), android.graphics.PorterDuff.Mode.SRC_IN);
                    } else {
                        toDo.setCompletionStatus(AppConstants.kCompletionStatusArray[1]);
                        tasksDetailViewModel.changeTaskStatus(toDo);
                        getViewDataBinding().tvMarkOpenComplete.setText(getResources().getString(R.string.mark_complete));
                        getViewDataBinding().imgVerify.setColorFilter(ContextCompat.getColor(getBaseActivity(),
                                R.color.black), android.graphics.PorterDuff.Mode.SRC_IN);
                    }
                } else {
                    showToast(getString(R.string.noInternet));
                }
                break;
        }
    }
}