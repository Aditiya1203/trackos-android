package com.taskos.ui.main.network.add;


/**
 * Created by Hemant Sharma on 23-02-20.
 */
public interface AddToNetworkNavigator {
    void updateNetworkUI(boolean isAccept);

    void networkInvitationCanceled();

    void updateEmployeeUI(boolean isAccept);

    void employeeInvitationCanceled();

    void onSaveError();
}
