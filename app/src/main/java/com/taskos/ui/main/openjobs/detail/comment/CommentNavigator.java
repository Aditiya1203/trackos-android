package com.taskos.ui.main.openjobs.detail.comment;


/**
 * Created by Hemant Sharma on 23-02-20.
 */
public interface CommentNavigator {
    void clearInput();

    void enableInput();
}
