package com.taskos.ui.main.client.detail;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.taskos.BR;
import com.taskos.R;
import com.taskos.TaskOS;
import com.taskos.ViewModelProviderFactory;
import com.taskos.data.model.api.Customer;
import com.taskos.databinding.FragmentClientDetailBinding;
import com.taskos.ui.auth.MUser;
import com.taskos.ui.base.BaseFragment;
import com.taskos.ui.emailCustomer.EmailCustomerActivity;
import com.taskos.ui.main.client.detail.comment.CommentActivity;
import com.taskos.ui.main.client.detail.status_sold.SoldBottomSheetDialog;
import com.taskos.ui.main.employee.EmployeesBottomSheetDialog;
import com.taskos.util.AppConstants;
import com.taskos.util.AppUtils;
import com.taskos.util.CalenderUtils;
import com.taskos.util.CommonUtils;
import com.taskos.util.PermissionUtils;
import com.taskos.util.UsPhoneNumberFormatter;

import java.lang.ref.WeakReference;
import java.util.Date;


public class ClientDetailFragment extends BaseFragment<FragmentClientDetailBinding, ClientDetailViewModel> implements ClientDetailNavigator, View.OnClickListener, OnMapReadyCallback {

    private ClientDetailViewModel clientDetailViewModel;
    private String phone = "", email = "";
    private String customerUUID = "";
    private Date appointmentTime, appointmentTimeSet;
    private Customer customer;
    private MUser mUser;
    private String customerStatus = AppConstants.kStatusArray[0];

    //map params
    private MapView mapView;
    private GoogleMap googleMap;

    private Bundle mapViewBundle;
    private LatLng latLng;

    public static ClientDetailFragment newInstance(String customerUUID) {

        Bundle args = new Bundle();

        ClientDetailFragment fragment = new ClientDetailFragment();
        args.putString("CustomerUUID", customerUUID);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_client_detail;
    }

    @Override
    public ClientDetailViewModel getViewModel() {
        clientDetailViewModel = new ViewModelProvider(this, ViewModelProviderFactory.getInstance()).get(ClientDetailViewModel.class);
        return clientDetailViewModel;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        clientDetailViewModel.setNavigator(this);
        if (getArguments() != null) {
            customerUUID = getArguments().getString("CustomerUUID");
        }

        if (savedInstanceState != null) {
            mapViewBundle = savedInstanceState.getBundle(AppConstants.MAP_VIEW_BUNDLE_KEY);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        TextView tvTitle = view.findViewById(R.id.tvTitle);
        tvTitle.setText(R.string.clients);
        setOnClickListener(tvTitle, view.findViewById(R.id.img_back)
                , getViewDataBinding().tvAddCalendar
                , getViewDataBinding().tvSold
                , getViewDataBinding().rlComments
                , getViewDataBinding().tvCall
                , getViewDataBinding().tvEmail
                , getViewDataBinding().llDirection
                , getViewDataBinding().tvEditAppointment
                , getViewDataBinding().tvSaveAppointment
                , getViewDataBinding().tvAppointTime
                , getViewDataBinding().tvAppointSet
                , getViewDataBinding().tvSaveContact
                , getViewDataBinding().tvEditCustInfo
                , getViewDataBinding().llSalesPerson
                , getViewDataBinding().tvEditSalesPerson
                , getViewDataBinding().tvSaveSalesPerson
                , getViewDataBinding().tvEditCustLoc
                , getViewDataBinding().tvSaveLoc
                , getViewDataBinding().rlSubscribe
                , getViewDataBinding().tvEditCustStatus
                , getViewDataBinding().tvSaveStatus);

        getViewDataBinding().loopWheelView.setOnItemSelectedListener((picker, data, position) -> customerStatus = data.toString());
        getViewDataBinding().loopWheelView.setData(AppConstants.getkStatusArray());


        clientDetailViewModel.getClientDetail(customerUUID).observe(getViewLifecycleOwner(), customer -> {
            if (customer != null) {
                this.customer = customer;
                customerStatus = customer.getCustomerStatus();
                getViewDataBinding().tvSalesPersonName.setText(customer.getSalesperson());
                getViewDataBinding().tvNameMap.setText(customer.getName());
                getViewDataBinding().tvName.setText(customer.getName());
                getViewDataBinding().etName.setText(customer.getName());
                getViewDataBinding().etProductInterest.setText(customer.getProductOfInterest());
                appointmentTime = customer.getAppointmentTime();
                appointmentTimeSet = customer.getAppointmentEntered();
                getViewDataBinding().tvAppointTime.setText(CalenderUtils.formatDate(customer.getAppointmentTime(), CalenderUtils.CUSTOM_TIMESTAMP_FORMAT).concat(" at ").concat(CalenderUtils.formatDate(customer.getAppointmentTime(), CalenderUtils.TIME_FORMAT_AM)));
                getViewDataBinding().tvAppointSet.setText(CalenderUtils.formatDate(customer.getAppointmentEntered(), CalenderUtils.CUSTOM_TIMESTAMP_FORMAT).concat(" at ").concat(CalenderUtils.formatDate(customer.getAppointmentEntered(), CalenderUtils.TIME_FORMAT_AM)));
                getViewDataBinding().etLeadSource.setText(customer.getSource());
                getViewDataBinding().etStatus.setText(customer.getCustomerStatus());

                getViewDataBinding().etStreet.setText(customer.getStreetAddress());
                getViewDataBinding().etCity.setText(customer.getCity());
                getViewDataBinding().etState.setText(customer.getState());
                getViewDataBinding().etZipCode.setText(customer.getZipcode());
                getViewDataBinding().tvStreet.setText(customer.getStreetAddress());
                getViewDataBinding().tvAddress.setText(customer.getCity().concat(", ").concat(customer.getState()).concat(" ").concat(customer.getZipcode()));
                phone = customer.getPhone();
                email = customer.getEmail();

                getViewDataBinding().etPhone.setText(CommonUtils.formatStringAsPhoneNumber(customer.getPhone()));
                getViewDataBinding().etEmail.setText(customer.getEmail());

                if (customer.getListOrganizer().equals(AppConstants.kStatusListOrganiser[0])) {
                    getViewDataBinding().tvSold.setText(R.string.change_to_sold);
                } else {
                    getViewDataBinding().tvSold.setText(R.string.add_new_project_sold);
                }

                if (customer.getSubscribers().contains(TaskOS.getInstance().getDataManager().getUUID())) {
                    getViewDataBinding().imgSubscribe.setColorFilter(ContextCompat.getColor(getBaseActivity(),
                            R.color.green), android.graphics.PorterDuff.Mode.SRC_IN);
                    getViewDataBinding().tvSubscribe.setText(R.string.subscribed);
                } else {
                    getViewDataBinding().imgSubscribe.setColorFilter(ContextCompat.getColor(getBaseActivity(),
                            R.color.gray), android.graphics.PorterDuff.Mode.SRC_IN);
                    getViewDataBinding().tvSubscribe.setText(R.string.subscribe_to_notifications);
                }

                String address = customer.getStreetAddress().concat(" ").concat(customer.getCity()).concat(", ").concat(customer.getState()).concat(" ").concat(customer.getZipcode());
                latLng = getLocationFromAddress(address);
                if (latLng != null) {
                    updateMapUI(latLng);
                }
            }
        });

        UsPhoneNumberFormatter addLineNumberFormatter = new UsPhoneNumberFormatter(
                new WeakReference<EditText>(getViewDataBinding().etPhone));
        getViewDataBinding().etPhone.addTextChangedListener(addLineNumberFormatter);

        // when the map is ready to be used.
        mapView = view.findViewById(R.id.map);
        assert mapView != null;
        mapView.onCreate(mapViewBundle);
        mapView.onResume();
        mapView.getMapAsync(this);
    }

    private void setOnClickListener(View... views) {
        for (View view : views) {
            view.setOnClickListener(this);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tvTitle:
            case R.id.img_back:
                getBaseActivity().onBackPressed();
                break;

            case R.id.rl_subscribe:
                if (isNetworkConnected()) {
                    String value = getViewDataBinding().tvSubscribe.getText().toString();
                    if (value.equals(getString(R.string.subscribed))) {
                        customer.getSubscribers().remove(TaskOS.getInstance().getDataManager().getUUID()); //customer.getUuid()
                        customer.setSubscribers(AppUtils.removeDuplicates(customer.getSubscribers()));
                        clientDetailViewModel.doSaveAppointmentInfo(customerUUID, customer);
                        getViewDataBinding().imgSubscribe.setColorFilter(ContextCompat.getColor(getBaseActivity(),
                                R.color.gray), android.graphics.PorterDuff.Mode.SRC_IN);
                        getViewDataBinding().tvSubscribe.setText(R.string.subscribe_to_notifications);
                    } else {
                        customer.getSubscribers().add(TaskOS.getInstance().getDataManager().getUUID()); //customer.getUuid()
                        clientDetailViewModel.doSaveAppointmentInfo(customerUUID, customer);
                        getViewDataBinding().imgSubscribe.setColorFilter(ContextCompat.getColor(getBaseActivity(),
                                R.color.green), android.graphics.PorterDuff.Mode.SRC_IN);
                        getViewDataBinding().tvSubscribe.setText(R.string.subscribed);
                    }
                } else {
                    showToast(getString(R.string.noInternet));
                }
                break;

            case R.id.tv_add_calendar:
                getViewModel().addEventToCalendar(getBaseActivity(), customer);
                break;

            case R.id.tv_sold:
                SoldBottomSheetDialog.newInstance(customerUUID).show(getChildFragmentManager());
                break;

            case R.id.tv_edit_cust_status:
                if (getViewDataBinding().tvEditCustStatus.getText().toString().equals("Edit")) {
                    getViewDataBinding().tvEditCustStatus.setText(R.string.cancel);
                    enableDisableStatusView(true);
                } else {
                    getViewDataBinding().tvEditCustStatus.setText(R.string.edit);
                    enableDisableStatusView(false);
                }
                break;

            case R.id.tv_save_status:
                if (isNetworkConnected()) {
                    enableDisableStatusView(false);
                    customer.setCustomerStatus(customerStatus);
                    if (AppConstants.getkUnsoldCustomerArray().contains(customerStatus)) {
                        customer.setListOrganizer(AppConstants.kStatusListOrganiser[1]);
                    } else if (AppConstants.getkClosedCustomerArray().contains(customerStatus)) {
                        customer.setListOrganizer(AppConstants.kStatusListOrganiser[2]);
                    } else {
                        customer.setListOrganizer(AppConstants.kStatusListOrganiser[0]);
                    }

                    if (customerStatus.equalsIgnoreCase(AppConstants.kStatusArray[1])) {
                        customer.setAppointmentSet(true);
                    }

                    clientDetailViewModel.doSaveAppointmentInfo(customerUUID, customer);
                    getViewDataBinding().tvEditCustStatus.setText(R.string.edit);
                } else {
                    showToast(getString(R.string.noInternet));
                }
                break;

            case R.id.rl_comments:
                Intent intent = CommentActivity.newIntent(getBaseActivity());
                intent.putExtra("CustomerUUID", customerUUID);
                startActivity(intent);
                break;

            case R.id.tv_call:
                if (PermissionUtils.requestCallPermission(getBaseActivity())) {
                    intent = new Intent(Intent.ACTION_CALL);

                    intent.setData(Uri.parse("tel:" + phone));
                    startActivity(intent);
                }
                break;

            case R.id.tv_email:
                Intent emailIntent = new Intent(getContext(), EmailCustomerActivity.class);
                emailIntent.putExtra("Email", email);
                startActivity(emailIntent);
                break;

            case R.id.ll_direction:
                //"geo:22.7196, 75.8577?q=22.7196, 75.8577(Indore)"
                String loc = "geo:".concat(String.valueOf(latLng.latitude)).concat(", ").concat(String.valueOf(latLng.longitude)).concat("?q=").concat(String.valueOf(latLng.latitude)).concat(", ").concat(String.valueOf(latLng.longitude)).concat("(").concat(customer.getCity()).concat(")");
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(loc));
                /*Uri gmmIntentUri = Uri.parse("geo: 22.7196, 75.8577");
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);*/
                mapIntent.setPackage("com.google.android.apps.maps");
                if (mapIntent.resolveActivity(getBaseActivity().getPackageManager()) != null) {
                    startActivity(mapIntent);
                }
                break;

            case R.id.tv_edit_appointment:
                if (getViewDataBinding().tvEditAppointment.getText().toString().equals("Edit")) {
                    getViewDataBinding().tvEditAppointment.setText(R.string.cancel);
                    enableDisableAppointmentView(true);
                } else {
                    enableDisableAppointmentView(false);
                    getViewDataBinding().tvEditAppointment.setText(R.string.edit);
                    getViewDataBinding().etName.setText(customer.getName());
                    appointmentTime = customer.getAppointmentTime();
                    appointmentTimeSet = customer.getAppointmentEntered();
                    getViewDataBinding().tvAppointTime.setText(CalenderUtils.formatDate(customer.getAppointmentTime(), CalenderUtils.CUSTOM_TIMESTAMP_FORMAT).concat(" at ").concat(CalenderUtils.formatDate(customer.getAppointmentTime(), CalenderUtils.TIME_FORMAT_AM)));
                    getViewDataBinding().tvAppointSet.setText(CalenderUtils.formatDate(customer.getAppointmentEntered(), CalenderUtils.CUSTOM_TIMESTAMP_FORMAT).concat(" at ").concat(CalenderUtils.formatDate(customer.getAppointmentEntered(), CalenderUtils.TIME_FORMAT_AM)));
                    getViewDataBinding().etLeadSource.setText(customer.getSource());
                    getViewDataBinding().etProductInterest.setText(customer.getProductOfInterest());
                }
                break;

            case R.id.tv_save_appointment:
                if (isNetworkConnected()) {
                    if (verifyInput()) {
                        enableDisableAppointmentView(false);
                        customer.setName(getViewDataBinding().etName.getText().toString());
                        customer.setAppointmentTime(appointmentTime);
                        customer.setAppointmentEntered(appointmentTimeSet);
                        customer.setSource(getViewDataBinding().etLeadSource.getText().toString());
                        customer.setProductOfInterest(getViewDataBinding().etProductInterest.getText().toString());

                        clientDetailViewModel.doSaveAppointmentInfo(customerUUID, customer);
                        getViewDataBinding().tvEditAppointment.setText(R.string.edit);
                    }
                } else {
                    showToast(getString(R.string.noInternet));
                }
                break;

            case R.id.tv_appoint_time:
                if (getViewDataBinding().dateAppointTimePicker.getVisibility() == View.VISIBLE) {
                    getViewDataBinding().viewTime.setVisibility(View.GONE);
                    getViewDataBinding().dateAppointTimePicker.setVisibility(View.GONE);
                } else {
                    getViewDataBinding().viewTime.setVisibility(View.VISIBLE);
                    getViewDataBinding().dateAppointTimePicker.setVisibility(View.VISIBLE);
                }

                getViewDataBinding().dateAppointTimePicker.addOnDateChangedListener((displayed, date) -> {
                    appointmentTime = date;
                    getViewDataBinding().tvAppointTime.setText(CalenderUtils.formatDate(date, CalenderUtils.TIMESTAMP_FORMAT));
                });
                break;

            case R.id.tv_appoint_set:
                if (getViewDataBinding().dateAppointSetPicker.getVisibility() == View.VISIBLE) {
                    getViewDataBinding().viewSet.setVisibility(View.GONE);
                    getViewDataBinding().dateAppointSetPicker.setVisibility(View.GONE);
                } else {
                    getViewDataBinding().viewSet.setVisibility(View.VISIBLE);
                    getViewDataBinding().dateAppointSetPicker.setVisibility(View.VISIBLE);
                }

                getViewDataBinding().dateAppointSetPicker.addOnDateChangedListener((displayed, date) -> {
                    appointmentTimeSet = date;
                    getViewDataBinding().tvAppointSet.setText(CalenderUtils.formatDate(date, CalenderUtils.TIMESTAMP_FORMAT));
                });
                break;

            case R.id.tv_edit_cust_info:
                if (getViewDataBinding().tvEditCustInfo.getText().toString().equals("Edit")) {
                    getViewDataBinding().tvEditCustInfo.setText(R.string.cancel);
                    enableDisableContactView(true);
                } else {
                    getViewDataBinding().tvEditCustInfo.setText(R.string.edit);
                    enableDisableContactView(false);
                    getViewDataBinding().etPhone.setText(customer.getPhone());
                    getViewDataBinding().etEmail.setText(customer.getEmail());
                }
                break;

            case R.id.tv_save_contact:
                if (isNetworkConnected()) {
                    enableDisableContactView(false);
                    customer.setPhone(getViewDataBinding().etPhone.getText().toString());
                    customer.setEmail(getViewDataBinding().etEmail.getText().toString());
                    clientDetailViewModel.doSaveAppointmentInfo(customerUUID, customer);
                    getViewDataBinding().tvEditCustInfo.setText(R.string.edit);
                } else {
                    showToast(getString(R.string.noInternet));
                }
                break;

            case R.id.tv_edit_cust_loc:
                if (getViewDataBinding().tvEditCustLoc.getText().toString().equals("Edit")) {
                    getViewDataBinding().tvEditCustLoc.setText(R.string.cancel);
                    enableDisableLocView(true);
                } else {
                    getViewDataBinding().tvEditCustLoc.setText(R.string.edit);
                    enableDisableLocView(false);
                }
                break;

            case R.id.tv_save_loc:
                if (isNetworkConnected()) {
                    customer.setStreetAddress(getViewDataBinding().etStreet.getText().toString());
                    customer.setCity(getViewDataBinding().etCity.getText().toString());
                    customer.setState(getViewDataBinding().etState.getText().toString());
                    customer.setZipcode(getViewDataBinding().etZipCode.getText().toString());
                    clientDetailViewModel.doSaveAppointmentInfo(customerUUID, customer);
                    getViewDataBinding().tvEditCustLoc.setText(R.string.edit);
                    enableDisableLocView(false);
                } else {
                    showToast(getString(R.string.noInternet));
                }
                break;

            case R.id.tv_edit_sales_person:
                if (getViewDataBinding().tvEditSalesPerson.getText().toString().equals("Edit")) {
                    getViewDataBinding().tvEditSalesPerson.setText(R.string.cancel);
                    getViewDataBinding().tvAddCalendar.setVisibility(View.GONE);
                    getViewDataBinding().tvSaveSalesPerson.setVisibility(View.VISIBLE);
                } else {
                    getViewDataBinding().tvEditSalesPerson.setText(R.string.edit);
                    getViewDataBinding().tvAddCalendar.setVisibility(View.VISIBLE);
                    getViewDataBinding().tvSaveSalesPerson.setVisibility(View.GONE);
                    getViewDataBinding().tvSalesPersonName.setText(customer.getSalesperson());
                }
                break;

            case R.id.ll_sales_person:
                if (getViewDataBinding().tvEditSalesPerson.getText().toString().equals("Cancel")) {
                    EmployeesBottomSheetDialog.newInstance(employee -> {
                        mUser = employee;
                        getViewDataBinding().tvSalesPersonName.setText(employee.getUserFirstName().concat(" ").concat(employee.getUserLastName()));
                    }).show(getChildFragmentManager());
                }
                break;

            case R.id.tv_save_sales_person:
                if (isNetworkConnected()) {
                    customer.setSalesperson(getViewDataBinding().tvSalesPersonName.getText().toString());
                    customer.setSalesUuid(mUser == null ? "" : mUser.getUuid());
                    clientDetailViewModel.doSaveAppointmentInfo(customerUUID, customer);

                    getViewDataBinding().tvEditAppointment.setText(R.string.edit);
                    getViewDataBinding().tvAddCalendar.setVisibility(View.VISIBLE);
                    getViewDataBinding().tvSaveSalesPerson.setVisibility(View.GONE);
                } else {
                    showToast(getString(R.string.noInternet));
                }
                break;
        }
    }

    private void enableDisableStatusView(boolean isEnable) {
        getViewDataBinding().tvSaveStatus.setVisibility(isEnable ? View.VISIBLE : View.GONE);
        getViewDataBinding().loopWheelView.setVisibility(isEnable ? View.VISIBLE : View.GONE);
        getViewDataBinding().loopView.setVisibility(isEnable ? View.VISIBLE : View.GONE);
        getViewDataBinding().tvSold.setVisibility(isEnable ? View.GONE : View.VISIBLE);
    }

    private void enableDisableLocView(boolean isEnable) {
        getViewDataBinding().llEditLocView.setVisibility(isEnable ? View.VISIBLE : View.GONE);
        getViewDataBinding().llDirection.setVisibility(isEnable ? View.GONE : View.VISIBLE);
    }

    private void enableDisableContactView(boolean isEnable) {
        getViewDataBinding().tvCall.setVisibility(isEnable ? View.GONE : View.VISIBLE);
        getViewDataBinding().viewCall.setVisibility(isEnable ? View.GONE : View.VISIBLE);
        getViewDataBinding().tvEmail.setVisibility(isEnable ? View.GONE : View.VISIBLE);
        getViewDataBinding().tvSaveContact.setVisibility(isEnable ? View.VISIBLE : View.GONE);
        getViewDataBinding().etPhone.setEnabled(isEnable);
        getViewDataBinding().etEmail.setEnabled(isEnable);
    }

    private boolean verifyInput() {
        if (getViewDataBinding().etName.getText().toString().isEmpty()) {
            showToast(getString(R.string.alert_cust_name_empty));
            return false;
        } else return true;
    }

    private void enableDisableAppointmentView(boolean isEnable) {
        if (!isEnable) {
            getViewDataBinding().viewTime.setVisibility(View.GONE);
            getViewDataBinding().dateAppointTimePicker.setVisibility(View.GONE);

            getViewDataBinding().viewSet.setVisibility(View.GONE);
            getViewDataBinding().dateAppointSetPicker.setVisibility(View.GONE);
        }
        getViewDataBinding().tvAppointSet.setEnabled(isEnable);
        getViewDataBinding().tvAppointTime.setEnabled(isEnable);
        getViewDataBinding().viewSaveAppointment.setVisibility(isEnable ? View.VISIBLE : View.GONE);
        getViewDataBinding().tvSaveAppointment.setVisibility(isEnable ? View.VISIBLE : View.GONE);
        getViewDataBinding().tvSaveAppointment.setEnabled(isEnable);
        getViewDataBinding().etName.setEnabled(isEnable);
        getViewDataBinding().etProductInterest.setEnabled(isEnable);
        getViewDataBinding().etLeadSource.setEnabled(isEnable);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        this.googleMap.getUiSettings().setMyLocationButtonEnabled(false);
        this.googleMap.getUiSettings().setCompassEnabled(false);
        this.googleMap.getUiSettings().setMapToolbarEnabled(false);
        this.googleMap.getUiSettings().setScrollGesturesEnabled(false);

        if (latLng != null) {
            updateMapUI(latLng);
        }
    }

    private void updateMapUI(LatLng latLng) {
        if (googleMap != null) {
            googleMap.clear();
            googleMap.addMarker(new MarkerOptions()
                    .position(latLng)
                    .title(customer.getCity())
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));
            googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
            googleMap.animateCamera(CameraUpdateFactory.zoomTo(16));
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);

        Bundle mapViewBundle = outState.getBundle(AppConstants.MAP_VIEW_BUNDLE_KEY);
        if (mapViewBundle == null) {
            mapViewBundle = new Bundle();
            outState.putBundle(AppConstants.MAP_VIEW_BUNDLE_KEY, mapViewBundle);
        }

        mapView.onSaveInstanceState(mapViewBundle);
    }
}