package com.taskos.ui.main.openjobs.share;


/**
 * Created by Hemant Sharma on 23-02-20.
 */
public interface JobShareNavigator {
    void onShareSuccess();

    void onSaveError();

    void onShowShareUsers();

    void onUnShareUsers();
}
