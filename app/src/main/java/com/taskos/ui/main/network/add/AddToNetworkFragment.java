package com.taskos.ui.main.network.add;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.taskos.BR;
import com.taskos.R;
import com.taskos.TaskOS;
import com.taskos.ViewModelProviderFactory;
import com.taskos.data.model.api.CompanyAddRequest;
import com.taskos.data.model.api.NetworkAddRequest;
import com.taskos.databinding.FragmentAddToNetworkBinding;
import com.taskos.ui.auth.MUser;
import com.taskos.ui.base.BaseFragment;

import java.util.Date;
import java.util.UUID;


public class AddToNetworkFragment extends BaseFragment<FragmentAddToNetworkBinding, AddToNetworkViewModel> implements View.OnClickListener, AddToNetworkNavigator {

    private AddToNetworkViewModel addToNetworkViewModel;
    private String userUUID = "";
    private MUser mUser;
    private NetworkAddRequest networkAddRequest;
    private CompanyAddRequest companyAddRequest;

    public static AddToNetworkFragment newInstance(String uuid) {

        Bundle args = new Bundle();

        AddToNetworkFragment fragment = new AddToNetworkFragment();
        args.putString("UserUUID", uuid);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_add_to_network;
    }

    @Override
    public AddToNetworkViewModel getViewModel() {
        addToNetworkViewModel = new ViewModelProvider(this, ViewModelProviderFactory.getInstance()).get(AddToNetworkViewModel.class);
        return addToNetworkViewModel;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            userUUID = getArguments().getString("UserUUID");
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        addToNetworkViewModel.setNavigator(this);
        TextView tvTitle = view.findViewById(R.id.tvTitle);
        tvTitle.setText(R.string.search);
        tvTitle.setOnClickListener(this);
        view.findViewById(R.id.ll_main).setOnClickListener(this);
        view.findViewById(R.id.img_back).setOnClickListener(this);
        getViewDataBinding().tvAddNetwork.setOnClickListener(this);
        getViewDataBinding().tvAddEmployee.setOnClickListener(this);

        handleSwitchEvent();

        addToNetworkViewModel.getMUser(userUUID).observe(getViewLifecycleOwner(), mUser -> {
            this.mUser = mUser;
            getViewDataBinding().tvName.setText(mUser.getUserFirstName().concat(" ").concat(mUser.getUserLastName()));

            if (!mUser.getUserProfileImageLink().isEmpty()) {
                Glide.with(getBaseActivity())
                        .load(mUser.getUserProfileImageLink())
                        .placeholder(R.drawable.ic_def_user)
                        .centerCrop()
                        .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                        .error(R.drawable.ic_def_user)
                        .into(getViewDataBinding().imgUser);
            }

            addToNetworkViewModel.getNetworkRequest(mUser.getUuid()).observe(getViewLifecycleOwner(), networkAddRequest -> {
                this.networkAddRequest = networkAddRequest;
                getViewDataBinding().etMessage.setText(networkAddRequest.getMessage());
                updateNetworkUI(networkAddRequest.isApproved());
                getViewDataBinding().switchNetwork.setChecked(true);
            });

            addToNetworkViewModel.getCompanyAddRequest(mUser.getUuid()).observe(getViewLifecycleOwner(), companyAddRequest -> {
                this.companyAddRequest = companyAddRequest;
                getViewDataBinding().etEnter.setText(companyAddRequest.getMessage());
                updateEmployeeUI(companyAddRequest.isApproved());
                getViewDataBinding().switchEmployee.setChecked(true);
            });
        });
    }

    private void handleSwitchEvent() {
        getViewDataBinding().switchNetwork.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                getViewDataBinding().llAddNetwork.setVisibility(View.VISIBLE);
            } else {
                getViewDataBinding().llAddNetwork.setVisibility(View.GONE);
            }
        });

        getViewDataBinding().switchEmployee.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                getViewDataBinding().llAddEmployee.setVisibility(View.VISIBLE);
            } else {
                getViewDataBinding().llAddEmployee.setVisibility(View.GONE);
            }
        });
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvTitle:
            case R.id.img_back:
                getBaseActivity().onBackPressed();
                break;

            case R.id.tv_add_network:
                if (isNetworkConnected()) {
                    if (mUser != null) {
                        getViewDataBinding().tvAddNetwork.setClickable(false);
                        if (getViewDataBinding().tvAddNetwork.getText().toString().equals(getString(R.string.add_to_network))) {
                            NetworkAddRequest networkAddRequest = new NetworkAddRequest();
                            networkAddRequest.setUuid(UUID.randomUUID().toString());
                            networkAddRequest.setRequestedUser(mUser.getUuid());
                            networkAddRequest.setSentRequestUser(TaskOS.getInstance().getDataManager().getUUID());
                            networkAddRequest.setApproved(false);
                            networkAddRequest.setSentAt(new Date());
                            networkAddRequest.setMessage(getViewDataBinding().etMessage.getText().toString());
                            addToNetworkViewModel.doAddNetwork(networkAddRequest, mUser);
                        } else {
                            addToNetworkViewModel.doRemoveRequestedNetwork(networkAddRequest);
                        }
                    } else showToast(getString(R.string.something_went_wrong));
                } else {
                    showToast(getString(R.string.noInternet));
                }
                break;

            case R.id.tv_add_employee:
                if (isNetworkConnected()) {
                    if (mUser != null) {
                        getViewDataBinding().tvAddEmployee.setClickable(false);
                        if (getViewDataBinding().tvAddEmployee.getText().toString().equals(getString(R.string.add_as_employee))) {
                            CompanyAddRequest companyAddRequest = new CompanyAddRequest();
                            companyAddRequest.setUuid(UUID.randomUUID().toString());
                            companyAddRequest.setRequestedUser(mUser.getUuid());
                            companyAddRequest.setCompanyRequester(TaskOS.getInstance().getDataManager().getCompanyID());
                            companyAddRequest.setApproved(false);
                            companyAddRequest.setSentAt(new Date());
                            companyAddRequest.setMessage(getViewDataBinding().etEnter.getText().toString());
                            addToNetworkViewModel.doAddEmployee(companyAddRequest);
                        } else {
                            addToNetworkViewModel.doRemoveEmployee(companyAddRequest);
                        }
                    } else showToast(getString(R.string.something_went_wrong));
                } else {
                    showToast(getString(R.string.noInternet));
                }
                break;
        }
    }


    @Override
    public void updateNetworkUI(boolean isAccept) {
        getViewDataBinding().etMessage.setVisibility(View.GONE);
        getViewDataBinding().tvRequested.setVisibility(View.VISIBLE);

        if (isAccept) {
            getViewDataBinding().tvAddNetwork.setVisibility(View.GONE);
            getViewDataBinding().tvRequested.setText(mUser.getUserFirstName().concat(" ").concat(mUser.getUserLastName()).concat("Invitation Accepted"));
        } else {
            getViewDataBinding().tvAddNetwork.setVisibility(View.VISIBLE);
            getViewDataBinding().tvRequested.setText(mUser.getUserFirstName().concat(" ").concat(mUser.getUserLastName()).concat("Already Requested"));
        }
        getViewDataBinding().tvAddNetwork.setText(R.string.cancel_invite);
        getViewDataBinding().tvAddNetwork.setTextColor(getResources().getColor(R.color.red));
        getViewDataBinding().tvAddNetwork.setClickable(true);
    }

    @Override
    public void networkInvitationCanceled() {
        getViewDataBinding().etMessage.setVisibility(View.VISIBLE);
        getViewDataBinding().tvRequested.setVisibility(View.GONE);
        getViewDataBinding().tvAddNetwork.setText(R.string.add_to_network);
        getViewDataBinding().tvAddNetwork.setTextColor(getResources().getColor(R.color.light_blue));
        getViewDataBinding().tvAddNetwork.setClickable(true);
    }

    @Override
    public void updateEmployeeUI(boolean isAccept) {
        getViewDataBinding().etEnter.setVisibility(View.GONE);
        getViewDataBinding().tvEmployeeRequested.setVisibility(View.VISIBLE);

        if (isAccept) {
            getViewDataBinding().tvAddEmployee.setVisibility(View.GONE);
            getViewDataBinding().tvEmployeeRequested.setText(R.string.employee_invitation_accept);
        } else {
            getViewDataBinding().tvAddEmployee.setVisibility(View.VISIBLE);
            getViewDataBinding().tvEmployeeRequested.setText(R.string.request_join_company);
        }
        getViewDataBinding().tvAddEmployee.setText(R.string.cancel_employee_invite);
        getViewDataBinding().tvAddEmployee.setTextColor(getResources().getColor(R.color.red));
        getViewDataBinding().tvAddEmployee.setClickable(true);
    }

    @Override
    public void employeeInvitationCanceled() {
        getViewDataBinding().etEnter.setVisibility(View.VISIBLE);
        getViewDataBinding().tvEmployeeRequested.setVisibility(View.GONE);
        getViewDataBinding().tvAddEmployee.setText(R.string.add_as_employee);
        getViewDataBinding().tvAddEmployee.setTextColor(getResources().getColor(R.color.light_blue));
        getViewDataBinding().tvAddEmployee.setClickable(true);
    }

    @Override
    public void onSaveError() {
        showToast(getString(R.string.something_went_wrong));
        getViewDataBinding().tvAddNetwork.setClickable(true);
        getViewDataBinding().tvAddEmployee.setClickable(true);
    }
}