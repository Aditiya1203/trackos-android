package com.taskos.ui.main.tasks.detail.edit;

import android.graphics.Paint;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProvider;

import com.taskos.R;
import com.taskos.ViewModelProviderFactory;
import com.taskos.data.model.api.ToDo;
import com.taskos.databinding.MyTaskDetailViewBinding;
import com.taskos.ui.base.BaseBottomSheetDialog;
import com.taskos.ui.main.employee.EmployeesBottomSheetDialog;
import com.taskos.util.CalenderUtils;

import java.util.Date;

public class EditTaskDetailsBottomSheetDialog extends BaseBottomSheetDialog<MyTaskDetailViewBinding, EditTaskDetailsViewModel>
        implements EditTasksDetailNavigator, View.OnClickListener {

    private static final String TAG = "EditTaskDetailsBottomSh";
    private EditTaskDetailsViewModel editTaskDetailsViewModel;
    private String taskKey = "";
    private Date taskCompletionDate = new Date();
    private ToDo toDo;


    public static EditTaskDetailsBottomSheetDialog newInstance(String taskKey) {
        Bundle args = new Bundle();
        EditTaskDetailsBottomSheetDialog fragment = new EditTaskDetailsBottomSheetDialog();
        args.putString("TaskKey", taskKey);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        editTaskDetailsViewModel.setNavigator(this);
        if (getArguments() != null) {
            taskKey = getArguments().getString("TaskKey");
        }
    }

    @Override
    public int getBindingVariable() { return com.taskos.BR.viewModel; }

    @Override
    public int getLayoutId() {
        return R.layout.my_task_detail_view;
    }

    @Override
    public EditTaskDetailsViewModel getViewModel() {
        editTaskDetailsViewModel = new ViewModelProvider(this, ViewModelProviderFactory.getInstance()).get(EditTaskDetailsViewModel.class);
        return editTaskDetailsViewModel;
    }

    public void show(FragmentManager fragmentManager) {
        super.show(fragmentManager, TAG);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        editTaskDetailsViewModel.setNavigator(this);
        getViewDataBinding().tvCancel.setOnClickListener(this);
        getViewDataBinding().rlAssignUserName.setOnClickListener(this);
        getViewDataBinding().rlDatePicker.setOnClickListener(this);
        getViewDataBinding().tvSaveEdits.setOnClickListener(this);

        getViewDataBinding().tvTaskLabel.setPaintFlags(getViewDataBinding().tvTaskLabel.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        getViewDataBinding().tvNoteLabel.setPaintFlags(getViewDataBinding().tvNoteLabel.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        editTaskDetailsViewModel.getTaskDetail(taskKey).observe(getViewLifecycleOwner(), toDo -> {
            this.toDo = toDo;
            getViewDataBinding().tvClientName.setText(toDo.getJobName());
            getViewDataBinding().etTaskName.setText(toDo.getTask());
            getViewDataBinding().etNotes.setText(toDo.getNotes());
            taskCompletionDate = toDo.getDateCompleted();
            getViewDataBinding().tvCompletionDate.setText(CalenderUtils.formatDate(toDo.getDateCompleted(), CalenderUtils.TIMESTAMP_FORMAT2));
            getViewDataBinding().etEnteredDate.setText(CalenderUtils.formatDate(toDo.getDateEntered(), CalenderUtils.CUSTOM_TIMESTAMP_FORMAT_AM));
            getViewDataBinding().etEnteredBy.setText(toDo.getAssignedBy());
            getViewDataBinding().tvAssignUserName.setText(toDo.getAssignedBy());
            getViewDataBinding().switch1.setOnCheckedChangeListener((buttonView, isChecked) -> {
                if (isChecked) {
                    getViewDataBinding().rlAssignUserName.setVisibility(View.VISIBLE);
                    getViewDataBinding().assignView.setVisibility(View.VISIBLE);
                } else {
                    getViewDataBinding().rlAssignUserName.setVisibility(View.GONE);
                    getViewDataBinding().assignView.setVisibility(View.GONE);
                }
            });
            getViewDataBinding().switch1.setChecked(!toDo.getAssignedBy().isEmpty());
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_cancel:
                dismissDialog(TAG);
                break;

            case R.id.rlAssignUserName:
                EmployeesBottomSheetDialog.newInstance(employee -> getViewDataBinding().tvAssignUserName.setText(employee.getUserFirstName().concat(" ").concat(employee.getUserLastName()))).show(getChildFragmentManager());
                break;

            case R.id.rl_date_picker:
                if (getViewDataBinding().dateTimePicker.getVisibility() == View.VISIBLE) {
                    getViewDataBinding().viewPicker.setVisibility(View.GONE);
                    getViewDataBinding().dateTimePicker.setVisibility(View.GONE);
                } else {
                    getViewDataBinding().viewPicker.setVisibility(View.VISIBLE);
                    getViewDataBinding().dateTimePicker.setVisibility(View.VISIBLE);
                }
                getViewDataBinding().dateTimePicker.setDefaultDate(taskCompletionDate);
                getViewDataBinding().dateTimePicker.addOnDateChangedListener((displayed, date) -> {
                    taskCompletionDate = date;
                    getViewDataBinding().tvCompletionDate.setText(CalenderUtils.formatDate(date, CalenderUtils.TIMESTAMP_FORMAT2));
                });
                break;

            case R.id.tv_save_edits:
                if (isNetworkConnected()) {
                    if (verifyInputs()) {
                        getViewDataBinding().tvSaveEdits.setClickable(false);
                        toDo.setTask(getViewDataBinding().etTaskName.getText().toString());
                        toDo.setDateCompleted(taskCompletionDate);
                        toDo.setAssignedBy(getViewDataBinding().tvAssignUserName.getText().toString());
                        toDo.setNotes(getViewDataBinding().etNotes.getText().toString());
                        editTaskDetailsViewModel.doSaveTaskDetail(toDo);
                    }
                } else {
                    showToast(getString(R.string.noInternet));
                }
                break;
        }
    }

    private boolean verifyInputs() {
        if (getViewDataBinding().etTaskName.getText().toString().isEmpty()) {
            showToast(getString(R.string.alert_complete_task));
            return false;
        } else return true;
    }

    @Override
    public void openDashboard() {
        getViewDataBinding().tvSaveEdits.setClickable(true);
        dismissDialog(TAG);
    }

    @Override
    public void onSaveError() {
        getViewDataBinding().tvSaveEdits.setClickable(true);
    }
}
