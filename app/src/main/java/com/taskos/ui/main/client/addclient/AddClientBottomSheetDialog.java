package com.taskos.ui.main.client.addclient;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProvider;

import com.taskos.R;
import com.taskos.TaskOS;
import com.taskos.ViewModelProviderFactory;
import com.taskos.data.model.api.Customer;
import com.taskos.data.model.api.UserCommentsCustomer;
import com.taskos.databinding.DialogAddClientBinding;
import com.taskos.ui.auth.MUser;
import com.taskos.ui.base.BaseBottomSheetDialog;
import com.taskos.ui.main.employee.EmployeesBottomSheetDialog;
import com.taskos.util.AppConstants;
import com.taskos.util.CalenderUtils;
import com.taskos.util.UsPhoneNumberFormatter;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;


public class AddClientBottomSheetDialog extends BaseBottomSheetDialog<DialogAddClientBinding, AddClientViewModel> implements View.OnClickListener, AddClientNavigator {

    private static final String TAG = "AddClientBottomSheetDia";
    private AddClientViewModel addClientViewModel;
    private Date appointmentTime = new Date();
    private MUser mUser;

    public static AddClientBottomSheetDialog newInstance() {

        Bundle args = new Bundle();

        AddClientBottomSheetDialog fragment = new AddClientBottomSheetDialog();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getBindingVariable() {
        return com.taskos.BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.dialog_add_client;
    }

    @Override
    public AddClientViewModel getViewModel() {
        addClientViewModel = new ViewModelProvider(this, ViewModelProviderFactory.getInstance()).get(AddClientViewModel.class);
        return addClientViewModel;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addClientViewModel.setNavigator(this);
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getViewDataBinding().tvCancel.setOnClickListener(this);
        getViewDataBinding().llAppointTime.setOnClickListener(this);
        getViewDataBinding().tvSave.setOnClickListener(this);
        getViewDataBinding().rlSalesperson.setOnClickListener(this);

        //if user is not selected then current user set
        addClientViewModel.getCurrentUser().observe(getViewLifecycleOwner(), mUser -> {
            this.mUser = mUser;
            getViewDataBinding().tvSalesperson.setText(mUser.getUserFirstName().concat(" ").concat(mUser.getUserLastName()));
        });

        UsPhoneNumberFormatter addLineNumberFormatter = new UsPhoneNumberFormatter(
                new WeakReference<EditText>(getViewDataBinding().etPhone));
        getViewDataBinding().etPhone.addTextChangedListener(addLineNumberFormatter);

    }

    public void show(FragmentManager fragmentManager) {
        super.show(fragmentManager, TAG);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_cancel:
                dismissDialog(TAG);
                break;

            case R.id.rl_salesperson:
                EmployeesBottomSheetDialog.newInstance(employee -> {
                    mUser = employee;
                    getViewDataBinding().tvSalesperson.setText(employee.getUserFirstName().concat(" ").concat(employee.getUserLastName()));
                }).show(getChildFragmentManager());
                break;

            case R.id.ll_appoint_time:
                if (getViewDataBinding().dateTimePicker.getVisibility() == View.VISIBLE) {
                    getViewDataBinding().view.setVisibility(View.GONE);
                    getViewDataBinding().dateTimePicker.setVisibility(View.GONE);
                } else {
                    getViewDataBinding().view.setVisibility(View.VISIBLE);
                    getViewDataBinding().dateTimePicker.setVisibility(View.VISIBLE);
                }
                getViewDataBinding().dateTimePicker.addOnDateChangedListener((displayed, date) -> {
                    appointmentTime = date;
                    getViewDataBinding().tvAppointTime.setText(CalenderUtils.formatDate(date, CalenderUtils.TIMESTAMP_FORMAT));
                });
                break;

            case R.id.tv_save:
                if (isNetworkConnected()) {
                    if (verifyInputs()) {
                        showLoading();
                        getViewDataBinding().tvSave.setClickable(false);
                        Customer customer = new Customer();
                        customer.setUuid(UUID.randomUUID().toString());
                        customer.setName(getViewDataBinding().etName.getText().toString());
                        customer.setPhone(getViewDataBinding().etPhone.getText().toString());
                        customer.setEmail(getViewDataBinding().etEmail.getText().toString());
                        customer.setStreetAddress(getViewDataBinding().etStreet.getText().toString());
                        customer.setCity(getViewDataBinding().etCity.getText().toString());
                        customer.setState(getViewDataBinding().etState.getText().toString());
                        customer.setZipcode(getViewDataBinding().etZipCode.getText().toString());
                        customer.setAppointmentTime(appointmentTime);
                        customer.setAppointmentEntered(new Date());
                        customer.setSalesperson(getViewDataBinding().tvSalesperson.getText().toString());
                        customer.setSource(getViewDataBinding().etLeadSource.getText().toString());
                        customer.setProductOfInterest(getViewDataBinding().etProdInterest.getText().toString());
                        customer.setCustomerStatus(AppConstants.kStatusArray[0]);
                        customer.setListOrganizer(AppConstants.kStatusListOrganiser[0]); // (options: Open, Not Sold, Customer)
                        customer.setOwner(TaskOS.getInstance().getDataManager().getCompanyID().isEmpty()
                                ? TaskOS.getInstance().getDataManager().getUUID()
                                : TaskOS.getInstance().getDataManager().getCompanyID());
                        customer.setSalesUuid(mUser == null ? "" : mUser.getUuid());

                        ArrayList<String> subscriber = new ArrayList<>();
                        if (mUser != null) subscriber.add(mUser.getUuid());
                        customer.setSubscribers(subscriber);
                        customer.setAppointmentSet(false);

                        UserCommentsCustomer userCommentsCustomer = null;
                        if (!getViewDataBinding().etNotes.getText().toString().isEmpty()) {
                            userCommentsCustomer = new UserCommentsCustomer();
                            userCommentsCustomer.setUuid(UUID.randomUUID().toString());
                            userCommentsCustomer.setUserFirstName(mUser.getUserFirstName());
                            userCommentsCustomer.setUserLastName(mUser.getUserLastName());
                            userCommentsCustomer.setUserUUID(mUser.getUuid());
                            userCommentsCustomer.setUsername(mUser.getUsername());
                            userCommentsCustomer.setCustomerUuid(customer.getUuid());
                            userCommentsCustomer.setComment(getViewDataBinding().etNotes.getText().toString());
                            userCommentsCustomer.setCommentPostedTime(new Date());
                            userCommentsCustomer.setCustomerStatus(customer.getCustomerStatus());
                            userCommentsCustomer.setImageLink(mUser.getUserProfileImageLink());
                            userCommentsCustomer.setOwner(customer.getOwner());
                        }

                        addClientViewModel.doAddClient(customer, userCommentsCustomer);
                    }
                } else {
                    showToast(getString(R.string.noInternet));
                }
                break;
        }
    }

    private boolean verifyInputs() {
        if (getViewDataBinding().etName.getText().toString().isEmpty()) {
            showToast(getString(R.string.alert_cust_name_empty));
            return false;
        }/*else if (getViewDataBinding().etStreet.getText().toString().isEmpty()){
            showToast(getString(R.string.alert_street_empty));
            return false;
        }else if (getViewDataBinding().etCity.getText().toString().isEmpty()){
            showToast(getString(R.string.alert_city_empty));
            return false;
        }else if (getViewDataBinding().etState.getText().toString().isEmpty()){
            showToast(getString(R.string.alert_state_empty));
            return false;
        }else if (getViewDataBinding().etPhone.getText().toString().isEmpty()){
            showToast(getString(R.string.alert_phone_empty));
            return false;
        }else if (getViewDataBinding().etEmail.getText().toString().isEmpty()){
            showToast(getString(R.string.alert_email_empty));
            return false;
        }else if (!CommonUtils.isEmailValid(getViewDataBinding().etEmail.getText().toString())){
            showToast(getString(R.string.alert_valid_email));
            return false;
        }else if (getViewDataBinding().tvAppointTime.getText().toString().isEmpty()){
            showToast(getString(R.string.alert_appoint_time_empty));
            return false;
        }else if (getViewDataBinding().etProdInterest.getText().toString().isEmpty()){
            showToast(getString(R.string.alert_prod_interest_empty));
            return false;
        }else if (getViewDataBinding().etNotes.getText().toString().isEmpty()){
            showToast(getString(R.string.alert_notes_empty));
            return false;
        }*/ else return true;
    }

    @Override
    public void openDashboard() {
        hideLoading();
        getViewDataBinding().tvSave.setClickable(true);
        dismissDialog(TAG);
    }

    @Override
    public void onSaveError() {
        hideLoading();
        getViewDataBinding().tvSave.setClickable(true);
    }
}
