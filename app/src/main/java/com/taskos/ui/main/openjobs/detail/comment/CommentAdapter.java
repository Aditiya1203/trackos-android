package com.taskos.ui.main.openjobs.detail.comment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.taskos.R;
import com.taskos.data.model.api.UserCommentsCustomer;
import com.taskos.ui.base.BaseViewHolder;
import com.taskos.util.CalenderUtils;

import java.util.List;

/**
 * Created by Hemant Sharma on 23-02-20.
 */
public class CommentAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    private List<UserCommentsCustomer> commentsList;

    public CommentAdapter(List<UserCommentsCustomer> commentsList) {
        this.commentsList = commentsList;
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(
                LayoutInflater.from(parent.getContext()).inflate(R.layout.item_comments, parent, false));
    }

    @Override
    public int getItemCount() {
        if (commentsList != null && commentsList.size() > 0) {
            return commentsList.size();
        } else {
            return 0;
        }
    }

    public class ViewHolder extends BaseViewHolder {
        private TextView tv_name, tv_msg, tv_date_time;
        private ImageView img_user;

        public ViewHolder(View itemView) {
            super(itemView);
            img_user = itemView.findViewById(R.id.img_user);
            tv_name = itemView.findViewById(R.id.tv_name);
            tv_msg = itemView.findViewById(R.id.tv_msg);
            tv_date_time = itemView.findViewById(R.id.tv_date_time);
        }

        public void onBind(int position) {
            UserCommentsCustomer tmpBean = commentsList.get(position);
            tv_name.setText(tmpBean.getUsername());
            tv_msg.setText(tmpBean.getComment());
            tv_date_time.setText(CalenderUtils.formatDate(tmpBean.getCommentPostedTime(), CalenderUtils.CUSTOM_TIMESTAMP_FORMAT_AM));
            if (!tmpBean.getImageLink().isEmpty()) {
                Glide.with(tv_msg.getContext())
                        .load(tmpBean.getImageLink())
                        .centerCrop()
                        .placeholder(R.drawable.ic_def_user)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .error(R.drawable.ic_def_user)
                        .into(img_user);
            } else {
                Glide.with(tv_msg.getContext())
                        .load(R.drawable.ic_def_user)
                        .placeholder(R.drawable.ic_def_user)
                        .centerCrop()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .error(R.drawable.ic_def_user)
                        .into(img_user);
            }
        }
    }
}
