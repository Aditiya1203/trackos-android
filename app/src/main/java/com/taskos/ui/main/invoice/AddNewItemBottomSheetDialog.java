package com.taskos.ui.main.invoice;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.taskos.R;
import com.taskos.ViewModelProviderFactory;
import com.taskos.data.model.api.Customer;
import com.taskos.data.model.api.EstimateInvoiceNewItem;
import com.taskos.databinding.DialogAddJobBinding;
import com.taskos.ui.base.BaseBottomSheetDialog;
import com.taskos.ui.main.myleads.OpenLeadsAdapter;
import com.taskos.ui.main.openjobs.addjob.AddJobViewModel;

import java.util.UUID;


public class AddNewItemBottomSheetDialog extends BaseBottomSheetDialog<DialogAddJobBinding, AddJobViewModel> implements View.OnClickListener {

    private static final String TAG = "AddNewItemBottomSheetDialog";
    private AddJobViewModel addJobViewModel;
    EditText et_itemname;
    EditText et_des;
    EditText et_quantity;
    EditText et_rate;
    TextView tv_total;
    Double total;
    public static AddNewItemBottomSheetDialog newInstance() {

        Bundle args = new Bundle();
        AddNewItemBottomSheetDialog fragment = new AddNewItemBottomSheetDialog();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getBindingVariable() {
        return com.taskos.BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.dialog_new_item;
    }

    @Override
    public AddJobViewModel getViewModel() {
        addJobViewModel = new ViewModelProvider(this, ViewModelProviderFactory.getInstance()).get(AddJobViewModel.class);
        return addJobViewModel;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        TextView tv_cancel = view.findViewById(R.id.tv_cancel);
        tv_cancel.setOnClickListener(this);
        TextView tv_add = view.findViewById(R.id.tv_add);
        et_itemname= view.findViewById(R.id.et_itemname);
        et_des= view.findViewById(R.id.et_des);
        et_quantity= view.findViewById(R.id.et_quantity);
        et_rate= view.findViewById(R.id.et_rate);
        et_rate.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                total=Double.parseDouble(et_quantity.getText().toString())*Double.parseDouble(s.toString());
                tv_total.setText("$"+total);
            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        tv_total= view.findViewById(R.id.tv_total);
        tv_add.setOnClickListener(this);
    }


    public void show(FragmentManager fragmentManager) {
        super.show(fragmentManager, TAG);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_cancel:
                dismissDialog(TAG);

                break;
                case R.id.tv_add:
                    EstimateInvoiceNewItem estimateInvoiceNewItem=new EstimateInvoiceNewItem();
                    estimateInvoiceNewItem.setUuid(UUID.randomUUID().toString());
                    estimateInvoiceNewItem.setName(et_itemname.getText().toString());
                    estimateInvoiceNewItem.setDescription(et_des.getText().toString().trim());
                    estimateInvoiceNewItem.setQuantity(Double.valueOf(et_quantity.getText().toString()));
                    estimateInvoiceNewItem.setRate(total);
                    SaveNewItem(estimateInvoiceNewItem);
                break;
        }
    }

    public void SaveNewItem(EstimateInvoiceNewItem estimateInvoiceNewItem) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("LineItem")
                .document(estimateInvoiceNewItem.getUuid())
                .set(estimateInvoiceNewItem)
                .addOnCompleteListener(task1 -> {
                    if (task1.isSuccessful()) {
                        dismissDialog(TAG);
                    } else {

                    }
                });
    }

}
