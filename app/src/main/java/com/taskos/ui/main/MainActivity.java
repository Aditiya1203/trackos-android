package com.taskos.ui.main;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.taskos.BR;
import com.taskos.R;
import com.taskos.TaskOS;
import com.taskos.ViewModelProviderFactory;
import com.taskos.data.local.prefs.AppPreferencesHelper;
import com.taskos.data.model.api.Customer;
import com.taskos.databinding.ActivityMainBinding;
import com.taskos.ui.auth.MUser;
import com.taskos.ui.auth.moreInfo.EditProfile;
import com.taskos.ui.base.BaseActivity;
import com.taskos.ui.fcm.FcmNotificationBuilder;
import com.taskos.ui.main.client.ClientFragment;
import com.taskos.ui.main.estimate.EstimateBottomSheetDialog;
import com.taskos.ui.main.help.HelpActivity;
import com.taskos.ui.main.invoice.InvoiceBottomSheetDialog;
import com.taskos.ui.main.more.MoreFragment;
import com.taskos.ui.main.myleads.AllOpenfragment;
import com.taskos.ui.main.myleads.ClientArchivefragment;
import com.taskos.ui.main.myleads.MyLeadsfragment;
import com.taskos.ui.main.myleads.OpenLeadsAdapter;
import com.taskos.ui.main.network.MyNetworkFragment;
import com.taskos.ui.main.openjobs.OpenJobsFragment;
import com.taskos.ui.main.tasks.TasksFragment;
import com.taskos.util.AppLogger;

import java.util.List;
import java.util.UUID;

import de.hdodenhof.circleimageview.CircleImageView;

public class MainActivity extends BaseActivity<ActivityMainBinding, MainViewModel> implements MainNavigator {

    private MainViewModel mainViewModel;
    private BottomNavigationView navView;
    //notification handling
    private Bundle bundle;
    DrawerLayout drawer;
    boolean Myleads = false;
    LinearLayout ll_close;
    LinearLayout ll_profile, ll_edit_profile;
    LinearLayout ll_logout;
    List<MUser> userlist;
    CircleImageView iv_user;
    TextView tv_name;

    public static Intent newIntent(Context context) {
        return new Intent(context, MainActivity.class);
    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    public MainViewModel getViewModel() {
        mainViewModel = new ViewModelProvider(this, ViewModelProviderFactory.getInstance()).get(MainViewModel.class);
        return mainViewModel;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mainViewModel.setNavigator(this);
        navView = findViewById(R.id.nav_view);

        replaceFragment(MyLeadsfragment.newInstance(bundle), R.id.nav_host_fragment);

        navView.setOnNavigationItemSelectedListener(item -> {
            if (item.getItemId() == R.id.navigation_myleads) {
                Myleads = true;
                replaceFragment(MyLeadsfragment.newInstance(bundle), R.id.nav_host_fragment);
                bundle = new Bundle();
                return true;
            } else if (item.getItemId() == R.id.navigation_openleads) {
                Myleads = false;
                replaceFragment(AllOpenfragment.newInstance(bundle), R.id.nav_host_fragment);
                bundle = new Bundle();
                return true;
            } else if (item.getItemId() == R.id.navigation_archive) {
                Myleads = false;
                replaceFragment(ClientArchivefragment.newInstance(bundle), R.id.nav_host_fragment);
                bundle = new Bundle();
                return true;
            }

           /* if (item.getItemId() == R.id.navigation_client) {
                replaceFragment(ClientFragment.newInstance(bundle), R.id.nav_host_fragment);
                bundle = new Bundle();
                return true;
            } else if (item.getItemId() == R.id.navigation_open_jobs) {
                replaceFragment(OpenJobsFragment.newInstance(bundle), R.id.nav_host_fragment);
                bundle = new Bundle();
                return true;
            } else if (item.getItemId() == R.id.navigation_tasks) {
                replaceFragment(TasksFragment.newInstance(bundle), R.id.nav_host_fragment);
                bundle = new Bundle();
                return true;
            } else if (item.getItemId() == R.id.navigation_network) {
                replaceFragment(MyNetworkFragment.newInstance(bundle), R.id.nav_host_fragment);
                bundle = new Bundle();
                return true;
            } else if (item.getItemId() == R.id.navigation_more) {
                replaceFragment(MoreFragment.newInstance(), R.id.nav_host_fragment);
                bundle = new Bundle();
                return true;
            }*/

            return false;
        });

        navigateNotification(getIntent());
        drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.navigationview);
        View header = navigationView.getHeaderView(0);
        ll_close = header.findViewById(R.id.ll_close);
        ll_profile = header.findViewById(R.id.ll_profile);
        ll_edit_profile = header.findViewById(R.id.ll_edit_profile);
        iv_user = header.findViewById(R.id.iv_user);
        tv_name = header.findViewById(R.id.tv_name);
        ll_logout = findViewById(R.id.ll_logout);

        ll_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                drawer.closeDrawer(GravityCompat.START);
            }
        });
        ll_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, EditProfile.class);
                startActivity(intent);
                drawer.closeDrawer(GravityCompat.START);
            }
        });
        ll_edit_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, EditProfile.class);
                startActivity(intent);
                drawer.closeDrawer(GravityCompat.START);
            }
        });
        ll_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TaskOS.getInstance().getDataManager().logout(MainActivity.this);
                drawer.closeDrawer(GravityCompat.START);
            }
        });
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();
                if (id == R.id.nav_home) {
                    showMyLeads();
                } else if (item.getItemId() == R.id.nav_clients) {
                    Myleads = false;
                    replaceFragment(ClientFragment.newInstance(bundle), R.id.nav_host_fragment);
                    bundle = new Bundle();
                } else if (item.getItemId() == R.id.nav_jobs) {
                    Myleads = false;
                    replaceFragment(OpenJobsFragment.newInstance(bundle), R.id.nav_host_fragment);
                    bundle = new Bundle();
                } else if (item.getItemId() == R.id.nav_tasks) {
                    Myleads = false;
                    replaceFragment(TasksFragment.newInstance(bundle), R.id.nav_host_fragment);
                    bundle = new Bundle();
                } else if (item.getItemId() == R.id.nav_network) {
                    Myleads = false;
                    replaceFragment(MyNetworkFragment.newInstance(bundle), R.id.nav_host_fragment);
                    bundle = new Bundle();
                } else if (item.getItemId() == R.id.nav_setting) {
                    Myleads = false;
                    replaceFragment(MoreFragment.newInstance(), R.id.nav_host_fragment);
                    bundle = new Bundle();
                } else if (item.getItemId() == R.id.nav_help) {
                    Myleads = false;
                    Intent intent = new Intent(MainActivity.this, HelpActivity.class);
                    startActivity(intent);
                }else if (item.getItemId() == R.id.nav_invoice) {
                    Myleads = false;
                    InvoiceBottomSheetDialog.newInstance().show(getSupportFragmentManager());

                }else if (item.getItemId() == R.id.nav_estimate) {
                    Myleads = false;
                    EstimateBottomSheetDialog.newInstance().show(getSupportFragmentManager());

                }
                drawer.closeDrawer(GravityCompat.START);
                return true;
            }
        });
    }

    private void navigateNotification(Intent intent) {
        if (intent.hasExtra("selectedView")) {
            String selectedView = intent.getStringExtra("selectedView");
            String tabView = intent.getStringExtra("tabView");
            String selectedUuid = intent.getStringExtra("selectedUuid");

            AppLogger.e("NotiUUID", "" + selectedUuid);
            bundle = new Bundle();
            bundle.putString("tabView", tabView);
            bundle.putString("selectedUuid", selectedUuid);
            assert selectedView != null;
            switch (selectedView) {

                case FcmNotificationBuilder.VIEW_CUSTOMER:
                    navView.setSelectedItemId(R.id.navigation_client);
                    break;

                case FcmNotificationBuilder.VIEW_JOB:
                    navView.setSelectedItemId(R.id.navigation_open_jobs);
                    break;

                case FcmNotificationBuilder.VIEW_TASK:
                    navView.setSelectedItemId(R.id.navigation_tasks);
                    break;

                case FcmNotificationBuilder.VIEW_NETWORK:
                    navView.setSelectedItemId(R.id.navigation_network);
                    break;
            }
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        navigateNotification(intent);
    }

    @Override
    public void onBackPressed() {
        if (this.drawer.isDrawerOpen(GravityCompat.START)) {
            this.drawer.closeDrawer(GravityCompat.START);
        } else {
            if (Myleads == true) {
                finish();
            } else {
                showMyLeads();
            }

        }
    }

    @Override
    protected void onResume() {
        getUserData();
        super.onResume();
    }

    public void showMyLeads() {
        replaceFragment(MyLeadsfragment.newInstance(bundle), R.id.nav_host_fragment);
        bundle = new Bundle();
        Myleads = true;
        navView.setSelectedItemId(R.id.navigation_myleads);
    }

    public void calldata() {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("MUser")
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        userlist = task.getResult().toObjects(MUser.class);

                        for (MUser mUser : userlist) {
                            Log.e("", mUser.getUserProfileImageLink() + "");
                            Glide.with(MainActivity.this)
                                    .load(mUser.getUserProfileImageLink())
                                    .centerCrop()
                                    .placeholder(R.drawable.ic_def_user)
                                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                                    .error(R.drawable.ic_def_user)
                                    .into(iv_user);
                        }
                    } else {

                    }
                });
    }
    public void getUserData(){
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        AppPreferencesHelper appPreferencesHelper=new AppPreferencesHelper(this);
        db.collection("MUser").document(appPreferencesHelper.getUUID())
                .addSnapshotListener((snapshot, e) -> {
                    MUser mUser = snapshot.toObject(MUser.class);
                        Log.e("", mUser.getUserProfileImageLink() + "");
                        Glide.with(MainActivity.this)
                                .load(mUser.getUserProfileImageLink())
                                .centerCrop()
                                .placeholder(R.drawable.ic_def_user)
                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                .error(R.drawable.ic_def_user)
                                .into(iv_user);
                    tv_name.setText(mUser.getUsername());

                });


    }
}
