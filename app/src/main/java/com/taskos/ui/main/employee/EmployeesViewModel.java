package com.taskos.ui.main.employee;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.taskos.data.DataManager;
import com.taskos.ui.auth.MUser;
import com.taskos.ui.base.BaseNavigator;
import com.taskos.ui.base.BaseViewModel;
import com.taskos.ui.company.Company;

import java.util.ArrayList;
import java.util.List;

public class EmployeesViewModel extends BaseViewModel<BaseNavigator> {

    private static final String TAG = "EmployeesViewModel";
    private List<MUser> mUsers = new ArrayList<>();
    private MutableLiveData<List<MUser>> mutableLiveDataUsers = new MutableLiveData<>();

    public EmployeesViewModel(DataManager dataManager) {
        super(dataManager);
    }


    MutableLiveData<List<MUser>> getEmployees() {
        mUsers.clear();
        if (getDataManager().getCompanyID().isEmpty()) {
            getDataManager().getFirestoreInstance().collection("MUser").document(getDataManager().getUUID()).addSnapshotListener((documentSnapshot, e) -> {
                if (e != null) {
                    Log.e(TAG, "onEvent: Listen failed.", e);
                    return;
                }
                if (documentSnapshot != null) {
                    MUser mUser1 = documentSnapshot.toObject(MUser.class);
                    if (mUser1 != null) {
                        mUsers.add(mUser1);
                    }
                    mutableLiveDataUsers.setValue(mUsers);
                }
            });
        } else {
            getDataManager().getFirestoreInstance().collection("Company").document(getDataManager().getCompanyID()).addSnapshotListener((documentSnapshot, e) -> {
                if (e != null) {
                    Log.e(TAG, "onEvent: Listen failed.", e);
                    return;
                }
                if (documentSnapshot != null) {
                    mUsers.clear();
                    Company company = documentSnapshot.toObject(Company.class);
                    if (company != null) {
                        for (String mUserUUID : company.getEmployeeList()) {
                            getDataManager().getFirestoreInstance().collection("MUser").document(mUserUUID).addSnapshotListener((documentSnapshot1, e1) -> {
                                if (e1 != null) {
                                    Log.e(TAG, "onEvent: Listen failed.", e1);
                                    return;
                                }
                                if (documentSnapshot1 != null) {
                                    MUser mUser1 = documentSnapshot1.toObject(MUser.class);
                                    if (mUser1 != null) {
                                        mUsers.add(mUser1);
                                    }
                                    mutableLiveDataUsers.setValue(mUsers);
                                }
                            });
                        }
                    }
                }
            });
        }
        return mutableLiveDataUsers;
    }
}