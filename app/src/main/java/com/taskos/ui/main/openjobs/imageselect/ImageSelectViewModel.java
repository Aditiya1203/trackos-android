package com.taskos.ui.main.openjobs.imageselect;

import android.annotation.SuppressLint;
import android.net.Uri;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.taskos.data.DataManager;
import com.taskos.data.model.api.Job;
import com.taskos.ui.auth.MUser;
import com.taskos.ui.base.BaseViewModel;
import com.taskos.util.AppLogger;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Objects;

public class ImageSelectViewModel extends BaseViewModel<ImageSelectNavigator> {
    private static final String TAG = "ImageSelectViewModel";
    private MutableLiveData<Job> mutableLiveData = new MutableLiveData<>();
    private MutableLiveData<MUser> mUserMutableLiveData = new MutableLiveData<>();

    public ImageSelectViewModel(DataManager dataManager) {
        super(dataManager);
    }

    MutableLiveData<MUser> getUserInfo(String uuid) {
        getDataManager().getFirestoreInstance().collection("MUser").document(uuid).addSnapshotListener((documentSnapshot, e) -> {
            if (e != null) {
                Log.e(TAG, "onEvent: Listen failed.", e);
                return;
            }
            if (documentSnapshot != null) {
                MUser mUser1 = documentSnapshot.toObject(MUser.class);
                if (mUser1 != null) {
                    mUserMutableLiveData.setValue(mUser1);
                }
            }
        });
        return mUserMutableLiveData;
    }

    MutableLiveData<Job> getJobDetail(String uuid) {
        DocumentReference query = getDataManager().getFirestoreInstance().collection("Job").document(uuid);

        query.addSnapshotListener((documentSnapshot, e) -> {
            if (e != null) {
                Log.e(TAG, "onEvent: Listen failed.", e);
                return;
            }

            if (documentSnapshot != null) {
                Job job = documentSnapshot.toObject(Job.class);
                if (job != null) {
                    mutableLiveData.setValue(job);
                }
            }
        });

        return mutableLiveData;
    }

    void doUploadImage(Job jobDetail, String name, Uri uri, List<String> tokens) {
        if (name.isEmpty()) {
            @SuppressLint("SimpleDateFormat")
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
            name = timeStamp + ".jpg";
        }
        StorageReference storageRef = FirebaseStorage.getInstance().getReference();
        final StorageReference ref = storageRef.child("images/".concat(name));
        UploadTask uploadTask = ref.putFile(uri);

        uploadTask.continueWithTask(task -> {
            if (!task.isSuccessful()) {
                throw Objects.requireNonNull(task.getException());
            }

            // Continue with the task to get the download URL
            return ref.getDownloadUrl();
        }).addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                Uri downloadUri = task.getResult();
                if (downloadUri != null) {
                    AppLogger.e("downloadUri", downloadUri.toString());
                    List<String> list = jobDetail.getImageLinks();
                    list.add(downloadUri.toString());
                    jobDetail.setImageLinks(list);

                    getDataManager().getFirestoreInstance().collection("Job").document(jobDetail.getUuid())
                            .set(jobDetail)
                            .addOnSuccessListener(aVoid -> {
                                /*ArrayList<String> tokenList = new ArrayList<>();
                                tokenList.add(getDataManager().getDeviceToken());
                                tokenList.addAll(tokens);
                                FcmNotificationBuilder.newInstance().setTitle("Job: ".concat(jobDetail.getName()))
                                        .setBody("New Image Added")
                                        .setSelectedView(FcmNotificationBuilder.VIEW_JOB)
                                        .setTabView(FcmNotificationBuilder.TAB_All_JOB)
                                        .setSelectedUUID(jobDetail.getUuid())
                                        .setReceiverFCMTokenList(AppUtils.removeDuplicates(tokenList))
                                        .sendNotification();*/
                                getNavigator().onUploadSuccess();
                            })
                            .addOnFailureListener(e -> getNavigator().onUploadFail());
                }
            } else {
                getNavigator().onUploadFail();
            }
        });
    }
}