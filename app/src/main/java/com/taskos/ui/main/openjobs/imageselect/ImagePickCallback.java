package com.taskos.ui.main.openjobs.imageselect;

import android.graphics.Bitmap;
import android.net.Uri;

/**
 * Created by hemant.
 * Date: 30/8/18
 * Time: 6:17 PM
 */

public interface ImagePickCallback {

    void onImageReceived(Uri uri, Bitmap bitmap);

}
