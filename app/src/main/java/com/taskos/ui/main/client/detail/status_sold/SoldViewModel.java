package com.taskos.ui.main.client.detail.status_sold;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.google.firebase.firestore.DocumentReference;
import com.taskos.data.DataManager;
import com.taskos.data.model.api.Customer;
import com.taskos.data.model.api.JobFinancial;
import com.taskos.ui.base.BaseViewModel;

public class SoldViewModel extends BaseViewModel<SoldNavigator> {

    private static final String TAG = "SoldViewModel";
    private MutableLiveData<Customer> mutableLiveData = new MutableLiveData<>();

    public SoldViewModel(DataManager dataManager) {
        super(dataManager);
    }

    MutableLiveData<Customer> getClientDetail(String customerKey) {
        DocumentReference query = getDataManager().getFirestoreInstance().collection("Customers").document(customerKey);

        query.addSnapshotListener((documentSnapshot, e) -> {
            if (e != null) {
                Log.e(TAG, "onEvent: Listen failed.", e);
                return;
            }

            if (documentSnapshot != null) {
                Customer customer = documentSnapshot.toObject(Customer.class);
                if (customer != null) {
                    mutableLiveData.setValue(customer);
                }
            }
        });

        return mutableLiveData;
    }

    void doSaveSaleInformation(String customerKey, Customer customer, JobFinancial jobFinancial) {
        getDataManager().getFirestoreInstance().collection("JobFinancial").document(jobFinancial.getUuid()).
                set(jobFinancial).addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                getDataManager().getFirestoreInstance().collection("Customers").document(customerKey)
                        .set(customer).addOnCompleteListener(task1 -> {
                    if (task1.isSuccessful()) {
                        getNavigator().dismissDialog();
                    } else {
                        getNavigator().onSaveError();
                    }
                });
            } else {
                getNavigator().onSaveError();
            }
        });
    }
}