package com.taskos.ui.main.client.detail.eventrepeat;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProvider;

import com.taskos.R;
import com.taskos.ViewModelProviderFactory;
import com.taskos.data.model.other.KeyValue;
import com.taskos.databinding.DialogEventCommonBinding;
import com.taskos.ui.base.BaseBottomSheetDialog;
import com.taskos.ui.main.client.ClientNavigator;
import com.taskos.util.CalenderUtils;

import java.util.ArrayList;
import java.util.List;


public class EventCommonBottomSheetDialog extends BaseBottomSheetDialog<DialogEventCommonBinding, EventCommonViewModel> implements ClientNavigator, View.OnClickListener {

    private static final String TAG = "EventCommonBottomSheetD";
    private EventCommonViewModel eventCommonViewModel;
    private List<KeyValue> keyValueList;
    private EventCommonCallback eventCommonCallback;


    public static EventCommonBottomSheetDialog newInstance(String TAG, EventCommonCallback eventCommonCallback) {

        Bundle args = new Bundle();

        EventCommonBottomSheetDialog fragment = new EventCommonBottomSheetDialog();
        fragment.setArguments(args);
        fragment.setCallback(TAG, eventCommonCallback);
        return fragment;
    }

    private void setCallback(String tag, EventCommonCallback eventCommonCallback) {
        this.eventCommonCallback = eventCommonCallback;
    }

    @Override
    public int getBindingVariable() {
        return com.taskos.BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.dialog_event_common;
    }

    @Override
    public EventCommonViewModel getViewModel() {
        eventCommonViewModel = new ViewModelProvider(this, ViewModelProviderFactory.getInstance()).get(EventCommonViewModel.class);
        return eventCommonViewModel;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        eventCommonViewModel.setNavigator(this);
        // weekday     = "SU" / "MO" / "TU" / "WE" / "TH" / "FR" / "SA"
        String day = CalenderUtils.getTimestamp(CalenderUtils.DAY);
        day = day.substring(0, day.length() - 1).toUpperCase();

        keyValueList = new ArrayList<>();
        keyValueList.add(new KeyValue("", "Never", true));
        keyValueList.add(new KeyValue("FREQ=DAILY", "Every Day"));
        keyValueList.add(new KeyValue("FREQ=WEEKLY;BYDAY=" + day, "Every Week"));
        keyValueList.add(new KeyValue("FREQ=WEEKLY;INTERVAL=2;BYDAY=" + day, "Every 2 Week"));
        keyValueList.add(new KeyValue("FREQ=MONTHLY;BYDAY=" + day, "Every Month"));
        keyValueList.add(new KeyValue("FREQ=YEARLY;BYDAY=" + day, "Every Year"));
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getViewDataBinding().imgBack.setOnClickListener(this);
        getViewDataBinding().tvTitle.setOnClickListener(this);
        getViewDataBinding().rlCustom.setOnClickListener(this);

        EventRepeatAdapter eventRepeatAdapter = new EventRepeatAdapter(keyValueList, pos -> {
            if (eventCommonCallback != null) {
                eventCommonCallback.onClick(keyValueList.get(pos));
                dismissDialog(TAG);
            }
        });
        getViewDataBinding().recyclerView.setAdapter(eventRepeatAdapter);
    }

    public void show(FragmentManager fragmentManager) {
        super.show(fragmentManager, TAG);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_title:
            case R.id.img_back:
                dismissDialog(TAG);
                break;

            case R.id.rl_custom:
                break;
        }
    }

    public interface EventCommonCallback {
        void onClick(KeyValue keyValue);
    }
}
