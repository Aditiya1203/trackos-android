package com.taskos.ui.main.help;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProvider;

import com.taskos.R;
import com.taskos.ViewModelProviderFactory;
import com.taskos.data.model.api.Customer;
import com.taskos.databinding.DialogAddJobBinding;
import com.taskos.ui.base.BaseBottomSheetDialog;
import com.taskos.ui.main.openjobs.addjob.AddJobViewModel;

import java.util.Date;


public class PrivacyPolicyBottomSheetDialog extends BaseBottomSheetDialog<DialogAddJobBinding, AddJobViewModel> implements View.OnClickListener {

    private static final String TAG = "PrivacyPolicyBottomSheetDialog";
    private AddJobViewModel addJobViewModel;

    public static PrivacyPolicyBottomSheetDialog newInstance() {

        Bundle args = new Bundle();
        PrivacyPolicyBottomSheetDialog fragment = new PrivacyPolicyBottomSheetDialog();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getBindingVariable() {
        return com.taskos.BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.dialog_privacy_policy;
    }

    @Override
    public AddJobViewModel getViewModel() {
        addJobViewModel = new ViewModelProvider(this, ViewModelProviderFactory.getInstance()).get(AddJobViewModel.class);
        return addJobViewModel;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        TextView tv_cancel=view.findViewById(R.id.tv_cancel);
        tv_cancel.setOnClickListener(this);
    }


    public void show(FragmentManager fragmentManager) {
        super.show(fragmentManager, TAG);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_cancel:
                dismissDialog(TAG);
                break;
        }
    }
}
