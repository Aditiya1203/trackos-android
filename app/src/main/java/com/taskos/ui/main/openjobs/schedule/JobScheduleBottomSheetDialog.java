package com.taskos.ui.main.openjobs.schedule;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProvider;

import com.taskos.R;
import com.taskos.ViewModelProviderFactory;
import com.taskos.data.model.api.Job;
import com.taskos.databinding.DialogScheduleJobBinding;
import com.taskos.ui.base.BaseBottomSheetDialog;
import com.taskos.ui.main.employee.EmployeesBottomSheetDialog;
import com.taskos.util.AppConstants;
import com.taskos.util.AppUtils;
import com.taskos.util.CalenderUtils;

import java.util.Date;


public class JobScheduleBottomSheetDialog extends BaseBottomSheetDialog<DialogScheduleJobBinding, JobScheduleViewModel> implements View.OnClickListener, JobScheduleNavigator {

    private static final String TAG = "JobScheduleBottomSheetD";
    private JobScheduleViewModel jobScheduleViewModel;
    private Date startDate = new Date(), endDate = new Date();
    private boolean isReschedule;
    private Job jobDetail;
    private boolean isEventAdded = false;

    public static JobScheduleBottomSheetDialog newInstance(boolean isReschedule, Job jobDetail) {

        Bundle args = new Bundle();

        JobScheduleBottomSheetDialog fragment = new JobScheduleBottomSheetDialog();
        args.putBoolean("isReschedule", isReschedule);
        fragment.setData(jobDetail);
        fragment.setArguments(args);
        return fragment;
    }

    private void setData(Job jobDetail) {
        this.jobDetail = jobDetail;
    }

    @Override
    public int getBindingVariable() {
        return com.taskos.BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.dialog_schedule_job;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            isReschedule = getArguments().getBoolean("isReschedule");
        }
    }

    @Override
    public JobScheduleViewModel getViewModel() {
        jobScheduleViewModel = new ViewModelProvider(this, ViewModelProviderFactory.getInstance()).get(JobScheduleViewModel.class);
        return jobScheduleViewModel;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        jobScheduleViewModel.setNavigator(this);
        getViewDataBinding().tvCancel.setOnClickListener(this);
        getViewDataBinding().tvStartDate.setOnClickListener(this);
        getViewDataBinding().tvEndDate.setOnClickListener(this);
        getViewDataBinding().rlAssignEmployee.setOnClickListener(this);
        getViewDataBinding().tvScheduleJob.setOnClickListener(this);

        if (isReschedule) {
            startDate = jobDetail.getInstallStart();
            endDate = jobDetail.getInstallEnd();
            if (!jobDetail.getAssignedEmployees().isEmpty()) {
                getViewDataBinding().llAssignView.setVisibility(View.VISIBLE);
                getViewDataBinding().tvAssignName.setText(jobDetail.getAssignedEmployees().get(0));
            }
        }
        getViewDataBinding().tvStartDate.setText(CalenderUtils.formatDate(startDate, CalenderUtils.TIMESTAMP_FORMAT2));
        getViewDataBinding().tvEndDate.setText(CalenderUtils.formatDate(endDate, CalenderUtils.TIMESTAMP_FORMAT2));
    }

    public void show(FragmentManager fragmentManager) {
        super.show(fragmentManager, TAG);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_cancel:
                dismissDialog(TAG);
                break;

            case R.id.tv_start_date:
                getViewDataBinding().viewStartTime.setVisibility(View.VISIBLE);
                getViewDataBinding().startTimePicker.setVisibility(View.VISIBLE);
                getViewDataBinding().startTimePicker.setDefaultDate(startDate);
                getViewDataBinding().startTimePicker.addOnDateChangedListener((displayed, date) -> {
                    startDate = date;
                    getViewDataBinding().tvStartDate.setText(CalenderUtils.formatDate(date, CalenderUtils.TIMESTAMP_FORMAT2));
                });
                break;

            case R.id.tv_end_date:
                getViewDataBinding().viewEndTime.setVisibility(View.VISIBLE);
                getViewDataBinding().endTimePicker.setVisibility(View.VISIBLE);
                getViewDataBinding().endTimePicker.setDefaultDate(endDate);
                getViewDataBinding().endTimePicker.addOnDateChangedListener((displayed, date) -> {
                    endDate = date;
                    getViewDataBinding().tvEndDate.setText(CalenderUtils.formatDate(date, CalenderUtils.TIMESTAMP_FORMAT2));
                });
                break;

            case R.id.rl_assign_employee:
                EmployeesBottomSheetDialog.newInstance(employee -> {
                    getViewDataBinding().llAssignView.setVisibility(View.VISIBLE);

                    getViewDataBinding().tvAssignName.setText(employee.getUserFirstName().concat(" ").concat(employee.getUserLastName()));
                    jobDetail.getAssignedEmployees().add(employee.getUserFirstName().concat(" ").concat(employee.getUserLastName()));
                    jobDetail.setAssignedEmployees(jobDetail.getAssignedEmployees());
                }).show(getChildFragmentManager());
                break;

            case R.id.tv_schedule_job:
                if (isNetworkConnected()) {
                    if (verifyInput()) {
                        if (isEventAdded) {
                            getViewDataBinding().tvScheduleJob.setClickable(false);
                            jobDetail.setInstallStart(startDate);
                            jobDetail.setInstallEnd(endDate);
                            jobDetail.setAssignedEmployees(AppUtils.removeDuplicates(jobDetail.getAssignedEmployees()));
                            jobDetail.setScheduledStatus(AppConstants.kJobsArray[2]);
                            jobScheduleViewModel.doScheduleJob(jobDetail);
                        } else {
                            getViewModel().addEventToCalendar(getBaseActivity(), jobDetail);
                            isEventAdded = true;
                        }
                    }
                } else {
                    showToast(getString(R.string.noInternet));
                }
                break;
        }
    }

    private boolean verifyInput() {
        if (getViewDataBinding().tvAssignName.getText().toString().isEmpty()) {
            showToast(getString(R.string.alert_select_employee));
            return false;
        } else return true;
    }


    @Override
    public void onJobScheduleSuccess() {
        getViewDataBinding().tvScheduleJob.setClickable(true);
        dismissDialog(TAG);
    }

    @Override
    public void onSaveError() {
        getViewDataBinding().tvScheduleJob.setClickable(true);
    }
}
