package com.taskos.ui.main.client.detail;

import android.app.Activity;
import android.content.Intent;
import android.provider.CalendarContract;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.google.firebase.firestore.DocumentReference;
import com.taskos.R;
import com.taskos.data.DataManager;
import com.taskos.data.model.api.Customer;
import com.taskos.ui.base.BaseViewModel;
import com.taskos.util.CommonUtils;

public class ClientDetailViewModel extends BaseViewModel<ClientDetailNavigator> {
    private static final String TAG = "ClientDetailViewModel";
    private MutableLiveData<Customer> mutableLiveData = new MutableLiveData<>();


    public ClientDetailViewModel(DataManager dataManager) {
        super(dataManager);
    }

    MutableLiveData<Customer> getClientDetail(String customerKey) {
        DocumentReference query = getDataManager().getFirestoreInstance().collection("Customers").document(customerKey);

        query.addSnapshotListener((documentSnapshot, e) -> {
            if (e != null) {
                Log.e(TAG, "onEvent: Listen failed.", e);
                return;
            }

            if (documentSnapshot != null) {
                Customer customer = documentSnapshot.toObject(Customer.class);
                if (customer != null) {
                    mutableLiveData.setValue(customer);
                }
            }
        });

        return mutableLiveData;
    }

    void doSaveAppointmentInfo(String customerKey, Customer customer) {
        getDataManager().getFirestoreInstance().collection("Customers").document(customerKey).set(customer);
    }

    void addEventToCalendar(Activity activity, Customer customer) {
        Intent intent = new Intent(Intent.ACTION_INSERT);
        intent.setType("vnd.android.cursor.item/event");


        intent.putExtra(CalendarContract.Events.EVENT_COLOR, R.color.light_pink);
        intent.putExtra(CalendarContract.Events.AVAILABILITY, CalendarContract.Events.AVAILABILITY_BUSY);

        intent.putExtra(CalendarContract.Events.TITLE, customer.getName().concat(" Appointment: ").concat(customer.getProductOfInterest()));

        String des = customer.getProductOfInterest() + "\n" + "Name: " + customer.getName() +
                "\n" + "Email: " + customer.getEmail() +
                "\n" + "Phone: " + CommonUtils.formatStringAsPhoneNumber(customer.getPhone());
        intent.putExtra(CalendarContract.Events.DESCRIPTION, des);

        String address = customer.getStreetAddress().concat(" ").concat(customer.getCity()).concat(", ").concat(customer.getState()).concat(" ").concat(customer.getZipcode());
        intent.putExtra(CalendarContract.Events.EVENT_LOCATION, address);

        activity.startActivity(intent);
    }
}