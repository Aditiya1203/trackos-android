package com.taskos.ui.main.openjobs;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;

import com.taskos.R;
import com.taskos.ViewModelProviderFactory;
import com.taskos.databinding.FragmentOpenJobsBinding;
import com.taskos.ui.base.BaseFragment;
import com.taskos.ui.main.client.addclient.AddClientBottomSheetDialog;
import com.taskos.ui.main.openjobs.addjob.AddJobBottomSheetDialog;
import com.taskos.ui.main.openjobs.detail.JobDetailFragment;
import com.taskos.ui.main.tasks.addtask.AddTaskBottomSheetDialog;
import com.taskos.util.AppConstants;


public class OpenJobsFragment extends BaseFragment<FragmentOpenJobsBinding, OpenJobsViewModel> implements View.OnClickListener, OpenJobsNavigator {

    private OpenJobsViewModel openJobsViewModel;
    private int currentPos = 0;

    public static OpenJobsFragment newInstance(Bundle bundle) {

        if (bundle == null) {
            bundle = new Bundle();
        }

        OpenJobsFragment fragment = new OpenJobsFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public int getBindingVariable() {
        return com.taskos.BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_open_jobs;
    }

    @Override
    public OpenJobsViewModel getViewModel() {
        openJobsViewModel = new ViewModelProvider(this, ViewModelProviderFactory.getInstance()).get(OpenJobsViewModel.class);
        return openJobsViewModel;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
           /* String view = getArguments().getString("tabView");
            switch (view == null ? "" : view) {
                case FcmNotificationBuilder.TAB_MY_LEAD:
                    currentPos = 1;
                    break;

                case FcmNotificationBuilder.TAB_ALL_CUSTOMER:
                    currentPos = 2;
                    break;

                default:
                    currentPos = 0;
                    break;

            }*/
            String uuid = getArguments().getString("selectedUuid");
            if (uuid != null) navigateToDetail(uuid);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        openJobsViewModel.setNavigator(this);
        getViewDataBinding().menuFloat.setOnMenuToggleListener(isVisible -> {
            if (isVisible) {
                getViewDataBinding().blurringView.setVisibility(View.VISIBLE);
                getViewDataBinding().blurringView.setBlurredView(getViewDataBinding().llMain);
            } else invalidateBlurView();
        });
        getViewDataBinding().fabAddClient.setOnClickListener(this);
        getViewDataBinding().fabAddJob.setOnClickListener(this);
        getViewDataBinding().fabAddTask.setOnClickListener(this);
        getViewDataBinding().tvSearch.setOnClickListener(this);

        getViewDataBinding().segmentedButtonGroup.setPosition(currentPos);
        filterData("");
        getViewDataBinding().segmentedButtonGroup.setOnClickedButtonListener(position -> {
            currentPos = position;
            filterData("");
        });

        doAddTextWatcher();
    }

    private void filterData(String search) {
        if (currentPos == 0) {
            getViewDataBinding().etSearch.setHint("Search Ready Jobs");
            doGetJobData(AppConstants.kJobsArray[0], 0, search);
        } else if (currentPos == 1) {
            getViewDataBinding().etSearch.setHint("Search Set Jobs");
            doGetJobData(AppConstants.kJobsArray[1], 1, search);
        } else {
            getViewDataBinding().etSearch.setHint("Search Go Jobs");
            doGetJobData(AppConstants.kJobsArray[3], 2, search);
        }
    }

    private void doAddTextWatcher() {
        getViewDataBinding().etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!editable.toString().isEmpty()) {
                    if (editable.toString().length() > 2)
                        filterData(editable.toString());
                } else {
                    filterData("");
                }
            }
        });
    }

    private void doGetJobData(String value, int currentPos, String search) {
        openJobsViewModel.getJobs(value, currentPos, search).observe(getViewLifecycleOwner(), jobArrayList -> {
                    OpenJobAdapter openJobAdapter = new OpenJobAdapter(currentPos == 2 ? OpenJobAdapter.VIEW_TYPE_JOB : OpenJobAdapter.VIEW_TYPE_COMMON,
                            jobArrayList, pos -> navigateToDetail(jobArrayList.get(pos).getUuid()));
                    getViewDataBinding().recyclerViewJobs.setAdapter(openJobAdapter);
                }
        );

    }

    private void navigateToDetail(String uuid) {
        getBaseActivity().addFragment(JobDetailFragment.newInstance(currentPos, uuid), R.id.nav_host_fragment, true);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fab_add_client:
                getViewDataBinding().llSearch.setVisibility(View.GONE);
                AddClientBottomSheetDialog.newInstance().show(getChildFragmentManager());
                getViewDataBinding().menuFloat.close(true);
                break;

            case R.id.fab_add_job:
                getViewDataBinding().llSearch.setVisibility(View.GONE);
                AddJobBottomSheetDialog.newInstance().show(getChildFragmentManager());
                getViewDataBinding().menuFloat.close(true);
                break;

            case R.id.fab_add_task:
                getViewDataBinding().llSearch.setVisibility(View.GONE);
                AddTaskBottomSheetDialog.newInstance().show(getChildFragmentManager());
                getViewDataBinding().menuFloat.close(true);
                break;

            case R.id.tv_search:
                getViewDataBinding().llSearch.setVisibility(getViewDataBinding().llSearch.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
                break;
        }
    }

    private void invalidateBlurView() {
        getViewDataBinding().blurringView.setVisibility(View.GONE);
        getViewDataBinding().blurringView.invalidate();
    }
}