package com.taskos.ui.main.more;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModelProvider;

import com.taskos.BR;
import com.taskos.R;
import com.taskos.TaskOS;
import com.taskos.ViewModelProviderFactory;
import com.taskos.databinding.FragmentMoreBinding;
import com.taskos.ui.auth.moreInfo.EditProfile;
import com.taskos.ui.base.BaseFragment;
import com.taskos.ui.company.AddCompanyActivity;
import com.taskos.ui.company.EditCompanyActivity;
import com.taskos.ui.emailCustomer.EmailCustomerActivity;
import com.taskos.ui.employee.EmployeeDetailsActivity;
import com.taskos.ui.howto.HowToActivity;
import com.taskos.ui.main.employee.EmployeesBottomSheetDialog;
import com.taskos.ui.main.network.search.SearchToNetworkFragment;
import com.taskos.ui.quickStart.QuickStartActivity;
import com.taskos.ui.tearm.TermsOfServices;

public class MoreFragment extends BaseFragment<FragmentMoreBinding, MoreViewModel> implements View.OnClickListener {

    private MoreViewModel moreViewModel;
    View view;

    public static MoreFragment newInstance() {

        Bundle args = new Bundle();

        MoreFragment fragment = new MoreFragment();
        fragment.setArguments(args);
        return fragment;
    }
    
    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_more;
    }

    @Override
    public MoreViewModel getViewModel() {
        moreViewModel = new ViewModelProvider(this, ViewModelProviderFactory.getInstance()).get(MoreViewModel.class);
        return moreViewModel;
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.view = view;
    }

    @Override
    public void onResume() {
        super.onResume();
        init(view);
    }

    private void init(View view) {
        view.findViewById(R.id.add_company).setOnClickListener(this);
        view.findViewById(R.id.edit_company).setOnClickListener(this);
        view.findViewById(R.id.edit_permission).setOnClickListener(this);
        view.findViewById(R.id.add_employee).setOnClickListener(this);
        view.findViewById(R.id.edit_profile).setOnClickListener(this);
        view.findViewById(R.id.quick_start).setOnClickListener(this);
        view.findViewById(R.id.how_to).setOnClickListener(this);
        view.findViewById(R.id.email_support).setOnClickListener(this);
        view.findViewById(R.id.terms_privacy).setOnClickListener(this);
        view.findViewById(R.id.logout).setOnClickListener(this);
        view.findViewById(R.id.email_template).setOnClickListener(this);

        if (TaskOS.getInstance().getDataManager().isCompanyAdded()) {
            getViewDataBinding().editCompanyLayout.setVisibility(View.VISIBLE);
            getViewDataBinding().addCompany.setVisibility(View.GONE);
        }else{
            getViewDataBinding().editCompanyLayout.setVisibility(View.GONE);
            getViewDataBinding().addCompany.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.add_company:
                startActivity(new Intent(getContext(), AddCompanyActivity.class));
                break;
            case R.id.edit_company:
                startActivity(new Intent(getContext(), EditCompanyActivity.class));
                break;
            case R.id.edit_permission:
                EmployeesBottomSheetDialog.newInstance(keyValue -> {
                    Intent intent = new Intent(getContext(), EmployeeDetailsActivity.class);
                    intent.putExtra("UUID", keyValue.getUuid());
                    startActivity(intent);
                }).show(getChildFragmentManager());
                break;
            case R.id.add_employee:
                getBaseActivity().addFragment(SearchToNetworkFragment.newInstance(), R.id.nav_host_fragment, true);
                break;
            case R.id.edit_profile:
                startActivity(new Intent(getContext(), EditProfile.class));
                break;
            case R.id.email_template:
                startActivity(new Intent(getContext(), EmailCustomerActivity.class));
                break;
            case R.id.quick_start:
                startActivity(new Intent(getContext(), QuickStartActivity.class));
                break;
            case R.id.how_to:
                startActivity(new Intent(getContext(), HowToActivity.class));
                break;
            case R.id.email_support:
                startActivity(new Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:".concat("support@traksos.com"))));
                break;
            case R.id.terms_privacy:
                startActivity(new Intent(getContext(), TermsOfServices.class));
                break;
            case R.id.logout:
                TaskOS.getInstance().getDataManager().logout(getActivity());
                break;
        }
    }
}