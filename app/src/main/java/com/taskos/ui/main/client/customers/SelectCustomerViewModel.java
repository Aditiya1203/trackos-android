package com.taskos.ui.main.client.customers;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.taskos.data.DataManager;
import com.taskos.data.model.api.Customer;
import com.taskos.data.model.other.CustomerSelect;
import com.taskos.ui.base.BaseNavigator;
import com.taskos.ui.base.BaseViewModel;

import java.util.ArrayList;

public class SelectCustomerViewModel extends BaseViewModel<BaseNavigator> {
    private static final String TAG = "SelectCustomerViewModel";
    private ArrayList<CustomerSelect> customerList = new ArrayList<>();
    private MutableLiveData<ArrayList<CustomerSelect>> mutableLiveDataCustomer = new MutableLiveData<>();

    public SelectCustomerViewModel(DataManager dataManager) {
        super(dataManager);
    }

    MutableLiveData<ArrayList<CustomerSelect>> getAllClients() {
        customerList.clear();
        Query query;
        query = getDataManager().getFirestoreInstance().collection("Customers")
                .whereEqualTo("owner", getDataManager().getCompanyID())
                .orderBy("appointmentEntered", Query.Direction.DESCENDING);

        query.addSnapshotListener((queryDocumentSnapshots, e) -> {
            if (e != null) {
                Log.e(TAG, "onEvent: Listen failed.", e);
                return;
            }

            if (queryDocumentSnapshots != null) {
                customerList.clear();
                for (QueryDocumentSnapshot doc : queryDocumentSnapshots) {
                    Customer customer = doc.toObject(Customer.class);
                    CustomerSelect customerSelect = new CustomerSelect(customer);
                    customerList.add(customerSelect);
                }
                mutableLiveDataCustomer.setValue(customerList);
            }
        });

        return mutableLiveDataCustomer;
    }

}