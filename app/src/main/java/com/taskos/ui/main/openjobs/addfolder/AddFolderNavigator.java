package com.taskos.ui.main.openjobs.addfolder;


/**
 * Created by Hemant Sharma on 23-02-20.
 */
public interface AddFolderNavigator {
    void onSaveSuccess(String tag);

    void onError();
}
