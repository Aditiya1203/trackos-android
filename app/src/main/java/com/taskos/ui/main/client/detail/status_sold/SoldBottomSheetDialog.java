package com.taskos.ui.main.client.detail.status_sold;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProvider;

import com.taskos.R;
import com.taskos.TaskOS;
import com.taskos.ViewModelProviderFactory;
import com.taskos.data.model.api.Customer;
import com.taskos.data.model.api.JobFinancial;
import com.taskos.databinding.DialogChangeSoldBinding;
import com.taskos.ui.base.BaseBottomSheetDialog;
import com.taskos.util.AppConstants;
import com.taskos.util.CalenderUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;


public class SoldBottomSheetDialog extends BaseBottomSheetDialog<DialogChangeSoldBinding, SoldViewModel> implements View.OnClickListener, SoldNavigator {

    private static final String TAG = "SoldBottomSheetDialog";
    private SoldViewModel soldViewModel;
    private Date targetTime = new Date();
    private String customerKey = "";
    private double soldPrice, total, salesTax;
    private Customer customer;

    public static SoldBottomSheetDialog newInstance(String customerKey) {

        Bundle args = new Bundle();

        SoldBottomSheetDialog fragment = new SoldBottomSheetDialog();
        args.putString("CustomerKey", customerKey);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getBindingVariable() {
        return com.taskos.BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.dialog_change_sold;
    }

    @Override
    public SoldViewModel getViewModel() {
        soldViewModel = new ViewModelProvider(this, ViewModelProviderFactory.getInstance()).get(SoldViewModel.class);
        return soldViewModel;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        soldViewModel.setNavigator(this);
        if (getArguments() != null) {
            customerKey = getArguments().getString("CustomerKey");
        }
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getViewDataBinding().tvCancel.setOnClickListener(this);
        getViewDataBinding().tvSave.setOnClickListener(this);
        getViewDataBinding().rlDate.setOnClickListener(this);
        getViewDataBinding().tvTargetDate.setText(CalenderUtils.formatDate(new Date(), CalenderUtils.CUSTOM_TIMESTAMP_FORMAT_SLASH));
        soldViewModel.getClientDetail(customerKey).observe(getViewLifecycleOwner(), customer -> {
            this.customer = customer;
            getViewDataBinding().tvName.setText(getString(R.string.new_sale_for).concat(" ").concat(customer.getName()));
        });

        addTextWatcher();
    }

    private void addTextWatcher() {
        getViewDataBinding().etSubTtl.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!editable.toString().isEmpty()) {
                    String soldTxt = getViewDataBinding().etSalesTax.getText().toString();
                    salesTax = soldTxt.isEmpty() ? 0 : Double.parseDouble(soldTxt);
                    soldPrice = editable.toString().equals(".") ? 0 : Double.parseDouble(editable.toString());
                    //old logic
                    //total = soldPrice + (soldPrice * (salesTax/100));
                    total = soldPrice + salesTax;
                    getViewDataBinding().tvTotal.setText(total+"");
                }
            }
        });


        getViewDataBinding().etSalesTax.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!editable.toString().isEmpty()) {
                    String soldTxt = getViewDataBinding().etSubTtl.getText().toString();
                    soldPrice = soldTxt.isEmpty() ? 0 : Double.parseDouble(soldTxt);
                    salesTax = editable.toString().equals(".") ? 0 : Double.parseDouble(editable.toString());
                    //old logic
                    //total = soldPrice + (soldPrice * (salesTax/100));
                    total = soldPrice + salesTax;
                    getViewDataBinding().tvTotal.setText(String.valueOf(total));
                }
            }
        });
    }

    public void show(FragmentManager fragmentManager) {
        super.show(fragmentManager, TAG);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_cancel:
                dismissDialog(TAG);
                break;

            case R.id.rl_date:
                if (getViewDataBinding().dateTimePicker.getVisibility() == View.VISIBLE) {
                    getViewDataBinding().view.setVisibility(View.GONE);
                    getViewDataBinding().dateTimePicker.setVisibility(View.GONE);
                } else {
                    getViewDataBinding().view.setVisibility(View.VISIBLE);
                    getViewDataBinding().dateTimePicker.setVisibility(View.VISIBLE);
                }

                getViewDataBinding().dateTimePicker.addOnDateChangedListener((displayed, date) -> {
                    targetTime = date;
                    getViewDataBinding().tvTargetDate.setText(CalenderUtils.formatDate(date, CalenderUtils.CUSTOM_TIMESTAMP_FORMAT_SLASH));
                });
                break;

            case R.id.tv_save:
                if (isNetworkConnected()) {
                    if (verifyInput()) {
                        getViewDataBinding().tvSave.setClickable(false);
                        JobFinancial jobFinancial = new JobFinancial();
                        jobFinancial.setUuid(UUID.randomUUID().toString());
                        jobFinancial.setCustomerUuid(customer.getUuid());
                        jobFinancial.setJobUuid("");
                        jobFinancial.setSoldDate(targetTime);
                        jobFinancial.setSubtotal(soldPrice);
                        jobFinancial.setSalesTax(salesTax);
                        jobFinancial.setSalesTotal(total);
                        jobFinancial.setPaymentAmountReceived(0.0);
                        jobFinancial.setInvoiceLinks("");
                        jobFinancial.setOwner(TaskOS.getInstance().getDataManager().getCompanyID());
                        jobFinancial.setPaymentAmounts(new ArrayList<>());
                        jobFinancial.setPaymentUuid(new ArrayList<>());
                        jobFinancial.setExpenseItem(new ArrayList<>());
                        jobFinancial.setExpenseAmount(new ArrayList<>());
                        jobFinancial.setExpenseUuid(new ArrayList<>());
                        jobFinancial.setPaymentType(new ArrayList<>());
                        customer.setCustomerStatus(AppConstants.kStatusArray[7]);
                        customer.setListOrganizer(AppConstants.kStatusListOrganiser[2]);
                        soldViewModel.doSaveSaleInformation(customerKey, customer, jobFinancial);
                    }
                } else {
                    showToast(getString(R.string.noInternet));
                }
                break;
        }
    }

    private boolean verifyInput() {
        if (getViewDataBinding().etSold.getText().toString().isEmpty()) {
            showToast(getString(R.string.alert_item_sold_empty));
            return false;
        }if (getViewDataBinding().etSubTtl.getText().toString().isEmpty()) {
            showToast(getString(R.string.alert_item_sub_total));
            return false;
        } else {
            return true;
        }
    }


    @Override
    public void dismissDialog() {
        getViewDataBinding().tvSave.setClickable(true);
        dismissDialog(TAG);
    }

    @Override
    public void onSaveError() {
        getViewDataBinding().tvSave.setClickable(true);
    }
}
