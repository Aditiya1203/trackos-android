package com.taskos.ui.main.client.addclient;


/**
 * Created by Hemant Sharma on 23-02-20.
 */
public interface AddClientNavigator {
    void openDashboard();

    void onSaveError();
}
