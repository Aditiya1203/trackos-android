package com.taskos.ui.main.openjobs.addjob;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProvider;

import com.taskos.R;
import com.taskos.ViewModelProviderFactory;
import com.taskos.data.model.api.Customer;
import com.taskos.data.model.api.Job;
import com.taskos.data.model.api.JobFinancial;
import com.taskos.databinding.DialogAddJobBinding;
import com.taskos.ui.base.BaseBottomSheetDialog;
import com.taskos.ui.main.client.customers.SelectCustomerBottomSheetDialog;
import com.taskos.util.CalenderUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;


public class AddJobBottomSheetDialog extends BaseBottomSheetDialog<DialogAddJobBinding, AddJobViewModel> implements View.OnClickListener, AddJobNavigator {

    private static final String TAG = "AddClientBottomSheetDia";
    private AddJobViewModel addJobViewModel;
    private Date targetTime = new Date();
    private double soldPrice, total, salesTax;
    private Customer customer;

    public static AddJobBottomSheetDialog newInstance() {

        Bundle args = new Bundle();

        AddJobBottomSheetDialog fragment = new AddJobBottomSheetDialog();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getBindingVariable() {
        return com.taskos.BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.dialog_add_job;
    }

    @Override
    public AddJobViewModel getViewModel() {
        addJobViewModel = new ViewModelProvider(this, ViewModelProviderFactory.getInstance()).get(AddJobViewModel.class);
        return addJobViewModel;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        addJobViewModel.setNavigator(this);
        getViewDataBinding().tvCancel.setOnClickListener(this);
        getViewDataBinding().tvStartDate.setOnClickListener(this);
        getViewDataBinding().tvSave.setOnClickListener(this);
        getViewDataBinding().tvCustomers.setOnClickListener(this);
        getViewDataBinding().tvStartDate.setText(CalenderUtils.formatDate(targetTime, CalenderUtils.TIMESTAMP_FORMAT));
        addTextWatcher();
    }

    private void addTextWatcher() {
        getViewDataBinding().etSubTtl.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!editable.toString().isEmpty()) {
                    String soldTxt = getViewDataBinding().etSalesTax.getText().toString();
                    salesTax = soldTxt.isEmpty() ? 0 : Double.parseDouble(soldTxt);
                    soldPrice = editable.toString().equals(".") ? 0 : Double.parseDouble(editable.toString());
                    //old logic
                    //total = soldPrice + (soldPrice * (salesTax/100));
                    total = soldPrice + salesTax;
                    getViewDataBinding().tvTotal.setText(total+"");
                }
            }
        });

        getViewDataBinding().etSalesTax.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!editable.toString().isEmpty()) {
                    String soldTxt = getViewDataBinding().etSubTtl.getText().toString();
                    soldPrice = soldTxt.isEmpty() ? 0 : Double.parseDouble(soldTxt);
                    salesTax = editable.toString().equals(".") ? 0 : Double.parseDouble(editable.toString());
                    //old logic
                    //total = soldPrice + (soldPrice * (salesTax/100));
                    total = soldPrice + salesTax;
                    getViewDataBinding().tvTotal.setText(String.valueOf(total));
                }
            }
        });
    }

    public void show(FragmentManager fragmentManager) {
        super.show(fragmentManager, TAG);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_cancel:
                dismissDialog(TAG);
                break;

            case R.id.tv_customers:
                SelectCustomerBottomSheetDialog.newInstance(customer -> {
                    this.customer = customer.getCustomer();
                    getViewDataBinding().tvCustomerName.setVisibility(View.VISIBLE);
                    getViewDataBinding().tvCustomerName.setText("Selected Customer: ".concat(customer.getCustomer().getName()));
                }).show(getChildFragmentManager());
                break;

            case R.id.tv_start_date:
                if (getViewDataBinding().startTimePicker.getVisibility() == View.VISIBLE) {
                    getViewDataBinding().viewStartTime.setVisibility(View.GONE);
                    getViewDataBinding().startTimePicker.setVisibility(View.GONE);
                } else {
                    getViewDataBinding().viewStartTime.setVisibility(View.VISIBLE);
                    getViewDataBinding().startTimePicker.setVisibility(View.VISIBLE);
                }
                getViewDataBinding().startTimePicker.addOnDateChangedListener((displayed, date) -> {
                    targetTime = date;
                    getViewDataBinding().tvStartDate.setText(CalenderUtils.formatDate(date, CalenderUtils.TIMESTAMP_FORMAT));
                });
                break;

            case R.id.tv_save:
                if (isNetworkConnected()) {
                    if (verifyInput()) {
                        showLoading();
                        getViewDataBinding().tvSave.setClickable(false);
                        Job job = new Job();
                        job.setUuid(UUID.randomUUID().toString());
                        job.setName(customer.getName());
                        job.setStreetAddress(customer.getStreetAddress());
                        job.setCity(customer.getCity());
                        job.setState(customer.getState());
                        job.setZipcode(customer.getZipcode());
                        job.setSoldItem(getViewDataBinding().etSold.getText().toString());
                        job.setInstallStart(targetTime);
                        job.setInstallEnd(targetTime);
                        job.setImageLinks(new ArrayList<>());
                        job.setSalesperson(customer.getSalesperson());
                        job.setCustomerId(customer.getUuid());
                        job.setAssignedEmployees(new ArrayList<>());
                        job.setScheduledStatus("Not Ready");  //(“Not Ready”, “Ready to Schedule”, etc.)
                        job.setOwner(customer.getOwner());
                        job.setSoldDate(targetTime);
                        job.setSubscribers(customer.getSubscribers());
                        job.setShared(false);
                        job.setSalesUuid(customer.getSalesUuid());

                        JobFinancial jobFinancial = new JobFinancial();
                        jobFinancial.setUuid(UUID.randomUUID().toString());
                        jobFinancial.setCustomerUuid(customer.getUuid());
                        jobFinancial.setJobUuid(job.getUuid());
                        jobFinancial.setSoldDate(job.getSoldDate());
                        jobFinancial.setSubtotal(soldPrice);
                        jobFinancial.setSalesTax(salesTax);
                        jobFinancial.setSalesTotal(total);
                        jobFinancial.setPaymentAmountReceived(0.0);
                        jobFinancial.setInvoiceLinks("");
                        jobFinancial.setOwner(customer.getOwner());
                        jobFinancial.setPaymentAmounts(new ArrayList<>());
                        jobFinancial.setPaymentUuid(new ArrayList<>());
                        jobFinancial.setExpenseItem(new ArrayList<>());
                        jobFinancial.setExpenseAmount(new ArrayList<>());
                        jobFinancial.setExpenseUuid(new ArrayList<>());
                        jobFinancial.setPaymentType(new ArrayList<>());

                        addJobViewModel.doSaveJobInformation(job, jobFinancial);
                    }
                } else {
                    showToast(getString(R.string.noInternet));
                }
                break;
        }
    }

    private boolean verifyInput() {
        if (getViewDataBinding().tvCustomerName.getText().toString().isEmpty()) {
            showToast(getString(R.string.alert_select_customer));
            return false;
        } else if (getViewDataBinding().etSold.getText().toString().isEmpty()) {
            showToast(getString(R.string.alert_item_sold_empty));
            return false;
        } else if (getViewDataBinding().etSubTtl.getText().toString().isEmpty()) {
            showToast(getString(R.string.alert_item_sub_total));
            return false;
        } else {
            return true;
        }
    }


    @Override
    public void dismiss(String TAG) {
        hideLoading();
        getViewDataBinding().tvSave.setClickable(true);
        dismissDialog(TAG);
    }

    @Override
    public void onSaveError() {
        hideLoading();
        getViewDataBinding().tvSave.setClickable(true);
    }
}
