package com.taskos.ui.main.openjobs.imageselect;


/**
 * Created by Hemant Sharma on 23-02-20.
 */
public interface ImageSelectNavigator {
    void onUploadSuccess();

    void onUploadFail();
}
