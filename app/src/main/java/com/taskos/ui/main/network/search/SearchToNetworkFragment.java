package com.taskos.ui.main.network.search;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModelProvider;

import com.taskos.BR;
import com.taskos.R;
import com.taskos.ViewModelProviderFactory;
import com.taskos.databinding.FragmentSearchToNetworkBinding;
import com.taskos.ui.auth.MUser;
import com.taskos.ui.base.BaseFragment;
import com.taskos.ui.base.BaseNavigator;
import com.taskos.ui.main.network.MyNetworkAdapter;
import com.taskos.ui.main.network.add.AddToNetworkFragment;

import java.util.ArrayList;
import java.util.List;


public class SearchToNetworkFragment extends BaseFragment<FragmentSearchToNetworkBinding, SearchToNetworkViewModel> implements View.OnClickListener, BaseNavigator {

    private SearchToNetworkViewModel searchToNetworkViewModel;
    private List<MUser> mUserList;
    private MyNetworkAdapter networkAdapter;

    public static SearchToNetworkFragment newInstance() {

        Bundle args = new Bundle();

        SearchToNetworkFragment fragment = new SearchToNetworkFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_search_to_network;
    }

    @Override
    public SearchToNetworkViewModel getViewModel() {
        searchToNetworkViewModel = new ViewModelProvider(this, ViewModelProviderFactory.getInstance()).get(SearchToNetworkViewModel.class);
        return searchToNetworkViewModel;
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        searchToNetworkViewModel.setNavigator(this);
        TextView tvTitle = view.findViewById(R.id.tvTitle);
        tvTitle.setText(R.string.search);
        tvTitle.setOnClickListener(this);
        view.findViewById(R.id.img_back).setOnClickListener(this);
        getViewDataBinding().tvJoin.setOnClickListener(this);
        getViewDataBinding().tvSearch.setOnClickListener(this);
        addTextWatcher();

        mUserList = new ArrayList<>();
        initAdapter();
    }

    private void initAdapter() {
        networkAdapter = new MyNetworkAdapter(mUserList, MyNetworkAdapter.VIEW_TYPE_NETWORK, pos -> getBaseActivity().addFragment(AddToNetworkFragment.newInstance(mUserList.get(pos).getUuid()), R.id.nav_host_fragment, true));
        getViewDataBinding().recyclerViewNetwork.setAdapter(networkAdapter);
    }

    private void addTextWatcher() {
        getViewDataBinding().etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().isEmpty()) {
                    getViewDataBinding().tvSearch.setAlpha(0.5f);
                    getViewDataBinding().tvSearch.setClickable(false);
                } else {
                    getViewDataBinding().tvSearch.setAlpha(1);
                    getViewDataBinding().tvSearch.setClickable(true);
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvTitle:
            case R.id.img_back:
                getBaseActivity().onBackPressed();
                break;

            case R.id.tv_join:
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT,
                        "I would like to collaborate with you on TraksOS! Download here: https://join.traksos.com/GetApp");
                sendIntent.setType("text/plain");

                Intent shareIntent = Intent.createChooser(sendIntent, null);
                startActivity(shareIntent);
                break;

            case R.id.tv_search:
                if (!getViewDataBinding().etSearch.getText().toString().isEmpty()) {
                    searchToNetworkViewModel.getUserList(getViewDataBinding().etSearch.getText().toString()).observe(getViewLifecycleOwner(), myUserList -> {
                                mUserList.clear();
                                mUserList.addAll(myUserList);
                                if (networkAdapter == null) initAdapter();
                                else networkAdapter.notifyDataSetChanged();
                            }
                    );
                }
                break;
        }
    }


}