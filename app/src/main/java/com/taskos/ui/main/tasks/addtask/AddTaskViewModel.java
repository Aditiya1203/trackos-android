package com.taskos.ui.main.tasks.addtask;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.taskos.data.DataManager;
import com.taskos.data.model.api.ToDo;
import com.taskos.ui.auth.MUser;
import com.taskos.ui.base.BaseViewModel;

import java.util.List;

public class AddTaskViewModel extends BaseViewModel<AddTaskNavigator> {

    private static final String TAG = "AddTaskViewModel";
    private MutableLiveData<MUser> mutableLiveData = new MutableLiveData<>();

    public AddTaskViewModel(DataManager dataManager) {
        super(dataManager);
    }

    void doAddTask(ToDo toDo, List<String> tokens) {
        getDataManager().getFirestoreInstance().collection("ToDo").document(toDo.getUuid()).set(toDo).addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
               /* ArrayList<String> tokenList = new ArrayList<>();
                tokenList.add(getDataManager().getDeviceToken());
                tokenList.addAll(tokens);
                FcmNotificationBuilder.newInstance().setTitle("You Have a new Customer Appointment")
                        .setBody("New Task Assigned by ".concat(toDo.getAssignedBy()))
                        .setSelectedView(FcmNotificationBuilder.VIEW_TASK)
                        .setTabView(FcmNotificationBuilder.TAB_MY_TASK)
                        .setSelectedUUID(toDo.getUuid())
                        .setReceiverFCMTokenList(AppUtils.removeDuplicates(tokenList))
                        .sendNotification();*/
                getNavigator().openDashboard();
            } else getNavigator().onSaveError();
        });
    }

    MutableLiveData<MUser> getCurrentUser() {
        getDataManager().getFirestoreInstance().collection("MUser").document(getDataManager().getUUID()).addSnapshotListener((documentSnapshot, e) -> {
            if (e != null) {
                Log.e(TAG, "onEvent: Listen failed.", e);
                return;
            }
            if (documentSnapshot != null) {
                MUser mUser1 = documentSnapshot.toObject(MUser.class);
                if (mUser1 != null) {
                    mutableLiveData.setValue(mUser1);
                }
            }
        });
        return mutableLiveData;
    }
}