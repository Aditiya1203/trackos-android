package com.taskos.ui.main.openjobs.schedule;


/**
 * Created by Hemant Sharma on 23-02-20.
 */
public interface JobScheduleNavigator {
    void onJobScheduleSuccess();

    void onSaveError();
}
