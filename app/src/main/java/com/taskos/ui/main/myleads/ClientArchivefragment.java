package com.taskos.ui.main.myleads;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import androidx.fragment.app.Fragment;

import com.taskos.R;
import com.taskos.ui.main.invoice.SelectClientBottomSheetDialog;


public class ClientArchivefragment extends Fragment {

    public static ClientArchivefragment newInstance(Bundle bundle) {

        if (bundle == null) {
            bundle = new Bundle();
        }
        ClientArchivefragment fragment = new ClientArchivefragment();
        fragment.setArguments(bundle);
        return fragment;

    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.client_archive_fragment, container, false);
        EditText et_client_search=v.findViewById(R.id.et_client_search);
        et_client_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ClientArchiveBottomSheetDialog.newInstance().show(getChildFragmentManager());
            }
        });
        return v;
    }
}
