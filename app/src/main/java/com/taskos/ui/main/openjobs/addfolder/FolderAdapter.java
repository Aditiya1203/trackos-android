package com.taskos.ui.main.openjobs.addfolder;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.taskos.R;
import com.taskos.data.model.api.DocumentFolder;
import com.taskos.data.model.api.ImageFolder;
import com.taskos.ui.base.BaseViewHolder;

import java.util.List;

/**
 * Created by Hemant Sharma on 23-02-20.
 */
public class FolderAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    private List<ImageFolder> imageFolders;
    private List<DocumentFolder> docFolders;
    private boolean isDoc;

    public FolderAdapter(List<ImageFolder> imageFolders) {
        isDoc = false;
        this.imageFolders = imageFolders;
    }

    public FolderAdapter(boolean isDoc, List<DocumentFolder> docFolders) {
        this.isDoc = isDoc;
        this.docFolders = docFolders;
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(
                LayoutInflater.from(parent.getContext()).inflate(R.layout.item_folders, parent, false));
    }

    @Override
    public int getItemCount() {
        if (imageFolders != null && imageFolders.size() > 0) {
            return imageFolders.size();
        } else if (docFolders != null && docFolders.size() > 0) {
            return docFolders.size();
        } else {
            return 0;
        }
    }


    public class ViewHolder extends BaseViewHolder {

        private TextView tv_user_label;

        public ViewHolder(View itemView) {
            super(itemView);
            tv_user_label = itemView.findViewById(R.id.tv_user_label);
        }

        public void onBind(int position) {

            if (isDoc) {
                DocumentFolder documentFolder = docFolders.get(position);
                tv_user_label.setText(documentFolder.getName());
            } else {
                ImageFolder imageFolder = imageFolders.get(position);
                tv_user_label.setText(imageFolder.getName());
            }
        }

    }

}
