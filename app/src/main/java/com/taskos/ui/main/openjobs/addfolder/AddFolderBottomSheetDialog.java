package com.taskos.ui.main.openjobs.addfolder;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProvider;

import com.taskos.R;
import com.taskos.ViewModelProviderFactory;
import com.taskos.data.model.api.DocumentFolder;
import com.taskos.data.model.api.ImageFolder;
import com.taskos.data.model.api.Job;
import com.taskos.databinding.DialogAddFolderBinding;
import com.taskos.ui.base.BaseBottomSheetDialog;

import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;


public class AddFolderBottomSheetDialog extends BaseBottomSheetDialog<DialogAddFolderBinding, AddFolderViewModel> implements View.OnClickListener, AddFolderNavigator {

    private static final String TAG = "AddFolderBottomSheetDia";
    private AddFolderViewModel addFolderViewModel;
    private boolean isImage;
    private Job job;

    public static AddFolderBottomSheetDialog newInstance(Job job, boolean isImage) {

        Bundle args = new Bundle();

        AddFolderBottomSheetDialog fragment = new AddFolderBottomSheetDialog();
        args.putBoolean("from", isImage);
        fragment.setJobData(job);
        fragment.setArguments(args);
        return fragment;
    }

    private void setJobData(Job job) {
        this.job = job;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            isImage = getArguments().getBoolean("from", false);
        }
    }

    @Override
    public int getBindingVariable() {
        return com.taskos.BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.dialog_add_folder;
    }

    @Override
    public AddFolderViewModel getViewModel() {
        addFolderViewModel = new ViewModelProvider(this, ViewModelProviderFactory.getInstance()).get(AddFolderViewModel.class);
        return addFolderViewModel;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        addFolderViewModel.setNavigator(this);
        getViewDataBinding().tvCancel.setOnClickListener(this);
        getViewDataBinding().tvSave.setOnClickListener(this);
    }


    public void show(FragmentManager fragmentManager) {
        super.show(fragmentManager, TAG);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_cancel:
                dismissDialog(TAG);
                break;

            case R.id.rl_users:
                break;

            case R.id.tv_employees:
                break;

            case R.id.tv_save:
                if (isNetworkConnected()) {
                    if (verifyInputs()) {
                        hideKeyboard();
                        if (job == null) {
                            showToast(getString(R.string.something_went_wrong));
                        } else {
                            getViewDataBinding().tvSave.setClickable(false);
                            showLoading();
                            if (isImage) {
                                ImageFolder imageFolder = new ImageFolder();
                                imageFolder.setUuid(UUID.randomUUID().toString());
                                imageFolder.setOwner(job.getOwner());
                                imageFolder.setName(getViewDataBinding().etFolderName.getText().toString());
                                imageFolder.setDateCreated(new Date());
                                imageFolder.setUserAccess(new ArrayList<>());
                                imageFolder.setJobUuid(job.getUuid());
                                imageFolder.setRestricted(false);
                                addFolderViewModel.doAddImageFolder(imageFolder);
                            } else {
                                DocumentFolder documentFolder = new DocumentFolder();
                                documentFolder.setUuid(UUID.randomUUID().toString());
                                documentFolder.setOwner(job.getOwner());
                                documentFolder.setName(getViewDataBinding().etFolderName.getText().toString());
                                documentFolder.setDateCreated(new Date());
                                documentFolder.setUserAccess(new ArrayList<>());
                                documentFolder.setJobUuid(job.getUuid());
                                documentFolder.setRestricted(false);
                                addFolderViewModel.doAddDocFolder(documentFolder);
                            }
                        }
                    }
                } else {
                    showToast(getString(R.string.noInternet));
                }
                break;
        }
    }

    private boolean verifyInputs() {
        if (getViewDataBinding().etFolderName.getText().toString().isEmpty()) {
            showToast(getString(R.string.alert_name_empty));
            return false;
        } else return true;
    }


    @Override
    public void onSaveSuccess(String tag) {
        hideLoading();
        dismissDialog(TAG);
        getViewDataBinding().tvSave.setClickable(true);
        showToast(getString(R.string.folder_create_success));
    }

    @Override
    public void onError() {
        hideLoading();
        getViewDataBinding().tvSave.setClickable(true);
        showToast(getString(R.string.something_went_wrong_folder));
    }
}
