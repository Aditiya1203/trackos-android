package com.taskos.ui.main.network;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.taskos.R;
import com.taskos.ui.auth.MUser;
import com.taskos.ui.base.BaseViewHolder;
import com.taskos.ui.base.ItemClickCallback;

import java.util.List;

/**
 * Created by Hemant Sharma on 23-02-20.
 */
public class MyNetworkAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    public static int VIEW_TYPE_COMMON = 1;
    public static int VIEW_TYPE_NETWORK = 2;
    private List<MUser> mUserList;
    private int viewType;
    private ItemClickCallback itemClickCallback;

    public MyNetworkAdapter(List<MUser> mUserList, int viewType) {
        this.mUserList = mUserList;
        this.viewType = viewType;
    }

    public MyNetworkAdapter(List<MUser> mUserList, int viewType, ItemClickCallback itemClickCallback) {
        this.mUserList = mUserList;
        this.viewType = viewType;
        this.itemClickCallback = itemClickCallback;
    }

    public void setViewType(int viewType) {
        this.viewType = viewType;
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_NETWORK) {
            return new ViewHolder(
                    LayoutInflater.from(parent.getContext()).inflate(R.layout.item_my_network, parent, false));
        } else {
            return new ViewHolder(
                    LayoutInflater.from(parent.getContext()).inflate(R.layout.item_common_network, parent, false));
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (viewType == VIEW_TYPE_NETWORK) {
            return VIEW_TYPE_NETWORK;
        } else {
            return VIEW_TYPE_COMMON;
        }
    }

    @Override
    public int getItemCount() {
        if (mUserList != null && mUserList.size() > 0) {
            return mUserList.size();
        } else {
            return 0;
        }
    }

    public class ViewHolder extends BaseViewHolder {
        private TextView tv_name, tv_city, tv_job;
        private ImageView img_user;

        public ViewHolder(View itemView) {
            super(itemView);
            img_user = itemView.findViewById(R.id.img_user);
            tv_name = itemView.findViewById(R.id.tv_name);
            tv_city = itemView.findViewById(R.id.tv_city);

            tv_job = itemView.findViewById(R.id.tv_job);

            itemView.setOnClickListener(v -> {
                if (itemClickCallback != null && getAdapterPosition() != -1) {
                    itemClickCallback.onItemClick(getAdapterPosition());
                }
            });
        }

        public void onBind(int position) {
            MUser tmpBean = mUserList.get(position);
            if (viewType == VIEW_TYPE_NETWORK) {
                tv_name.setText(tmpBean.getUserFirstName().concat(" ").concat(tmpBean.getUserLastName()));
                tv_city.setText(tmpBean.getCity());
                if (!tmpBean.getUserProfileImageLink().isEmpty()) {
                    Glide.with(img_user.getContext())
                            .load(tmpBean.getUserProfileImageLink())
                            .centerCrop()
                            .placeholder(R.drawable.ic_def_user)
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .error(R.drawable.ic_def_user)
                            .into(img_user);
                } else {
                    Glide.with(img_user.getContext())
                            .load(R.drawable.ic_def_user)
                            .centerCrop()
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .error(R.drawable.ic_def_user)
                            .into(img_user);
                }
            } else {
                tv_name.setText(tmpBean.getUserLastName().concat(", ").concat(tmpBean.getUserFirstName()));
                tv_job.setText("Job: ".concat(tmpBean.getJobRole()));
            }

        }
    }
}
