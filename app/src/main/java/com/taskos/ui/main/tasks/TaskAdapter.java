package com.taskos.ui.main.tasks;

import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.taskos.R;
import com.taskos.data.model.api.ToDo;
import com.taskos.ui.base.BaseViewHolder;
import com.taskos.ui.base.ItemClickCallback;
import com.taskos.util.CalenderUtils;

import java.util.List;

/**
 * Created by Hemant Sharma on 23-02-20.
 */
public class TaskAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    static int VIEW_TYPE_COMMON = 1;
    static int VIEW_ALL_OPEN_TASK = 2;
    private ItemClickCallback callback;
    private List<ToDo> todoList;
    private int viewType;

    TaskAdapter(int viewType, List<ToDo> todoList, ItemClickCallback callback) {
        this.viewType = viewType;
        this.todoList = todoList;
        this.callback = callback;
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == VIEW_ALL_OPEN_TASK) {
            return new ViewHolder(
                    LayoutInflater.from(parent.getContext()).inflate(R.layout.item_all_open_task, parent, false));
        } else {
            return new ViewHolder(
                    LayoutInflater.from(parent.getContext()).inflate(R.layout.item_my_task, parent, false));
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (viewType == VIEW_ALL_OPEN_TASK) {
            return VIEW_ALL_OPEN_TASK;
        } else {
            return VIEW_TYPE_COMMON;
        }
    }

    @Override
    public int getItemCount() {
        if (todoList != null && todoList.size() > 0) {
            return todoList.size();
        } else {
            return 0;
        }
    }

    public class ViewHolder extends BaseViewHolder {
        private TextView tv_name, tv_task_name, tv_status, tv_appoint_time, tv_assign_name;
        private FrameLayout frame_status, frame_time;

        ViewHolder(View itemView) {
            super(itemView);
            tv_name = itemView.findViewById(R.id.tv_name);
            tv_task_name = itemView.findViewById(R.id.tv_task_name);
            tv_status = itemView.findViewById(R.id.tv_status);
            tv_appoint_time = itemView.findViewById(R.id.tv_appoint_time);
            tv_assign_name = itemView.findViewById(R.id.tv_assign_name);
            frame_status = itemView.findViewById(R.id.frame_status);
            frame_time = itemView.findViewById(R.id.frame_time);

            itemView.setOnClickListener(view -> {
                if (callback != null && getAdapterPosition() != -1) {
                    callback.onItemClick(getAdapterPosition());
                }
            });
        }

        public void onBind(int position) {
            ToDo tmpBean = todoList.get(position);

            if (viewType == VIEW_ALL_OPEN_TASK) {
                tv_name.setText(tmpBean.getJobName());
                tv_task_name.setText(tmpBean.getTask());
                tv_status.setText(tmpBean.getCompletionStatus());
                tv_assign_name.setText(tv_assign_name.getContext().getString(R.string.assigned_to_col).concat(" ").concat(tmpBean.getAssignedBy()));
                tv_appoint_time.setText(CalenderUtils.formatDate(tmpBean.getDateEntered(), CalenderUtils.CUSTOM_TIMESTAMP_FORMAT_AM));
                if (tmpBean.getCompletionStatus().equals("Past Due")) {
                    tv_appoint_time.setTextColor(tv_appoint_time.getContext().getResources().getColor(R.color.white));
                    tv_status.setTextColor(tv_appoint_time.getContext().getResources().getColor(R.color.white));
                    frame_time.setBackgroundResource(R.drawable.bg_red_drawable);
                    frame_status.setBackgroundResource(R.drawable.bg_red_drawable);
                } else {
                    tv_appoint_time.setTextColor(tv_appoint_time.getContext().getResources().getColor(R.color.black));
                    tv_status.setTextColor(tv_appoint_time.getContext().getResources().getColor(R.color.black));
                    frame_status.setBackgroundResource(R.drawable.bg_green_drawable);
                    frame_time.setBackgroundResource(R.color.transparent);
                }
            } else {
                tv_name.setPaintFlags(tv_name.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
                tv_name.setText(tmpBean.getJobName());
                tv_task_name.setText(tmpBean.getTask());
                tv_appoint_time.setText(CalenderUtils.formatDate(tmpBean.getDateEntered(), CalenderUtils.CUSTOM_TIMESTAMP_FORMAT_AM));
                tv_status.setText(tmpBean.getCompletionStatus());
                if (tmpBean.getCompletionStatus().equals("Past Due")) {
                    tv_appoint_time.setTextColor(tv_appoint_time.getContext().getResources().getColor(R.color.white));
                    tv_status.setTextColor(tv_appoint_time.getContext().getResources().getColor(R.color.white));
                    frame_time.setBackgroundResource(R.drawable.bg_red_drawable);
                    frame_status.setBackgroundResource(R.drawable.bg_red_drawable);
                } else {
                    tv_appoint_time.setTextColor(tv_appoint_time.getContext().getResources().getColor(R.color.black));
                    tv_status.setTextColor(tv_appoint_time.getContext().getResources().getColor(R.color.black));
                    frame_status.setBackgroundResource(R.drawable.bg_green_drawable);
                    frame_time.setBackgroundResource(R.color.transparent);
                }
            }
        }
    }
}
