package com.taskos.ui.main.openjobs.detail;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.taskos.BR;
import com.taskos.R;
import com.taskos.TaskOS;
import com.taskos.ViewModelProviderFactory;
import com.taskos.data.model.api.Job;
import com.taskos.databinding.FragmentJobDetailBinding;
import com.taskos.ui.base.BaseFragment;
import com.taskos.ui.main.client.detail.ClientDetailFragment;
import com.taskos.ui.main.openjobs.detail.comment.CommentActivity;
import com.taskos.ui.main.openjobs.documents.DocumentFragment;
import com.taskos.ui.main.openjobs.financial.FinancialFragment;
import com.taskos.ui.main.openjobs.images.ImagesFragment;
import com.taskos.ui.main.openjobs.schedule.JobScheduleBottomSheetDialog;
import com.taskos.ui.main.openjobs.share.JobShareFragment;
import com.taskos.util.AppConstants;
import com.taskos.util.AppUtils;
import com.taskos.util.CalenderUtils;

import java.util.Date;

public class JobDetailFragment extends BaseFragment<FragmentJobDetailBinding, JobDetailViewModel> implements JobDetailNavigator, View.OnClickListener, OnMapReadyCallback {

    private JobDetailViewModel jobDetailViewModel;
    private String jobUUID = "";
    private int tabPos;
    private Date lastDate;
    private Date soldTime;
    private Job jobDetail;

    //map params
    private MapView mapView;
    private GoogleMap googleMap;

    private Bundle mapViewBundle;
    private LatLng latLng;

    public static JobDetailFragment newInstance(int currentPos, String jobUUID) {

        Bundle args = new Bundle();

        JobDetailFragment fragment = new JobDetailFragment();
        args.putInt("TabPos", currentPos);
        args.putString("JobKey", jobUUID);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_job_detail;
    }

    @Override
    public JobDetailViewModel getViewModel() {
        jobDetailViewModel = new ViewModelProvider(this, ViewModelProviderFactory.getInstance()).get(JobDetailViewModel.class);
        return jobDetailViewModel;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        jobDetailViewModel.setNavigator(this);
        if (getArguments() != null) {
            jobUUID = getArguments().getString("JobKey");
            tabPos = getArguments().getInt("TabPos");
        }

        if (savedInstanceState != null) {
            mapViewBundle = savedInstanceState.getBundle(AppConstants.MAP_VIEW_BUNDLE_KEY);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        TextView tvTitle = view.findViewById(R.id.tvTitle);
        tvTitle.setText(R.string.open_jobs);
        setOnClickListener(tvTitle
                , view.findViewById(R.id.img_back)
                , getViewDataBinding().rlShare
                , getViewDataBinding().rlSubscribe
                , getViewDataBinding().rlJobFinancial
                , getViewDataBinding().rlCustDetail
                , getViewDataBinding().rlShowImages
                , getViewDataBinding().rlShowDocument
                , getViewDataBinding().llDirection
                , getViewDataBinding().tvJobSchedule
                , getViewDataBinding().tvEditJob
                , getViewDataBinding().tvSaveJobInfo
                , getViewDataBinding().tvSoldTime
                , getViewDataBinding().tvEditJobLoc
                , getViewDataBinding().tvSaveLoc
                , getViewDataBinding().rlJobComment
                , getViewDataBinding().tvJobComplete);

        jobDetailViewModel.getJobDetail(jobUUID).observe(getViewLifecycleOwner(), jobDetail -> {
            this.jobDetail = jobDetail;
            getViewDataBinding().etJobName.setText(jobDetail.getName());
            getViewDataBinding().tvSalesperson.setText(jobDetail.getSalesperson());
            getViewDataBinding().tvNameMap.setText(jobDetail.getName());
            getViewDataBinding().tvName.setText(jobDetail.getName());
            getViewDataBinding().etSoldItem.setText(jobDetail.getSoldItem());
            getViewDataBinding().tvSoldTime.setText(CalenderUtils.formatDate(jobDetail.getSoldDate(), CalenderUtils.CUSTOM_TIMESTAMP_FORMAT_AM));
            lastDate = jobDetail.getSoldDate();
            soldTime = lastDate;

            getViewDataBinding().etStreet.setText(jobDetail.getStreetAddress());
            getViewDataBinding().etCity.setText(jobDetail.getCity());
            getViewDataBinding().etState.setText(jobDetail.getState());
            getViewDataBinding().etZipCode.setText(jobDetail.getZipcode());
            getViewDataBinding().tvStreet.setText(jobDetail.getStreetAddress());
            getViewDataBinding().tvAddress.setText(jobDetail.getCity().concat(", ").concat(jobDetail.getState()).concat(" ").concat(jobDetail.getZipcode()));
            getViewDataBinding().tvShareLabel.setText(jobDetail.isShared() ? R.string.shared : R.string.share_job);

            if (jobDetail.getSubscribers().contains(TaskOS.getInstance().getDataManager().getUUID())) {
                getViewDataBinding().imgSubscribe.setColorFilter(ContextCompat.getColor(getBaseActivity(),
                        R.color.green), android.graphics.PorterDuff.Mode.SRC_IN);
                getViewDataBinding().tvSubscribe.setText(R.string.subscribed);
            } else {
                getViewDataBinding().imgSubscribe.setColorFilter(ContextCompat.getColor(getBaseActivity(),
                        R.color.gray), android.graphics.PorterDuff.Mode.SRC_IN);
                getViewDataBinding().tvSubscribe.setText(R.string.subscribe_to_notifications);
            }

            String address = jobDetail.getStreetAddress().concat(" ").concat(jobDetail.getCity()).concat(", ").concat(jobDetail.getState()).concat(" ").concat(jobDetail.getZipcode());
            latLng = getLocationFromAddress(address);
            if (latLng != null) {
                updateMapUI(latLng);
            }

            updateJobStatusView();
        });

        // when the map is ready to be used.
        mapView = view.findViewById(R.id.map);
        assert mapView != null;
        mapView.onCreate(mapViewBundle);
        mapView.onResume();
        mapView.getMapAsync(this);
    }

    private void updateJobStatusView() {
        switch (tabPos) {
            case 0:
                if (jobDetail.getScheduledStatus().equals(AppConstants.kJobsArray[0]))
                    getViewDataBinding().tvJobSchedule.setText(R.string.book_job);
                else getViewDataBinding().tvJobSchedule.setText(R.string.job_ready_for_scheduling);
                break;

            case 1:
                if (jobDetail.getScheduledStatus().equals(AppConstants.kJobsArray[1]))
                    getViewDataBinding().tvJobSchedule.setText(R.string.job_scheduling_assign);
                else getViewDataBinding().tvJobSchedule.setText(R.string.job_scheduled);
                break;

            case 2:
                getViewDataBinding().tvJobComplete.setVisibility(View.VISIBLE);
                getViewDataBinding().tvJobCompleteLabel.setVisibility(View.VISIBLE);
                if (jobDetail.getScheduledStatus().equals(AppConstants.kJobsArray[2])) {
                    getViewDataBinding().tvJobScheduleLabel.setText(R.string.reschedule_job);
                    getViewDataBinding().tvJobSchedule.setText(R.string.job_rescheduling_assign);
                    getViewDataBinding().tvJobComplete.setText(R.string.mark_job_complete);
                } else {
                    getViewDataBinding().tvJobScheduleLabel.setText(R.string.reschedule_job);
                    getViewDataBinding().tvJobSchedule.setText(R.string.job_rescheduling_assign);
                    getViewDataBinding().tvJobScheduleLabel.setVisibility(View.GONE);
                    getViewDataBinding().tvJobSchedule.setVisibility(View.GONE);

                    getViewDataBinding().tvJobComplete.setText(R.string.job_completed);
                }
                break;
        }
    }

    private void setOnClickListener(View... views) {
        for (View view : views) {
            view.setOnClickListener(this);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tvTitle:
            case R.id.img_back:
                getBaseActivity().onBackPressed();
                break;

            case R.id.rl_share:
                getBaseActivity().addFragment(JobShareFragment.newInstance(jobUUID), R.id.nav_host_fragment, true);
                break;

            case R.id.rl_subscribe:
                if (isNetworkConnected()) {
                    String value = getViewDataBinding().tvSubscribe.getText().toString();
                    if (value.equals(getString(R.string.subscribed))) {
                        jobDetail.getSubscribers().remove(TaskOS.getInstance().getDataManager().getUUID());  //jobDetail.getCustomerId()
                        jobDetail.setSubscribers(AppUtils.removeDuplicates(jobDetail.getSubscribers()));
                        jobDetailViewModel.doSaveJobInfo(jobDetail);
                        getViewDataBinding().imgSubscribe.setColorFilter(ContextCompat.getColor(getBaseActivity(),
                                R.color.gray), android.graphics.PorterDuff.Mode.SRC_IN);
                        getViewDataBinding().tvSubscribe.setText(R.string.subscribe_to_notifications);
                    } else {
                        jobDetail.getSubscribers().add(TaskOS.getInstance().getDataManager().getUUID());
                        jobDetailViewModel.doSaveJobInfo(jobDetail);
                        getViewDataBinding().imgSubscribe.setColorFilter(ContextCompat.getColor(getBaseActivity(),
                                R.color.green), android.graphics.PorterDuff.Mode.SRC_IN);
                        getViewDataBinding().tvSubscribe.setText(R.string.subscribed);
                    }
                } else {
                    showToast(getString(R.string.noInternet));
                }
                break;

            case R.id.rl_job_financial:
//                getBaseActivity().addFragment(FinancialFragment.newInstance(jobUUID), R.id.nav_host_fragment, true);
                break;

            case R.id.rl_cust_detail:
                getBaseActivity().addFragment(ClientDetailFragment.newInstance(jobDetail.getCustomerId()), R.id.nav_host_fragment, true);
                break;

            case R.id.rl_show_images:
//                getBaseActivity().addFragment(ImagesFragment.newInstance(jobUUID), R.id.nav_host_fragment, true);
                break;

            case R.id.rl_show_document:
//                getBaseActivity().addFragment(DocumentFragment.newInstance(jobUUID), R.id.nav_host_fragment, true);
                break;

            case R.id.rl_job_comment:
                Intent intent = CommentActivity.newIntent(getBaseActivity());
                intent.putExtra("jobUui", jobUUID);
                startActivity(intent);
                break;

            case R.id.ll_direction:
                //"geo:22.7196, 75.8577?q=22.7196, 75.8577(Indore)"
                String loc = "geo:".concat(String.valueOf(latLng.latitude)).concat(", ").concat(String.valueOf(latLng.longitude)).concat("?q=").concat(String.valueOf(latLng.latitude)).concat(", ").concat(String.valueOf(latLng.longitude)).concat("(").concat(jobDetail.getCity()).concat(")");
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(loc));
                /*Uri gmmIntentUri = Uri.parse("geo: 22.7196, 75.8577");
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);*/
                mapIntent.setPackage("com.google.android.apps.maps");
                if (mapIntent.resolveActivity(getBaseActivity().getPackageManager()) != null) {
                    startActivity(mapIntent);
                }
                break;

            case R.id.tv_edit_job_loc:
                if (getViewDataBinding().tvEditJobLoc.getText().toString().equals("Edit")) {
                    getViewDataBinding().tvEditJobLoc.setText(R.string.cancel);
                    enableDisableLocView(true);
                } else {
                    getViewDataBinding().tvEditJobLoc.setText(R.string.edit);
                    enableDisableLocView(false);
                }
                break;

            case R.id.tv_save_loc:
                if (isNetworkConnected()) {
                    jobDetail.setStreetAddress(getViewDataBinding().etStreet.getText().toString());
                    jobDetail.setCity(getViewDataBinding().etCity.getText().toString());
                    jobDetail.setState(getViewDataBinding().etState.getText().toString());
                    jobDetail.setZipcode(getViewDataBinding().etZipCode.getText().toString());
                    jobDetailViewModel.doSaveJobInfo(jobDetail);
                    getViewDataBinding().tvEditJobLoc.setText(R.string.edit);
                    enableDisableLocView(false);
                } else {
                    showToast(getString(R.string.noInternet));
                }
                break;

            case R.id.tv_edit_job:
                hideKeyboard();
                if (getViewDataBinding().tvEditJob.getText().toString().equals("Edit")) {
                    getViewDataBinding().tvEditJob.setText(R.string.cancel);
                    enableDisableJobInformation(true);
                } else {
                    getViewDataBinding().tvSoldTime.setText(CalenderUtils.formatDate(lastDate, CalenderUtils.CUSTOM_TIMESTAMP_FORMAT_AM));
                    getViewDataBinding().tvEditJob.setText(R.string.edit);
                    enableDisableJobInformation(false);
                }
                break;

            case R.id.tv_sold_time:
                if (getViewDataBinding().tvEditJob.getText().toString().equals("Cancel")) {
                    if (getViewDataBinding().dateSoldTimePicker.getVisibility() == View.VISIBLE) {
                        getViewDataBinding().llSalesPerson.setVisibility(View.VISIBLE);
                        getViewDataBinding().viewSalesPerson.setVisibility(View.VISIBLE);
                        getViewDataBinding().viewTime.setVisibility(View.GONE);
                        getViewDataBinding().dateSoldTimePicker.setVisibility(View.GONE);
                    } else {
                        getViewDataBinding().llSalesPerson.setVisibility(View.GONE);
                        getViewDataBinding().viewSalesPerson.setVisibility(View.GONE);
                        getViewDataBinding().viewTime.setVisibility(View.VISIBLE);
                        getViewDataBinding().dateSoldTimePicker.setVisibility(View.VISIBLE);
                    }

                    getViewDataBinding().dateSoldTimePicker.addOnDateChangedListener((displayed, date) -> {
                        soldTime = date;
                        getViewDataBinding().tvSoldTime.setText(CalenderUtils.formatDate(date, CalenderUtils.TIMESTAMP_FORMAT));
                    });
                }
                break;

            case R.id.tv_save_job_info:
                if (isNetworkConnected()) {
                    if (verifyInput()) {
                        enableDisableJobInformation(false);
                        //save job info
                        showToast(getString(R.string.under_development));
                    }
                } else {
                    showToast(getString(R.string.noInternet));
                }
                break;

            case R.id.tv_job_schedule:
                switch (tabPos) {
                    case 0:
                        if (getViewDataBinding().tvJobSchedule.getText().toString().equals(getString(R.string.book_job))) {
                            if (isNetworkConnected()) {
                                jobDetail.setScheduledStatus(AppConstants.kJobsArray[1]);
                                jobDetailViewModel.doScheduleJob(jobDetail);
                            } else {
                                showToast(getString(R.string.noInternet));
                            }
                        }
                        break;

                    case 1:
                        if (getViewDataBinding().tvJobSchedule.getText().toString().equals(getString(R.string.job_scheduling_assign))) {
                            JobScheduleBottomSheetDialog.newInstance(false, jobDetail).show(getChildFragmentManager());
                        }
                        break;

                    case 2:
                        if (getViewDataBinding().tvJobSchedule.getText().toString().equals(getString(R.string.job_rescheduling_assign))) {
                            JobScheduleBottomSheetDialog.newInstance(true, jobDetail).show(getChildFragmentManager());
                        }
                        break;
                }
                break;

            case R.id.tv_job_complete:
                if (getViewDataBinding().tvJobComplete.getText().toString().equals(getString(R.string.mark_job_complete))) {
                    if (isNetworkConnected()) {
                        jobDetail.setScheduledStatus(AppConstants.kJobsArray[3]);
                        jobDetailViewModel.doScheduleJob(jobDetail);
                    } else {
                        showToast(getString(R.string.noInternet));
                    }
                }
                break;
        }
    }

    private boolean verifyInput() {
        if (getViewDataBinding().etJobName.getText().toString().isEmpty()) {
            showToast(getString(R.string.alert_job_name_empty));
            return false;
        } else return true;
    }

    private void enableDisableLocView(boolean isEnable) {
        getViewDataBinding().llEditLocView.setVisibility(isEnable ? View.VISIBLE : View.GONE);
        getViewDataBinding().llDirection.setVisibility(isEnable ? View.GONE : View.VISIBLE);
    }

    private void enableDisableJobInformation(boolean isEnable) {
        if (!isEnable) {
            getViewDataBinding().viewTime.setVisibility(View.GONE);
            getViewDataBinding().dateSoldTimePicker.setVisibility(View.GONE);
        }
        getViewDataBinding().rlJobFinancial.setVisibility(isEnable ? View.GONE : View.VISIBLE);
        getViewDataBinding().llSalesPerson.setVisibility(isEnable ? View.GONE : View.VISIBLE);
        getViewDataBinding().viewSalesPerson.setVisibility(isEnable ? View.GONE : View.VISIBLE);
        getViewDataBinding().tvSaveJobInfo.setVisibility(isEnable ? View.VISIBLE : View.GONE);
        getViewDataBinding().etJobName.setEnabled(isEnable);
        getViewDataBinding().etSoldItem.setEnabled(isEnable);
    }

    @Override
    public void onJobScheduleSuccess() {
        switch (tabPos) {
            case 0:
                getViewDataBinding().tvJobSchedule.setText(R.string.job_ready_for_scheduling);
                break;

            case 1:
                getViewDataBinding().tvJobSchedule.setText(R.string.job_scheduled);
                break;

            case 2:
                getViewDataBinding().tvJobSchedule.setText(R.string.job_scheduled);
                break;
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        this.googleMap.getUiSettings().setMyLocationButtonEnabled(false);
        this.googleMap.getUiSettings().setCompassEnabled(false);
        this.googleMap.getUiSettings().setMapToolbarEnabled(false);
        this.googleMap.getUiSettings().setScrollGesturesEnabled(false);

        if (latLng != null) {
            updateMapUI(latLng);
        }
    }

    private void updateMapUI(LatLng latLng) {
        if (googleMap != null) {
            googleMap.clear();
            googleMap.addMarker(new MarkerOptions()
                    .position(latLng)
                    .title(jobDetail.getCity())
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));
            googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
            googleMap.animateCamera(CameraUpdateFactory.zoomTo(16));
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);

        Bundle mapViewBundle = outState.getBundle(AppConstants.MAP_VIEW_BUNDLE_KEY);
        if (mapViewBundle == null) {
            mapViewBundle = new Bundle();
            outState.putBundle(AppConstants.MAP_VIEW_BUNDLE_KEY, mapViewBundle);
        }

        mapView.onSaveInstanceState(mapViewBundle);
    }
}