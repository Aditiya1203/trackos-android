package com.taskos.ui.main.client.detail.addevent;

import android.content.Intent;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProvider;

import com.taskos.R;
import com.taskos.ViewModelProviderFactory;
import com.taskos.data.model.api.Customer;
import com.taskos.data.model.api.Job;
import com.taskos.databinding.DialogAddEventBinding;
import com.taskos.ui.base.BaseBottomSheetDialog;
import com.taskos.ui.main.client.ClientNavigator;
import com.taskos.ui.main.client.detail.eventrepeat.EventCommonBottomSheetDialog;
import com.taskos.util.CalenderUtils;
import com.taskos.util.CommonUtils;

import java.util.Date;


public class AddEventBottomSheetDialog extends BaseBottomSheetDialog<DialogAddEventBinding, AddEventViewModel> implements View.OnClickListener, ClientNavigator {

    private static final String TAG = "AddEventBottomSheetDial";
    private AddEventViewModel addClientViewModel;
    private Date startTime = new Date(), endTime = new Date();
    private String repeatValue = "";
    private Customer customer;
    private Job jobDetail;
    private EventCallback callback;

    public static AddEventBottomSheetDialog newInstance(Customer customer) {

        Bundle args = new Bundle();

        AddEventBottomSheetDialog fragment = new AddEventBottomSheetDialog();
        fragment.setData(customer);
        fragment.setArguments(args);
        return fragment;
    }

    public static AddEventBottomSheetDialog newInstance(Job jobDetail, EventCallback callback) {

        Bundle args = new Bundle();

        AddEventBottomSheetDialog fragment = new AddEventBottomSheetDialog();
        fragment.setData(jobDetail, callback);
        fragment.setArguments(args);
        return fragment;
    }

    private void setData(Job jobDetail, EventCallback callback) {
        this.jobDetail = jobDetail;
        this.callback = callback;
    }

    private void setData(Customer customer) {
        this.customer = customer;
    }

    @Override
    public int getBindingVariable() {
        return com.taskos.BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.dialog_add_event;
    }

    @Override
    public AddEventViewModel getViewModel() {
        addClientViewModel = new ViewModelProvider(this, ViewModelProviderFactory.getInstance()).get(AddEventViewModel.class);
        return addClientViewModel;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        addClientViewModel.setNavigator(this);
        getViewDataBinding().frameCancel.setOnClickListener(this);
        getViewDataBinding().tvCancel.setOnClickListener(this);
        getViewDataBinding().llStartTime.setOnClickListener(this);
        getViewDataBinding().llEndTime.setOnClickListener(this);
        getViewDataBinding().tvAdd.setOnClickListener(this);
        getViewDataBinding().rlRepeat.setOnClickListener(this);

        if (customer != null) {
            getViewDataBinding().tvName.setText(customer.getName().concat(" Appointment: "));
            getViewDataBinding().etProdInterest.setText(customer.getProductOfInterest());
            String address = customer.getStreetAddress().concat(" ").concat(customer.getCity()).concat(", ").concat(customer.getState()).concat(" ").concat(customer.getZipcode());
            getViewDataBinding().etAddress.setText(address);
            startTime = customer.getAppointmentTime();
            endTime = CalenderUtils.addHoursToJavaUtilDate(startTime, 2);

        } else if (jobDetail != null) {
            getViewDataBinding().tvName.setText("Job: ".concat(jobDetail.getName()));
            getViewDataBinding().etProdInterest.setVisibility(View.GONE);
            String address = jobDetail.getStreetAddress().concat(" ").concat(jobDetail.getCity()).concat(", ").concat(jobDetail.getState()).concat(" ").concat(jobDetail.getZipcode());
            getViewDataBinding().etAddress.setText(address);
            startTime = jobDetail.getInstallStart();
            endTime = jobDetail.getInstallEnd();
        }
        getViewDataBinding().dateTimePicker.setDefaultDate(startTime);
        getViewDataBinding().dateTimePickerEnd.setDefaultDate(endTime);

        getViewDataBinding().tvAppointTime.setText(CalenderUtils.formatDate(startTime, CalenderUtils.CUSTOM_TIMESTAMP_FORMAT_DASH));
        getViewDataBinding().tvEndTime.setText(CalenderUtils.formatDate(endTime, CalenderUtils.CUSTOM_TIMESTAMP_FORMAT_DASH));
    }

    public void show(FragmentManager fragmentManager) {
        super.show(fragmentManager, TAG);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_cancel:
                dismissDialog(TAG);
                break;

            case R.id.ll_start_time:
                if (getViewDataBinding().dateTimePicker.getVisibility() == View.VISIBLE) {
                    getViewDataBinding().view.setVisibility(View.GONE);
                    getViewDataBinding().dateTimePicker.setVisibility(View.GONE);
                } else {
                    getViewDataBinding().view.setVisibility(View.VISIBLE);
                    getViewDataBinding().dateTimePicker.setVisibility(View.VISIBLE);
                }
                getViewDataBinding().dateTimePicker.addOnDateChangedListener((displayed, date) -> {
                    startTime = date;
                    getViewDataBinding().dateTimePickerEnd.setDefaultDate(CalenderUtils.addHoursToJavaUtilDate(startTime, 2));
                    getViewDataBinding().dateTimePickerEnd.setMinDate(CalenderUtils.addHoursToJavaUtilDate(startTime, 2));
                    getViewDataBinding().tvAppointTime.setText(CalenderUtils.formatDate(date, CalenderUtils.CUSTOM_TIMESTAMP_FORMAT_DASH));
                    getViewDataBinding().tvEndTime.setText(CalenderUtils.formatDate(CalenderUtils.addHoursToJavaUtilDate(startTime, 2), CalenderUtils.CUSTOM_TIMESTAMP_FORMAT_DASH));
                });
                break;

            case R.id.ll_end_time:
                if (getViewDataBinding().dateTimePickerEnd.getVisibility() == View.VISIBLE) {
                    getViewDataBinding().viewEnd.setVisibility(View.GONE);
                    getViewDataBinding().dateTimePickerEnd.setVisibility(View.GONE);
                } else {
                    getViewDataBinding().viewEnd.setVisibility(View.VISIBLE);
                    getViewDataBinding().dateTimePickerEnd.setVisibility(View.VISIBLE);
                }

                getViewDataBinding().dateTimePickerEnd.addOnDateChangedListener((displayed, date) -> {
                    endTime = date;
                    getViewDataBinding().tvEndTime.setText(CalenderUtils.formatDate(date, CalenderUtils.CUSTOM_TIMESTAMP_FORMAT_DASH));
                });
                break;

            case R.id.frame_cancel:
                getViewDataBinding().etAddress.setText("");
                break;

            case R.id.rl_repeat:
                EventCommonBottomSheetDialog.newInstance("Repeat", keyValue -> {
                    repeatValue = keyValue.getKey();
                    getViewDataBinding().tvRepeat.setText(keyValue.getValue());
                }).show(getChildFragmentManager());
                break;

            case R.id.tv_add:
                if (verifyInputs()) {
                    addEventToCalendar();
                }
                break;
        }
    }

    private void addEventToCalendar() {
        Intent intent = new Intent(Intent.ACTION_INSERT);
        intent.setType("vnd.android.cursor.item/event");

        intent.putExtra(CalendarContract.EXTRA_EVENT_ALL_DAY, getViewDataBinding().switchAllday.isChecked());
        intent.putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, startTime.getTime());
        intent.putExtra(CalendarContract.EXTRA_EVENT_END_TIME, endTime.getTime());

        if (!repeatValue.isEmpty())
            intent.putExtra(CalendarContract.Events.RRULE, repeatValue);  //YEARLY, MONTHLY, WEEKLY, DAILY

        intent.putExtra(CalendarContract.Events.EVENT_COLOR, R.color.light_pink);
        intent.putExtra(CalendarContract.Events.AVAILABILITY, CalendarContract.Events.AVAILABILITY_BUSY);

        if (customer != null) {
            intent.putExtra(CalendarContract.Events.TITLE, customer.getName().concat(" Appointment: ").concat(getViewDataBinding().etProdInterest.getText().toString()));

            StringBuilder des = new StringBuilder(getViewDataBinding().etProdInterest.getText().toString());
            des.append("\n").append("Name: ").append(customer.getName());
            des.append("\n").append("Email: ").append(customer.getEmail());
            des.append("\n").append("Phone: ").append(CommonUtils.formatStringAsPhoneNumber(customer.getPhone()));
            intent.putExtra(CalendarContract.Events.DESCRIPTION, des.toString());
        } else if (jobDetail != null) {
            intent.putExtra(CalendarContract.Events.TITLE, "Job: ".concat(jobDetail.getName()));
            intent.putExtra(CalendarContract.Events.DESCRIPTION, "");
        }


        intent.putExtra(CalendarContract.Events.EVENT_LOCATION, getViewDataBinding().etAddress.getText().toString());

        if (callback != null) callback.onEventAdded();
        startActivity(intent);
        dismissDialog(TAG);
    }

    private boolean verifyInputs() {
        if (customer != null) {
            if (getViewDataBinding().etProdInterest.getText().toString().isEmpty()) {
                showToast(getString(R.string.alert_name_empty));
                return false;
            } else return true;
        } else return true;
    }

    public interface EventCallback {
        void onEventAdded();
    }
}
