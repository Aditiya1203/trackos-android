package com.taskos.ui.main.tasks.jobs;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.taskos.data.DataManager;
import com.taskos.data.model.api.Job;
import com.taskos.data.model.other.JobSelect;
import com.taskos.ui.base.BaseNavigator;
import com.taskos.ui.base.BaseViewModel;

import java.util.ArrayList;

public class SelectJobsViewModel extends BaseViewModel<BaseNavigator> {
    private static final String TAG = "SelectJobsViewModel";
    private ArrayList<JobSelect> jobSelectList = new ArrayList<>();
    private MutableLiveData<ArrayList<JobSelect>> mutableLiveDataJob = new MutableLiveData<>();

    public SelectJobsViewModel(DataManager dataManager) {
        super(dataManager);
    }

    MutableLiveData<ArrayList<JobSelect>> getAllJobs() {
        jobSelectList.clear();
        Query query;
        query = getDataManager().getFirestoreInstance().collection("Job")
                .whereEqualTo("owner", getDataManager().getCompanyID())
                .orderBy("installStart", Query.Direction.DESCENDING);

        query.addSnapshotListener((queryDocumentSnapshots, e) -> {
            if (e != null) {
                Log.e(TAG, "onEvent: Listen failed.", e);
                return;
            }

            if (queryDocumentSnapshots != null) {
                jobSelectList.clear();
                for (QueryDocumentSnapshot doc : queryDocumentSnapshots) {
                    Job job = doc.toObject(Job.class);
                    JobSelect jobSelect = new JobSelect(job);
                    jobSelectList.add(jobSelect);
                }
                mutableLiveDataJob.setValue(jobSelectList);
            }
        });

        return mutableLiveDataJob;
    }

}