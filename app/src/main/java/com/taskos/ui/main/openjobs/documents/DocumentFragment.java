package com.taskos.ui.main.openjobs.documents;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;

import com.taskos.BR;
import com.taskos.R;
import com.taskos.ViewModelProviderFactory;
import com.taskos.data.model.api.Job;
import com.taskos.databinding.FragmentDocumentBinding;
import com.taskos.ui.base.BaseFragment;
import com.taskos.ui.base.BaseNavigator;
import com.taskos.ui.main.openjobs.addfolder.AddFolderBottomSheetDialog;
import com.taskos.ui.main.openjobs.addfolder.FolderAdapter;

public class DocumentFragment extends BaseFragment<FragmentDocumentBinding, DocumentViewModel> implements View.OnClickListener, BaseNavigator {

    private DocumentViewModel documentViewModel;
    private String jobUUID = "";
    private Job job;

    public static DocumentFragment newInstance(String jobUUID) {

        Bundle args = new Bundle();

        DocumentFragment fragment = new DocumentFragment();
        args.putString("JobKey", jobUUID);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_document;
    }

    @Override
    public DocumentViewModel getViewModel() {
        documentViewModel = new ViewModelProvider(this, ViewModelProviderFactory.getInstance()).get(DocumentViewModel.class);
        return documentViewModel;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            jobUUID = getArguments().getString("JobKey");
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        documentViewModel.setNavigator(this);
        TextView tvTitle = view.findViewById(R.id.tvTitle);
        tvTitle.setText(R.string.open_jobs);
        setOnClickListener(tvTitle
                , view.findViewById(R.id.img_back), getViewDataBinding().rlAddFolder
                , getViewDataBinding().rlAddDoc, getViewDataBinding().llMain);

        getViewDataBinding().rcvFolders.setNestedScrollingEnabled(false);
        getViewDataBinding().rcvDoc.setNestedScrollingEnabled(false);

        documentViewModel.getJobDetail(jobUUID).observe(getViewLifecycleOwner(), job -> {
            this.job = job;
           /* ImageAdapter imageAdapter = new ImageAdapter(job.getImageLinks());
            getViewDataBinding().rcvDoc.setAdapter(imageAdapter);*/
        });

        documentViewModel.getFolders(jobUUID).observe(getViewLifecycleOwner(), documentFolders -> {
            FolderAdapter folderAdapter = new FolderAdapter(true, documentFolders);
            getViewDataBinding().rcvFolders.setAdapter(folderAdapter);
            getViewDataBinding().rcvFolders.setVisibility(View.VISIBLE);
        });
    }

    private void setOnClickListener(View... views) {
        for (View view : views) {
            view.setOnClickListener(this);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tvTitle:
            case R.id.img_back:
                getBaseActivity().onBackPressed();
                break;

            case R.id.rl_add_folder:
                AddFolderBottomSheetDialog.newInstance(job, false).show(getChildFragmentManager());
                break;

            case R.id.rl_add_doc:
                showToast(getString(R.string.under_development));
                break;
        }
    }

}