package com.taskos.ui.main;

import com.taskos.data.DataManager;
import com.taskos.ui.base.BaseViewModel;

public class MainViewModel extends BaseViewModel<MainNavigator> {

    public MainViewModel(DataManager dataManager) {
        super(dataManager);
    }

}
