package com.taskos.ui.main.openjobs.imageselect;

import com.taskos.data.DataManager;
import com.taskos.ui.base.BaseNavigator;
import com.taskos.ui.base.BaseViewModel;

public class ImagePickModel extends BaseViewModel<BaseNavigator> {
    private static final String TAG = "ImagePickModel";

    public ImagePickModel(DataManager dataManager) {
        super(dataManager);
    }

}