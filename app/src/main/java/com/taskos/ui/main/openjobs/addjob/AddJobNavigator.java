package com.taskos.ui.main.openjobs.addjob;


/**
 * Created by Hemant Sharma on 23-02-20.
 */
public interface AddJobNavigator {
    void dismiss(String TAG);

    void onSaveError();
}
