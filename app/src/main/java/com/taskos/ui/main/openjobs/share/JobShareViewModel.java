package com.taskos.ui.main.openjobs.share;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.taskos.data.DataManager;
import com.taskos.data.model.api.Job;
import com.taskos.data.model.api.SharedJob;
import com.taskos.ui.auth.MUser;
import com.taskos.ui.base.BaseViewModel;

import java.util.ArrayList;
import java.util.List;

public class JobShareViewModel extends BaseViewModel<JobShareNavigator> {
    private static final String TAG = "JobShareViewModel";
    private MutableLiveData<Job> mutableLiveData = new MutableLiveData<>();

    private List<MUser> mUsers = new ArrayList<>();
    private MutableLiveData<List<MUser>> mutableLiveDataShareUsers = new MutableLiveData<>();
    private boolean isUnshare = false;
    private ListenerRegistration listenerRegistration;

    public JobShareViewModel(DataManager dataManager) {
        super(dataManager);
    }

    MutableLiveData<Job> getJobDetail(String customerKey) {
        DocumentReference query = getDataManager().getFirestoreInstance().collection("Job").document(customerKey);

        query.addSnapshotListener((documentSnapshot, e) -> {
            if (e != null) {
                Log.e(TAG, "onEvent: Listen failed.", e);
                return;
            }

            if (documentSnapshot != null) {
                Job job = documentSnapshot.toObject(Job.class);
                if (job != null) {
                    mutableLiveData.setValue(job);
                }
            }
        });

        return mutableLiveData;
    }

    MutableLiveData<List<MUser>> getShareJobUsers(String jobUUID) {
        mUsers.clear();
        Query query = getDataManager().getFirestoreInstance().collection("SharedJob").whereEqualTo("jobUuid", jobUUID);

        query.addSnapshotListener((documentSnapshot, e) -> {
            if (e != null) {
                Log.e(TAG, "onEvent: Listen failed.", e);
                return;
            }

            if (documentSnapshot != null) {
                mUsers.clear();
                for (QueryDocumentSnapshot doc : documentSnapshot) {
                    SharedJob sharedJob = doc.toObject(SharedJob.class);

                    //if click on unshare then avoid listener callback
                    if (!isUnshare) {
                        getDataManager().getFirestoreInstance().collection("MUser").document(sharedJob.getSharedUser()).addSnapshotListener((documentSnapshot1, e1) -> {
                            if (e1 != null) {
                                Log.e(TAG, "onEvent: Listen failed.", e1);
                                return;
                            }
                            if (documentSnapshot1 != null) {
                                MUser mUser1 = documentSnapshot1.toObject(MUser.class);

                                if (mUser1 != null) {
                                    mUsers.add(mUser1);
                                }
                                mutableLiveDataShareUsers.setValue(mUsers);
                            }
                        });
                    } else {
                        mutableLiveDataShareUsers.setValue(mUsers);
                    }
                }
            }
        });

        return mutableLiveDataShareUsers;
    }

    void doShareJob(SharedJob sharedJob, Job jobDetail, List<MUser> userList) {
        getDataManager().getFirestoreInstance().collection("SharedJob").document(sharedJob.getUuid()).
                set(sharedJob).addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                isUnshare = false;
                getDataManager().getFirestoreInstance().collection("Job").document(jobDetail.getUuid()).
                        set(jobDetail).addOnCompleteListener(task2 -> {
                    if (task2.isSuccessful()) {
                        getNavigator().onShareSuccess();
                    } else {
                        getNavigator().onSaveError();
                    }
                });

                /*ArrayList<String> tokenList = new ArrayList<>();
                tokenList.add(getDataManager().getDeviceToken());
                for (MUser mUser : userList) {
                    tokenList.addAll(mUser.getToken());
                }
                FcmNotificationBuilder.newInstance().setTitle("New Shared Job")
                        .setBody("Job: ".concat(sharedJob.getName()).concat(" was shared with you!"))
                        .setSelectedView(FcmNotificationBuilder.VIEW_NETWORK)
                        .setTabView(FcmNotificationBuilder.TAB_SHARED_JOB)
                        .setSelectedUUID(jobDetail.getUuid())
                        .setReceiverFCMTokenList(AppUtils.removeDuplicates(tokenList))
                        .sendNotification();*/
            } else {
                getNavigator().onSaveError();
            }
        });
    }

    void doUnshareJob(String jobUUID) {
        Query query = getDataManager().getFirestoreInstance().collection("SharedJob").whereEqualTo("jobUuid", jobUUID);

        listenerRegistration = query.addSnapshotListener((queryDocumentSnapshots, e) -> {
            if (e != null) {
                Log.e(TAG, "onEvent: Listen failed.", e);
                return;
            }

            if (queryDocumentSnapshots != null) {
                isUnshare = true;
                for (QueryDocumentSnapshot doc : queryDocumentSnapshots) {
                    SharedJob sharedJob = doc.toObject(SharedJob.class);
                    getDataManager().getFirestoreInstance().collection("SharedJob").document(sharedJob.getUuid()).
                            delete();
                }
                getDataManager().getFirestoreInstance().collection("Job").document(jobUUID).
                        update("shared", false).addOnCompleteListener(task2 -> {
                    if (task2.isSuccessful()) {
                        getNavigator().onUnShareUsers();
                    } else {
                        getNavigator().onSaveError();
                    }
                });

                //Need to remove listener because whenever we share job to new user old share job deleted because above listener call
                removeListener();
            }
        });
    }

    private void removeListener() {
        if (listenerRegistration != null) {
            listenerRegistration.remove();
        }
    }
}