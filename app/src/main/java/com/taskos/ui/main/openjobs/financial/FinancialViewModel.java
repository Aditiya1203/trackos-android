package com.taskos.ui.main.openjobs.financial;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.taskos.data.DataManager;
import com.taskos.data.model.api.Job;
import com.taskos.data.model.api.JobFinancial;
import com.taskos.ui.base.BaseViewModel;
import com.taskos.ui.main.openjobs.addjob.AddJobNavigator;

public class FinancialViewModel extends BaseViewModel<AddJobNavigator> {
    private static final String TAG = "FinancialViewModel";
    private MutableLiveData<Job> jobMutableLiveData = new MutableLiveData<>();
    private MutableLiveData<JobFinancial> jobFinancialMutableLiveData = new MutableLiveData<>();

    public FinancialViewModel(DataManager dataManager) {
        super(dataManager);
    }

    MutableLiveData<Job> getJobDetail(String uuid) {
        DocumentReference query = getDataManager().getFirestoreInstance().collection("Job").document(uuid);

        query.addSnapshotListener((documentSnapshot, e) -> {
            if (e != null) {
                Log.e(TAG, "onEvent: Listen failed.", e);
                return;
            }

            if (documentSnapshot != null) {
                Job job = documentSnapshot.toObject(Job.class);
                if (job != null) {
                    jobMutableLiveData.setValue(job);
                }
            }
        });

        return jobMutableLiveData;
    }

    MutableLiveData<JobFinancial> getJobFinancialDetail(String uuid) {
        Query query = getDataManager().getFirestoreInstance().collection("JobFinancial").whereEqualTo("jobUuid", uuid).limit(1);

        query.addSnapshotListener((queryDocumentSnapshots, e) -> {
            if (e != null) {
                Log.e(TAG, "onEvent: Listen failed.", e);
                return;
            }

            if (queryDocumentSnapshots != null) {
                for (QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                    JobFinancial jobFinancial = documentSnapshot.toObject(JobFinancial.class);
                    jobFinancialMutableLiveData.setValue(jobFinancial);
                }
            }
        });

        return jobFinancialMutableLiveData;
    }

    void doSaveJobInformation(Job job, JobFinancial jobFinancial, String TAG) {
        // Get a reference to the Customers collection
        getDataManager().getFirestoreInstance().collection("Job").document(job.getUuid()).
                set(job).addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                getDataManager().getFirestoreInstance().collection("JobFinancial").document(jobFinancial.getUuid()).
                        set(jobFinancial).addOnCompleteListener(task1 -> {
                    if (task1.isSuccessful()) {
                        getNavigator().dismiss(TAG);
                    } else getNavigator().onSaveError();
                });
            } else getNavigator().onSaveError();
        });
    }
}