package com.taskos.ui.main.client.detail.comment;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.taskos.data.DataManager;
import com.taskos.data.model.api.Customer;
import com.taskos.data.model.api.UserCommentsCustomer;
import com.taskos.ui.auth.MUser;
import com.taskos.ui.base.BaseViewModel;
import com.taskos.ui.fcm.FcmNotificationBuilder;
import com.taskos.util.AppUtils;

import java.util.ArrayList;
import java.util.List;

public class CommentViewModel extends BaseViewModel<CommentNavigator> {

    private static final String TAG = "CommentViewModel";
    private MutableLiveData<MUser> mUserMutableLiveData = new MutableLiveData<>();
    private MutableLiveData<Customer> customerMutableLiveData = new MutableLiveData<>();
    private ArrayList<UserCommentsCustomer> userCommentsArrayList = new ArrayList<>();
    private MutableLiveData<List<UserCommentsCustomer>> commentsListMutableLiveData = new MutableLiveData<>();


    public CommentViewModel(DataManager dataManager) {
        super(dataManager);
    }

    MutableLiveData<Customer> getClientDetail(String customerUUID) {
        DocumentReference query = getDataManager().getFirestoreInstance().collection("Customers").document(customerUUID);

        query.addSnapshotListener((queryDocumentSnapshots, e) -> {
            if (e != null) {
                Log.e(TAG, "onEvent: Listen failed.", e);
                return;
            }

            if (queryDocumentSnapshots != null) {
                Customer customer = queryDocumentSnapshots.toObject(Customer.class);
                if (customer != null) {
                    customerMutableLiveData.setValue(customer);
                }
            }
        });

        return customerMutableLiveData;
    }

    MutableLiveData<MUser> getUserInfo(String uuid) {
        getDataManager().getFirestoreInstance().collection("MUser").document(uuid).addSnapshotListener((documentSnapshot, e) -> {
            if (e != null) {
                Log.e(TAG, "onEvent: Listen failed.", e);
                return;
            }
            if (documentSnapshot != null) {
                MUser mUser1 = documentSnapshot.toObject(MUser.class);
                if (mUser1 != null) {
                    mUserMutableLiveData.setValue(mUser1);
                }
            }
        });
        return mUserMutableLiveData;
    }

    MutableLiveData<List<UserCommentsCustomer>> getComments(String customerUUID) {
        Query query = getDataManager().getFirestoreInstance().collection("Comments")
                .whereEqualTo("customerUuid", customerUUID).orderBy("commentPostedTime", Query.Direction.DESCENDING);

        query.addSnapshotListener((queryDocumentSnapshots, e) -> {
            if (e != null) {
                Log.e(TAG, "onEvent: Listen failed.", e);
                return;
            }

            if (queryDocumentSnapshots != null) {
                userCommentsArrayList.clear();
                for (QueryDocumentSnapshot doc : queryDocumentSnapshots) {
                    UserCommentsCustomer userCommentsCustomer = doc.toObject(UserCommentsCustomer.class);
                    userCommentsArrayList.add(userCommentsCustomer);
                }
                commentsListMutableLiveData.setValue(userCommentsArrayList);
            }
        });

        return commentsListMutableLiveData;
    }

    void sendComment(UserCommentsCustomer userCommentsCustomer, List<String> tokens) {
        // Get a reference to the Comments collection
        getDataManager().getFirestoreInstance().collection("Comments").document(userCommentsCustomer.getUuid())
                .set(userCommentsCustomer).addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                ArrayList<String> tokenList = new ArrayList<>(tokens);
                FcmNotificationBuilder.newInstance().setTitle("Customer Comment")
                        .setBody("New Comment posted by: ".concat(userCommentsCustomer.getUserFirstName()))
                        .setSelectedView(FcmNotificationBuilder.VIEW_CUSTOMER)
                        .setTabView(FcmNotificationBuilder.TAB_ALL_CUSTOMER)
                        .setSelectedUUID(userCommentsCustomer.getCustomerUuid())
                        .setReceiverFCMTokenList(AppUtils.removeDuplicates(tokenList))
                        .sendNotification();
                getNavigator().clearInput();
            } else {
                getNavigator().enableInput();
            }
        });
    }
}
