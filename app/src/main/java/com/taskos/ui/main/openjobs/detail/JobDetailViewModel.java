package com.taskos.ui.main.openjobs.detail;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.google.firebase.firestore.DocumentReference;
import com.taskos.data.DataManager;
import com.taskos.data.model.api.Job;
import com.taskos.ui.base.BaseViewModel;

public class JobDetailViewModel extends BaseViewModel<JobDetailNavigator> {
    private static final String TAG = "JobDetailViewModel";
    private MutableLiveData<Job> mutableLiveData = new MutableLiveData<>();


    public JobDetailViewModel(DataManager dataManager) {
        super(dataManager);
    }

    MutableLiveData<Job> getJobDetail(String customerKey) {
        DocumentReference query = getDataManager().getFirestoreInstance().collection("Job").document(customerKey);

        query.addSnapshotListener((documentSnapshot, e) -> {
            if (e != null) {
                Log.e(TAG, "onEvent: Listen failed.", e);
                return;
            }

            if (documentSnapshot != null) {
                Job job = documentSnapshot.toObject(Job.class);
                if (job != null) {
                    mutableLiveData.setValue(job);
                }
            }
        });

        return mutableLiveData;
    }

    void doSaveJobInfo(Job jobDetail) {
        getDataManager().getFirestoreInstance().collection("Job").document(jobDetail.getUuid()).
                set(jobDetail);
    }

    void doScheduleJob(Job jobDetail) {
        getDataManager().getFirestoreInstance().collection("Job").document(jobDetail.getUuid()).
                set(jobDetail).addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                getNavigator().onJobScheduleSuccess();
            }
        });
    }

    /*public void doSaveAppointmentInfo(String customerKey, Customer customer) {
        getDataManager().getFirestoreInstance().collection("Customers").document(customerKey).set(customer);
    }*/
}