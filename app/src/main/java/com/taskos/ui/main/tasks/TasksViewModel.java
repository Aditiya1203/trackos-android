package com.taskos.ui.main.tasks;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.taskos.data.DataManager;
import com.taskos.data.model.api.ToDo;
import com.taskos.ui.base.BaseViewModel;
import com.taskos.util.AppConstants;

import java.util.ArrayList;
import java.util.List;

public class TasksViewModel extends BaseViewModel<TasksNavigator> {

    private List<ToDo> toDoList = new ArrayList<>();
    private MutableLiveData<List<ToDo>> mutableLiveData = new MutableLiveData<>();
    private static final String TAG = "TasksViewModel";

    public TasksViewModel(DataManager dataManager) {
        super(dataManager);
    }

    MutableLiveData<List<ToDo>> getOpenTask(String value, int currentPos, String search) {
        toDoList.clear();
        Query query;
        if (currentPos == 0) {
            if (search.isEmpty()) {
                query = getDataManager().getFirestoreInstance().collection("ToDo")
                        .whereEqualTo("owner", getDataManager().getCompanyID())
                        .orderBy("dateEntered", Query.Direction.DESCENDING);
            } else {
                query = getDataManager().getFirestoreInstance().collection("ToDo")
                        .whereEqualTo("owner", getDataManager().getCompanyID())
                        .whereEqualTo("task", search)
                        .orderBy("dateEntered", Query.Direction.DESCENDING);

            }
        } else {
            if (search.isEmpty()) {
                query = getDataManager().getFirestoreInstance().collection("ToDo")
                        .whereEqualTo("completionStatus", value)
                        .whereEqualTo("owner", getDataManager().getCompanyID())
                        .orderBy("dateEntered", Query.Direction.DESCENDING);
            } else {
                query = getDataManager().getFirestoreInstance().collection("ToDo")
                        .whereEqualTo("completionStatus", value)
                        .whereEqualTo("owner", getDataManager().getCompanyID())
                        .whereEqualTo("task", search)
                        .orderBy("dateEntered", Query.Direction.DESCENDING);
            }
        }

        query.addSnapshotListener((queryDocumentSnapshots, e) -> {
            if (e != null) {
                Log.e(TAG, "onEvent: Listen failed.", e);
                return;
            }
            if (queryDocumentSnapshots != null) {
                toDoList.clear();
                for (QueryDocumentSnapshot doc : queryDocumentSnapshots) {
                    ToDo toDo = doc.toObject(ToDo.class);
                    if (!toDo.getCompletionStatus().equals(AppConstants.kCompletionStatusArray[1])) {
                        toDoList.add(toDo);
                    }
                }
                mutableLiveData.setValue(toDoList);
            }
        });
        return mutableLiveData;
    }
}