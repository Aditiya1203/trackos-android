package com.taskos.ui.main.tasks.detail.edit;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.google.firebase.firestore.DocumentReference;
import com.taskos.data.DataManager;
import com.taskos.data.model.api.ToDo;
import com.taskos.ui.base.BaseViewModel;

public class EditTaskDetailsViewModel extends BaseViewModel<EditTasksDetailNavigator> {
    private static final String TAG = "AllTaskDetailsViewModel";
    private MutableLiveData<ToDo> mutableLiveData = new MutableLiveData<>();


    public EditTaskDetailsViewModel(DataManager dataManager) {
        super(dataManager);
    }

    MutableLiveData<ToDo> getTaskDetail(String taskKey) {
        DocumentReference query = getDataManager().getFirestoreInstance().collection("ToDo").document(taskKey);

        query.addSnapshotListener((documentSnapshot, e) -> {
            if (e != null) {
                Log.e(TAG, "onEvent: Listen failed.", e);
                return;
            }

            if (documentSnapshot != null) {
                ToDo toDo = documentSnapshot.toObject(ToDo.class);
                if (toDo != null) {
                    mutableLiveData.setValue(toDo);
                }
            }
        });

        return mutableLiveData;
    }

    void doSaveTaskDetail(ToDo toDo) {
        getDataManager().getFirestoreInstance().collection("ToDo").document(toDo.getUuid()).set(toDo).addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                getNavigator().openDashboard();
            } else getNavigator().onSaveError();
        });
    }
}
