package com.taskos.ui.main.openjobs.addfolder;

import com.taskos.data.DataManager;
import com.taskos.data.model.api.DocumentFolder;
import com.taskos.data.model.api.ImageFolder;
import com.taskos.ui.base.BaseViewModel;

public class AddFolderViewModel extends BaseViewModel<AddFolderNavigator> {
    private static final String TAG = "AddJobViewModel";

    public AddFolderViewModel(DataManager dataManager) {
        super(dataManager);
    }


    void doAddImageFolder(ImageFolder imageFolder) {
        getDataManager().getFirestoreInstance().collection("ImageFolder").document(imageFolder.getUuid()).set(imageFolder)
                .addOnSuccessListener(aVoid -> getNavigator().onSaveSuccess("Image"))
                .addOnFailureListener(e -> getNavigator().onError());
    }

    void doAddDocFolder(DocumentFolder documentFolder) {
        getDataManager().getFirestoreInstance().collection("DocumentFolder").document(documentFolder.getUuid()).set(documentFolder)
                .addOnSuccessListener(aVoid -> getNavigator().onSaveSuccess("Image"))
                .addOnFailureListener(e -> getNavigator().onError());
    }
}