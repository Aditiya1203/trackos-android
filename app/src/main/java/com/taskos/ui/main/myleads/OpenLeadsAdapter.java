package com.taskos.ui.main.myleads;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.taskos.R;
import com.taskos.data.model.api.Customer;
import java.util.ArrayList;
import java.util.List;

public class OpenLeadsAdapter extends RecyclerView.Adapter {
    List<Customer> openleadslist;
    Context context;
    public OpenLeadsAdapter(List<Customer> openleadslist, Context ctx) {
        this.openleadslist = openleadslist;
        this.context = ctx;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.open_leads_item_layout, parent, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, @SuppressLint("RecyclerView") int position) {
        ViewHolder viewHolder = (ViewHolder) holder;

        viewHolder.tv_name.setText(openleadslist.get(position).getName());
        viewHolder.tv_assignedby.setText(openleadslist.get(position).getSalesperson());
        viewHolder.tv_address.setText(openleadslist.get(position).getCity());

        if(openleadslist.get(position).getCustomerStatus().equals("Open Lead")){
            viewHolder.tv_status.setBackgroundResource(R.drawable.openleeds_status_open);
        }else if(openleadslist.get(position).getCustomerStatus().equals("Quoted")){
            viewHolder.tv_status.setBackgroundResource(R.drawable.openleeds_status_quoted);
        }else if(openleadslist.get(position).getCustomerStatus().equals("Sold")){
            viewHolder.tv_status.setBackgroundResource(R.drawable.openleeds_status_contacted);
        }else if(openleadslist.get(position).getCustomerStatus().equals("Appointment Set")){
            viewHolder.tv_status.setBackgroundResource(R.drawable.openleeds_status_appointment);
        }else if(openleadslist.get(position).getCustomerStatus().equals("Pre-Qualified")){
            viewHolder.tv_status.setBackgroundResource(R.drawable.openleeds_status_prequalified);
        }else if(openleadslist.get(position).getCustomerStatus().equals("Not-Qualified")){
            viewHolder.tv_status.setBackgroundResource(R.drawable.openleeds_status_notqualified);
        }else if(openleadslist.get(position).getCustomerStatus().equals("Contacted")){
            viewHolder.tv_status.setBackgroundResource(R.drawable.openleeds_status_contacted);
        }else if(openleadslist.get(position).getCustomerStatus().equals("Not Contacted")){
            viewHolder.tv_status.setBackgroundResource(R.drawable.openleeds_status_notconnected);
        }else if(openleadslist.get(position).getCustomerStatus().equals("Closed Did Not Win")){
            viewHolder.tv_status.setBackgroundResource(R.drawable.openleeds_status_notconnected);
        }else {
            viewHolder.tv_status.setBackgroundResource(R.drawable.openleeds_status_notqualified);
        }
        viewHolder.tv_status.setText(openleadslist.get(position).getCustomerStatus());

        viewHolder.tv_appointment.setText(openleadslist.get(position).getAppointmentEntered()+"");
        Log.e("date",openleadslist.get(position).getAppointmentEntered()+"");

    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tv_name;
        TextView tv_assignedby;
        TextView tv_address;
        TextView tv_appointment;
        TextView tv_status;
        public ViewHolder(View itemView) {
            super(itemView);
            tv_name = itemView.findViewById(R.id.tv_name);
            tv_assignedby = itemView.findViewById(R.id.tv_assignedby);
            tv_address = itemView.findViewById(R.id.tv_address);
            tv_appointment = itemView.findViewById(R.id.tv_appointment);
            tv_status = itemView.findViewById(R.id.tv_status);

        }
    }

    @Override
    public int getItemCount() {
        return openleadslist.size();
    }
}
