package com.taskos.ui.main.openjobs.images;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;

import com.taskos.BR;
import com.taskos.R;
import com.taskos.ViewModelProviderFactory;
import com.taskos.data.model.api.Job;
import com.taskos.databinding.FragmentImagesBinding;
import com.taskos.ui.base.BaseFragment;
import com.taskos.ui.base.BaseNavigator;
import com.taskos.ui.main.openjobs.addfolder.AddFolderBottomSheetDialog;
import com.taskos.ui.main.openjobs.addfolder.FolderAdapter;
import com.taskos.ui.main.openjobs.imageselect.ImageSelectBottomSheetDialog;

public class ImagesFragment extends BaseFragment<FragmentImagesBinding, ImagesViewModel> implements View.OnClickListener, BaseNavigator {

    private ImagesViewModel imagesViewModel;
    private String jobUUID = "";
    private Job job;

    public static ImagesFragment newInstance(String jobUUID) {

        Bundle args = new Bundle();

        ImagesFragment fragment = new ImagesFragment();
        args.putString("JobKey", jobUUID);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_images;
    }

    @Override
    public ImagesViewModel getViewModel() {
        imagesViewModel = new ViewModelProvider(this, ViewModelProviderFactory.getInstance()).get(ImagesViewModel.class);
        return imagesViewModel;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            jobUUID = getArguments().getString("JobKey");
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        imagesViewModel.setNavigator(this);
        TextView tvTitle = view.findViewById(R.id.tvTitle);
        tvTitle.setText(R.string.open_jobs);
        setOnClickListener(tvTitle
                , view.findViewById(R.id.img_back)
                , getViewDataBinding().rlAddFolder
                , getViewDataBinding().rlAddImageOrDoc, getViewDataBinding().llMain);

        getViewDataBinding().rcvFolders.setNestedScrollingEnabled(false);
        getViewDataBinding().rcvImages.setNestedScrollingEnabled(false);

        imagesViewModel.getJobDetail(jobUUID).observe(getViewLifecycleOwner(), job -> {
            this.job = job;
            ImageAdapter imageAdapter = new ImageAdapter(job.getImageLinks());
            getViewDataBinding().rcvImages.setAdapter(imageAdapter);
        });

        imagesViewModel.getFolders(jobUUID).observe(getViewLifecycleOwner(), imageFolders -> {
            FolderAdapter folderAdapter = new FolderAdapter(imageFolders);
            getViewDataBinding().rcvFolders.setAdapter(folderAdapter);
            getViewDataBinding().rcvFolders.setVisibility(View.VISIBLE);
        });
    }

    private void setOnClickListener(View... views) {
        for (View view : views) {
            view.setOnClickListener(this);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tvTitle:
            case R.id.img_back:
                getBaseActivity().onBackPressed();
                break;

            case R.id.rl_add_folder:
                AddFolderBottomSheetDialog.newInstance(job, true).show(getChildFragmentManager());
                break;

            case R.id.rl_add_image_or_doc:
                ImageSelectBottomSheetDialog.newInstance(jobUUID).show(getChildFragmentManager());
                break;
        }
    }

}