package com.taskos.ui.main.openjobs.addjob;

import com.taskos.data.DataManager;
import com.taskos.data.model.api.Job;
import com.taskos.data.model.api.JobFinancial;
import com.taskos.ui.base.BaseViewModel;

public class AddJobViewModel extends BaseViewModel<AddJobNavigator> {
    private static final String TAG = "AddJobViewModel";

    public AddJobViewModel(DataManager dataManager) {
        super(dataManager);
    }

    void doSaveJobInformation(Job job, JobFinancial jobFinancial) {
        // Get a reference to the Customers collection
        getDataManager().getFirestoreInstance().collection("Job").document(job.getUuid()).
                set(job).addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                getDataManager().getFirestoreInstance().collection("JobFinancial").document(jobFinancial.getUuid()).
                        set(jobFinancial).addOnCompleteListener(task1 -> {
                    if (task1.isSuccessful()) {
                        getNavigator().dismiss("");
                    } else getNavigator().onSaveError();
                });
            } else getNavigator().onSaveError();
        });
    }
}