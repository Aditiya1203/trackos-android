package com.taskos.ui.main.help;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import com.taskos.R;
import com.taskos.ui.main.client.ClientFragment;
import com.taskos.ui.main.openjobs.addjob.AddJobBottomSheetDialog;

public class HelpActivity extends AppCompatActivity {

    TextView tv_email;
    TextView tv_terms_of_service;
    TextView tv_privacy_policy;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.help_activity);

        tv_email=findViewById(R.id.tv_email);
        tv_terms_of_service=findViewById(R.id.tv_terms_of_service);
        tv_privacy_policy=findViewById(R.id.tv_privacy_policy);

        tv_email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EmailBottomSheetDialog.newInstance().show(getSupportFragmentManager());
            }
        });
        tv_terms_of_service.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TermsOfServiceBottomSheetDialog.newInstance().show(getSupportFragmentManager());
            }
        });
        tv_privacy_policy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PrivacyPolicyBottomSheetDialog.newInstance().show(getSupportFragmentManager());
            }
        });
    }
}
