package com.taskos.ui.main.openjobs.images;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.taskos.data.DataManager;
import com.taskos.data.model.api.ImageFolder;
import com.taskos.data.model.api.Job;
import com.taskos.ui.base.BaseNavigator;
import com.taskos.ui.base.BaseViewModel;

import java.util.ArrayList;
import java.util.List;

public class ImagesViewModel extends BaseViewModel<BaseNavigator> {
    private static final String TAG = "ImagesViewModel";
    private MutableLiveData<Job> jobMutableLiveData = new MutableLiveData<>();
    private List<ImageFolder> folderList = new ArrayList<>();
    private MutableLiveData<List<ImageFolder>> imageFolderMutableLiveData = new MutableLiveData<>();

    public ImagesViewModel(DataManager dataManager) {
        super(dataManager);
    }

    MutableLiveData<Job> getJobDetail(String uuid) {
        DocumentReference query = getDataManager().getFirestoreInstance().collection("Job").document(uuid);

        query.addSnapshotListener((documentSnapshot, e) -> {
            if (e != null) {
                Log.e(TAG, "onEvent: Listen failed.", e);
                return;
            }

            if (documentSnapshot != null) {
                Job job = documentSnapshot.toObject(Job.class);
                if (job != null) {
                    jobMutableLiveData.setValue(job);
                }
            }
        });

        return jobMutableLiveData;
    }

    MutableLiveData<List<ImageFolder>> getFolders(String jobUUID) {
        folderList.clear();
        Query query = getDataManager().getFirestoreInstance().collection("ImageFolder")
                .whereEqualTo("jobUuid", jobUUID);

        query.addSnapshotListener((queryDocumentSnapshots, e) -> {
            if (e != null) {
                Log.e(TAG, "onEvent: Listen failed.", e);
                return;
            }

            if (queryDocumentSnapshots != null) {
                folderList.clear();
                for (QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                    ImageFolder imageFolder = documentSnapshot.toObject(ImageFolder.class);
                    folderList.add(imageFolder);
                }
                imageFolderMutableLiveData.setValue(folderList);
            }
        });

        return imageFolderMutableLiveData;
    }
}