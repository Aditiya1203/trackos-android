package com.taskos.ui.main.openjobs;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.taskos.data.DataManager;
import com.taskos.data.model.api.Job;
import com.taskos.ui.base.BaseViewModel;
import com.taskos.util.AppConstants;

import java.util.ArrayList;
import java.util.List;

public class OpenJobsViewModel extends BaseViewModel<OpenJobsNavigator> {

    private static final String TAG = "OpenJobsViewModel";
    private ArrayList<Job> list = new ArrayList<>();
    private MutableLiveData<ArrayList<Job>> mutableLiveData = new MutableLiveData<>();


    public OpenJobsViewModel(DataManager dataManager) {
        super(dataManager);
    }

    MutableLiveData<ArrayList<Job>> getJobs(String value, int currentPos, String search) {
        list.clear();
        Query query;
        if (currentPos == 0) {
            if (search.isEmpty()) {
                query = getDataManager().getFirestoreInstance().collection("Job")
                        .whereEqualTo("scheduledStatus", value)
                        .whereEqualTo("owner", getDataManager().getCompanyID())
                        .orderBy("installStart", Query.Direction.DESCENDING);
            } else {
                query = getDataManager().getFirestoreInstance().collection("Job")
                        .whereEqualTo("name", search)
                        .whereEqualTo("owner", getDataManager().getCompanyID())
                        .whereEqualTo("scheduledStatus", value)
                        .orderBy("installStart", Query.Direction.DESCENDING);
            }
        } else if (currentPos == 1) {


            if (search.isEmpty()) {
                query = getDataManager().getFirestoreInstance().collection("Job")
                        .whereEqualTo("scheduledStatus", value)
                        .whereEqualTo("owner", getDataManager().getCompanyID())
                        .orderBy("installStart", Query.Direction.DESCENDING);
            } else {
                query = getDataManager().getFirestoreInstance().collection("Job")
                        .whereEqualTo("name", search)
                        .whereEqualTo("owner", getDataManager().getCompanyID())
                        .whereEqualTo("scheduledStatus", value)
                        .orderBy("installStart", Query.Direction.DESCENDING);
            }
        } else {
            List<String> status = new ArrayList<>();
            status.add(value);
            status.add(AppConstants.kJobsArray[2]);
            if (search.isEmpty()) {
                query = getDataManager().getFirestoreInstance().collection("Job")
                        .whereIn("scheduledStatus", status)
                        .whereEqualTo("owner", getDataManager().getCompanyID())
                        .orderBy("installStart", Query.Direction.DESCENDING);
            } else {
                query = getDataManager().getFirestoreInstance().collection("Job")
                        .whereEqualTo("name", search)
                        .whereEqualTo("owner", getDataManager().getCompanyID())
                        .whereIn("scheduledStatus", status)
                        .orderBy("installStart", Query.Direction.DESCENDING);
            }
        }

        query.addSnapshotListener((queryDocumentSnapshots, e) -> {
            if (e != null) {
                Log.e(TAG, "onEvent: Listen failed.", e);
                return;
            }

            if (queryDocumentSnapshots != null) {
                list.clear();
                for (QueryDocumentSnapshot doc : queryDocumentSnapshots) {
                    Job job = doc.toObject(Job.class);
                    list.add(job);
                }
                mutableLiveData.setValue(list);
            }
        });

        return mutableLiveData;
    }
}