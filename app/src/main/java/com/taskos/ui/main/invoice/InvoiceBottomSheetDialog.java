package com.taskos.ui.main.invoice;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProvider;

import com.google.firebase.firestore.FirebaseFirestore;
import com.taskos.R;
import com.taskos.ViewModelProviderFactory;
import com.taskos.data.model.api.EstimateInvoice;
import com.taskos.data.model.api.EstimateInvoiceNewItem;
import com.taskos.databinding.DialogAddJobBinding;
import com.taskos.ui.base.BaseBottomSheetDialog;
import com.taskos.ui.main.openjobs.addjob.AddJobViewModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;


public class InvoiceBottomSheetDialog extends BaseBottomSheetDialog<DialogAddJobBinding, AddJobViewModel> implements View.OnClickListener {

    private static final String TAG = "InvoiceBottomSheetDialog";
    private AddJobViewModel addJobViewModel;

    TextView tv_client_name;
    TextView tv_date;
    TextView tv_ponumberenter;
    TextView tv_total;
    EditText tv_tax;
    EditText tv_discount;
    EditText tv_markup;
    EditText tv_subtotal;
    Switch switch_mysignature;
    Switch switch_client_sign;
    EditText et_project;
    EditText et_client_notes;
    Double total;
    Boolean clientswitch=false,myswitch=false;
    public static InvoiceBottomSheetDialog newInstance() {

        Bundle args = new Bundle();
        InvoiceBottomSheetDialog fragment = new InvoiceBottomSheetDialog();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getBindingVariable() {
        return com.taskos.BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.dialog_invoice;
    }

    @Override
    public AddJobViewModel getViewModel() {
        addJobViewModel = new ViewModelProvider(this, ViewModelProviderFactory.getInstance()).get(AddJobViewModel.class);
        return addJobViewModel;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        TextView tv_cancel = view.findViewById(R.id.tv_cancel);
        TextView tv_add = view.findViewById(R.id.tv_add);
        tv_client_name = view.findViewById(R.id.tv_client_name);
        tv_date = view.findViewById(R.id.tv_date);
        tv_ponumberenter = view.findViewById(R.id.tv_ponumberenter);
        et_project = view.findViewById(R.id.et_project);
        switch_mysignature = view.findViewById(R.id.switch_mysignature);
        switch_client_sign = view.findViewById(R.id.switch_client_sign);
        et_client_notes = view.findViewById(R.id.et_client_notes);
        tv_total = view.findViewById(R.id.tv_total);
        tv_tax = view.findViewById(R.id.tv_tax);
        tv_markup = view.findViewById(R.id.tv_markup);
        tv_subtotal = view.findViewById(R.id.tv_subtotal);
        tv_discount = view.findViewById(R.id.tv_discount);
        RelativeLayout rl_add_item = view.findViewById(R.id.rl_add_item);
        LinearLayout ll_select_client = view.findViewById(R.id.ll_select_client);
        tv_cancel.setOnClickListener(this);
        tv_add.setOnClickListener(this);
        rl_add_item.setOnClickListener(this);
        ll_select_client.setOnClickListener(this);
        switch_client_sign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(switch_client_sign.isEnabled()){
                    clientswitch=true;
                }else {
                    clientswitch=false;
                }

            }
        });
        tv_tax.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                total=Double.parseDouble(tv_subtotal.getText().toString())+Double.parseDouble(tv_markup.getText().toString())-Double.parseDouble(tv_discount.getText().toString())+Double.parseDouble(s.toString());
                tv_total.setText("$"+total);
            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
            @Override
            public void afterTextChanged(Editable s) {

            }
        });
   switch_mysignature.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(switch_mysignature.isEnabled()){
                    myswitch=true;
                }else {
                    myswitch=false;
                }

            }
        });

    }


    public void show(FragmentManager fragmentManager) {
        super.show(fragmentManager, TAG);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_cancel:
                dismissDialog(TAG);
                break;
            case R.id.rl_add_item:
                AddNewItemBottomSheetDialog.newInstance().show(getChildFragmentManager());
                break;
            case R.id.ll_select_client:
                SelectClientBottomSheetDialog.newInstance().show(getChildFragmentManager());
                break;
        case R.id.tv_add:
            EstimateInvoice estimateInvoice=new EstimateInvoice();
            estimateInvoice.setUuid(UUID.randomUUID().toString());
            estimateInvoice.setClientNames(tv_client_name.getText().toString().trim());
            estimateInvoice.setClientSignatures(tv_client_name.getText().toString().trim());
            estimateInvoice.setDocNotes(et_client_notes.getText().toString().trim());
            try{
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yy");
                Date d = sdf.parse(tv_date.getText().toString().trim());
                estimateInvoice.setInvoiceDate(d);
            }catch(ParseException ex){
            }
            estimateInvoice.setMarkup(Double.valueOf(tv_markup.getText().toString().trim()));
            estimateInvoice.setPoNumber(Integer.valueOf(tv_ponumberenter.getText().toString().trim()));
            estimateInvoice.setSalesTax(Double.valueOf(tv_tax.getText().toString().trim()));
            estimateInvoice.setSubtotal(Double.valueOf(tv_subtotal.getText().toString().trim()));

            estimateInvoice.setPaymentTotal(total);
            estimateInvoice.setClientSignatureRequired(clientswitch);
            estimateInvoice.setSalesSignatureRequired(myswitch);

            SaveInvoiceItem(estimateInvoice);
                break;
        }
    }

    public void SaveInvoiceItem(EstimateInvoice estimateInvoice) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("Invoice_Estimate")
                .document(estimateInvoice.getUuid())
                .set(estimateInvoice)
                .addOnCompleteListener(task1 -> {
                    if (task1.isSuccessful()) {
                        dismissDialog(TAG);
                        Toast.makeText(getContext(),"Invoice Added",Toast.LENGTH_SHORT).show();
                    } else {

                    }
                });
    }


}
