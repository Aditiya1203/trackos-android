package com.taskos.ui.main.tasks;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;

import com.taskos.R;
import com.taskos.ViewModelProviderFactory;
import com.taskos.databinding.FragmentTasksBinding;
import com.taskos.ui.base.BaseFragment;
import com.taskos.ui.fcm.FcmNotificationBuilder;
import com.taskos.ui.main.client.addclient.AddClientBottomSheetDialog;
import com.taskos.ui.main.openjobs.addjob.AddJobBottomSheetDialog;
import com.taskos.ui.main.tasks.addtask.AddTaskBottomSheetDialog;
import com.taskos.ui.main.tasks.detail.TaskDetailsFragment;
import com.taskos.util.AppConstants;


public class TasksFragment extends BaseFragment<FragmentTasksBinding, TasksViewModel> implements View.OnClickListener {

    private TasksViewModel tasksViewModel;
    private int currentPos = 0;

    public static TasksFragment newInstance(Bundle bundle) {

        if (bundle == null) {
            bundle = new Bundle();
        }

        TasksFragment fragment = new TasksFragment();
        fragment.setArguments(bundle);
        return fragment;
    }
    
    @Override
    public int getBindingVariable() {
        return com.taskos.BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_tasks;
    }

    @Override
    public TasksViewModel getViewModel() {
        tasksViewModel = new ViewModelProvider(this, ViewModelProviderFactory.getInstance()).get(TasksViewModel.class);
        return tasksViewModel;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            String view = getArguments().getString("tabView");
            if (FcmNotificationBuilder.TAB_MY_TASK.equals(view == null ? "" : view)) {
                currentPos = 0;
            }
            String uuid = getArguments().getString("selectedUuid");
            if (uuid != null)
                navigateToDetail(uuid);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getViewDataBinding().menuFloat.setOnMenuToggleListener(isVisible -> {
            if (isVisible) {
                getViewDataBinding().blurringView.setVisibility(View.VISIBLE);
                getViewDataBinding().blurringView.setBlurredView(getViewDataBinding().llMain);
            } else invalidateBlurView();
        });
        getViewDataBinding().fabAddClient.setOnClickListener(this);
        getViewDataBinding().fabAddJob.setOnClickListener(this);
        getViewDataBinding().fabAddTask.setOnClickListener(this);
        getViewDataBinding().tvSearch.setOnClickListener(this);


        getViewDataBinding().segmentedButtonGroup.setPosition(currentPos);
        filterData("");
        getViewDataBinding().segmentedButtonGroup.setOnClickedButtonListener(position -> {
            currentPos = position;
            filterData("");
        });
        doAddTextWatcher();
    }


    private void doAddTextWatcher() {
        getViewDataBinding().etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!editable.toString().isEmpty()) {
                    if (editable.toString().length() > 2)
                        filterData(editable.toString());
                } else {
                    filterData("");
                }
            }
        });
    }

    private void filterData(String search) {
        if (currentPos == 0) {
            getViewDataBinding().etSearch.setHint("Search my Tasks");
            doTaskData("", 0, search);
        } else {
            getViewDataBinding().etSearch.setHint("Search All Open Tasks");
            doTaskData(AppConstants.kCompletionStatusArray[0], 1, search);
        }
    }

    private void doTaskData(String value, int currentPos, String search) {
        tasksViewModel.getOpenTask(value, currentPos, search).observe(getViewLifecycleOwner(), taskList -> {
            TaskAdapter taskAdapter = new TaskAdapter(currentPos == 1 ? TaskAdapter.VIEW_ALL_OPEN_TASK : TaskAdapter.VIEW_TYPE_COMMON, taskList, pos -> navigateToDetail(taskList.get(pos).getUuid()));
            getViewDataBinding().rcvTaskList.setAdapter(taskAdapter);
        });
    }

    private void navigateToDetail(String uuid) {
        getBaseActivity().addFragment(TaskDetailsFragment.newInstance(uuid), R.id.nav_host_fragment, true);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fab_add_client:
                getViewDataBinding().llSearch.setVisibility(View.GONE);
                AddClientBottomSheetDialog.newInstance().show(getChildFragmentManager());
                getViewDataBinding().menuFloat.close(true);
                break;

            case R.id.fab_add_job:
                getViewDataBinding().llSearch.setVisibility(View.GONE);
                AddJobBottomSheetDialog.newInstance().show(getChildFragmentManager());
                getViewDataBinding().menuFloat.close(true);
                break;

            case R.id.fab_add_task:
                getViewDataBinding().llSearch.setVisibility(View.GONE);
                AddTaskBottomSheetDialog.newInstance().show(getChildFragmentManager());
                getViewDataBinding().menuFloat.close(true);
                break;

            case R.id.tv_search:
                getViewDataBinding().llSearch.setVisibility(getViewDataBinding().llSearch.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
                break;
        }
    }

    private void invalidateBlurView() {
        getViewDataBinding().blurringView.setVisibility(View.GONE);
        getViewDataBinding().blurringView.invalidate();
    }
}