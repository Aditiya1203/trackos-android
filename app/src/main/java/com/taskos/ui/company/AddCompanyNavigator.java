

package com.taskos.ui.company;

import android.app.Activity;

public interface AddCompanyNavigator {

    void successful(String message);
    void goToMainScreen();
    void error(String message);
    String getCompanyName();
    String getStreetAddress();
    String getCity();
    String getState();
    String getZipCode();
    String getCompanyPhone();
    String getCompanyEmail();
    String getCompanyWebsite();
    boolean isWebsiteAvailable();
    void getCompanyData(Company company);
    Activity getActivity();

}
