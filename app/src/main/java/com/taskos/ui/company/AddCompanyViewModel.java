package com.taskos.ui.company;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.taskos.data.DataManager;
import com.taskos.data.local.PermissionData;
import com.taskos.ui.auth.MUser;
import com.taskos.ui.base.BaseViewModel;
import com.taskos.util.CommonUtils;

import java.util.ArrayList;
import java.util.List;

public class AddCompanyViewModel extends BaseViewModel<AddCompanyNavigator> {


    private FirebaseAuth mAuth;
    private Company company = new Company();

    public AddCompanyViewModel(DataManager dataManager) {
        super(dataManager);
        mAuth = FirebaseAuth.getInstance();
    }

    public String getUID(){
        return mAuth.getCurrentUser().getUid();
    }

    public void createCompany(){
        if(getNavigator().getCity().isEmpty()
                || getNavigator().getCompanyEmail().isEmpty()
                || getNavigator().getCompanyName().isEmpty()
                || getNavigator().getCompanyPhone().isEmpty()
                || getNavigator().getState().isEmpty()
                || getNavigator().getStreetAddress().isEmpty()
                || getNavigator().getZipCode().isEmpty()
                || (getNavigator().isWebsiteAvailable() && getNavigator().getCompanyWebsite().isEmpty())){
            getNavigator().error("Please enter all company information");
        }else{
            setIsLoading(true);
            addCompanyToFirestore();
        }

    }
    public void updateCompany(){
        if(getNavigator().getCity().isEmpty()
                || getNavigator().getCompanyEmail().isEmpty()
                || getNavigator().getCompanyName().isEmpty()
                || getNavigator().getCompanyPhone().isEmpty()
                || getNavigator().getState().isEmpty()
                || getNavigator().getStreetAddress().isEmpty()
                || getNavigator().getZipCode().isEmpty()
                || (getNavigator().isWebsiteAvailable() && getNavigator().getCompanyWebsite().isEmpty())){
            getNavigator().error("Please enter all company information");
        }else{
            setIsLoading(true);
            updateCompanyToFirestore();
        }
    }
    private Company getCompany(){
        Company company = new Company();
        List<String> admin = new ArrayList<>();
        admin.add(getUID());
        company.setAdministrator(admin);
        company.setCity(getNavigator().getCity());
        company.setDateAdded(CommonUtils.getCurrentDateTimeCompany());
        company.setEmail(getNavigator().getCompanyEmail());
        company.setEmployeeList(admin);
        company.setHasWebsite(getNavigator().isWebsiteAvailable());
        company.setName(getNavigator().getCompanyName());
        company.setPhone(getNavigator().getCompanyPhone());
        company.setState(getNavigator().getState());
        company.setStreetAddress(getNavigator().getStreetAddress());
        company.setSubscriptionEnd(CommonUtils.getCurrentDateTimeCompany());
        company.setUserCount(1);
        company.setWebsite(getNavigator().getCompanyWebsite());
        company.setZipcode(getNavigator().getZipCode());
        return company ;
    }

    public void getCompanyInformation(){
        getDataManager().getFirestoreInstance().collection("Company").document(getDataManager().getCompanyID())
                .addSnapshotListener(new EventListener<DocumentSnapshot>() {
                    @Override
                    public void onEvent(@Nullable DocumentSnapshot snapshot,
                                        @Nullable FirebaseFirestoreException e) {

                        if (snapshot != null && snapshot.exists()) {
                            Log.e("User Data already ", "Current data: all" + snapshot.getData());
                            Log.e("User Data already ", "Current data: all" + snapshot.getData().get("companyUuid"));

                            company = snapshot.toObject(Company.class);

//                            List<String> admin = new ArrayList<>();
//                            admin.add(getUID());
//                            company.setAdministrator(admin);
//                            company.setCity(snapshot.getData().get("city").toString());
//                            company.setDateAdded(snapshot.getData().get("dateAdded").toString());
//                            company.setEmail(snapshot.getData().get("email").toString());
//                            company.setEmployeeList(admin);
//                            company.setHasWebsite((Boolean) snapshot.getData().get("hasWebsite"));
//                            company.setName(snapshot.getData().get("name").toString());
//                            company.setPhone(snapshot.getData().get("phone").toString());
//                            company.setState(snapshot.getData().get("state").toString());
//                            company.setStreetAddress(snapshot.getData().get("streetAddress").toString());
//                            company.setSubscriptionEnd(snapshot.getData().get("subscriptionEnd").toString());
//                            company.setUserCount(1);
//                            company.setWebsite(snapshot.getData().get("website").toString());
//                            company.setZipcode(snapshot.getData().get("zipcode").toString());

                            getNavigator().getCompanyData(company);
                        }

                    }
                });
    }

    private void addCompanyToFirestore(){
        getDataManager().getFirestoreInstance().collection("Company").add(getCompany())
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        getDataManager().setCompanyID(documentReference.getId());
                        getDataManager().setCompanyName(getCompany().getName());
                        getDataManager().getFirestoreInstance().collection("MUser").document(getUID()).update("companyUuid", documentReference.getId());
                        getDataManager().getFirestoreInstance().collection("MUser").document(getUID()).update("company", getCompany().getName());
                        getDataManager().getFirestoreInstance().collection("MUser").document(getUID()).update("permissionLevel", PermissionData.getFullPermission());
                        getDataManager().getFirestoreInstance().collection("Company").document(documentReference.getId()).update("uuid", documentReference.getId());
                        getDataManager().setCompanyAdded(true);
                        getNavigator().goToMainScreen();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        getNavigator().error(e.getMessage());
                    }
                });
    }

    private void updateCompanyToFirestore(){

        company.setCity(getNavigator().getCity());
        company.setEmail(getNavigator().getCompanyEmail());
        company.setHasWebsite(getNavigator().isWebsiteAvailable());
        company.setName(getNavigator().getCompanyName());
        company.setPhone(getNavigator().getCompanyPhone());
        company.setState(getNavigator().getState());
        company.setStreetAddress(getNavigator().getStreetAddress());
        company.setWebsite(getNavigator().getCompanyWebsite());
        company.setZipcode(getNavigator().getZipCode());

        getDataManager().getFirestoreInstance().collection("Company").document(getDataManager().getCompanyID())
                .update(
                        "city", getNavigator().getCity(),
                        "city", getNavigator().getCity(),
                        "city", getNavigator().getCity(),
                        "email", getNavigator().getCompanyEmail(),
                        "hasWebsite", getNavigator().isWebsiteAvailable(),
                        "name", getNavigator().getCompanyName(),
                        "phone", getNavigator().getCompanyPhone(),
                        "state", getNavigator().getState(),
                        "streetAddress", getNavigator().getStreetAddress(),
                        "website", getNavigator().getCompanyWebsite(),
                        "zipcode", getNavigator().getZipCode()
                        )
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        getDataManager().setCompanyName(getNavigator().getCompanyName());
                        getNavigator().successful("Updated Successfully");
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        getNavigator().error(e.getMessage());
                    }
                });
//
    }


}
