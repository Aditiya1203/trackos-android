package com.taskos.ui.company;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;

import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;

import com.taskos.R;
import com.taskos.TaskOS;
import com.taskos.ViewModelProviderFactory;
import com.taskos.databinding.ActivityAddCompanyBinding;
import com.taskos.ui.auth.moreInfo.CompleteRegistration;
import com.taskos.ui.base.BaseActivity;
import com.taskos.util.UsPhoneNumberFormatter;

import java.lang.ref.WeakReference;

public class AddCompanyActivity extends BaseActivity<ActivityAddCompanyBinding, AddCompanyViewModel> implements AddCompanyNavigator, View.OnClickListener {

    private AddCompanyViewModel mAddCompanyViewModel;

    @Override
    public int getBindingVariable() {
        return com.taskos.BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_add_company;
    }

    @Override
    public AddCompanyViewModel getViewModel() {
        mAddCompanyViewModel = new ViewModelProvider(this, ViewModelProviderFactory.getInstance()).get(AddCompanyViewModel.class);
        return mAddCompanyViewModel;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAddCompanyViewModel.setNavigator(this);
        init();
    }

    private void init() {
        findViewById(R.id.tv_cancel).setOnClickListener(this);
        findViewById(R.id.tv_save).setOnClickListener(this);

        getViewDataBinding().switch1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                getViewDataBinding().companyWebLayout.setVisibility(b ? View.VISIBLE : View.GONE);
            }
        });


        try {
            UsPhoneNumberFormatter addLineNumberFormatter = new UsPhoneNumberFormatter(
                    new WeakReference<EditText>(getViewDataBinding().edtCompanyPhone));
            getViewDataBinding().edtCompanyPhone.addTextChangedListener(addLineNumberFormatter);
        }catch (Exception e){}
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.tv_cancel:
                if (TaskOS.getInstance().getDataManager().isRegisterAdded()) {
                    onBackPressed();
                }else {
                    TaskOS.getInstance().getDataManager().setCompanyAdded(false);
                    startActivity(new Intent(AddCompanyActivity.this, CompleteRegistration.class));
                }
                break;
            case R.id.tv_save:
                showLoading();
                mAddCompanyViewModel.createCompany();
                break;
        }
    }

    @Override
    public void successful(String message) {
        hideLoading();
        showOkAlert(message);
    }

    @Override
    public void goToMainScreen() {
        hideLoading();
        TaskOS.getInstance().getDataManager().setCompanyAdded(true);
        if(!TaskOS.getInstance().getDataManager().isRegisterAdded()) {
            startActivity(new Intent(AddCompanyActivity.this, CompleteRegistration.class));
            finish();
        }else{
            onBackPressed();
        }
    }

    @Override
    public void error(String message) {
        hideLoading();
        showOkAlert(message);
    }

    @Override
    public String getCompanyName() {
        return getViewDataBinding().edtCompanyName.getText().toString().trim();
    }

    @Override
    public String getStreetAddress() {
        return getViewDataBinding().edtStreetAddress.getText().toString().trim();
    }

    @Override
    public String getCity() {
        return getViewDataBinding().edtCity.getText().toString().trim();
    }

    @Override
    public String getState() {
        return getViewDataBinding().edtState.getText().toString().trim();
    }

    @Override
    public String getZipCode() {
        return getViewDataBinding().edtZipcode.getText().toString().trim();
    }

    @Override
    public String getCompanyPhone() {
        return getViewDataBinding().edtCompanyPhone.getText().toString().trim();
    }

    @Override
    public String getCompanyEmail() {
        return getViewDataBinding().edtCompanyEmail.getText().toString().trim();
    }

    @Override
    public String getCompanyWebsite() {
        return isWebsiteAvailable() ? getViewDataBinding().edtCompanyWebsite.getText().toString().trim() : "";
    }

    @Override
    public boolean isWebsiteAvailable() {
        return getViewDataBinding().switch1.isChecked();
    }

    @Override
    public void getCompanyData(Company company) {

    }

    @Override
    public Activity getActivity() {
        return this;
    }
}
