package com.taskos.ui.onboard;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.taskos.R;
import com.taskos.TaskOS;
import com.taskos.ui.auth.moreInfo.CompleteRegistration;
import com.taskos.ui.company.AddCompanyActivity;
import com.taskos.ui.main.MainActivity;

public class SelectRoleActivity extends AppCompatActivity implements View.OnClickListener {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_role);
        init();
    }

    private void init() {
        findViewById(R.id.company_owner).setOnClickListener(this);
        findViewById(R.id.employee).setOnClickListener(this);
        findViewById(R.id.individual).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.company_owner:
                if (!TaskOS.getInstance().getDataManager().isCompanyAdded()) {
                    startActivity(new Intent(SelectRoleActivity.this, AddCompanyActivity.class));
                } else if (!TaskOS.getInstance().getDataManager().isRegisterAdded()) {
                    startActivity(new Intent(SelectRoleActivity.this, CompleteRegistration.class));
                }else {
                    startActivity(new Intent(SelectRoleActivity.this, MainActivity.class));
                }
                break;

            case R.id.employee:
                if (!TaskOS.getInstance().getDataManager().isRegisterAdded()) {
                    startActivity(new Intent(SelectRoleActivity.this, CompleteRegistration.class));
                }else {
                    startActivity(new Intent(SelectRoleActivity.this, MainActivity.class));
                }
                break;

            case R.id.individual:
                if (!TaskOS.getInstance().getDataManager().isRegisterAdded()) {
                    startActivity(new Intent(SelectRoleActivity.this, CompleteRegistration.class));
                }else {
                    startActivity(new Intent(SelectRoleActivity.this, MainActivity.class));
                }
                break;
        }
    }
}
