package com.taskos.ui.emailCustomer;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.auth.FirebaseAuth;
import com.taskos.R;
import com.taskos.TaskOS;
import com.taskos.util.CommonUtils;

import java.util.Objects;

public class EmailCustomerActivity extends AppCompatActivity implements View.OnClickListener {

    private FirebaseAuth mAuth;
    String customerName = "";
    String sendTO = "";
    String appointmentTime = "";
    String salesperson = "";
    String determineWebsite = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_email_customer);
        init();
    }

    private void init(){
        mAuth = FirebaseAuth.getInstance();
        if (getIntent().hasExtra("Email")) {
            sendTO = getIntent().getStringExtra("Email");
        } else {
            sendTO = Objects.requireNonNull(mAuth.getCurrentUser()).getEmail();
        }
        customerName = TaskOS.getInstance().getDataManager().getUserDisplayName();
        appointmentTime = CommonUtils.getCurrentDateTimeEmail();
        salesperson = TaskOS.getInstance().getDataManager().getUserDisplayName();

        findViewById(R.id.new_customer).setOnClickListener(this);
        findViewById(R.id.appointment_email).setOnClickListener(this);
        findViewById(R.id.write_own).setOnClickListener(this);

        findViewById(R.id.custom).setOnClickListener(this);

        findViewById(R.id.tv_cancel).setOnClickListener(view -> onBackPressed());
    }
//    https://developer.android.com/guide/components/intents-common#ComposeEmail
    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.new_customer:
                try {
                    Intent intent = new Intent(Intent.ACTION_SENDTO);
                    intent.setData(Uri.parse("mailto:")); // only email apps should handle this
                    intent.putExtra(Intent.EXTRA_EMAIL, sendTO);
                    intent.putExtra(Intent.EXTRA_SUBJECT, getSubject());
                    intent.putExtra(Intent.EXTRA_TEXT, getWelcomeMessage());
                    if (intent.resolveActivity(getPackageManager()) != null) {
                        startActivity(intent);
                    }
                }catch (Exception e){e.printStackTrace();}
            break;
            case R.id.appointment_email:
                try {
                    Intent intent = new Intent(Intent.ACTION_SENDTO);
                    intent.setData(Uri.parse("mailto:")); // only email apps should handle this
                    intent.putExtra(Intent.EXTRA_EMAIL, sendTO);
                    intent.putExtra(Intent.EXTRA_SUBJECT, getSubjectAppointment());
                    intent.putExtra(Intent.EXTRA_TEXT, getAppointmentMessage());
                    if (intent.resolveActivity(getPackageManager()) != null) {
                        startActivity(intent);
                    }
                }catch (Exception e){e.printStackTrace();}
            break;
            case R.id.write_own:
                try {
                    Intent intent = new Intent(Intent.ACTION_SENDTO);
                    intent.setData(Uri.parse("mailto:")); // only email apps should handle this
                    intent.putExtra(Intent.EXTRA_EMAIL, sendTO);
                    if (intent.resolveActivity(getPackageManager()) != null) {
                        startActivity(intent);
                    }
                }catch (Exception e){e.printStackTrace();}
            break;
            case R.id.custom:
                try {
                    Intent intent = new Intent(Intent.ACTION_SENDTO);
                    intent.setData(Uri.parse("mailto:")); // only email apps should handle this
                    intent.putExtra(Intent.EXTRA_EMAIL, sendTO);
                    if (intent.resolveActivity(getPackageManager()) != null) {
                        startActivity(intent);
                    }
                }catch (Exception e){e.printStackTrace();}
            break;
        }
    }

    private String getWelcomeMessage(){
        return "Hello "+customerName+"\nThank you for contacting us today. I have your appointment set for "
                +appointmentTime+" with "+salesperson+""+determineWebsite+ " \nHave a great week!";
    }
    private String getAppointmentMessage(){
        return "Hello "+customerName+"\nI am looking forward to meeting with you on "
                +appointmentTime+" to discuss your project\n If you need to reschedule for any reason, please " +
                "let me know. If i don't hear from you otherwise I'll look forward to seeing you then.\nBest,";
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (intent.hasExtra("Email")) {
            sendTO = intent.getStringExtra("Email");
        }
    }
    private String getSubject(){
        if(TaskOS.getInstance().getDataManager().getCompanyName()!=null && !TaskOS.getInstance().getDataManager().getCompanyName().equalsIgnoreCase(""))
            return "Welcome to "+TaskOS.getInstance().getDataManager().getCompanyName();
        return "Welcome to Here!";
    }
    private String getSubjectAppointment(){
        if(TaskOS.getInstance().getDataManager().getCompanyName()!=null && !TaskOS.getInstance().getDataManager().getCompanyName().equalsIgnoreCase(""))
            return TaskOS.getInstance().getDataManager().getCompanyName()+" Appointment Confirmation";
        return "Appointment Confirmation";
    }
}
