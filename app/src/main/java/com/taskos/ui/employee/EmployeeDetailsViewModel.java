package com.taskos.ui.employee;

import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.taskos.data.DataManager;
import com.taskos.data.local.PermissionData;
import com.taskos.ui.auth.MUser;
import com.taskos.ui.base.BaseViewModel;
import com.taskos.ui.company.Company;
import com.taskos.util.CommonUtils;

import java.util.ArrayList;
import java.util.List;

public class EmployeeDetailsViewModel extends BaseViewModel<EmployeeDetailsNavigator> {


    private FirebaseAuth mAuth;

    public EmployeeDetailsViewModel(DataManager dataManager) {
        super(dataManager);
        mAuth = FirebaseAuth.getInstance();
    }

    public String getUID(){
        return mAuth.getCurrentUser().getUid();
    }

    public void getEmployeeInformation(){
        getDataManager().getFirestoreInstance().collection("MUser").document(getNavigator().getUUID())
                .addSnapshotListener(new EventListener<DocumentSnapshot>() {
                    @Override
                    public void onEvent(@Nullable DocumentSnapshot snapshot,
                                        @Nullable FirebaseFirestoreException e) {

                        if (snapshot != null && snapshot.exists()) {
                            MUser mUser = snapshot.toObject(MUser.class);
                            getNavigator().setCompanyData(mUser);
                        }else{
                            getNavigator().getActivity().onBackPressed();
                        }

                    }
                });
    }

    public void savePermission(){
        getDataManager().getFirestoreInstance().collection("MUser").document(getUID()).update("permissionLevel", getNavigator().getPermissionList());
    }

    public void removeEmployee(){
        if(getDataManager().getUUID().equalsIgnoreCase(getUID())){
            getDataManager().getFirestoreInstance().collection("Company").document(getDataManager().getCompanyID()).delete().addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    getDataManager().setCompanyAdded(false);
                    getDataManager().setCompanyID("");
                    getDataManager().setCompanyName("");

                    getDataManager().getFirestoreInstance().collection("MUser").document(getDataManager().getUUID()).update("companyUuid", "");
                    getDataManager().getFirestoreInstance().collection("MUser").document(getDataManager().getUUID()).update("company", "");
                    getDataManager().getFirestoreInstance().collection("MUser").document(getDataManager().getUUID()).update("permissionLevel", new ArrayList<String>());

                    Toast.makeText(getNavigator().getActivity(),"Removed Successful",Toast.LENGTH_SHORT).show();
                    getNavigator().getActivity().onBackPressed();

                }
            });
        }else{
            getDataManager().getFirestoreInstance().collection("Company").document(getDataManager().getCompanyID()).update("employeeList", FieldValue.arrayRemove(getDataManager().getUUID())).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    Toast.makeText(getNavigator().getActivity(),"Removed Successful",Toast.LENGTH_SHORT).show();
                    getNavigator().getActivity().onBackPressed();
                }
            });
        }
    }

    public void removeNetwork(){
        getDataManager().getFirestoreInstance().collection("MUser").document(getDataManager().getUUID()).update("userNetwork", FieldValue.arrayRemove(getUID())).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                Toast.makeText(getNavigator().getActivity(),"Removed Successful",Toast.LENGTH_SHORT).show();
                getNavigator().getActivity().onBackPressed();
            }
        });
    }

}
