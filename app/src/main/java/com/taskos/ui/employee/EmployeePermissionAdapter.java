package com.taskos.ui.employee;

import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.taskos.R;
import com.taskos.data.local.PermissionModel;
import com.taskos.data.model.api.Customer;
import com.taskos.ui.base.BaseViewHolder;
import com.taskos.ui.base.ItemClickCallback;
import com.taskos.util.CalenderUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Hemant Sharma on 23-02-20.
 */
public class EmployeePermissionAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    private ItemClickCallback callback;
    private List<PermissionModel> permissionModels;

    public EmployeePermissionAdapter(List<PermissionModel> permissionModels) {
        this.permissionModels = permissionModels;
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new ViewHolder(
                    LayoutInflater.from(parent.getContext()).inflate(R.layout.item_permission, parent, false));
    }

    @Override
    public int getItemCount() {
        if (permissionModels != null && permissionModels.size() > 0) {
            return permissionModels.size();
        } else {
            return 0;
        }
    }

    public class ViewHolder extends BaseViewHolder {
        private TextView tv_name;
        private CheckBox switch_permission;

        public ViewHolder(View itemView) {
            super(itemView);
            tv_name = itemView.findViewById(R.id.tv_name);
            switch_permission = itemView.findViewById(R.id.switch_permission);

        }

        public void onBind(int position) {
            PermissionModel tmpBean = permissionModels.get(position);
            tv_name.setText(tmpBean.getName());
            switch_permission.setChecked(tmpBean.isSelected());

            switch_permission.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    tmpBean.setSelected(b);
                }
            });

        }
    }
}
