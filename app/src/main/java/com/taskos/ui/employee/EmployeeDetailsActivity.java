package com.taskos.ui.employee;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.taskos.R;
import com.taskos.ViewModelProviderFactory;
import com.taskos.data.local.PermissionData;
import com.taskos.data.local.PermissionModel;
import com.taskos.databinding.ActivityAddCompanyBinding;
import com.taskos.databinding.ActivityEmployeeDetailBinding;
import com.taskos.ui.auth.MUser;
import com.taskos.ui.base.BaseActivity;
import com.taskos.ui.company.AddCompanyViewModel;
import com.taskos.util.PermissionUtils;

import java.util.ArrayList;
import java.util.List;

public class EmployeeDetailsActivity  extends BaseActivity<ActivityEmployeeDetailBinding, EmployeeDetailsViewModel> implements EmployeeDetailsNavigator, View.OnClickListener{


    private EmployeePermissionAdapter companyPermission, customerPermission, jobPermission, taskPermission;
    private EmployeeDetailsViewModel mEmployeeDetailsViewModel;
    List<PermissionModel> companyPermissionList, customerPermissionList, jobPermissionList, taskPermissionList;
    List<String> addedPermission = new ArrayList<>();

    @Override
    public int getBindingVariable() {
        return com.taskos.BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_employee_detail;
    }

    @Override
        public EmployeeDetailsViewModel getViewModel() {
        mEmployeeDetailsViewModel = new ViewModelProvider(this, ViewModelProviderFactory.getInstance()).get(EmployeeDetailsViewModel.class);
        return mEmployeeDetailsViewModel;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mEmployeeDetailsViewModel.setNavigator(this);
        init();
    }

    private void init(){

        TextView tvTitle = findViewById(R.id.tvTitle);
        tvTitle.setText("  Employee");

        getViewDataBinding().removeCompany.setOnClickListener(this);
        getViewDataBinding().savePermission.setOnClickListener(this);
        getViewDataBinding().callUser.setOnClickListener(this);
        getViewDataBinding().emailUser.setOnClickListener(this);
        findViewById(R.id.img_back).setOnClickListener(this);
        tvTitle.setOnClickListener(this);

        getViewDataBinding().switchPermission.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    getViewDataBinding().permissionLy.setVisibility(View.VISIBLE);
                }else{
                    getViewDataBinding().permissionLy.setVisibility(View.GONE);
                }
            }
        });

        mEmployeeDetailsViewModel.getEmployeeInformation();

        setPermission();

    }

    private boolean isPermitted(){
        return false;
    }

    private void setPermission(){

        companyPermissionList = PermissionData.getCompanyPermission(addedPermission);
        customerPermissionList = PermissionData.getCustomerPermission(addedPermission);
        jobPermissionList = PermissionData.getJobPermission(addedPermission);
        taskPermissionList = PermissionData.getTaskPermission(addedPermission);

        companyPermission = new EmployeePermissionAdapter(companyPermissionList);
        customerPermission = new EmployeePermissionAdapter(customerPermissionList);
        jobPermission = new EmployeePermissionAdapter(jobPermissionList);
        taskPermission = new EmployeePermissionAdapter(taskPermissionList);

        getViewDataBinding().rvCompanyPermission.setAdapter(companyPermission);
        getViewDataBinding().rvCustomerPermission.setAdapter(customerPermission);
        getViewDataBinding().rvJobPermission.setAdapter(jobPermission);
        getViewDataBinding().rvTaskPermission.setAdapter(taskPermission);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){
            case R.id.save_permission:
                mEmployeeDetailsViewModel.savePermission();
                break;
            case R.id.img_back:
                onBackPressed();
                break;
            case R.id.tvTitle:
                onBackPressed();
                break;
            case R.id.call_user:
                if (PermissionUtils.requestCallPermission(getActivity())) {
                    Intent intent1 = new Intent(Intent.ACTION_CALL);
                    intent1.setData(Uri.parse("tel:" + getViewDataBinding().phone.getText().toString()));
                    startActivity(intent1);
                }
                break;
            case R.id.email_user:
                startActivity(new Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:".concat(getViewDataBinding().email.getText().toString()))));
                break;
            case R.id.remove_company:
                showAlert();
                break;
        }

    }

    @Override
    public List<String> getPermissionList() {
        List<String> addedPermission = new ArrayList<>();


        for (int i = 0; i < taskPermissionList.size(); i++){
            if(taskPermissionList.get(i).isSelected())
                addedPermission.add(taskPermissionList.get(i).getName());
        }
        for (int i = 0; i < jobPermissionList.size(); i++){
            if(jobPermissionList.get(i).isSelected())
                addedPermission.add(jobPermissionList.get(i).getName());
        }
        for (int i = 0; i < customerPermissionList.size(); i++){
            if(customerPermissionList.get(i).isSelected())
                addedPermission.add(customerPermissionList.get(i).getName());
        }
        for (int i = 0; i < companyPermissionList.size(); i++){
            if(companyPermissionList.get(i).isSelected())
                addedPermission.add(companyPermissionList.get(i).getName());
        }

        return addedPermission;
    }

    @Override
    public void setCompanyData(MUser mUser) {
            getViewDataBinding().tvName.setText(mUser.getUserFirstName()+" "+mUser.getUserFirstName());
            getViewDataBinding().name.setText(mUser.getUserFirstName()+" "+mUser.getUserFirstName());
            getViewDataBinding().company.setText(mUser.getCompany());
            getViewDataBinding().job.setText(mUser.getJobRole());
            getViewDataBinding().phone.setText(mUser.getPhone());
            getViewDataBinding().email.setText(mUser.getEmail());
            addedPermission = mUser.getPermissionLevel();
            setPermission();

            if(mUser.getUserProfileImageLink()!=null && !mUser.getUserProfileImageLink().equalsIgnoreCase("")) {
                Glide.with(getActivity())
                        .load(mUser.getUserProfileImageLink())
                        .centerCrop()
                        .placeholder(R.drawable.ic_def_user)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .error(R.drawable.ic_def_user)
                        .into(getViewDataBinding().userProfile);
            }
    }

    @Override
    public String getUUID() {
        return getIntent().getStringExtra("UUID");
    }

    @Override
    public Activity getActivity() {
        return this;
    }

    public AlertDialog showAlert(){
        AlertDialog dialog;
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Are you Sure?");
        builder.setMessage("This will delete all tast for this user. Client salesperson, Scheduled Jobs will have to be manually changed. Do you want to continue?")
                .setPositiveButton("Remove Employee", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        mEmployeeDetailsViewModel.removeEmployee();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        // Create the AlertDialog object and return it
        dialog = builder.create();
        dialog.show();
        return dialog;
    }
}
