

package com.taskos.ui.employee;

import android.app.Activity;

import com.taskos.ui.auth.MUser;
import com.taskos.ui.company.Company;

import java.util.List;

public interface EmployeeDetailsNavigator {

    List<String> getPermissionList();

    void setCompanyData(MUser mUser);

    String getUUID();

    Activity getActivity();


}
