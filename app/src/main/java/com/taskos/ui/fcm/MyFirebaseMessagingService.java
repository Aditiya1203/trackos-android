package com.taskos.ui.fcm;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;

import androidx.core.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.taskos.R;
import com.taskos.TaskOS;
import com.taskos.ui.main.MainActivity;
import com.taskos.util.AppLogger;

import java.util.Objects;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMessaging";


    //PAYLOAD
    /*notification: {
        title: "New Job Comment posted by: " + (comment['username']),
        body: notificationBody,
        sound: "default",
        badge: "1"
        },

        data: {
        selectedView : jobsView,
        tabView : allJobs,
        selectedUuid : jobID,
        }*/

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {


        AppLogger.d(TAG, "getData From : " + remoteMessage.getFrom());
        AppLogger.d(TAG, "getData Noti: " + Objects.requireNonNull(remoteMessage.getNotification()).getTitle());
        AppLogger.d(TAG, "getData Noti: " + Objects.requireNonNull(remoteMessage.getNotification()).getBody());
        AppLogger.d(TAG, "getData : " + remoteMessage.getData().toString());

        if (TaskOS.getInstance().getDataManager().isLoggedIn()) {
            notificationHandle(remoteMessage);
        }
    }

    private void notificationHandle(RemoteMessage remoteMessage) {
        String title = remoteMessage.getData().get("title");
        String body = remoteMessage.getData().get("body");
        String selectedView = remoteMessage.getData().get("selectedView");
        String tabView = remoteMessage.getData().get("tabView");
        String selectedUuid = remoteMessage.getData().get("selectedUuid");

        if (title == null)
            title = Objects.requireNonNull(remoteMessage.getNotification()).getTitle();
        if (body == null)
            body = Objects.requireNonNull(remoteMessage.getNotification()).getBody();

        showNotification(title, body, selectedView, tabView, selectedUuid);
    }


    private void showNotification(String title, String body, String selectedView, String tabView, String selectedUuid) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("selectedView", selectedView);
        intent.putExtra("tabView", tabView);
        intent.putExtra("selectedUuid", selectedUuid);

        int iUniqueId = (int) (System.currentTimeMillis() & 0xfffffff);
        PendingIntent pendingIntent = PendingIntent.getActivities(this, iUniqueId, new Intent[]{intent}, PendingIntent.FLAG_ONE_SHOT);
        Uri notificaitonSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        String CHANNEL_ID = getString(R.string.default_notification_channel_id);// The id of the channel.
        CharSequence name = "Abc";// The user-visible name of the channel.
        int importance = 0;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            importance = NotificationManager.IMPORTANCE_HIGH;
        }
        /*Bitmap largeIcon=null;

        try {
            //largeIcon = Picasso.with(getApplicationContext()).load(profilePic).get();
            largeIcon = BitmapFactory.decodeResource(getApplicationContext().getResources(), R.drawable.traksos);
        } catch (Exception e) {
            e.printStackTrace();
        }*/
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle(title)//title
                .setContentText(body)//body
                .setAutoCancel(true)
                .setSound(notificaitonSound)
                .setContentIntent(pendingIntent)
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(body)); //body

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            notificationBuilder.setSmallIcon(R.drawable.logo_tr);
            notificationBuilder.setColor(getResources().getColor(R.color.colorPrimary));
        } else {
            notificationBuilder.setSmallIcon(R.drawable.traksos);
        }

       /* if(largeIcon!=null){
            notificationBuilder.setLargeIcon(largeIcon);
        }*/

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
            mChannel.setShowBadge(true);
            mChannel.enableLights(true);
            assert notificationManager != null;
            notificationManager.createNotificationChannel(mChannel);
        }

        assert notificationManager != null;
        notificationManager.notify(0, notificationBuilder.build());
    }

}
