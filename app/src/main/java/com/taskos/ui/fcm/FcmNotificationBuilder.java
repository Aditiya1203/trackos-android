package com.taskos.ui.fcm;


import androidx.annotation.NonNull;

import com.taskos.util.AppLogger;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


public class FcmNotificationBuilder {
    //Views
    public static final String VIEW_JOB = "jobsView";
    public static final String VIEW_TASK = "taskView";
    public static final String VIEW_CUSTOMER = "customerView";
    public static final String VIEW_NETWORK = "networkView";
    //tabs
    public static final String TAB_MY_LEAD = "myLeads";
    public static final String TAB_ALL_CUSTOMER = "allCustomers";
    public static final String TAB_MY_TASK = "myTasks";
    public static final String TAB_All_JOB = "allJobs";
    public static final String TAB_SEARCH_NETWORK = "searchNetwork";
    public static final String TAB_SHARED_JOB = "sharedJob";
    public static final String TAB_MY_NETWORK = "myNetwork";
    public static final String TAB_COWORKERS = "coworkers";
    private static final MediaType MEDIA_TYPE_JSON = MediaType.parse("application/json; charset=utf-8");
    private static final String TAG = "FcmNotificationBuilder";
    //private static final String SERVER_API_KEY = "AIzaSyAmXjttTe4uwGbQuJhFKiR7FQIrfWrhzGM";
    private static final String SERVER_API_KEY = "AIzaSyB2rHrk7LJePqiKoZ-e5m0az6TtMz6QyFg";
    private static final String CONTENT_TYPE = "Content-Type";
    private static final String APPLICATION_JSON = "application/json";
    private static final String AUTHORIZATION = "Authorization";
    private static final String AUTH_KEY = "key=" + SERVER_API_KEY;
    private static final String FCM_URL = "https://fcm.googleapis.com/fcm/send";
    //PAYLOAD
    /*notification: {
        title: "New Job Comment posted by: " + (comment['username']),
        body: notificationBody,
        sound: "default",
        badge: "1"
        },

        data: {
        selectedView : jobsView,
        tabView : allJobs,
        selectedUuid : jobID,
        }*/
    // json related keys
    private static final String KEY_TO = "to";
    private static final String KEY_TO_MULTIPLE = "registration_ids";
    //Notification payload
    private static final String KEY_NOTIFICATION = "notification";
    private static final String KEY_TITLE = "title";
    private static final String KEY_BODY = "body";
    private static final String KEY_SOUND = "sound";
    private static final String KEY_BADGE = "badge";
    //Data payload
    private static final String KEY_DATA = "data";
    private static final String KEY_SELECT_VIEW = "selectedView";
    private static final String KEY_TAB_VIEW = "tabView";
    private static final String KEY_SELECT_UUID = "selectedUuid";
    private String mTitle;
    private String mBody;
    private String mSelectedView;
    private String mTabView;
    private String mSelectedUuid;
    private String mReceiverFirebaseToken;
    private List<String> mReceiverFirebaseTokenList;

    private FcmNotificationBuilder() {

    }

    public static FcmNotificationBuilder newInstance() {
        return new FcmNotificationBuilder();
    }

    public FcmNotificationBuilder setTitle(String title) {
        mTitle = title;
        return this;
    }

    public FcmNotificationBuilder setBody(String body) {
        mBody = body;
        return this;
    }

    public FcmNotificationBuilder setSelectedView(String selectedView) {
        mSelectedView = selectedView;
        return this;
    }

    public FcmNotificationBuilder setTabView(String tabView) {
        mTabView = tabView;
        return this;
    }

    public FcmNotificationBuilder setSelectedUUID(String selectedUuid) {
        mSelectedUuid = selectedUuid;
        return this;
    }

    public FcmNotificationBuilder setReceiverFCMToken(String receiverFcmToken) {
        mReceiverFirebaseToken = receiverFcmToken;
        return this;
    }

    public FcmNotificationBuilder setReceiverFCMTokenList(ArrayList<String> receiverFcmTokenList) {
        mReceiverFirebaseTokenList = receiverFcmTokenList;
        return this;
    }

    public void sendNotification() {
        RequestBody requestBody = null;
        try {
            requestBody = RequestBody.create(MEDIA_TYPE_JSON, getValidJsonBody().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        assert requestBody != null;
        Request request = new Request.Builder()
                .addHeader(CONTENT_TYPE, APPLICATION_JSON)
                .addHeader(AUTHORIZATION, AUTH_KEY)
                .url(FCM_URL)
                .post(requestBody)
                .build();

        Call call = new OkHttpClient().newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(@NonNull Call call, @NonNull IOException e) {
                AppLogger.e(TAG, "onGetAllUsersFailure: " + e.getMessage());
            }

            @Override
            public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
                AppLogger.e(TAG, "onResponse: " + Objects.requireNonNull(response.body()).string());
            }
        });
    }

    private JSONObject getValidJsonBody() throws JSONException {
        if (mBody.startsWith("https://firebasestorage.googleapis.com/") || mBody.startsWith("content://")) {
            mBody = "Attachment";
        }

        JSONObject jsonObjectData = new JSONObject();
        jsonObjectData.put(KEY_TITLE, mTitle);
        jsonObjectData.put(KEY_BODY, mBody);
        jsonObjectData.put("icon", "icon");
        jsonObjectData.put(KEY_SOUND, "default");
        jsonObjectData.put(KEY_BADGE, "1");
        jsonObjectData.put(KEY_SELECT_VIEW, mSelectedView);
        jsonObjectData.put(KEY_TAB_VIEW, mTabView);
        jsonObjectData.put(KEY_SELECT_UUID, mSelectedUuid);

        JSONObject jsonObjectNotification = new JSONObject();
        jsonObjectNotification.put(KEY_TITLE, mTitle);
        jsonObjectNotification.put(KEY_BODY, mBody);
        jsonObjectNotification.put("icon", "icon");
        jsonObjectNotification.put(KEY_SOUND, "default");
        jsonObjectNotification.put(KEY_BADGE, "1");
        jsonObjectNotification.put(KEY_SELECT_VIEW, mSelectedView);
        jsonObjectNotification.put(KEY_TAB_VIEW, mTabView);
        jsonObjectNotification.put(KEY_SELECT_UUID, mSelectedUuid);

        JSONObject jsonObjectBody = new JSONObject();
        if (mReceiverFirebaseTokenList == null) {
            jsonObjectBody.put(KEY_TO, mReceiverFirebaseToken);
        } else {
            JSONArray jsonArray = new JSONArray(mReceiverFirebaseTokenList);
            jsonObjectBody.put(KEY_TO_MULTIPLE, jsonArray);
        }
        jsonObjectBody.put(KEY_DATA, jsonObjectData);
        jsonObjectBody.put(KEY_NOTIFICATION, jsonObjectNotification);

        return jsonObjectBody;
    }
}


