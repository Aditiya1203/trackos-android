package com.taskos.ui.howto;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.taskos.R;

public class HowToActivity extends AppCompatActivity implements View.OnClickListener {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_how_to);
        init();
    }

    private void init(){
        TextView tvTitle = findViewById(R.id.tvTitle);
        tvTitle.setText(R.string.more_header);
        tvTitle.setOnClickListener(this);
        findViewById(R.id.img_back).setOnClickListener(this);
        findViewById(R.id.add_client).setOnClickListener(this);
        findViewById(R.id.client_details).setOnClickListener(this);
        findViewById(R.id.job_details).setOnClickListener(this);
        findViewById(R.id.navigation_network).setOnClickListener(this);
        findViewById(R.id.sending_network).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.tvTitle:
                onBackPressed();
                break;
            case R.id.img_back:
                onBackPressed();
                break;
            case R.id.add_client:
                startActivity(new Intent(this,DemoImageActivity.class));
                break;
            case R.id.client_details:
                startActivity(new Intent(this,DemoImageActivity.class));
                break;
            case R.id.add_job:
                startActivity(new Intent(this,DemoImageActivity.class));
                break;
            case R.id.job_details:
                startActivity(new Intent(this,DemoImageActivity.class));
                break;
            case R.id.navigation_network:
                startActivity(new Intent(this,DemoImageActivity.class));
                break;
            case R.id.sending_network:
                startActivity(new Intent(this,DemoImageActivity.class));
                break;

        }
    }
}
