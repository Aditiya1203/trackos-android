package com.taskos.ui.howto;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.taskos.R;

public class DemoImageActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demo_image);
        init();
    }

    private void init(){
        TextView tvTitle = findViewById(R.id.tvTitle);
        tvTitle.setText(R.string.back);
        tvTitle.setOnClickListener(onclick);
        findViewById(R.id.img_back).setOnClickListener(onclick);
    }

    View.OnClickListener onclick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            onBackPressed();
        }
    };
}
