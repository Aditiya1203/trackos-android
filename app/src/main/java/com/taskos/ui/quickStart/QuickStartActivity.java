package com.taskos.ui.quickStart;

import android.content.Intent;
import android.os.Bundle;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.taskos.R;
import com.taskos.TaskOS;
import com.taskos.ui.onboard.SelectRoleActivity;
import com.taskos.ui.quickStart.fragment.Page1;
import com.taskos.ui.quickStart.fragment.Page2;
import com.taskos.ui.quickStart.fragment.Page3;
import com.taskos.ui.quickStart.fragment.Page4;
import com.taskos.ui.quickStart.fragment.PageAccessOnTheWeb;
import com.taskos.ui.quickStart.fragment.PageCollaborateInRealTime;
import com.taskos.ui.quickStart.fragment.PageFollowYourSale;
import com.taskos.ui.quickStart.fragment.PageGetStartedNow;
import com.tbuonomo.viewpagerdotsindicator.SpringDotsIndicator;

import java.util.ArrayList;
import java.util.List;

public class QuickStartActivity extends AppCompatActivity {

    ViewPager viewPager;
    Button next;
    PagerAdapter pagerAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activty_quick_start);
        init();
    }

    private void init(){
        viewPager = findViewById(R.id.viewPager);
        next = findViewById(R.id.next);
        pagerAdapter=new PagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(pagerAdapter);
        pagerAdapter.addFragment(new Page1(),"1");
       // pagerAdapter.addFragment(new Page2(),"2");
        pagerAdapter.addFragment(new PageFollowYourSale(),"3");
        pagerAdapter.addFragment(new Page3(),"4");
        pagerAdapter.addFragment(new PageCollaborateInRealTime(),"5");
        pagerAdapter.addFragment(new PageGetStartedNow(),"6");
        pagerAdapter.addFragment(new Page4(),"7");
        pagerAdapter.addFragment(new PageAccessOnTheWeb(),"8");
        pagerAdapter.notifyDataSetChanged();
        SpringDotsIndicator dotsIndicator = findViewById(R.id.dots_indicator);
        dotsIndicator.setViewPager(viewPager);
        findViewById(R.id.next).setOnClickListener(view -> {
            if (viewPager.getCurrentItem() < 6) {
                viewPager.setCurrentItem(viewPager.getCurrentItem() + 1);
            } else {
                if (TaskOS.getInstance().getDataManager().isRegisterAdded()) {
                    onBackPressed();
                } else {
                    startActivity(new Intent(QuickStartActivity.this, SelectRoleActivity.class));
                    finish();
                }
            }
        });

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if(position<=2){
                    next.setText("Next");
                }else if(position==3){
                    next.setText("Add Contacts");
                }else if(position==4){
                    next.setText("Invite");
                }else if(position==5){
                    next.setText("Next");
                }else{
                    next.setText("Finish Profile");
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    class PagerAdapter extends FragmentPagerAdapter {

        private final List<Fragment> fragmentList = new ArrayList<>();
        private final List<String> fragmentTitleList = new ArrayList<>();
        public PagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }
        public void addFragment(Fragment fragment, String title) {
            fragmentList.add(fragment);
            fragmentTitleList.add(title);
        }
        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            super.destroyItem(container, position, object);
        }
        @Override
        public Fragment getItem(int position) {
            return fragmentList.get(position);
        }
        @Override
        public int getCount() {
            return fragmentList.size();
        }
        @Override
        public long getItemId(int position) {
            return super.getItemId(position);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return fragmentTitleList.get(position);
        }
    }

}
