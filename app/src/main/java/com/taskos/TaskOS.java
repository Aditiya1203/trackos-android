package com.taskos;

import android.app.Application;

import com.google.firebase.iid.FirebaseInstanceId;
import com.taskos.data.AppDataManager;
import com.taskos.data.local.prefs.AppPreferencesHelper;
import com.taskos.util.AppLogger;

import java.util.Objects;


/**
 * Created by Hemant Sharma on 23-02-20.
 */
public class TaskOS extends Application {
    private static TaskOS instance;
    private AppDataManager appInstance;
    private static final String TAG = "TaskOS";

    public static synchronized TaskOS getInstance() {
        if (instance != null) {
            return instance;
        }
        return new TaskOS();
    }

    public AppDataManager getDataManager() {
        return appInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        appInstance = AppDataManager.getInstance(this);

        deviceToken();
    }

    private void deviceToken() {
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(task -> {
                    if (!task.isSuccessful()) {
                        AppLogger.e(TAG, "getInstanceId failed " + task.getException());
                        return;
                    }
                    // Get new Instance ID token
                    appInstance.setDeviceToken(Objects.requireNonNull(task.getResult()).getToken());
                     AppLogger.e("Token", appInstance.getDeviceToken());
                });
    }

}
