package com.taskos.util;

import androidx.annotation.NonNull;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Hemant Sharma on 23-02-20.
 */

public final class CalenderUtils {
    public static final String CLIENT_TIMESTAMP_FORMAT = "EEE MMM, d h:mm a";
    public static final String TIMESTAMP_FORMAT = "dd/MM/yyyy, hh:mm a";
    public static final String TIMESTAMP_FORMAT2 = "dd/MM/yy, hh:mm a";
    public static final String CUSTOM_TIMESTAMP_FORMAT = "EEEE MMM, dd";
    public static final String CUSTOM_TIMESTAMP_FORMAT_AM = "EEE MMM, dd hh:mm a";
    public static final String CUSTOM_TIMESTAMP_FORMAT_SLASH = "dd/MM/yy";
    public static final String CUSTOM_TIMESTAMP_FORMAT_DASH = "dd-MMM-yyyy hh:mm a";
    public static final String DB_TIMESTAMP_FORMAT = "yyyy-MM-dd HH:mm:ss";
    public static final String TIME_FORMAT = "HH:mm:ss";
    public static final String DAY = "EEE";
    public static final String TIME_FORMAT_AM = "hh:mm a";

    private CalenderUtils() {
        // This class is not publicly instantiable
    }

    public static String getTimestamp(String format) {
        return new SimpleDateFormat(format, Locale.US).format(new Date());
    }

    public static String getTimestamp() {
        return String.valueOf(new Date().getTime());
    }

    public static String format12HourTime(String time, @NonNull String pFormat, @NonNull String dFormat) {
        try {
            SimpleDateFormat parseFormat = new SimpleDateFormat(pFormat, Locale.US);
            SimpleDateFormat displayFormat = new SimpleDateFormat(dFormat, Locale.US);
            Date dTime = parseFormat.parse(time);
            assert dTime != null;
            return displayFormat.format(dTime);
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String formatDate(String date, @NonNull String pFormat, @NonNull String dFormat) {
        try {
            SimpleDateFormat parseFormat = new SimpleDateFormat(pFormat, Locale.getDefault());
            SimpleDateFormat displayFormat = new SimpleDateFormat(dFormat, Locale.getDefault());
            Date dTime = parseFormat.parse(date);
            assert dTime != null;
            return displayFormat.format(dTime);
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String formatDate(Date date, @NonNull String dFormat) {
        try {
            SimpleDateFormat displayFormat = new SimpleDateFormat(dFormat, Locale.getDefault());
            return displayFormat.format(date);
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static Date getDateFormat(String date, @NonNull String format) {
        try {
            SimpleDateFormat parseFormat = new SimpleDateFormat(format, Locale.getDefault());

            return parseFormat.parse(date);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @NonNull
    public static String getDateTime(String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.US);
        return sdf.format(new Date());
    }

    public static Long getTimerDifference(String timer, String myTime) {
        SimpleDateFormat formatter = new SimpleDateFormat(CalenderUtils.DB_TIMESTAMP_FORMAT, Locale.getDefault());

        try {
            Date startDate = formatter.parse(timer);

            Date endDate = formatter.parse(myTime);

            assert startDate != null;
            assert endDate != null;
            long diffInMilliSec = endDate.getTime() - startDate.getTime();

            return (diffInMilliSec / (1000 * 60)) % 60;
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Date addHoursToJavaUtilDate(Date date, int hours) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.HOUR_OF_DAY, hours);
        return calendar.getTime();
    }

    @NonNull
    public static String getCurrentDate() {
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH) + 1;
        int day = c.get(Calendar.DAY_OF_MONTH);
        return (day + "/" + month + "/" + year);

    }

    @NonNull
    public static String getCurrentTime() {
        final Calendar c = Calendar.getInstance();
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);
        return (hour + ":" + minute);
    }

    public static boolean isDateBefore(Date myDate) {
        Date curDate = new Date();
        if (myDate.before(curDate)) {
            return true;
        } else if (myDate.equals(curDate)) {
            return false;
        } else return false;
    }
}
