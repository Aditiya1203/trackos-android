

package com.taskos.util;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.provider.Settings;
import android.util.Patterns;

import com.taskos.R;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Hemant Sharma on 23-02-20.
 */

public final class CommonUtils {

    private CommonUtils() {
        // This utility class is not publicly instantiable
    }

    @SuppressLint("all")
    public static String getDeviceId(Context context) {
        return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    public static boolean isEmailValid(String email) {
        return Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public static String loadJSONFromAsset(Context context, String jsonFileName) throws IOException {
        AssetManager manager = context.getAssets();
        InputStream is = manager.open(jsonFileName);

        int size = is.available();
        byte[] buffer = new byte[size];
        is.read(buffer);
        is.close();

        return new String(buffer, StandardCharsets.UTF_8);
    }

    public static ProgressDialog showLoadingDialog(Context context) {
        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.show();
        if (progressDialog.getWindow() != null) {
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        progressDialog.setContentView(R.layout.custom_progress);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        return progressDialog;
    }

    public static Date getCurrentDateTime(){
        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("yyyy.MM.dd G ',' HH:mm");
        String formattedDate = df.format(c);
        Date d = null;
        try {
            d = df.parse(formattedDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return d;
    }

    public static String getCurrentDateTimeCompany(){
        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("MM.dd.yy HH:mm");
        return  df.format(c);
    }


    public static String getCurrentDateTimeEmail(){
        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("dd MMM yy 'at' HH:mm");
        return  df.format(c);
    }


    public static String formatStringAsPhoneNumber(String input) {
        String output;
        switch (input.length()) {
            case 7:
                output = String.format("%s-%s", input.substring(0, 3), input.substring(3, 7));
                break;
            case 10:
                output = String.format("(%s) %s-%s", input.substring(0, 3), input.substring(3, 6), input.substring(6, 10));
                break;
            case 11:
                output = String.format("%s (%s) %s-%s", input.substring(0, 1), input.substring(1, 4), input.substring(4, 7), input.substring(7, 11));
                break;
            case 12:
                output = String.format("+%s (%s) %s-%s", input.substring(0, 2), input.substring(2, 5), input.substring(5, 8), input.substring(8, 12));
                break;
            default:
                return input;
        }
        return output;
    }
}
